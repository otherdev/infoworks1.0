- C:\Tibero6\bin 경로가 없다면 생성한다.
- 환경변수에 TB_HOME (C:\Tibero6)을 추가한다.
- Tibero ODBC Driver(tbodbc_driver_install_6_32.exe)와 libtbcli.dll을
  C:\Tibero6\bin 경로에 복사한 후 다음 명령어를 관리자 권한의 명령 프롬프트에 입력한다.(옵션 –i : install, -r : remove)

tbodbc_driver_installer_6_32.exe –i