﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTI.InfoWorks.Models.DataMng.Dao
{
    class INPDao
    {
        DBManager dbmanager = new DBManager();

        /// <summary>
        /// INP 파일 업로드
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_File(Hashtable conditions)
        {
            dbmanager.QueryForInsert("Insert_INP_File", conditions);
        }

        /// <summary>
        /// INP 파일 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_INP_File(Hashtable conditions)
        {
            dbmanager.QueryForUpdate("Update_INP_File", conditions);
        }

        /// <summary>
        /// INP 파일 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_INP_File(Hashtable conditions)
        {
            dbmanager.QueryForUpdate("Delete_INP_File", conditions);
        }

        /// <summary>
        /// INP 파일 List조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_File_List(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_INP_File_List", conditions);
        }

        /// <summary>
        /// 결과 마스터 insert
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public object Insert_INP_RSLT_MST(Hashtable conditions)
        {
            return dbmanager.QueryForInsert("Insert_INP_RSLT_MST", conditions);
        }

        /// <summary>
        /// 결과 Node insert
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_RSLT_NODE(Hashtable conditions)
        {
            dbmanager.QueryForInsert("Insert_INP_RSLT_NODE", conditions);
        }

        /// <summary>
        /// 결과 Link insert
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_RSLT_LINK(Hashtable conditions)
        {
            dbmanager.QueryForInsert("Insert_INP_RSLT_LINK", conditions);
        }

        /// <summary>
        /// 결과 Node insert
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_RSLT_NODE_for(Hashtable conditions, ArrayList arr)
        {
            dbmanager.QueryForInsertForeach("Insert_INP_RSLT_NODE", conditions, arr);
        }

        /// <summary>
        /// 결과 Link insert
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_RSLT_LINK_for(Hashtable conditions, ArrayList arr)
        {
            dbmanager.QueryForInsertForeach("Insert_INP_RSLT_LINK", conditions, arr);
        }

        /// <summary>
        /// 결과 마스터 select
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_RSLT_MST(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_INP_RSLT_MST", conditions);
        }

        /// <summary>
        /// 결과 Node select
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_RSLT_NODE(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_INP_RSLT_NODE", conditions);
        }

        /// <summary>
        /// 결과 Link select
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_RSLT_LINK(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_INP_RSLT_LINK", conditions);
        }

        /// <summary>
        /// 결과 마스터 CAT에 따라 select
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_RSLT_MST_CAT(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_INP_RSLT_MST_CAT", conditions);
        }
    }
}
