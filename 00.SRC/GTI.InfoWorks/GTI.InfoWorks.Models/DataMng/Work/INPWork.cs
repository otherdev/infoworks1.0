﻿using GTI.InfoWorks.Models.DataMng.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTI.InfoWorks.Models.DataMng.Work
{
    public class INPWork
    {
        INPDao dao = new INPDao();

        /// <summary>
        /// INP 파일 업로드
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_File(Hashtable conditions)
        {
            dao.Insert_INP_File(conditions);
        }

        /// <summary>
        /// INP 파일 업데이트
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_INP_File(Hashtable conditions)
        {
            dao.Update_INP_File(conditions);
        }

        /// <summary>
        /// INP 파일 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_INP_File(Hashtable conditions)
        {
            dao.Delete_INP_File(conditions);
        }

        /// <summary>
        /// INP 파일 List조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_File_List(Hashtable conditions)
        {
            return dao.Select_INP_File_List(conditions);
        }

        /// <summary>
        /// 결과 마스터 insert
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public object Insert_INP_RSLT_MST(Hashtable conditions)
        {
            return dao.Insert_INP_RSLT_MST(conditions);
        }

        /// <summary>
        /// 결과 Node insert
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_RSLT_NODE(Hashtable conditions)
        {
            dao.Insert_INP_RSLT_NODE(conditions);
        }

        /// <summary>
        /// 결과 Link insert
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_RSLT_LINK(Hashtable conditions)
        {
            dao.Insert_INP_RSLT_LINK(conditions);
        }

        /// <summary>
        /// 결과 Node insert
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_RSLT_NODE_for(Hashtable conditions, ArrayList arr)
        {
            dao.Insert_INP_RSLT_NODE_for(conditions, arr);
        }

        /// <summary>
        /// 결과 Link insert
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INP_RSLT_LINK_for(Hashtable conditions, ArrayList arr)
        {
            dao.Insert_INP_RSLT_LINK_for(conditions, arr);
        }

        /// <summary>
        /// 결과 마스터 select
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_RSLT_MST(Hashtable conditions)
        {
            return dao.Select_INP_RSLT_MST(conditions);
        }

        /// <summary>
        /// 결과 Node select
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_RSLT_NODE(Hashtable conditions)
        {
            return dao.Select_INP_RSLT_NODE(conditions);
        }

        /// <summary>
        /// 결과 Link select
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_RSLT_LINK(Hashtable conditions)
        {
            return dao.Select_INP_RSLT_LINK(conditions);
        }

        /// <summary>
        /// 결과 마스터 CAT에 따라 select
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_INP_RSLT_MST_CAT(Hashtable conditions)
        {
            return dao.Select_INP_RSLT_MST_CAT(conditions);
        }
    }
}
