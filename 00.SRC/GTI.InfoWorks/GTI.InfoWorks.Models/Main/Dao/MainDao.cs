﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Collections;

using GTIFramework.Core.Managers;
using GTIFramework.Common.Log;

namespace GTI.InfoWorks.Models.Main.Dao
{
    class MainDao
    {
        DBManager dbmanager = new DBManager();

        #region =======이부분은 infouser 계정으로 처리
        /// <summary>
        /// DataBase 연결 테스트
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_SYSDATE(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_SYSDATE", conditions, Logs.useConfig);
        }

        /// <summary>
        /// 사이트 정보 조회(로그인 화면에서 사용)
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable SelectBaseSiteInfo(Hashtable conditions)
        {
            return dbmanager.QueryForTable("SelectBaseSiteInfo", conditions);
        }

        /// <summary>
        /// 인포매니저 DB의 접속 IP 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable SelectDBInfo(Hashtable conditions)
        {
            return dbmanager.QueryForTable("SelectDBInfo", conditions);
        }

        /// <summary>
        /// 로그인
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable LoginCheck(Hashtable conditions)
        {
            return dbmanager.QueryForTable("LoginCheck", conditions);
        }

        /// <summary>
        /// 사용자 정책카운터 Update
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public void Update_SYS_USER_INFO_Policy(Hashtable conditions)
        {
            dbmanager.QueryForUpdate("Update_SYS_USER_INFO_Policy", conditions);
        }

        /// <summary>
        /// 로그인 사용자 권한확인
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_LoginUser_Permission(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_LoginUser_Permission", conditions);
        }

        /// <summary>
        /// 메뉴 바인딩을 위한 메뉴 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_MNU_LIST(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_MNU_LIST", conditions);
        }

        /// <summary>
        /// 유저 정보 수정
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public void Update_SYS_USER_INFO(Hashtable conditions)
        {
            dbmanager.QueryForUpdate("Update_SYS_USER_INFO", conditions);
        }

        /// <summary>
        /// 부서관리 테이블 조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_SITE_DEPT_INFO(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_SITE_DEPT_INFO", conditions);
        }

        /// <summary>
        /// 사용자정보관리
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_Log_User_Info(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_Log_User_Info", conditions);
        }

        /// <summary>
        /// 즐겨찾기 설정 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_MNU_BKMK_MNG(Hashtable conditions)
        {
            dbmanager.QueryForDelete("Delete_MNU_BKMK_MNG", conditions);
        }

        /// <summary>
        /// 즐겨찾기 설정 입력
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_MNU_BKMK_MNG(Hashtable conditions)
        {
            dbmanager.QueryForInsert("Insert_MNU_BKMK_MNG", conditions);
        }

        /// <summary>
        /// 설정안되어 있는 메뉴
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_MNU_BKMK_MNG_R(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_MNU_BKMK_MNG_R", conditions);
        }

        /// <summary>
        /// 설정되어 있는 메뉴
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_MNU_BKMK_MNG_L(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_MNU_BKMK_MNG_L", conditions);
        }

        /// <summary>
        /// 관심블록 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_INTRST_BLK_MNG(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_INTRST_BLK_MNG", conditions);
        }

        /// <summary>
        /// 관심블록 설정 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_INTRST_BLK_MNG(Hashtable conditions)
        {
            dbmanager.QueryForDelete("Delete_INTRST_BLK_MNG", conditions);
        }

        /// <summary>
        /// 관심블록 설정 입력
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_INTRST_BLK_MNG(Hashtable conditions)
        {
            dbmanager.QueryForInsert("Insert_INTRST_BLK_MNG", conditions);
        }

        /// <summary>
        /// 타일 기본설정조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_BaseTileSetting(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_BaseTileSetting", conditions);
        }

        /// <summary>
        /// 타일 설정조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_TileSetting(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_TileSetting", conditions);
        }

        /// <summary>
        /// 타일 설정 삭제
        /// </summary>
        /// <param name="conditions"></param>
        public void Delete_TileSetting(Hashtable conditions)
        {
            dbmanager.QueryForDelete("Delete_TileSetting", conditions);
        }

        /// <summary>
        /// 타일 설정 입력
        /// </summary>
        /// <param name="conditions"></param>
        public void Insert_TileSetting(Hashtable conditions)
        {
            dbmanager.QueryForInsert("Insert_TileSetting", conditions);
        }

        /// <summary>
        /// 타일 설정 관리 입력
        /// </summary>
        /// <param name="conditions"></param>
        public void Merge_MAIN_TILE_SET_MNG(Hashtable conditions)
        {
            dbmanager.QueryForInsert("Merge_MAIN_TILE_SET_MNG", conditions);
        }

        /// <summary>
        /// 도움말 팝업 데이터
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_MNU_HELP_POP(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_MNU_HELP_POP", conditions);
        }

        /// <summary>
        /// 도움말 알람
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_MNU_HELP_POP_ALM(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_MNU_HELP_POP_ALM", conditions);
        }
        #endregion

        /// <summary>
        /// 로그 기록 insert
        /// </summary>
        /// <param name="conditions"></param>
        public object Insert_SYS_LOG_BY_SEQ(Hashtable conditions)
        {
            return dbmanager.QueryForInsert("Insert_SYS_LOG_BY_SEQ", conditions);
        }

        /// <summary>
        /// 로그 종료 시간 Update
        /// </summary>
        /// <param name="conditions"></param>
        public void Update_SYS_LOG_END_DT(Hashtable conditions)
        {
            dbmanager.QueryForUpdate("Update_SYS_LOG_END_DT", conditions);
        }

        /// <summary>
        /// 도움말 - 설치시스템 목록
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_Installed_System(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_Installed_System", conditions);
        }

        /// <summary>
        /// 경보 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_ALERT_RECORD_MNG_Main(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_ALERT_RECORD_MNG_Main", conditions);
        }

        public void Update_ALERT_RECORD_MNG_Main(Hashtable condition)
        {
            dbmanager.QueryForUpdate("Update_ALERT_RECORD_MNG_Main", condition);
        }

        public DataTable Select_Rockey_Certification(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_Rockey_Certification", conditions);
        }
    }
}
