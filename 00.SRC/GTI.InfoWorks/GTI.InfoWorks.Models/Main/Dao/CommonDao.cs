﻿using GTIFramework.Core.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTI.InfoWorks.Models.Main.Dao
{
    class CommonDao
    {
        DBManager dbmanager = new DBManager();

        /// <summary>
        /// 코드 리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_CD_DTL_INFO_List(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_CD_DTL_INFO_List", conditions);
        }

        /// <summary>
        /// 사업소 코드(최상위 블록) 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_BASE_SITE_INFO(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_BASE_SITE_INFO", conditions);
        }

        /// <summary>
        /// 블록라인 SELECT Tree
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_BASE_LINE_INFO_Tree(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_BASE_LINE_INFO_Tree", conditions);
        }

        /// <summary>
        /// 블록라인 SELECT Tree 사이트 포함
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_BASE_LINE_INFO_Tree_SITE(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_BASE_LINE_INFO_Tree_SITE", conditions);
        }

        /// <summary>
        /// 블록라인 SELECT Tree와 합칠 TAG그룹
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_BASE_TAG_INFO_Tree(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_BASE_TAG_INFO_Tree", conditions);
        }

        public DataTable Select_ALERT_ITEMS(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_ALERT_ITEMS", conditions);
        }

        /// <summary>
        /// 특정 블록 트리형태 구조정렬로 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_BLK_Tree_Hierarchy(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_BLK_Tree_Hierarchy", conditions);
        }

        /// <summary>
        /// 지도 기본 설정 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_GIS_BASE_Setting(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_GIS_BASE_Setting", conditions);
        }

        /// <summary>
        /// 계절 설정 select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_BASE_SEASON_INFO(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_BASE_SEASON_INFO", conditions);
        }

        /// <summary>
        /// 사업소 포함 전체 블록 트리 정렬
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_AllBLK_Tree(Hashtable conditions)
        {
            return dbmanager.QueryForTable("Select_AllBLK_Tree", conditions);
        }


    }
}
