﻿using GTI.InfoWorks.Models.Main.Dao;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTI.InfoWorks.Models.Main.Work
{
    public class CommonWork
    {
        CommonDao dao = new CommonDao();

        /// <summary>
        /// 코드 리스트 조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_CD_DTL_INFO_List(Hashtable conditions)
        {
            return dao.Select_CD_DTL_INFO_List(conditions);
        }

        /// <summary>
        /// 사업소 코드(최상위 블록) 조회
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_BASE_SITE_INFO(Hashtable conditions)
        {
            return dao.Select_BASE_SITE_INFO(conditions);
        }

        /// <summary>
        /// 블록라인 SELECT Tree
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_BASE_LINE_INFO_Tree(Hashtable conditions)
        {
            return dao.Select_BASE_LINE_INFO_Tree(conditions);
        }

        /// <summary>
        /// 블록라인 SELECT Tree 사이트 포함
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_BASE_LINE_INFO_Tree_SITE(Hashtable conditions)
        {
            return dao.Select_BASE_LINE_INFO_Tree_SITE(conditions);
        }

        /// <summary>
        /// 블록라인 SELECT Tree와 합칠 TAG그룹
        /// </summary>
        /// <param name="conditions"></param>
        public DataTable Select_BASE_TAG_INFO_Tree(Hashtable conditions)
        {
            try
            {
                DataTable dtresult = new DataTable();
                DataTable dttemp = new DataTable();

                dtresult = dao.Select_BASE_LINE_INFO_Tree(conditions);
                dttemp = dao.Select_BASE_TAG_INFO_Tree(conditions);

                dtresult.Columns.Add("IN_CAT");
                dtresult.Columns.Add("GRP_NO");
                dtresult.Columns.Add("BLKGRP_NM");

                foreach (DataRow dr in dttemp.Rows)
                {
                    DataRow dradd = dtresult.NewRow();

                    dradd["BLK_CD"] = dr["BLK_CD"].ToString() + "_" + dr["IN_CAT"].ToString() + "_" + dr["GRP_NO"].ToString();
                    dradd["UPPER_BLK"] = dr["BLK_CD"].ToString();
                    dradd["BLK_NM"] = dr["NM"].ToString();
                    dradd["IN_CAT"] = dr["IN_CAT"].ToString();
                    dradd["GRP_NO"] = dr["GRP_NO"].ToString();

                    dradd["BLKGRP_NM"] = dr["BLK_NM"].ToString() + "_" + dr["NM"].ToString();
                    dtresult.Rows.InsertAt(dradd, 0);
                }

                foreach (DataRow dr in dtresult.Rows)
                {
                    DataRow[] drList = dttemp.Select("[BLK_CD] = '" + dr["UPPER_BLK"].ToString() + "' AND [IN_CAT] = '" + dr["IN_CAT"].ToString() + "' AND [GRP_NO] = '" + dr["GRP_NO"].ToString() + "'");
                    if (drList.Length == 1)
                    {
                        if (!drList[0]["ALIAS_NM"].ToString().Equals(""))
                            dr["BLK_NM"] = drList[0]["ALIAS_NM"].ToString();
                    }
                }

                return dtresult;
            }
            catch (Exception ex)
            {
                return null;
                throw ex;
            }
        }

        public DataTable Select_ALERT_ITEMS(Hashtable conditions)
        {
            return dao.Select_ALERT_ITEMS(conditions);
        }

        /// <summary>
        /// 특정 블록 트리형태 구조정렬로 Select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_BLK_Tree_Hierarchy(Hashtable conditions)
        {
            return dao.Select_BLK_Tree_Hierarchy(conditions);
        }

        /// <summary>
        /// 지도 기본 설정 조회
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_GIS_BASE_Setting(Hashtable conditions)
        {
            return dao.Select_GIS_BASE_Setting(conditions);
        }

        /// <summary>
        /// 계절 설정 select
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_BASE_SEASON_INFO(Hashtable conditions)
        {
            return dao.Select_BASE_SEASON_INFO(conditions);
        }

        /// <summary>
        /// 사업소 포함 전체 블록 트리 정렬
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public DataTable Select_AllBLK_Tree(Hashtable conditions)
        {
            return dao.Select_AllBLK_Tree(conditions);
        }

    }
}
