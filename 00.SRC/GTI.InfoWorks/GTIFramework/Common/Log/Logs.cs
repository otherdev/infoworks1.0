﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using log4net;
using log4net.Config;
using GTIFramework.Common.ConfigClass;
using System.Collections;
using System.Threading;
using System.Windows.Controls;
using GTIFramework.Common.MessageBox;
using System.Management;
using GTIFramework.Common.Utils.Converters.TatukGeo.Epanet;
using GTIFramework.Analysis.Epanet;

namespace GTIFramework.Common.Log
{
    public class Logs
    {
        public static Thread WaterRateSavethread;
        public static ProgressBar progressunlimit;

        public static Hashtable htPermission = new Hashtable();
        public static Hashtable htMenuGoParam = new Hashtable();
        public static string strFocusMNU_CD = "";

        public static string strLogin_ID = "";
        public static string strLogin_PW = "";
        public static string strLogin_SITE = "";
        public static string strLogin_IP = "";
        public static string strLogin_NM = "";

        public static ConnectConfig DefaultConfig = new ConnectConfig();
        public static ConnectConfig WNMSConfig = new ConnectConfig();
        public static ConnectConfig ViewConfig = new ConnectConfig();
        public static ConnectConfig WQMSConfig = new ConnectConfig();
        public static ConnectConfig WPMSConfig = new ConnectConfig();
        public static ConnectConfig HMIConfig = new ConnectConfig();

        //실 접속 정보
        public static ConnectConfig useConfig = new ConnectConfig();

        //쓰레드 Watch
        public static Hashtable htthread = new Hashtable();
        
        //쓰레드 MainWatch
        public static Hashtable htMainthread = new Hashtable();

        public static void setLogFile(string strnm)
        {
            log4net.GlobalContext.Properties["LogName"] = strnm;
        }

        public static void ErrLogging(Exception e)
        {
            //XmlConfigurator.Configure(new FileInfo(Properties.Resources.RES_LOG_CONF));

            if (Properties.Resources.RES_LOG_ERR_YN.ToUpper().Equals("N")) return;

            ILog errLog = LogManager.GetLogger("errLogger");

            errLog.Debug("=Start=========================================================================================================");
            errLog.Debug("");
            errLog.Debug(e);
            errLog.Debug("");
            errLog.Debug("=End===========================================================================================================");
        }

        public static void ErrLogging(string strMessage)
        {
            //XmlConfigurator.Configure(new FileInfo(Properties.Resources.RES_LOG_CONF));

            if (Properties.Resources.RES_LOG_ERR_YN.ToUpper().Equals("N")) return;

            ILog errLog = LogManager.GetLogger("errLogger");

            errLog.Debug("=Start=========================================================================================================");
            errLog.Debug("");
            errLog.Debug(strMessage);
            errLog.Debug("");
            errLog.Debug("=End===========================================================================================================");
        }

        //private static ManagementClass cls = new ManagementClass("Win32_OperatingSystem");
        //static int itotalMem = 0; // 총 메모리 KB 단위 
        //static int itotalMemMB = 0; // 총 메모리 MB 단위 
        //static int ifreeMem = 0; // 사용 가능 메모리 KB 단위 
        //static int ifreeMemMB = 0; // 사용 가능 메모리 MB 단위

        public static void ErrAccMemory(string strmessage)
        {
            try
            {
                if (Properties.Resources.RES_LOG_ERR_YN.ToUpper().Equals("N")) return;

                ILog errLog = LogManager.GetLogger("accLogger");

                errLog.Debug("== " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " ========================================== (" + strmessage + ")");

                //ManagementObjectCollection moc = cls.GetInstances();

                //foreach (ManagementObject mo in moc)
                //{
                //    itotalMem = int.Parse(mo["TotalVisibleMemorySize"].ToString());
                //    ifreeMem = int.Parse(mo["FreePhysicalMemory"].ToString());
                //}

                //itotalMemMB = itotalMem / 1024; // 총 메모리 MB 단위 변경
                //ifreeMemMB = ifreeMem / 1024; // 사용 가능 메모리 MB 단위 변경

                //errLog.Debug("itotalMem : " + itotalMem.ToString() + "  itotalMemMB : " + itotalMemMB.ToString());
                //errLog.Debug("ifreeMem : " + ifreeMem.ToString() + "  ifreeMemMB : " + ifreeMemMB.ToString());


                System.Diagnostics.Process[] allPro = System.Diagnostics.Process.GetProcesses();

                foreach (System.Diagnostics.Process pro in allPro)
                {
                    if(pro.ProcessName.Equals("InfoSchedule"))
                    {
                        pro.Refresh();

                        
                        errLog.Debug("NonpagedSystemMemorySize(MB) : " + (pro.NonpagedSystemMemorySize / (1024 * 1024)).ToString());
                        errLog.Debug("PagedMemorySize(MB) : " + (pro.PagedMemorySize / (1024 * 1024)).ToString());
                        errLog.Debug("PagedSystemMemorySize(MB) : " + (pro.PagedSystemMemorySize / (1024 * 1024)).ToString());
                        errLog.Debug("PeakPagedMemorySize(MB) : " + (pro.PeakPagedMemorySize / (1024 * 1024)).ToString());
                        errLog.Debug("PeakVirtualMemorySize(MB) : " + (pro.PeakVirtualMemorySize / (1024 * 1024)).ToString());
                        errLog.Debug("PeakWorkingSet(MB) : " + (pro.PeakWorkingSet / (1024 * 1024)).ToString());
                        errLog.Debug("VirtualMemorySize(MB) : " + (pro.VirtualMemorySize / (1024 * 1024)).ToString());
                        errLog.Debug("WorkingSet(MB) : " + (pro.WorkingSet / (1024 * 1024)).ToString());

                        errLog.Debug("NonpagedSystemMemorySize64(MB) : " + (pro.NonpagedSystemMemorySize64 / (1024 * 1024)).ToString());
                        errLog.Debug("PagedMemorySize64(MB) : " + (pro.PagedMemorySize64 / (1024 * 1024)).ToString());
                        errLog.Debug("PagedSystemMemorySize64(MB) : " + (pro.PagedSystemMemorySize64 / (1024 * 1024)).ToString());
                        errLog.Debug("PeakPagedMemorySize64(MB) : " + (pro.PeakPagedMemorySize64 / (1024 * 1024)).ToString());
                        errLog.Debug("PeakVirtualMemorySize64(MB) : " + (pro.PeakVirtualMemorySize64 / (1024 * 1024)).ToString());
                        errLog.Debug("PeakWorkingSet64(MB) : " + (pro.PeakWorkingSet64 / (1024 * 1024)).ToString());
                        errLog.Debug("VirtualMemorySize64(MB) : " + (pro.VirtualMemorySize64 / (1024 * 1024)).ToString());
                        errLog.Debug("WorkingSet64(MB) : " + (pro.WorkingSet64 / (1024 * 1024)).ToString());

                        foreach (System.Diagnostics.ProcessModule Module in pro.Modules)
                            errLog.Debug(Module.ModuleName  + " Module(MB) : " + (Module.ModuleMemorySize / (1024 * 1024)).ToString());

                        errLog.Debug("==========================================================================================================");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrLogging(ex);
            }
        }

        public static void DBdefault()
        {
            DefaultConfig.strIP = Properties.Settings.Default.strIP;
            DefaultConfig.strPort = Properties.Settings.Default.strPort;
            DefaultConfig.strSID = Properties.Settings.Default.strSID;
            DefaultConfig.strID = Properties.Settings.Default.strID;
            DefaultConfig.strPWD = Properties.Settings.Default.strPWD;

            configChange(DefaultConfig);
        }

        public static void configChange(ConnectConfig tempconfig)
        {
            useConfig = tempconfig;
        }

        public static void setDBConfig(string strDBCAT)
        {
            Properties.Settings.Default.RES_DB_INS_DEFAULT = strDBCAT;
            Properties.Settings.Default.Save();
        }
    }
}
