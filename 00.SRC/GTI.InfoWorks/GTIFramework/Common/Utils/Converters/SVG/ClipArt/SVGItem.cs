﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml;

namespace ClipArtViewer
{
    public class SVGItem
    {
        DrawingGroup m_image = null;

        public SVG m_svg = null;

        public Byte[] bytesvg;

        public string FullPath { get; private set; }
        public string Filename { get; private set; }
        public SVGRender SVGRender { get; private set; }
        public DrawingGroup SVGImage
        {
            get
            {
                EnsureLoaded();
                return m_image;
            }
        }
        public SVGItem(string fullpath)
        {
            FullPath = fullpath;
            Filename = System.IO.Path.GetFileNameWithoutExtension(fullpath);
        }

        //byte[] 추가
        public SVGItem(Byte[] _bytesvg)
        {
            bytesvg = _bytesvg;
        }

        public void Reload()
        {
            //m_image = SVGRender.LoadDrawing(FullPath);
            m_image = SVGRender.LoadDrawing(m_svg);
        }
        void EnsureLoaded()
        {
            if (m_image != null)
                return;
            //Console.WriteLine("{0} - loading {1}", DateTime.Now.ToLongDateString(), Filename);
            SVGRender = new SVGRender();

            if(FullPath!=null)
            {
                m_image = SVGRender.LoadDrawing(FullPath);
            }
            else if(bytesvg.Length != 0)
            {
                m_image = SVGRender.LoadDrawing(bytesvg);
            }
        }

        public void RenderToImage()
        {
            m_image = null;
            SVGRender = new SVGRender();
            m_image = SVGRender.LoadDrawing(m_svg);
        }
    }
}
