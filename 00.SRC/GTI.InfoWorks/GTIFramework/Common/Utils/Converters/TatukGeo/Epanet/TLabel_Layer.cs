﻿using GTIFramework.Analysis.Epanet.Items;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.Linq;
using TatukGIS.NDK;

namespace GTIFramework.Common.Utils.Converters.TatukGeo.Epanet
{
    /// <summary>
    /// Label 레이어 Class
    /// </summary>
    public class TLabel_Layer : TGIS_LayerVector
    {
        public List<Label> labellist;

        public TLabel_Layer()
        {
            this.Name = "labellayer";
            DefaultShapeType = TGIS_ShapeType.Point;
            Extent = TGIS_Utils.GisNoWorld();
            UseRTree = true;

            this.AddField("FTR", TGIS_FieldType.String, 100, 0);
            this.AddField("ID", TGIS_FieldType.String, 100, 0);
            this.AddField("LABEL", TGIS_FieldType.String, 100, 0);

            this.Params.Labels.Color = TGIS_Color.Black;
            this.Params.Labels.Field = "LABEL";
        }
        
        /// <summary>
        /// Link 객체 Import
        /// </summary>
        public void Import(List<Label> implabellist)
        {
            labellist = implabellist;
            if (labellist == null) return;
            if (labellist.Count == 0) return;

            try
            {
                this.CS = this.Viewer.Ref.CS;

                DateTime sdt = DateTime.Now;

                var query = from c in labellist
                            select new TLabel_Shape(c, this) { };
                
                foreach (TLabel_Shape labelshp in query.ToArray())
                    AddShape(labelshp, true);

                Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,, 라벨 add 완료");
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// Link 객체 Add
        /// </summary>
        /// <param name="_label"></param>
        public void Add(Label _label)
        {
            try
            {
                TLabel_Shape labelshp = new TLabel_Shape(_label, this);
                labelshp.Lock(TGIS_Lock.Projection);
                labelshp.AddPart();
                labelshp.AddPoint(new TGIS_Point(_label.X, _label.Y));
                this.AddShape(labelshp, true);
                labelshp.Unlock();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// Link 레이어 Delete
        /// </summary>
        /// <param name="_linkshp"></param>
        public void Delete(TLabel_Shape _labelshp)
        {
            try
            {
                labellist.Remove(_labelshp.label);
                _labelshp.Lock(TGIS_Lock.Projection);
                this.Items.Delete(this.Items.IndexOf(_labelshp));
                _labelshp.Unlock();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
    }

    /// <summary>
    /// Node Shape
    /// </summary>
    public class TLabel_Shape : TGIS_ShapePoint
    {
        public Label label { get; set; }

        public TLabel_Shape(object type, TGIS_LayerVector layer) : base(TGIS_DimensionType.XY)
        {
            try
            {
                if (!(type is Label)) return;
                else label = type as Label;

                this.Params.Marker.Pattern = TGIS_BrushStyle.Clear;
                this.Params.Labels.Position = TGIS_LabelPosition.DownLeft;
                this.Params.Labels.Color = TGIS_Color.Green;
                this.Params.Labels.Value = label.Text.Replace("\"", "");

                this.Params.Labels.SmartSizeAsText = label.Text;

                this.Layer = layer;

                this.Lock(TGIS_Lock.Projection);
                this.AddPart();
                this.SetField("FTR", label.GetType().ToString());
                this.SetField("ID", label.ID);
                this.SetField("LABEL", label.Text);
                this.AddPoint(new TGIS_Point(label.X, label.Y));
                this.Unlock();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        public string GetLabelID()
        {
            if (label == null)
                return null;

            return label.ID;
        }
    }
}
