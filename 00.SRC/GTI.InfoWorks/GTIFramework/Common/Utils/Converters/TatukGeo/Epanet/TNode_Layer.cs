﻿using GTIFramework.Analysis.Epanet.Items;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.Linq;
using TatukGIS.NDK;

namespace GTIFramework.Common.Utils.Converters.TatukGeo.Epanet
{
    /// <summary>
    /// Node 레이어 Class
    /// </summary>
    public class TNode_Layer : TGIS_LayerVector
    {
        public List<Node> nodelist;

        public TNode_Layer()
        {
            this.Name = "nodelayer";
            DefaultShapeType = TGIS_ShapeType.Point;
            Extent = TGIS_Utils.GisNoWorld();
            UseRTree = true;

            this.AddField("FTR", TGIS_FieldType.String, 100, 0);
            this.AddField("ID", TGIS_FieldType.String, 100, 0);
        }

        /// <summary>
        /// Node 객체 Import
        /// </summary>
        public void Import(List<Node> impnodelist)
        {
            nodelist = impnodelist;
            if (nodelist == null) return;
            if (nodelist.Count == 0) return;

            try
            {
                this.CS = this.Viewer.Ref.CS;

                DateTime sdt = DateTime.Now;

                var query = from c in impnodelist
                            select new TNode_Shape(c, this) { };

                foreach (TNode_Shape nodeshp in query.ToArray())
                    this.AddShape(nodeshp, true);

                Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,, 노드 add 완료");
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// Node 객체 Add
        /// </summary>
        /// <param name="_node"></param>
        public void Add(Node _node)
        {
            try
            {
                nodelist.Add(_node);
                TNode_Shape nodeshp = new TNode_Shape(_node, this);
                this.AddShape(nodeshp, true);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// Node 레이어 Delete
        /// </summary>
        /// <param name="_node"></param>
        public void Delete(TNode_Shape _nodeshp)
        {
            try
            {
                nodelist.Remove(_nodeshp.node);
                _nodeshp.Lock(TGIS_Lock.Projection);
                this.Items.Delete(this.Items.IndexOf(_nodeshp));
                _nodeshp.Unlock();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
    }

    /// <summary>
    /// Node Shape
    /// </summary>
    public class TNode_Shape : TGIS_ShapePoint
    {
        public Node node { get; set; }

        public TNode_Shape(object type, TGIS_LayerVector layer) : base(TGIS_DimensionType.XY)
        {
            if (!(type is Node)) return;
            else node = type as Node;

            if (type is Junction)
            {
                this.Params.Marker.Color = TGIS_Color.Black;
                this.Params.Marker.Size = 70;
                this.Params.Marker.OutlineWidth = -1;
                this.Params.Marker.OutlineColor = TGIS_Color.Black;
                this.Params.Marker.Style = TGIS_MarkerStyle.Circle;
            }
            else if (type is Reservoir)
            {
                this.Params.Marker.Color = TGIS_Color.Black;
                this.Params.Marker.Size = 100;
                this.Params.Marker.OutlineWidth = -1;
                this.Params.Marker.OutlineColor = TGIS_Color.Black;
                this.Params.Marker.Style = TGIS_MarkerStyle.Box;
            }
            else if (type is Tank)
            {
                this.Params.Marker.Color = TGIS_Color.Black;
                this.Params.Marker.Size = 100;
                this.Params.Marker.OutlineWidth = -1;
                this.Params.Marker.OutlineColor = TGIS_Color.Black;
                this.Params.Marker.Style = TGIS_MarkerStyle.DiagCross;
            }

            this.Layer = layer;

            this.Lock(TGIS_Lock.None);
            this.AddPart();
            this.SetField("FTR", node.GetType().ToString());
            this.SetField("ID", node.ID);
            this.AddPoint(new TGIS_Point(node.X, node.Y));
            this.Unlock();
        }

        public string GetNodeID()
        {
            if (node == null)
                return null;

            return node.ID;
        }
    }
}
