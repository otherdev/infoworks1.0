﻿using GTIFramework.Analysis.Epanet;
using GTIFramework.Analysis.Epanet.Items;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace GTIFramework.Common.Utils.Converters.TatukGeo.Epanet
{
    /// <summary>
    /// EpanetTatukInfoPopup.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class EpanetTatukInfoPopup : Window
    {
        object selectobj;
        int lperiod;

        public EpanetTatukInfoPopup(object _selectobj, int _lperiod)
        {
            InitializeComponent();
            selectobj = _selectobj;
            lperiod = _lperiod;

            ThemeApply.Themeapply(this);

            btnXSignClose.Click += BtnXSignClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;

            InfoFindBinding();
        }

        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == WindowState.Maximized)
                {
                    Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    WindowState = WindowState.Normal;
                }
                DragMove();
            }
        }

        private void BtnXSignClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void InfoFindBinding()
        {
            try
            {
                DataTable dtitemresult = new DataTable();
                dtitemresult.Columns.Add("KEY");
                dtitemresult.Columns.Add("VAL");
                DataTable dtvalueresult = dtitemresult.Clone();

                if (selectobj is GTIFramework.Analysis.Epanet.Items.Node)
                {
                    GTIFramework.Analysis.Epanet.Items.Node node = selectobj as GTIFramework.Analysis.Epanet.Items.Node;
                    TNodeValue value = GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetNodeValue(lperiod, node.ID);

                    if (value == null)
                        return;

                    if (node is Junction)
                    {
                        Junction item = node as Junction;
                        dtitemresult.Rows.Add("* Junction ID", item.ID);
                        dtitemresult.Rows.Add("X-Coordinate", item.X);
                        dtitemresult.Rows.Add("Y-Coordinate", item.Y);
                        dtitemresult.Rows.Add("Description", item.Description);
                        dtitemresult.Rows.Add("Tag", item.Tag);
                        dtitemresult.Rows.Add("* Elevation", item.Elevation);
                        dtitemresult.Rows.Add("Base Demand", item.BaseDemand);
                        dtitemresult.Rows.Add("Demand Pattern", item.DemandPattern);
                        dtitemresult.Rows.Add("Demand Categories", item.Categories);
                        dtitemresult.Rows.Add("Emitter Coeff.", item.EmitterCoeff);
                        dtitemresult.Rows.Add("Initial Quality", item.InitQuality);
                        dtitemresult.Rows.Add("Source Quality", item.SourceQuality);

                        dtvalueresult.Rows.Add("Actual Demand", value.demand);
                        dtvalueresult.Rows.Add("Total Head", value.head);
                        dtvalueresult.Rows.Add("Pressure", value.pressure);
                        dtvalueresult.Rows.Add("Quality", value.quality);
                    }
                    else if(node is Reservoir)
                    {
                        Reservoir item = node as Reservoir;
                        dtitemresult.Rows.Add("* Reservoir ID", item.ID);
                        dtitemresult.Rows.Add("X-Coordinate", item.X);
                        dtitemresult.Rows.Add("Y-Coordinate", item.Y);
                        dtitemresult.Rows.Add("Description", item.Description);
                        dtitemresult.Rows.Add("Tag", item.Tag);
                        dtitemresult.Rows.Add("* Total Head", item.Head);
                        dtitemresult.Rows.Add("Head Pattern", item.Pattern);
                        dtitemresult.Rows.Add("Initial Quality", item.InitQuality);
                        dtitemresult.Rows.Add("Source Quality", item.SourceQuality);

                        dtvalueresult.Rows.Add("Net Inflow", value.demand);
                        dtvalueresult.Rows.Add("Elevation", value.head);
                        dtvalueresult.Rows.Add("Pressure", value.pressure);
                        dtvalueresult.Rows.Add("Quality", value.quality);
                    }
                    else if(node is Tank)
                    {
                        Tank item = node as Tank;
                        dtitemresult.Rows.Add("* Tank ID", item.ID);
                        dtitemresult.Rows.Add("X-Coordinate", item.X);
                        dtitemresult.Rows.Add("Y-Coordinate", item.Y);
                        dtitemresult.Rows.Add("Description", item.Description);
                        dtitemresult.Rows.Add("Tag", item.Tag);
                        dtitemresult.Rows.Add("* Elevation", item.Elevation);
                        dtitemresult.Rows.Add("* Initial Level", item.InitLevel);
                        dtitemresult.Rows.Add("* Minimum Level", item.MinLevel);
                        dtitemresult.Rows.Add("* Maximum Level", item.MaxLevel);
                        dtitemresult.Rows.Add("* Diameter", item.Diameter);
                        dtitemresult.Rows.Add("Minimum Volume", item.MinVol);
                        dtitemresult.Rows.Add("Volume Curve", item.VolCurve);
                        dtitemresult.Rows.Add("Can Overflow", "?????");                      //값 확인??
                        dtitemresult.Rows.Add("Mixing Model", item.MixingModel);
                        dtitemresult.Rows.Add("Mixing Fraction", item.MixingFraction);
                        dtitemresult.Rows.Add("Reaction Coeff.", item.ReactionCoeff);
                        dtitemresult.Rows.Add("Initial Quality", item.InitQuality);
                        dtitemresult.Rows.Add("Source Quality", item.SourceQuality);


                        dtvalueresult.Rows.Add("Net Inflow", value.demand);
                        dtvalueresult.Rows.Add("Elevation", value.head);
                        dtvalueresult.Rows.Add("Pressure", value.pressure);
                        dtvalueresult.Rows.Add("Quality", value.quality);
                    }
                }
                else if(selectobj is GTIFramework.Analysis.Epanet.Items.Link)
                {
                    GTIFramework.Analysis.Epanet.Items.Link link = selectobj as GTIFramework.Analysis.Epanet.Items.Link;
                    TLinkValue value = GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetLinkValue(lperiod, link.ID);

                    if (value == null)
                        return;

                    if (link is Pipe)
                    {
                        Pipe item = link as Pipe;
                        dtitemresult.Rows.Add("* Pipe ID", item.ID);
                        dtitemresult.Rows.Add("* Start Node", item.StartNode);
                        dtitemresult.Rows.Add("* End Node", item.EndNode);
                        dtitemresult.Rows.Add("Description", item.Description);
                        dtitemresult.Rows.Add("Tag", item.Tag);
                        dtitemresult.Rows.Add("* Length", item.Length);
                        dtitemresult.Rows.Add("* Diameter", item.Diameter);
                        dtitemresult.Rows.Add("* Roughness", item.Roughness);
                        dtitemresult.Rows.Add("Loss Coeff.", item.MinorLoss);
                        dtitemresult.Rows.Add("Initial Status", item.InitialStatus);
                        dtitemresult.Rows.Add("Bulk Coeff.", item.BulkCoeff);
                        dtitemresult.Rows.Add("Wall Coeff.", item.WallCoeff);

                        dtvalueresult.Rows.Add("Flow", value.flow);
                        dtvalueresult.Rows.Add("Velocity", value.velocity);
                        dtvalueresult.Rows.Add("Unit Headloss", value.headloss);
                        dtvalueresult.Rows.Add("Friction Factor", value.frictionFactor);
                        dtvalueresult.Rows.Add("Reaction Rate", value.reactionRate);
                        dtvalueresult.Rows.Add("Quality", value.quality);
                        dtvalueresult.Rows.Add("Status", value.status);  //값 확인 (코드값)??
                    }
                    else if (link is Pump)
                    {
                        Pump item = link as Pump;
                        dtitemresult.Rows.Add("* Pump ID", item.ID);
                        dtitemresult.Rows.Add("* Start Node", item.StartNode);
                        dtitemresult.Rows.Add("* End Node", item.EndNode);
                        dtitemresult.Rows.Add("Description", item.Description);
                        dtitemresult.Rows.Add("Tag", item.Tag);
                        dtitemresult.Rows.Add("Pump Curve", "????");                      //값 확인??
                        dtitemresult.Rows.Add("Power", item.Power);
                        dtitemresult.Rows.Add("Speed", item.Speed);
                        dtitemresult.Rows.Add("Pattern", item.Pattern);
                        dtitemresult.Rows.Add("Initial Status", item.InitialStatus);
                        dtitemresult.Rows.Add("Effic. Curve", item.EfficCurve);
                        dtitemresult.Rows.Add("Energy Price", item.EnergyPrice);
                        dtitemresult.Rows.Add("Price Pattern", item.PricePattern);

                        dtvalueresult.Rows.Add("Flow", value.flow);
                        dtvalueresult.Rows.Add("Headloss", value.headloss);
                        dtvalueresult.Rows.Add("Quality", value.quality);
                        dtvalueresult.Rows.Add("Status", value.status);  //값 확인 (코드값)??
                    }
                    else if (link is Valve)
                    {
                        Valve item = link as Valve;
                        dtitemresult.Rows.Add("* Valve ID", item.ID);
                        dtitemresult.Rows.Add("* Start Node", item.StartNode);
                        dtitemresult.Rows.Add("* End Node", item.EndNode);
                        dtitemresult.Rows.Add("Description", item.Description);
                        dtitemresult.Rows.Add("Tag", item.Tag);
                        dtitemresult.Rows.Add("* Diameter", item.Diameter);
                        dtitemresult.Rows.Add("* Type", item.Type);
                        dtitemresult.Rows.Add("* Setting", item.Setting);
                        dtitemresult.Rows.Add("Loss Coeff.", item.MinorLoss);
                        dtitemresult.Rows.Add("Fixed Status", item.FixedStatus);

                        dtvalueresult.Rows.Add("Flow", value.flow);
                        dtvalueresult.Rows.Add("Velocity", value.velocity);
                        dtvalueresult.Rows.Add("Headloss", value.headloss);
                        dtvalueresult.Rows.Add("Quality", value.quality);
                        dtvalueresult.Rows.Add("Status", value.status);  //값 확인 (코드값)??
                    }
                }

                gcItemData.ItemsSource = dtitemresult;
                gcvalueData.ItemsSource = dtvalueresult;
            }
            catch (Exception ex) { Messages.ShowErrMsgBoxLog(ex); }
        }
    }
}
