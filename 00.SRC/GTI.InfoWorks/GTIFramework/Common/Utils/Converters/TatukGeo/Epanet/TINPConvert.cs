﻿using GTIFramework.Analysis.Epanet;
using GTIFramework.Analysis.Epanet.Items;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using TatukGIS.NDK;
using TatukGIS.NDK.WPF;
using GTIFramework.Common.Log;
using System.Data;
using GTIFramework.Common.Utils.Handle;
using DevExpress.Xpf.Core;

namespace GTIFramework.Common.Utils.Converters.TatukGeo.Epanet
{
    public class TINPConvert
    {
        TGIS_ViewerWnd mapcontrol;
        string strPathINP;
        string strNameINP;
        public ModelNetwork network;

        TNode_Layer nodeLayer;
        TLink_Layer linkLayer;
        TLabel_Layer labelLayer;

        #region 맵컨트롤 관련 리스트 정리
        public DataTable dtDataComboList;
        public DataTable dtNodeLegendList;
        public DataTable dtLinkLegendList;
        public DataTable dtTimeList;

        iniHandle ini;
        #endregion

        /// <summary>
        /// ModelNetwork를 만들어 내는것 까지
        /// </summary>
        /// <param name="_strPathINP"></param>
        public TINPConvert(string _strPathINP, string _strNameINP)
        {
            try
            {
                strPathINP = _strPathINP;
                strNameINP = _strNameINP;

                INPRead();

                dtDataComboList = null;
                dtNodeLegendList = null;
                dtLinkLegendList = null;
                dtTimeList = null;

                #region 데이터, 노드, 링크 항목
                dtDataComboList = new DataTable();
                dtDataComboList.Columns.Add("CD");
                dtDataComboList.Columns.Add("NM");

                dtNodeLegendList = new DataTable();
                dtNodeLegendList = dtDataComboList.Clone();
                dtNodeLegendList.Columns.Add("Legend", typeof(string[]));
                dtNodeLegendList.Columns.Add("Unit", typeof(string));

                dtLinkLegendList = new DataTable();
                dtLinkLegendList = dtDataComboList.Clone();
                dtLinkLegendList.Columns.Add("Legend", typeof(string[]));
                dtLinkLegendList.Columns.Add("Unit", typeof(string));


                List<string> strdatalist = new List<string> { "Junctions", "Tanks", "Reservoirs", "Pipes", "Pumps", "Valves", "Labels", "Patterns", "Options" };

                ini = null;
                ini = new iniHandle(Environment.CurrentDirectory + "\\GTIConfig.ini");

                dtNodeLegendList.Rows.Add("NodeDemand", "Demand", (ini.GetIniValue("INP", "NodeDemand").ToString().Split(',') as string[]), "GPM");
                dtNodeLegendList.Rows.Add("NodeHead", "Head", (ini.GetIniValue("INP", "NodeHead").ToString().Split(',') as string[]), "ft");
                dtNodeLegendList.Rows.Add("NodePressure", "Pressure", (ini.GetIniValue("INP", "NodePressure").ToString().Split(',') as string[]), "psi");
                dtNodeLegendList.Rows.Add("NodeChlorine", "Chlorine", (ini.GetIniValue("INP", "NodeChlorine").ToString().Split(',') as string[]), "");

                //dtLinkLegendList.Rows.Add("LinkBulkCoeff", "Bulk Coeff.", (ini.GetIniValue("INP", "LinkBulkCoeff").ToString().Split(',') as string[]), "");
                //dtLinkLegendList.Rows.Add("LinkWallCoeff", "Wall Coeff.", (ini.GetIniValue("INP", "LinkWallCoeff").ToString().Split(',') as string[]), "");
                dtLinkLegendList.Rows.Add("LinkfrictionFactor", "Friction Factor", (ini.GetIniValue("INP", "LinkfrictionFactor").ToString().Split(',') as string[]), "");
                dtLinkLegendList.Rows.Add("LinkreactionRate", "Reaction Rate", (ini.GetIniValue("INP", "LinkreactionRate").ToString().Split(',') as string[]), "mg/L/d");
                dtLinkLegendList.Rows.Add("LinkFlow", "Flow", (ini.GetIniValue("INP", "LinkFlow").ToString().Split(',') as string[]), "GPM");
                dtLinkLegendList.Rows.Add("LinkVelocity", "Velocity", (ini.GetIniValue("INP", "LinkVelocity").ToString().Split(',') as string[]), "fps");
                dtLinkLegendList.Rows.Add("LinkUnitHeadloss", "Unit Headloss", (ini.GetIniValue("INP", "LinkUnitHeadloss").ToString().Split(',') as string[]), "ft/kft");
                dtLinkLegendList.Rows.Add("LinkChlorine", "Chlorine", (ini.GetIniValue("INP", "LinkChlorine").ToString().Split(',') as string[]), "");
                #endregion

                foreach (string strdata in strdatalist)
                    dtDataComboList.Rows.Add(strdata, strdata);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        public void TSEPointConvert()
        {
            var pipes = (from c in linkLayer.Items
                         join d in network.Links.OfType<GTIFramework.Analysis.Epanet.Items.Pipe>().Select(x=>x)
                         on c.GetField("ID").ToString() equals d.ID
                         select new pipesvalue { shparc = c as TGIS_ShapeArc, link = d }).ToArray();

            foreach (pipesvalue pipe in pipes)
            {
                string strtemp = pipe.shparc.Params.Line.Symbol.Name.Replace("&", "");
                pipe.shparc.Params.Line.Symbol = TGIS_Utils.SymbolList.Prepare("&L(100%)");
                pipe.shparc.Params.Line.Color = TGIS_Color.Blue;
            }

            mapcontrol.InvalidateWholeMap();
        }

        /// <summary>
        /// INP읽어 Tatuk 표출
        /// </summary>
        public void INPRead()
        {
            try
            {
                network = new ModelNetwork();
                network.ReadFile(strPathINP);
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// ModelNetwork을 Tatuk Utill을 이용하여 INP생성
        /// </summary>
        public void INPWrite(string strpath)
        {
            try
            {
                ModelNetwork modinetwork = new ModelNetwork();
                modinetwork = network;

                foreach (Node node in modinetwork.Nodes)
                {
                    TGIS_Point point = mapcontrol.CS.FromCS((mapcontrol.Items[1] as TGIS_LayerVector).CS, new TGIS_Point(node.X, node.Y));
                    node.X = point.X;
                    node.Y = point.Y;
                }

                foreach (Link link in modinetwork.Links)
                {
                    foreach (Vertex ver in link.Vertices)
                    {
                        TGIS_Point point = mapcontrol.CS.FromCS((mapcontrol.Items[1] as TGIS_LayerVector).CS, new TGIS_Point(ver.X, ver.Y));
                        ver.X = point.X;
                        ver.Y = point.Y;
                    }
                }

                modinetwork.WriteFIle(strpath);
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// MapControl이 선언 되어 있을경우
        /// </summary>
        /// <param name="_mapcontrol"></param>
        public void INPtoGIS(TGIS_ViewerWnd _mapcontrol)
        {
            try
            {
                mapcontrol = _mapcontrol;
                INPLayerCreate();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 레이어정리 및 레이어 추가
        /// </summary>
        public void INPLayerCreate()
        {
            try
            {
                #region //기존에 바인딩 되어 있는 노드, 링크레이어 정리
                for (int i = mapcontrol.Items.Count - 1; i >= 0; i--)
                {
                    if (mapcontrol.Items[i] is TGIS_Layer)
                    {
                        if ((mapcontrol.Items[i] is TNode_Layer) || (mapcontrol.Items[i] is TLink_Layer) || (mapcontrol.Items[i] is TLabel_Layer))
                            mapcontrol.Items.Remove((mapcontrol.Items[i] as TGIS_Layer));
                    }
                }
                #endregion

                DateTime sdt = DateTime.Now;
                #region //링크 레이어 생성 및 객체 생성2
                linkLayer = null;
                linkLayer = new TLink_Layer();
                mapcontrol.Add(linkLayer);
                linkLayer.Import(network.Links);
                #endregion
                Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,, 링크 완료");

                sdt = DateTime.Now;
                #region //노드 레이어 생성 및 객체 생성2
                nodeLayer = null;
                nodeLayer = new TNode_Layer();
                mapcontrol.Add(nodeLayer);
                nodeLayer.Import(network.Nodes);
                #endregion
                Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,, 노드 완료");

                sdt = DateTime.Now;
                #region //라벨 레이어 생성 및 객체 생성2
                labelLayer = null;
                labelLayer = new TLabel_Layer();
                mapcontrol.Add(labelLayer);
                labelLayer.Import(network.Labels);
                #endregion
                Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,, 라벨 완료");

                mapcontrol.FullExtent();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }

    public class pipesvalue
    {
        public TGIS_ShapeArc shparc { get; set; }
        public Link link { get; set; }
    }
}
