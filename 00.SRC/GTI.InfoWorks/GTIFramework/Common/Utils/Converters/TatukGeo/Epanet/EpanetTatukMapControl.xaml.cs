﻿using DevExpress.Xpf.Editors;
using GTIFramework.Analysis.Epanet;
using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Handle;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TatukGIS.NDK;
using TatukGIS.NDK.WPF;

namespace GTIFramework.Common.Utils.Converters.TatukGeo.Epanet
{
    /// <summary>
    /// EpanetTatukMapControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class EpanetTatukMapControl : UserControl
    {
        public TGIS_Shape selectshp;
        EpanetTatukInfoPopup Infopopup;
        iniHandle ini;
        double dzoom;

        #region 방향표시 일반 Link
        //방향 없음
        string strLine = "&L(100%)";
        //정상 방향
        string strForwardLine = "&L(100%)G(50%)M(40S 0)D(-80S 40S)M(80S -40S)D(-80S -40S)G(100%)M(100% 0)";
        //반대 방향
        string strOppositeLine = "&L(100%)G(50%)M(-40S 0)D(80S 40S)M(-80S -40S)D(80S -40S)G(100%)M(100% 0)";
        #endregion

        public TGIS_ViewerWnd mapcontrol { get; set; }

        #region 레전드, 맵pop Show 여부
        public Visibility MapData { get; set; }

        public Visibility Legend { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            mapdatapop.Visibility = MapData;
            linklgd.Visibility = Legend;
            nodelgd.Visibility = Legend;

            if (EpanetVar.tINPConvert == null)
                return;
            if (EpanetVar.tINPConvert.network.Analysis.IsSimulation == false)
                return;

            if (Legend == Visibility.Visible)
            {
                //tINPConvert가 있을경우
                if (EpanetVar.tINPConvert != null)
                    cbDataList.ItemsSource = EpanetVar.tINPConvert.dtDataComboList;

                //modelAnalysis 있을경우
                if (EpanetVar.tINPConvert.network.Analysis.IsSimulation)
                {
                    
                }

                ini = null;
                ini = new iniHandle(Environment.CurrentDirectory + "\\GTIConfig.ini");

                cbDataList.SelectedIndexChanged += CbDataList_SelectedIndexChanged;

                btnNodeSave.Click += BtnNodeSave_Click;
                btnLinkSave.Click += BtnLinkSave_Click;
            }

            if (MapData == Visibility.Visible)
            {
                //tINPConvert가 있을경우
                if (EpanetVar.tINPConvert != null)
                {
                    cbNodeLegendList.ItemsSource = EpanetVar.tINPConvert.dtNodeLegendList;
                    cbLinkLegendList.ItemsSource = EpanetVar.tINPConvert.dtLinkLegendList;
                }

                //modelAnalysis 있을경우
                if (EpanetVar.tINPConvert.network.Analysis.IsSimulation)
                {
                    cbTimeList.ItemsSource = EpanetVar.tINPConvert.network.Analysis.GetTimeList();
                    cbTimeList.SelectedIndexChanged += CbTimeList_SelectedIndexChanged;
                }
                
                cbNodeLegendList.SelectedIndexChanged += CbNodeLegendList_SelectedIndexChanged;
                cbLinkLegendList.SelectedIndexChanged += CbLinkLegendList_SelectedIndexChanged;

                btngostart.Click += Btngostart_Click;
                btngopre.Click += Btngopre_Click;
                btnstop.Click += Btnstop_Click;
                btngonext.Click += Btngonext_Click;

                chklabelNode.Checked += Chklabel_Checked;
                chklabelLink.Checked += Chklabel_Checked;
                chklabelNode.Unchecked += Chklabel_Checked;
                chklabelLink.Unchecked += Chklabel_Checked;
            }
        }

        bool bnodelbvisible { get; set; } = false;
        bool blinklbvisible { get; set; } = false;

        private void Chklabel_Checked(object sender, RoutedEventArgs e)
        {
            if (!(sender is CheckEdit)) return;

            CheckEdit chk = sender as CheckEdit;

            switch (chk.Name)
            {
                case "chklabelNode":
                    if (chklabelNode.IsChecked == true)
                        bnodelbvisible = true;
                    else
                        bnodelbvisible = false;

                    foreach (TGIS_ShapePoint item in (mapcontrol.Items[2] as TNode_Layer).Items)
                        item.Params.Labels.Visible = bnodelbvisible;
                    break;

                case "chklabelLink":
                    if (chklabelLink.IsChecked == true)
                        blinklbvisible = true;
                    else
                        blinklbvisible = false;

                    foreach (TGIS_ShapeArc item in (mapcontrol.Items[1] as TLink_Layer).Items)
                        item.Params.Labels.Visible = blinklbvisible;
                    break;
            }

            mapcontrol.InvalidateWholeMap();
        }

        //맵 - 다음으로
        private void Btngonext_Click(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                new Action(delegate ()
                {
                    if (cbTimeList.SelectedIndex == (cbTimeList.ItemsSource as DataTable).Rows.Count - 1)
                        cbTimeList.SelectedIndex = 0;
                    else
                        cbTimeList.SelectedIndex = cbTimeList.SelectedIndex + 1;
                }));
        }

        //맵 - 시작 정지
        //Thread threadpaly;
        System.Threading.Timer timerpaly;
        bool bstat = false;
        private void Btnstop_Click(object sender, RoutedEventArgs e)
        {
            if(bstat)
            {
                timerpaly.Dispose();
                bstat = false;
            }
            else
            {
                timerpaly = new Timer(Timerpaly_fx, null, 0, 500);
                bstat = true;
            }
        }

        private void Timerpaly_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                Btngonext_Click(null, null);
            }
            catch (ThreadAbortException)
            {
                Thread.ResetAbort();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void Timerpaly_fx(object o)
        {
            try
            {
                Btngonext_Click(null, null);
            }
            catch (ThreadAbortException)
            {
                Thread.ResetAbort();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        //맵 - 이전으로
        private void Btngopre_Click(object sender, RoutedEventArgs e)
        {
            if (cbTimeList.SelectedIndex == 0)
                return;

            cbTimeList.SelectedIndex = cbTimeList.SelectedIndex - 1;
        }

        //맵 - 처음으로
        private void Btngostart_Click(object sender, RoutedEventArgs e)
        {
            cbTimeList.SelectedIndex = 0;
        }

        private void CbTimeList_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cbNodeLegendList.SelectedItem != null)
                    NodeResultBinding(cbNodeLegendList.SelectedItem as DataRowView);

                if (cbLinkLegendList.SelectedItem != null)
                    LinkResultBinding(cbLinkLegendList.SelectedItem as DataRowView);

                mapcontrol.InvalidateWholeMap();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void BtnLinkSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strval = string.Empty;
                double dval1;
                double dval2;

                SpinEdit se;
                for (int i = 1; i < 5; i++)
                {
                    se = null;
                    se = (this.FindName("Legend2L" + i.ToString()) as SpinEdit);

                    if (se.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("범위를 모두 입력해야 합니다.");
                        return;
                    }

                    if (i + 1 < 4)
                    {
                        if (double.TryParse(se.EditValue.ToString(), out dval1) & double.TryParse((this.FindName("Legend2L" + (i + 1).ToString()) as SpinEdit).EditValue.ToString(), out dval2))
                        {
                            if (dval1 < dval2)
                                strval = strval + dval1.ToString() + ",";
                            else
                            {
                                Messages.ShowInfoMsgBox("범위 값을 확인하세요.");
                                return;
                            }
                        }
                        else
                        {
                            Messages.ShowInfoMsgBox("범위 값을 확인하세요.");
                            return;
                        }
                    }
                    else
                    {
                        if (double.TryParse(se.EditValue.ToString(), out dval1))
                            strval = strval + dval1.ToString() + ",";
                    }
                }


                ini.SetIniValue("INP", (cbLinkLegendList.SelectedItem as DataRowView).Row["CD"].ToString(), strval.TrimEnd(','));

                (cbLinkLegendList.SelectedItem as DataRowView).Row["Legend"] = (ini.GetIniValue("INP", (cbLinkLegendList.SelectedItem as DataRowView).Row["CD"].ToString()).ToString().Split(',') as string[]);

                //linklegendSelectedIndexChangedAction();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void BtnNodeSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strval = string.Empty;
                double dval1;
                double dval2;

                SpinEdit se;
                for (int i = 1; i < 5; i++)
                {
                    se = null;
                    se = (this.FindName("Legend1L" + i.ToString()) as SpinEdit);

                    if (se.Text.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("범위를 모두 입력해야 합니다.");
                        return;
                    }

                    if (i + 1 < 4)
                    {
                        if (double.TryParse(se.EditValue.ToString(), out dval1) & double.TryParse((this.FindName("Legend1L" + (i + 1).ToString()) as SpinEdit).EditValue.ToString(), out dval2))
                        {
                            if (dval1 < dval2)
                                strval = strval + dval1.ToString() + ",";
                            else
                            {
                                Messages.ShowInfoMsgBox("범위 값을 확인하세요.");
                                return;
                            }
                        }
                        else
                        {
                            Messages.ShowInfoMsgBox("범위 값을 확인하세요.");
                            return;
                        }
                    }
                    else
                    {
                        if (double.TryParse(se.EditValue.ToString(), out dval1))
                            strval = strval + dval1.ToString() + ",";
                    }
                }

                ini.SetIniValue("INP", (cbNodeLegendList.SelectedItem as DataRowView).Row["CD"].ToString(), strval.TrimEnd(','));

                (cbNodeLegendList.SelectedItem as DataRowView).Row["Legend"] = (ini.GetIniValue("INP", (cbNodeLegendList.SelectedItem as DataRowView).Row["CD"].ToString()).ToString().Split(',') as string[]);

                //nodelegendSelectedIndexChangedAction();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// Data 콤보박스 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbDataList_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            try
            {

                switch ((cbDataList.SelectedItem as DataRowView)["CD"].ToString())
                {
                    case "Junctions":
                        gcDataList.ItemsSource = (EpanetVar.tINPConvert.network.Nodes as List<GTIFramework.Analysis.Epanet.Items.Node>)
                            .OfType<GTIFramework.Analysis.Epanet.Items.Junction>().ToList();
                        gcDataList.Columns[0].FieldName = "ID";
                        break;
                    case "Tanks":
                        gcDataList.ItemsSource = (EpanetVar.tINPConvert.network.Nodes as List<GTIFramework.Analysis.Epanet.Items.Node>)
                            .OfType<GTIFramework.Analysis.Epanet.Items.Tank>().ToList();
                        gcDataList.Columns[0].FieldName = "ID";
                        break;
                    case "Reservoirs":
                        gcDataList.ItemsSource = (EpanetVar.tINPConvert.network.Nodes as List<GTIFramework.Analysis.Epanet.Items.Node>)
                            .OfType<GTIFramework.Analysis.Epanet.Items.Reservoir>().ToList();
                        gcDataList.Columns[0].FieldName = "ID";
                        break;
                    case "Pipes":
                        gcDataList.ItemsSource = (EpanetVar.tINPConvert.network.Links as List<GTIFramework.Analysis.Epanet.Items.Link>)
                            .OfType<GTIFramework.Analysis.Epanet.Items.Pipe>().ToList();
                        gcDataList.Columns[0].FieldName = "ID";
                        break;
                    case "Pumps":
                        gcDataList.ItemsSource = (EpanetVar.tINPConvert.network.Links as List<GTIFramework.Analysis.Epanet.Items.Link>)
                            .OfType<GTIFramework.Analysis.Epanet.Items.Pump>().ToList();
                        gcDataList.Columns[0].FieldName = "ID";
                        break;
                    case "Valves":
                        gcDataList.ItemsSource = (EpanetVar.tINPConvert.network.Links as List<GTIFramework.Analysis.Epanet.Items.Link>)
                            .OfType<GTIFramework.Analysis.Epanet.Items.Valve>().ToList();
                        gcDataList.Columns[0].FieldName = "ID";
                        break;
                    case "Labels":
                        gcDataList.ItemsSource = (EpanetVar.tINPConvert.network.Labels as List<GTIFramework.Analysis.Epanet.Items.Label>)
                            .OfType<GTIFramework.Analysis.Epanet.Items.Label>().ToList();
                        gcDataList.Columns[0].FieldName = "Text";
                        break;
                    case "Patterns":
                        gcDataList.ItemsSource = null;
                        break;
                    case "Options":
                        gcDataList.ItemsSource = null;
                        break;
                }

                gcDataList.MouseDown += GcDataList_MouseDown;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 더블클릭시 Info창 표시
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GcDataList_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //왼쪽 버튼 버블클릭
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (e.ClickCount == 2)
                {
                    if (gcDataList.SelectedItem == null)
                        return;

                    int lperiod;
                    if (!int.TryParse(cbTimeList.EditValue.ToString(), out lperiod)) return;

                    if(gcDataList.SelectedItem is GTIFramework.Analysis.Epanet.Items.Node 
                        || gcDataList.SelectedItem is GTIFramework.Analysis.Epanet.Items.Link)
                    {
                        Infopopup = null;
                        Infopopup = new EpanetTatukInfoPopup(gcDataList.SelectedItem, lperiod);
                        Infopopup.ShowDialog();
                    }
                }
            }
        }

        /// <summary>
        /// 맵 link 콤보박스 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbLinkLegendList_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            if (cbLinkLegendList.SelectedItem == null)
                return;

            try
            {
                lbLinkTitle.Content = (cbLinkLegendList.SelectedItem as DataRowView).Row["NM"].ToString();
                lbLinkUnit.Content = (cbLinkLegendList.SelectedItem as DataRowView).Row["Unit"].ToString();

                if ((cbLinkLegendList.SelectedItem as DataRowView).Row["Legend"] is string[])
                {
                    for (int i = 1; i < 5; i++)
                        (this.FindName("Legend2L" + i.ToString()) as SpinEdit).EditValue = ((cbLinkLegendList.SelectedItem as DataRowView).Row["Legend"] as string[])[i - 1];
                }

                LinkResultBinding(cbLinkLegendList.SelectedItem as DataRowView);

                mapcontrol.InvalidateWholeMap();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 맵 node 콤보박스 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbNodeLegendList_SelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            if (cbNodeLegendList.SelectedItem == null)
                return;

            try
            {
                lbNodeTitle.Content = (cbNodeLegendList.SelectedItem as DataRowView).Row["NM"].ToString();
                lbNodeUnit.Content = (cbNodeLegendList.SelectedItem as DataRowView).Row["Unit"].ToString();

                if ((cbNodeLegendList.SelectedItem as DataRowView).Row["Legend"] is string[])
                {
                    for (int i = 1; i < 5; i++)
                        (this.FindName("Legend1L" + i.ToString()) as SpinEdit).EditValue = ((cbNodeLegendList.SelectedItem as DataRowView).Row["Legend"] as string[])[i - 1];
                }

                NodeResultBinding(cbNodeLegendList.SelectedItem as DataRowView);

                mapcontrol.InvalidateWholeMap();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 선택시 해당 shp select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GcDataList_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            DeSelectShapeAll();

            if (gcDataList.SelectedItem == null) return;

            TGIS_LayerVector selectLayer = null;
            string strtype = string.Empty;
            var item = gcDataList.SelectedItem;

            try
            {
                switch (cbDataList.EditValue)
                {
                    case "Junctions":
                        selectLayer = mapcontrol.Items[2] as TGIS_LayerVector;
                        strtype = "GTIFramework.Analysis.Epanet.Items.Junction";
                        break;

                    case "Tanks":
                        selectLayer = mapcontrol.Items[2] as TGIS_LayerVector;
                        strtype = "GTIFramework.Analysis.Epanet.Items.Tank";
                        break;

                    case "Reservoirs":
                        selectLayer = mapcontrol.Items[2] as TGIS_LayerVector;
                        strtype = "GTIFramework.Analysis.Epanet.Items.Reservoir";
                        break;

                    case "Pipes":
                        selectLayer = mapcontrol.Items[1] as TGIS_LayerVector;
                        strtype = "GTIFramework.Analysis.Epanet.Items.Pipe";
                        break;

                    case "Pumps":
                        selectLayer = mapcontrol.Items[1] as TGIS_LayerVector;
                        strtype = "GTIFramework.Analysis.Epanet.Items.Pump";
                        break;

                    case "Valves":
                        selectLayer = mapcontrol.Items[1] as TGIS_LayerVector;
                        strtype = "GTIFramework.Analysis.Epanet.Items.Valve";
                        break;

                    case "Labels":
                        //selectLayer = mapcontrol.Items[3] as TGIS_LayerVector;
                        //strtype = "GTIFramework.Analysis.Epanet.Items.Label";
                        return;

                    case "Patterns":

                        return;

                    case "Options":

                        return;
                }

                if (selectLayer == null) return;

                TGIS_DataTable dtdbf1 = new TGIS_DataTable() { ShowInternalFields = true };
                dtdbf1.Open(selectLayer, selectLayer.Extent);

                string Query = $"FTR = '{item.GetType().ToString()}' AND ID = '{(item as SectionItemBase).ID}'";
                TGIS_Shape selectshp = selectLayer.FindFirst(TGIS_Utils.GisWholeWorld(), Query);

                selectshp.IsSelected = true;
                selectshp.Invalidate();

            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion

        #region 맵에 Node, Link선택에 따라 결과 재바인딩
        private void NodeResultBinding(DataRowView ComboValue)
        {
            try
            {
                int lperiod;
                if (!int.TryParse(cbTimeList.EditValue.ToString(), out lperiod)) return;

                TNode_Layer nodelayper = mapcontrol.Items[2] as TNode_Layer;

                List<double> dlist = new List<double>();
                foreach (string val in ComboValue["Legend"] as string[])
                {
                    if (double.TryParse(val, out double dtemp))
                        dlist.Add(dtemp);
                }

                IEnumerable<selectvalue> selectvalues = null;

                switch (ComboValue["CD"])
                {
                    case "NodeDemand":
                        var queryDemand = (from c in nodelayper.Items
                                           join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetNodeValues(lperiod)
                                           on c.GetField("ID").ToString() equals d.ID
                                           select new selectvalue { shppoint = c as TGIS_ShapePoint, dvalue = d.demand } ).ToArray();

                        selectvalues = queryDemand.ToArray();
                        break;
                    case "NodeHead":
                        var queryHead = (from c in nodelayper.Items
                                           join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetNodeValues(lperiod)
                                           on c.GetField("ID").ToString() equals d.ID
                                           select new selectvalue { shppoint = c as TGIS_ShapePoint, dvalue = d.head }).ToArray();
                        selectvalues = queryHead.ToArray();
                        break;
                    case "NodePressure":
                        var queryPressure = (from c in nodelayper.Items
                                         join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetNodeValues(lperiod)
                                         on c.GetField("ID").ToString() equals d.ID
                                         select new selectvalue { shppoint = c as TGIS_ShapePoint, dvalue = d.pressure }).ToArray();
                        selectvalues = queryPressure.ToArray();
                        break;
                    case "NodeChlorine":
                        var queryChlorine = (from c in nodelayper.Items
                                             join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetNodeValues(lperiod)
                                             on c.GetField("ID").ToString() equals d.ID
                                             select new selectvalue { shppoint = c as TGIS_ShapePoint, dvalue = d.quality }).ToArray();
                        selectvalues = queryChlorine.ToArray();
                        break;
                }

                foreach (var item in selectvalues)
                {
                    NodeLabel(item.shppoint, item.dvalue);

                    if (double.TryParse(item.dvalue.ToString(), out double dtemp))
                        item.shppoint.Params.Marker.Color = GISColor(ColorBinding(dtemp, dlist));
                    else
                        item.shppoint.Params.Marker.Color = GISColor((Color)ColorConverter.ConvertFromString("#FF000000"));
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void NodeLabel(TGIS_ShapePoint item, double? dvalue)
        {
            item.Params.Labels.Value = string.Format("{0:0.0#}", dvalue);
            item.Params.Labels.Visible = bnodelbvisible;
            item.Params.Labels.Position = TGIS_LabelPosition.DownCenter;
            item.Params.Labels.Color = TGIS_Color.LightGray;
        }

        private void LinkResultBinding(DataRowView ComboValue)
        {
            try
            {
                int lperiod;
                if (!int.TryParse(cbTimeList.EditValue.ToString(), out lperiod)) return;

                TLink_Layer linklayper = mapcontrol.Items[1] as TLink_Layer;

                List<double> dlist = new List<double>();
                foreach (string val in ComboValue["Legend"] as string[])
                {
                    if (double.TryParse(val, out double dtemp))
                        dlist.Add(dtemp);
                }

                IEnumerable<selectvalue> selectvalues = null;

                switch (ComboValue["CD"])
                {
                    case "LinkfrictionFactor":
                        var queryFactor = (from c in linklayper.Items
                                           join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetLinkValues(lperiod)
                                           on c.GetField("ID").ToString() equals d.ID
                                           select new selectvalue { shparc = c as TGIS_ShapeArc, dvalue = d.frictionFactor, dflow = d.flow }).ToArray();
                        selectvalues = queryFactor.ToArray();
                        break;
                    case "LinkreactionRate":
                        var queryRate = (from c in linklayper.Items
                                         join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetLinkValues(lperiod)
                                         on c.GetField("ID").ToString() equals d.ID
                                         select new selectvalue { shparc = c as TGIS_ShapeArc, dvalue = d.reactionRate, dflow = d.flow }).ToArray();
                        selectvalues = queryRate.ToArray();
                        break;
                    case "LinkFlow":
                        var queryFlow = (from c in linklayper.Items
                                             join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetLinkValues(lperiod)
                                             on c.GetField("ID").ToString() equals d.ID
                                             select new selectvalue { shparc = c as TGIS_ShapeArc, dvalue = d.flow, dflow = d.flow }).ToArray();
                        selectvalues = queryFlow.ToArray();
                        break;
                    case "LinkVelocity":
                        var queryVelocity = (from c in linklayper.Items
                                             join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetLinkValues(lperiod)
                                             on c.GetField("ID").ToString() equals d.ID
                                             select new selectvalue { shparc = c as TGIS_ShapeArc, dvalue = d.velocity, dflow = d.flow }).ToArray();
                        selectvalues = queryVelocity.ToArray();
                        break;
                    case "LinkUnitHeadloss":
                        var queryHeadloss = (from c in linklayper.Items
                                             join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetLinkValues(lperiod)
                                             on c.GetField("ID").ToString() equals d.ID
                                             select new selectvalue { shparc = c as TGIS_ShapeArc, dvalue = d.headloss, dflow = d.flow }).ToArray();
                        selectvalues = queryHeadloss.ToArray();
                        break;
                    case "LinkChlorine":
                        var queryChlorine = (from c in linklayper.Items
                                             join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Analysis.GetLinkValues(lperiod)
                                             on c.GetField("ID").ToString() equals d.ID
                                             select new selectvalue { shparc = c as TGIS_ShapeArc, dvalue = d.quality, dflow = d.flow }).ToArray();
                        selectvalues = queryChlorine.ToArray();
                        break;

                }

                foreach (var item in selectvalues)
                {
                    if (double.TryParse(item.dflow.ToString(), out double dflow))
                    {
                        if (dflow > 0)
                            item.shparc.Params.Line.Symbol = TGIS_Utils.SymbolList.Prepare(strForwardLine);
                        else if (dflow < 0)
                            item.shparc.Params.Line.Symbol = TGIS_Utils.SymbolList.Prepare(strOppositeLine);
                        else if (dflow == 0)
                            item.shparc.Params.Line.Symbol = TGIS_Utils.SymbolList.Prepare(strLine);
                    }

                    LinkLabel(item.shparc, item.dvalue);

                    if (double.TryParse(item.dvalue.ToString(), out double dtemp))
                    {
                        item.shparc.Params.Line.Color = GISColor(ColorBinding(dtemp, dlist));

                    }
                    else
                        item.shparc.Params.Line.Color = GISColor((Color)ColorConverter.ConvertFromString("#FF000000"));
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void LinkLabel(TGIS_ShapeArc item, double? dvalue)
        {
            item.Params.Labels.Value = string.Format("{0:0.0#}", dvalue);
            item.Params.Labels.Visible = blinklbvisible;
            item.Params.Labels.Position = TGIS_LabelPosition.DownCenter;
            item.Params.Labels.Color = TGIS_Color.Aqua;
        }

        /// <summary>
        /// TGIS 색상 변환
        /// </summary>
        /// <param name="_color"></param>
        /// <returns></returns>
        private TGIS_Color GISColor(Color color)
        {
            return (TGIS_Color.FromARGB(color.A, color.R, color.G, color.B));
        }

        /// <summary>
        /// 값에 따른 색상 선택
        /// </summary>
        /// <param name="dval"></param>
        /// <param name="dLegendVal"></param>
        /// <returns></returns>
        private Color ColorBinding(double dval, List<double> dLegendVal)
        {
            try
            {
                dval = Math.Abs(dval);

                Color color = (Color)ColorConverter.ConvertFromString("#FF000000");

                if (dval <= dLegendVal[0])
                {
                    color = (Color)ColorConverter.ConvertFromString("#FF0000FF");
                }
                else if (dLegendVal[0] <= dval & dval < dLegendVal[1])
                {
                    color = (Color)ColorConverter.ConvertFromString("#FF00FFFF");
                }
                else if (dLegendVal[1] <= dval & dval < dLegendVal[2])
                {
                    color = (Color)ColorConverter.ConvertFromString("#FF32CD32");
                }
                else if (dLegendVal[2] <= dval & dval < dLegendVal[3])
                {
                    color = (Color)ColorConverter.ConvertFromString("#FFFFFF00");
                }
                else if (dLegendVal[3] <= dval)
                {
                    color = (Color)ColorConverter.ConvertFromString("#FFFF0000");
                }

                return color;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return (Color)ColorConverter.ConvertFromString("#FF000000");
            }
        }
        #endregion

        public EpanetTatukMapControl()
        {
            InitializeComponent();
            mapcontrol = this._mapcontrol;
        }

        #region 맵컨트롤 이벤트
        /// <summary>
        /// mousedown & mousemove 했을경우
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed & mapcontrol.Mode == TGIS_ViewerMode.Select)
                    mapcontrol.Mode = TGIS_ViewerMode.Drag;

                TGIS_Point pt = mapcontrol.ScreenToMap(new System.Drawing.Point(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y));
                TGIS_Point ptg = mapcontrol.CS.ToWGS(pt);
                lbXY.Content = "X : " + pt.X.ToString() + "(" + TGIS_Utils.GisLongitudeToStr(ptg.X).ToString() + "),  " + pt.Y.ToString() + "(" + TGIS_Utils.GisLatitudeToStr(ptg.Y).ToString() + ")";
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// mousedown 했을경우 select
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Mapcontrol_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (mapcontrol.IsEmpty) return;

                if (e.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
                {
                    if (e.ClickCount == 2)
                    {
                        if (selectshp == null) return;

                        int lperiod;
                        if (!int.TryParse(cbTimeList.EditValue.ToString(), out lperiod)) return;

                        object objitem = null;

                        if(selectshp.Layer is TLink_Layer)
                            objitem = GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.FIndLink(selectshp.GetField("ID").ToString());
                        else if(selectshp.Layer is TNode_Layer)
                            objitem = GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.FIndNode(selectshp.GetField("ID").ToString());

                        if (objitem == null) return;

                        Infopopup = new EpanetTatukInfoPopup(objitem, lperiod);
                        Infopopup.ShowDialog();

                        mapcontrol.Mode = TGIS_ViewerMode.Select;
                    }
                    else
                    {
                        selectshp = null;
                        DeSelectShapeAll();

                        System.Drawing.Point pt = new System.Drawing.Point((int)Math.Truncate(e.GetPosition(mapcontrol).X), (int)Math.Truncate(e.GetPosition(mapcontrol).Y));
                        selectshp = (TGIS_Shape)mapcontrol.Locate(mapcontrol.ScreenToMap(pt), 5 / mapcontrol.Zoom);

                        if (selectshp == null) return;
                        selectshp.IsSelected = !selectshp.IsSelected;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// premousedown 했을경우 drag 모드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (mapcontrol == null) { }
                if (mapcontrol.IsEmpty) return;

                mapcontrol.ReleaseMouseCapture();
                mapcontrol.Mode = TGIS_ViewerMode.Drag;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// premouseup 했을경우 select 모드 & Drag로 이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                mapcontrol.Mode = TGIS_ViewerMode.Select;
                mapcontrol.ZoomBy(1, 0, 0);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 휠 줌 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (mapcontrol.IsEmpty) return;

                if (e.Delta > 120) return;

                if (e.Delta == 120)
                {
                    dzoom = mapcontrol.ZoomEx;
                    if (dzoom > 10000) return;

                    mapcontrol.ZoomBy(5 / 4.0, (int)Math.Truncate(e.GetPosition(mapcontrol).X), (int)Math.Truncate(e.GetPosition(mapcontrol).Y));
                }
                else if (e.Delta == -120)
                    mapcontrol.ZoomBy(4 / 5.0, (int)Math.Truncate(e.GetPosition(mapcontrol).X), (int)Math.Truncate(e.GetPosition(mapcontrol).Y));

            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 전체DeSelect
        /// </summary>
        private void DeSelectShapeAll()
        {
            foreach (var item in mapcontrol.Items)
            {
                if (item == null)
                    continue;

                if (item is TGIS_LayerVector)
                    (item as TGIS_LayerVector).DeselectAll();
            }
        }

        /// <summary>
        /// 선택한 Shp가져오기
        /// </summary>
        /// <param name="_layer"></param>
        /// <param name="_field"></param>
        /// <param name="_id"></param>
        /// <returns></returns>
        public static TGIS_Shape GetShapeInLayer(TGIS_LayerVector _layer, Type _type, string _shpid)
        {
            string Query = string.Empty;
            //string Query = $"{_field} = {_id}";
            return _layer.FindFirst(TGIS_Utils.GisWholeWorld(), Query);
        }
        #endregion

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    if (threadpaly != null)
            //    {
            //        timerpaly.Stop();
            //        timerpaly.Dispose();
            //        timerpaly = null;
            //        threadpaly.Abort();
            //        threadpaly = null;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    if (threadpaly != null)
            //    {
            //        timerpaly.Stop();
            //        timerpaly.Dispose();
            //        timerpaly = null;
            //        threadpaly.Abort();
            //        threadpaly = null;
            //    }
            //}
        }
    }

    public class selectvalue
    {
        public TGIS_ShapePoint shppoint { get; set; }
        public TGIS_ShapeArc shparc { get; set; }
        public double dvalue { get; set; }
        public double dflow { get; set; }
    }
}
