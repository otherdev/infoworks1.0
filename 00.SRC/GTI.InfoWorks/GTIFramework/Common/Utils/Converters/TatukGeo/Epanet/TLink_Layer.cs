﻿using GTIFramework.Analysis.Epanet.Items;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.Linq;
using TatukGIS.NDK;

namespace GTIFramework.Common.Utils.Converters.TatukGeo.Epanet
{
    /// <summary>
    /// Link 레이어 Class
    /// </summary>
    public class TLink_Layer : TGIS_LayerVector
    {
        public List<Link> linklist;

        public TLink_Layer() : base()
        {
            this.Name = "linklayer";
            DefaultShapeType = TGIS_ShapeType.Arc;
            Extent = TGIS_Utils.GisNoWorld();
            UseRTree = true;

            this.AddField("FTR", TGIS_FieldType.String, 100, 0);
            this.AddField("ID", TGIS_FieldType.String, 100, 0);
        }

        /// <summary>
        /// Link 객체 Import
        /// </summary>
        public void Import(List<Link> implinklist)
        {
            linklist = implinklist;
            if (linklist == null) return;
            if (linklist.Count == 0) return;

            try
            {
                this.CS = this.Viewer.Ref.CS;

                DateTime sdt = DateTime.Now;

                var querystartx = (from c in linklist
                                   join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Nodes on c.StartNode equals d.ID
                                   select c.startX = d.X).ToArray();

                var querystarty = (from c in linklist
                                   join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Nodes on c.StartNode equals d.ID
                                   select c.startY = d.Y).ToArray();

                var queryendx = (from c in linklist
                                 join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Nodes on c.EndNode equals d.ID
                                 select c.endX = d.X).ToArray();

                var queryendy = (from c in linklist
                                 join d in GTIFramework.Analysis.Epanet.EpanetVar.tINPConvert.network.Nodes on c.EndNode equals d.ID
                                 select c.endY = d.Y).ToArray();

                var query = from c in linklist
                            select new TLink_Shape(c, this) { };
                
                foreach (TLink_Shape linkshp in query.ToArray())
                    AddShape(linkshp, true);

                Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,, 링크 Add 완료");
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// Link 객체 Add
        /// </summary>
        /// <param name="_link"></param>
        public void Add(Link _link)
        {
            try
            {
                
                _link.startX = _link.NodeStart.X;
                _link.startY = _link.NodeStart.Y;
                _link.endX = _link.NodeEnd.X;
                _link.endY = _link.NodeEnd.X;
                linklist.Add(_link);
                TLink_Shape linkshp = new TLink_Shape(_link, this);
                
                this.AddShape(linkshp, true);
                linkshp.Unlock();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// Link 레이어 Delete
        /// </summary>
        /// <param name="_linkshp"></param>
        public void Delete(TLink_Shape _linkshp)
        {
            try
            {
                linklist.Remove(_linkshp.link);
                _linkshp.Lock(TGIS_Lock.Projection);
                this.Items.Delete(this.Items.IndexOf(_linkshp));
                _linkshp.Unlock();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
    }

    /// <summary>
    /// Link shape
    /// </summary>
   
    /// <summary>
    /// Link shape
    /// </summary>
    public class TLink_Shape : TGIS_ShapeArc
    {
        public Link link { get; set; } = null;

        public TLink_Shape(object type, TGIS_LayerVector layer) : base(TGIS_DimensionType.XY)
        {
            if (!(type is Link)) return;
            link = type as Link;

            #region 주석
            //TGIS_Bytes bytes = new TGIS_Bytes((link.Vertices.Count * 2) + 4);
            //int off = 0;

            //bytes.WriteDouble(off, link.NodeStart.X);
            //off++;
            //bytes.WriteDouble(off, link.NodeStart.Y);
            //off++;

            //foreach (Vertex vertex in link.Vertices)
            //{
            //    bytes.WriteDouble(off, vertex.X);
            //    off++;
            //    bytes.WriteDouble(off, vertex.Y);
            //    off++;
            //}

            //bytes.WriteDouble(0, link.NodeStart.X);
            //off++;
            //bytes.WriteDouble(1, link.NodeStart.Y);
            //off++; 
            #endregion

            if (type is Pipe)
            {
                this.Params.Line.Color = TGIS_Color.Black;
                this.Params.Line.OutlineColor = TGIS_Color.Black;
                //this.Params.Line.Pattern = TGIS_BrushStyle.Solid;
                this.Params.Line.Symbol = TGIS_Utils.SymbolList.Prepare("&L(100%)");
            }
            else if (type is Pump)
            {
                this.Params.Line.Color = TGIS_Color.Black;
                this.Params.Line.OutlineColor = TGIS_Color.Black;
                //this.Params.Line.Pattern = TGIS_BrushStyle.Solid;
                //this.Params.Line.Symbol = TGIS_Utils.SymbolList.Prepare("&L(100%)G(50%)I(+6+0+6+0+0-6+6+0-12+0)G(100%)M(100% 0)");
                this.Params.Line.Symbol = TGIS_Utils.SymbolList.Prepare("&L(100%)G(50%)I(+0+0+0+15S+15S+15S+30S+0+15S-15S+0-30S-15S-15S-30S+0-15S+15S+0+15S)I(+5+0+5+0+0-3-7+0)G(100%)M(100% 0)");
            }
            else if (type is Valve)
            {
                this.Params.Line.Color = TGIS_Color.Black;
                this.Params.Line.OutlineColor = TGIS_Color.Black;
                //this.Params.Line.Pattern = TGIS_BrushStyle.Solid;
                this.Params.Line.Symbol = TGIS_Utils.SymbolList.Prepare("&L(100%)G(50%)I(-3+3+0-6+3+3+3+3+0-6-3+3)G(100%)M(100% 0)");
                
            }

            this.Layer = layer;

            this.Lock(TGIS_Lock.None);
            this.AddPart();
            this.SetField("FTR", link.GetType().ToString());
            this.SetField("ID", link.ID);

            #region 포인트 생성
            //DateTime sdt = DateTime.Now;

            this.AddPoint(new TGIS_Point(link.startX, link.startY));
            //Console.WriteLine((DateTime.Now - sdt).TotalMilliseconds.ToString() + "링크 Point add");

            var query = from c in link.Vertices
                        select new TGIS_Point(c.X, c.Y);

            foreach (TGIS_Point point in query)
                this.AddPoint(point);

            this.AddPoint(new TGIS_Point(link.endX, link.endY));
            #endregion

            this.Unlock();
        }
        public string GetLinkID()
        {
            if (link == null)
                return null;

            return link.ID;
        }
    }
}
