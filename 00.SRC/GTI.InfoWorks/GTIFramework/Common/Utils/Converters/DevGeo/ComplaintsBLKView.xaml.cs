﻿using DevExpress.Xpf.Map;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTIFramework.Common.Utils.Converters.DevGeo
{
    /// <summary>
    /// ComplaintsBLKView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ComplaintsBLKView : UserControl
    {
        static object _objOWNER;
        static string _STRBLK_CD;
        static string _STRBLK_NM;
        static MapItem _ITEM;
        static DataTable _GBNList;
        object objOWNER;
        string STRBLK_CD;
        string STRBLK_NM;

        MapItem ITEM;

        DataTable GBNList;

        static int staticinttt = 0;

        static Hashtable htlbVlaue { get; set; }

        #region DATA
        public object DATA
        {
            get { return (object)GetValue(DATAProperty); }
            set { SetValue(DATAProperty, value); }
        }

        public static DependencyProperty DATAProperty =
            DependencyProperty.Register(
                "DATA", typeof(object), typeof(ComplaintsBLKView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(DATAPropertyChanged)));

        public static void DATAPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (ComplaintsBLKView)obj;
                int intTT;

                if (control.DATA != null)
                {
                    DataRow[] drselect;

                    if(control.DATA is DataRow[])
                    {
                        foreach (DictionaryEntry delb in htlbVlaue)
                        {
                            drselect = (control.DATA as DataRow[]).CopyToDataTable().Select("[MINWON_CD] = '" + delb.Key.ToString().Replace("lb", "") + "'");

                            if(drselect.Length == 1)
                            {
                                (control.FindName(delb.Key.ToString()) as Label).Content = drselect[0]["CNT"].ToString();
                            }
                        }

                        if(int.TryParse((control.DATA as DataRow[]).CopyToDataTable().Compute("SUM(CNT)", "0=0").ToString(), out intTT))
                        {
                            (control.FindName("lbTT") as Label).Content = intTT.ToString();
                        }
                    }
                }
                else
                {
                    foreach (DictionaryEntry delb in htlbVlaue)
                    {
                        (control.FindName(delb.Key.ToString()) as Label).Content = "0";
                    }

                    (control.FindName("lbTT") as Label).Content = "0";
                }

                BorderColorPropertyChanged(control, e);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        #region OWNER
        public Hashtable OWNER
        {
            get { return (Hashtable)GetValue(OWNERProperty); }
            set { SetValue(OWNERProperty, value); }
        }

        public static DependencyProperty OWNERProperty =
            DependencyProperty.Register(
                "OWNER", typeof(Hashtable), typeof(ComplaintsBLKView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(OWNERPropertyChanged)));

        public static void OWNERPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (ComplaintsBLKView)obj;

                if (control.OWNER is Hashtable)
                {
                    control.lbname.Content = (control.OWNER as Hashtable)["BLK_NM"].ToString();

                    _objOWNER = (control.OWNER as Hashtable)["OWNER"] as object;
                    _STRBLK_CD = (control.OWNER as Hashtable)["BLK_CD"].ToString();
                    _STRBLK_NM = (control.OWNER as Hashtable)["BLK_NM"].ToString();
                    _GBNList = (control.OWNER as Hashtable)["GBN"] as DataTable;
                    _ITEM = (control.OWNER as Hashtable)["ITEM"] as MapItem;

                    control.Height = control.Height + ((_GBNList.Rows.Count + 1) * 15);
                    Hashtable _htlbVlaue = new Hashtable();

                    foreach (DataRow drGBN in _GBNList.Rows)
                    {
                        //보더 생성
                        Border Lborder = new Border();
                        Grid.SetColumn(Lborder, 0);
                        Grid.SetRow(Lborder, _GBNList.Rows.IndexOf(drGBN) + 2);
                        Lborder.Background = System.Windows.Media.Brushes.White;
                        Lborder.BorderBrush = Application.Current.FindResource("PopupCOLOR2Brush") as SolidColorBrush;
                        Label Llabel = new Label() { Content = drGBN["DTL_NM"].ToString(), HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Center };
                        Llabel.FontSize = 11;
                        Llabel.Style = Application.Current.FindResource("malgun") as Style;
                        Llabel.Padding = new Thickness(0);
                        Llabel.Margin = new Thickness(5,0,0,0);
                        Lborder.Child = Llabel;

                        Border Rborder = new Border();
                        Grid.SetColumn(Rborder, 1);
                        Grid.SetRow(Rborder, _GBNList.Rows.IndexOf(drGBN) + 2);
                        Rborder.Background = System.Windows.Media.Brushes.White;
                        Rborder.BorderBrush = Application.Current.FindResource("PopupCOLOR2Brush") as SolidColorBrush;

                        Label Rlabel = new Label() { Name = "lb" + drGBN["DTL_CD"].ToString(), Content = "0", HorizontalAlignment = HorizontalAlignment.Right, VerticalAlignment = VerticalAlignment.Center };
                        Rlabel.FontSize = 11;
                        Rlabel.Style = Application.Current.FindResource("malgun") as Style;
                        Rlabel.Padding = new Thickness(0);
                        Rlabel.Margin = new Thickness(0, 0, 5, 0);
                        Rborder.Child = Rlabel;

                        _htlbVlaue.Add(Rlabel.Name, Rlabel);
                        control.RegisterName(Rlabel.Name, Rlabel);

                        Lborder.BorderThickness = new Thickness(0, 0, 1, 1);
                        Rborder.BorderThickness = new Thickness(0, 0, 0, 1);

                        if (_GBNList.Rows.IndexOf(drGBN) == _GBNList.Rows.Count-1)
                        {
                            //총계
                            Border LTTborder = new Border();
                            Grid.SetColumn(LTTborder, 0);
                            Grid.SetRow(LTTborder, _GBNList.Rows.IndexOf(drGBN) + 3);
                            LTTborder.Background = System.Windows.Media.Brushes.White;
                            LTTborder.BorderBrush = Application.Current.FindResource("PopupCOLOR2Brush") as SolidColorBrush;
                            Label LTTlabel = new Label() { Content = "총계", HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Center };
                            LTTlabel.FontSize = 11;
                            LTTlabel.Style = Application.Current.FindResource("malgun") as Style;
                            LTTlabel.Padding = new Thickness(0);
                            LTTlabel.Margin = new Thickness(5, 0, 0, 0);
                            LTTborder.Child = LTTlabel;

                            Border RTTborder = new Border();
                            Grid.SetColumn(RTTborder, 1);
                            Grid.SetRow(RTTborder, _GBNList.Rows.IndexOf(drGBN) + 3);
                            RTTborder.Background = System.Windows.Media.Brushes.White;
                            RTTborder.BorderBrush = Application.Current.FindResource("PopupCOLOR2Brush") as SolidColorBrush;

                            Label RTTlabel = new Label() { Name = "lbTT", Content = "0", HorizontalAlignment = HorizontalAlignment.Right, VerticalAlignment = VerticalAlignment.Center };
                            RTTlabel.FontSize = 11;
                            RTTlabel.Style = Application.Current.FindResource("malgun") as Style;
                            RTTlabel.Padding = new Thickness(0);
                            RTTlabel.Margin = new Thickness(0, 0, 5, 0);
                            RTTborder.Child = RTTlabel;

                            LTTborder.BorderThickness = new Thickness(0, 0, 1, 0);
                            RTTborder.BorderThickness = new Thickness(0, 0, 0, 0);

                            control.DataGrid.Children.Add(RTTborder);
                            control.DataGrid.Children.Add(LTTborder);

                            control.RegisterName(RTTlabel.Name, RTTlabel);

                            control.DataGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(15, GridUnitType.Pixel) });
                        }

                        control.DataGrid.Children.Add(Rborder);
                        control.DataGrid.Children.Add(Lborder);

                        control.DataGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(15, GridUnitType.Pixel) });
                    }

                    control.InfoSave(_objOWNER, _STRBLK_CD, _STRBLK_NM, _GBNList, _htlbVlaue, _ITEM);
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        #region BorderColor
        public DataTable BorderColor
        {
            get { return (DataTable)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static DependencyProperty BorderColorProperty =
            DependencyProperty.Register(
                "BorderColor", typeof(DataTable), typeof(ComplaintsBLKView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(BorderColorPropertyChanged)));

        public static void BorderColorPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (ComplaintsBLKView)obj;

                int LLimit;
                int LMimit;
                int LHimit;
                int inttt;

                if (!(control.BorderColor is DataTable)) return;
                DataTable temp = control.BorderColor as DataTable;

                if (temp.Rows.Count != 1) return;

                if(int.TryParse(temp.Rows[0]["CRTC_L_VAL"].ToString(), out LLimit) 
                    & int.TryParse(temp.Rows[0]["CRTC_M_VAL"].ToString(), out LMimit)
                    & int.TryParse(temp.Rows[0]["CRTC_U_VAL"].ToString(), out LHimit)
                    & int.TryParse((control.FindName("lbTT") as Label).Content.ToString(), out inttt))
                {
                    if(inttt <= LLimit)
                    {
                        control.BorderTH.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_L_COLOR"].ToString()));
                        (control.ITEM as MapPath).Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_L_COLOR"].ToString()));
                        (control.ITEM as MapPath).Stroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_L_COLOR"].ToString()));
                        (control.ITEM as MapPath).Fill.Opacity = 0.3;
                    }
                    else if(LLimit < inttt & inttt < LMimit)
                    {
                        control.BorderTH.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_M_COLOR"].ToString()));
                        (control.ITEM as MapPath).Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_M_COLOR"].ToString()));
                        (control.ITEM as MapPath).Stroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_M_COLOR"].ToString()));
                        (control.ITEM as MapPath).Fill.Opacity = 0.3;
                    }
                    else if(LHimit <= inttt)
                    {
                        control.BorderTH.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_U_COLOR"].ToString()));
                        (control.ITEM as MapPath).Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_U_COLOR"].ToString()));
                        (control.ITEM as MapPath).Stroke = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + temp.Rows[0]["CRTC_U_COLOR"].ToString()));
                        (control.ITEM as MapPath).Fill.Opacity = 0.3;
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        public ComplaintsBLKView()
        {
            InitializeComponent();
        }

        private void InfoSave(object OWNER, string BLK_CD, string BLK_NM, DataTable _GBNList, Hashtable _htlbVlaue, MapItem _ITEM)
        {
            objOWNER = OWNER;
            STRBLK_CD = BLK_CD;
            STRBLK_NM = BLK_NM;
            GBNList = _GBNList;
            htlbVlaue = _htlbVlaue;
            ITEM = _ITEM;
        }
    }
}
