﻿using DevExpress.Xpf.Map;
using GTIFramework.Common.Utils.Handle;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GTIFramework.Common.Utils.Converters.DevGeo
{
    public class CustomMapDataProvider : MapDataProviderBase
    {
        public SphericalMercatorProjection projection = new SphericalMercatorProjection();

        public override ProjectionBase Projection { get { return projection; } }

        public CustomMapDataProvider()
        {
            SetTileSource(new CustomTileSource());
        }
        protected override MapDependencyObject CreateObject()
        {
            return new CustomMapDataProvider();
        }
        public override Size GetMapSizeInPixels(double zoomLevel)
        {
            return new Size(Math.Pow(5.0, zoomLevel) * 256,
                Math.Pow(5.0, zoomLevel) * 256);
        }
    }

    public class CustomTileSource : MapTileSourceBase
    {
        iniHandle ini = new iniHandle(Environment.CurrentDirectory + "\\GTIConfig.ini");
        public const int maxZoomLevel = 18;
        public const int tileSize = 256;

        string url;

        static int imageWidth = (int)Math.Pow(2.0, maxZoomLevel) * tileSize;
        static int imageHeight = (int)Math.Pow(2.0, maxZoomLevel) * tileSize;
        //static string[] subdomains = new string[] { "a", "b", "c" };

        public CustomTileSource()
            : base(imageWidth, imageHeight, tileSize, tileSize)
        {
            url = ini.GetIniValue("GIS", "strGISpath").ToString();
        }
        public override Uri GetTileByZoomLevel(int zoomLevel, long tilePositionX, long tilePositionY)
        {
            try
            {
                string urltemp = url + "/{tileLevel}/{tileX}/{tileY}.png";

                urltemp = urltemp.Replace("{tileX}", (tilePositionX).ToString(CultureInfo.InvariantCulture));
                urltemp = urltemp.Replace("{tileY}", (tilePositionY).ToString(CultureInfo.InvariantCulture));
                urltemp = urltemp.Replace("{tileLevel}", (zoomLevel).ToString(CultureInfo.InvariantCulture));

                Console.WriteLine(urltemp);

                return new Uri(urltemp);
            }
            catch (Exception ex)
            {
                return null;
            }

            #region ??
            ////url = url.Replace("{tileLevel}", (zoomLevel).ToString(CultureInfo.InvariantCulture).Replace("L", "").PadLeft(2, '0'));
            ////url = url.Replace("{subdomain}", subdomains[GetSubdomainIndex(subdomains.Length)]);

            //System.IO.FileInfo fi = new System.IO.FileInfo(url);
            ////Console.WriteLine(url);

            //if (fi.Exists)
            //    return new Uri(url);
            //else
            //    return new Uri(@"G:/내가 받은 Tile/OUT.out.png"); 
            #endregion
        }
    }
}
