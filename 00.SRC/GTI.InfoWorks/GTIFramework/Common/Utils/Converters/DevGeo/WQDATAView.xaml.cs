﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTIFramework.Common.Utils.Converters.DevGeo
{
    /// <summary>
    /// WQDATAView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class WQDATAView : UserControl
    {
        static object _objOWNER;
        static string _POIT_NM;
        static string _STRTAG_1;
        static string _STRTAG_2;
        static string _STRTAG_3;
        static string _STRTAG_4;

        object objOWNER;
        string POIT_NM;
        string STRTAG_1;
        string STRTAG_2;
        string STRTAG_3;
        string STRTAG_4;

        Hashtable conditios = new Hashtable();
        DateTime nowtime;

        #region DATA
        public object DATA
        {
            get { return (object)GetValue(DATAProperty); }
            set { SetValue(DATAProperty, value); }
        }

        public static DependencyProperty DATAProperty =
            DependencyProperty.Register(
                "DATA", typeof(object), typeof(WQDATAView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(DATAPropertyChanged)));

        public static void DATAPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var control = (WQDATAView)obj;

            if(control.DATA is DataTable)
            {
                try
                {
                    DataRow[] arr;

                    arr = (control.DATA as DataTable).Select("[PH_MESR_VAL] IS NOT NULL", "[MESR_TM] DESC");
                    if (arr.Length > 0)
                        control.lbPH.Content = arr[0]["PH_MESR_VAL"].ToString();
                    else
                        control.lbPH.Content = "";

                    arr = (control.DATA as DataTable).Select("[CL_MESR_VAL] IS NOT NULL", "[MESR_TM] DESC");
                    if (arr.Length > 0)
                        control.lbCL.Content = arr[0]["CL_MESR_VAL"].ToString();
                    else
                        control.lbCL.Content = "";

                    arr = (control.DATA as DataTable).Select("[TUB_MESR_VAL] IS NOT NULL", "[MESR_TM] DESC");
                    if (arr.Length > 0)
                        control.lbTUB.Content = arr[0]["TUB_MESR_VAL"].ToString();
                    else
                        control.lbTUB.Content = "";

                    arr = (control.DATA as DataTable).Select("[TMP_MESR_VAL] IS NOT NULL", "[MESR_TM] DESC");
                    if (arr.Length > 0)
                        control.lbTMP.Content = arr[0]["TMP_MESR_VAL"].ToString();
                    else
                        control.lbTMP.Content = "";

                    arr = (control.DATA as DataTable).Select("[COND_MESR_VAL] IS NOT NULL", "[MESR_TM] DESC");
                    if (arr.Length > 0)
                        control.lbCOND.Content = arr[0]["COND_MESR_VAL"].ToString();
                    else
                        control.lbCOND.Content = "";
                }
                catch (Exception ex)
                {
                    Messages.ErrLog(ex);
                }
            }
        }
        #endregion

        #region OWNER
        public Hashtable OWNER
        {
            get { return (Hashtable)GetValue(OWNERProperty); }
            set { SetValue(OWNERProperty, value); }
        }

        public static DependencyProperty OWNERProperty =
            DependencyProperty.Register(
                "OWNER", typeof(Hashtable), typeof(WQDATAView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(OWNERPropertyChanged)));

        public static void OWNERPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (WQDATAView)obj;

                if (control.OWNER is Hashtable)
                {
                    control.lbname.Content = (control.OWNER as Hashtable)["POIT_NM"].ToString();

                    control.InfoSave((control.OWNER as Hashtable));
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion


        public WQDATAView()
        {
            InitializeComponent();
            btnChart.Click += BtnChart_Click;
        }

        private void BtnChart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                nowtime = DateTime.Now;

                conditios.Clear();
                conditios.Add("VIEW", "T");  //실시간  htthreadconditions.Add("VIEW", "T"); //기간
                conditios.Add("SDT", nowtime.ToString("yyyyMMdd") + "0000");
                conditios.Add("EDT", nowtime.ToString("yyyyMMddHHmm"));
                conditios.Add("TUB_TAG", STRTAG_1);
                conditios.Add("CL_TAG", STRTAG_2);
                conditios.Add("PH_TAG", STRTAG_3);
                conditios.Add("TMP_TAG", STRTAG_4);
                conditios.Add("DEFAULT_COLOR", "'" + Application.Current.FindResource("Default_Chart_Color").ToString() + "'");

                object[] objParam = { conditios, POIT_NM };
                objOWNER.GetType().GetMethod("PopupChart").Invoke(objOWNER, objParam);
            }
            catch (Exception ex)
            {
            }
        }

        private void InfoSave(Hashtable htOWNER)
        {
            objOWNER = htOWNER["OWNER"] as object;
            POIT_NM = htOWNER["POIT_NM"].ToString();
            STRTAG_1 = htOWNER["TAG_1"].ToString();
            STRTAG_2 = htOWNER["TAG_2"].ToString();
            STRTAG_3 = htOWNER["TAG_3"].ToString();
            STRTAG_4 = htOWNER["TAG_4"].ToString();
        }
    }
}
