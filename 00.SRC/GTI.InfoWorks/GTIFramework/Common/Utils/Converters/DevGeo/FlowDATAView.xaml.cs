﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTIFramework.Common.Utils.Converters.DevGeo
{
    /// <summary>
    /// FlowDATAView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class FlowDATAView : UserControl
    {
        static DataTable dtresult = new DataTable();

        static double dFLData = 0;
        static double dFLSMCData = 0;
        static double dPRSData = 0;

        Hashtable htOWNER;
        static object objOWNER;
        DataTable dtTAG_LIST = new DataTable();

        static string _STRBLK_NM;
        string STRBLK_NM;

        Hashtable htDataLabel = new Hashtable();

        #region DATA
        public object DATA
        {
            get { return (object)GetValue(DATAProperty); }
            set { SetValue(DATAProperty, value); }
        }

        public static DependencyProperty DATAProperty =
            DependencyProperty.Register(
                "DATA", typeof(object), typeof(FlowDATAView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(DATAPropertyChanged)));

        public static void DATAPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (FlowDATAView)obj;

                if (control.DATA is DataTable)
                {
                    dtresult = control.DATA as DataTable;

                    //(obj as FlowDATAView).lastestDataStore(dtresult);

                    if (dtresult.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtresult.Rows)
                        {
                            switch (dr["ITEM_CAT"].ToString())
                            {
                                case "FL":
                                    if (double.TryParse(dr["MESR_VAL"].ToString(), out dFLData))
                                        ((control.FindName("GIS_" + dr["GIS_ID"].ToString()) as Hashtable)[dr["BLK_CD"].ToString() + "_" + dr["TAG_ID"].ToString()] as Label).Content = dFLData.ToString("N1");
                                    break;
                                case "FLSMC":
                                    if (double.TryParse(dr["MESR_VAL"].ToString(), out dFLSMCData))
                                        ((control.FindName("GIS_" + dr["GIS_ID"].ToString()) as Hashtable)[dr["BLK_CD"].ToString() + "_" + dr["TAG_ID"].ToString()] as Label).Content = dFLSMCData.ToString("N2");
                                    break;
                                case "PRS":
                                    if (double.TryParse(dr["MESR_VAL"].ToString(), out dPRSData))
                                        ((control.FindName("GIS_" + dr["GIS_ID"].ToString()) as Hashtable)[dr["BLK_CD"].ToString() + "_" + dr["TAG_ID"].ToString()] as Label).Content = dPRSData.ToString("N2");
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        #region OWNER
        public Hashtable OWNER
        {
            get { return (Hashtable)GetValue(OWNERProperty); }
            set { SetValue(OWNERProperty, value); }
        }

        public static DependencyProperty OWNERProperty =
            DependencyProperty.Register(
                "OWNER", typeof(Hashtable), typeof(FlowDATAView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(OWNERPropertyChanged)));

        public static void OWNERPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (FlowDATAView)obj;

                if (control.OWNER is Hashtable)
                {

                    control.lbname.Content = (control.OWNER as Hashtable)["BLK_NM"].ToString();
                    _STRBLK_NM = (control.OWNER as Hashtable)["BLK_NM"].ToString();

                    (obj as FlowDATAView).OWNER_Store((control.OWNER as Hashtable), _STRBLK_NM);

                    //control.lbname.Content = (control.OWNER as Hashtable)["BLK_NM"].ToString();
                    //STRBLK_CD = (control.OWNER as Hashtable)["BLK_CD"].ToString();
                    //objOWNER = (control.OWNER as Hashtable)["OWNER"] as object;
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        public FlowDATAView()
        {
            InitializeComponent();
            btn.Click += Btn_Click;
        }

        private void OWNER_Store(Hashtable ht, string _strBLK_NM)
        {
            try
            {
                htOWNER = ht;
                STRBLK_NM = _strBLK_NM;
                dtTAG_LIST = htOWNER["TAG_LIST"] as DataTable;
                objOWNER = htOWNER["OWNER"];

                CreateDataWindow(dtTAG_LIST, STRBLK_NM);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void CreateDataWindow(DataTable dtTAG, string srtBLKNM)
        {
            try
            {
                //데이터창 높이
                this.Height = 30 + (dtTAG.Rows.Count * 35);

                htDataLabel.Clear();
                this.RegisterName("GIS_" + dtTAG.Rows[0]["GIS_ID"].ToString(), htDataLabel);


                foreach (DataRow dr in dtTAG.Rows)
                {
                    lbname.Content = srtBLKNM;

                    gridFLOWDATA.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(35d, GridUnitType.Pixel) });

                    //가로 구분선
                    Border bdRowLine_ = new Border();
                    bdRowLine_.BorderBrush = bdRowLine.BorderBrush;
                    bdRowLine_.BorderThickness = bdRowLine.BorderThickness;
                    Grid.SetRow(bdRowLine_, dtTAG.Rows.IndexOf(dr) + 1);
                    Grid.SetColumnSpan(bdRowLine_, 2);
                    gridFLOWDATA.Children.Add(bdRowLine_);

                    //명칭 라벨 영역 Border
                    Border bdITEM_ = new Border();
                    bdITEM_.Background = bdITEM.Background;
                    bdITEM_.Margin = bdITEM.Margin;
                    Grid.SetRow(bdITEM_, dtTAG.Rows.IndexOf(dr) + 1);
                    gridFLOWDATA.Children.Add(bdITEM_);

                    Label lbITEM_ = new Label();
                    lbITEM_.Content = dr["NM"].ToString() + " " + dr["ITEM_NM"].ToString();
                    lbITEM_.FontFamily = lbITEM.FontFamily;
                    lbITEM_.Foreground = lbITEM.Foreground;
                    lbITEM_.FontSize = lbITEM.FontSize;
                    lbITEM_.Margin = lbITEM.Margin;
                    lbITEM_.Padding = lbITEM.Padding;
                    lbITEM_.HorizontalContentAlignment = lbITEM.HorizontalContentAlignment;
                    lbITEM_.VerticalContentAlignment = lbITEM.VerticalContentAlignment;
                    lbITEM_.ToolTip = dr["NM"].ToString() + " " + dr["ITEM_NM"].ToString();
                    Grid.SetRow(lbITEM_, dtTAG.Rows.IndexOf(dr) + 1);
                    gridFLOWDATA.Children.Add(lbITEM_);


                    //값 영역 Border
                    Border bdVAL = new Border();
                    bdVAL.Background = bdValue.Background;
                    bdVAL.Margin = bdValue.Margin;
                    Grid.SetRow(bdVAL, dtTAG.Rows.IndexOf(dr) + 1);
                    Grid.SetColumn(bdVAL, 1);
                    gridFLOWDATA.Children.Add(bdVAL);

                    StackPanel spVAL = new StackPanel();
                    spVAL.Orientation = Orientation.Horizontal;
                    spVAL.HorizontalAlignment = HorizontalAlignment.Right;
                    spVAL.VerticalAlignment = VerticalAlignment.Center;
                    spVAL.Margin = new Thickness(0, 0, 1, 0);

                    Label lbVAL_ = new Label();
                    lbVAL_.Padding = new Thickness(0, 0, 3, 0);
                    lbVAL_.Content = "-";
                    //폰트family 설정 필요
                    spVAL.Children.Add(lbVAL_);

                    htDataLabel.Add(dr["BLK_CD"].ToString() + "_" + dr["TAG_ID"].ToString(), lbVAL_);

                    Label lbUnit = new Label();
                    lbUnit.Padding = new Thickness(0, 0, 3, 0);
                    lbUnit.FontSize = 12;
                    //폰트family 설정 필요

                    switch (dr["ITEM_CAT"].ToString())
                    {
                        case "FL":
                            lbUnit.Content = "㎥/h";
                            break;
                        case "FLSMC":
                            lbUnit.Content = "㎥";
                            break;
                        case "PRS":
                            lbUnit.Content = "kgf/cm²";
                            break;
                    }

                    spVAL.Children.Add(lbUnit);
                    Grid.SetRow(spVAL, dtTAG.Rows.IndexOf(dr) + 1);
                    Grid.SetColumn(spVAL, 1);
                    gridFLOWDATA.Children.Add(spVAL);

                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                object[] objParam = { dtTAG_LIST, STRBLK_NM };
                objOWNER.GetType().GetMethod("PopupChart").Invoke(objOWNER, objParam);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }

        }
    }
}
