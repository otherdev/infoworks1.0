﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTIFramework.Common.Utils.Converters.DevGeo
{
    /// <summary>
    /// RevenueView.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class RevenueView : UserControl
    {
        static object _objOWNER;
        static string _STRBLK_CD;
        static string _STRBLK_NM;
        object objOWNER;
        string STRBLK_CD;
        string STRBLK_NM;

        static double dlbRate = 0;
        static double dlbbefo = 0;
        static double dlbsupp = 0;
        static double dlbuse = 0;
        static double dlbleak = 0;

        #region DATA
        public object DATA
        {
            get { return (object)GetValue(DATAProperty); }
            set { SetValue(DATAProperty, value); }
        }

        public static DependencyProperty DATAProperty =
            DependencyProperty.Register(
                "DATA", typeof(object), typeof(RevenueView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(DATAPropertyChanged)));

        public static void DATAPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (RevenueView)obj;

                if (control.DATA is DataRow)
                {
                        if (double.TryParse((control.DATA as DataRow)["REVENUE_RATE"].ToString(), out dlbRate))
                            control.lbRate.Content = dlbRate.ToString("N2");
                        else
                            control.lbRate.Content = (control.DATA as DataRow)["REVENUE_RATE"].ToString();

                        if (double.TryParse((control.DATA as DataRow)["RATE_GAP"].ToString(), out dlbbefo))
                        {
                            if (dlbbefo < 0)
                            {
                                control.img.Visibility = Visibility.Visible;
                                control.img.Style = Application.Current.FindResource("IncreaseArrow") as Style;
                            }
                            if (dlbbefo > 0)
                            {
                                control.img.Visibility = Visibility.Visible;
                                control.img.Style = Application.Current.FindResource("DecreaseArrow") as Style;
                            }
                            if (dlbbefo == 0)
                                control.img.Visibility = Visibility.Collapsed;

                            control.lbbefo.Content = Math.Abs(dlbbefo).ToString("N2");
                        }
                        else
                        {
                            control.lbbefo.Content = (control.DATA as DataRow)["RATE_GAP"].ToString();
                        }

                        if (double.TryParse((control.DATA as DataRow)["SUPP_QTY"].ToString(), out dlbsupp))
                            control.lbsupp.Content = dlbsupp.ToString("N2");
                        else
                            control.lbsupp.Content = (control.DATA as DataRow)["SUPP_QTY"].ToString();

                        if (double.TryParse((control.DATA as DataRow)["REVENUE_QTY"].ToString(), out dlbuse))
                            control.lbuse.Content = dlbuse.ToString("N2");
                        else
                            control.lbuse.Content = (control.DATA as DataRow)["REVENUE_QTY"].ToString();

                        if (double.TryParse((control.DATA as DataRow)["LEAK_QTY"].ToString(), out dlbleak))
                            control.lbleak.Content = dlbleak.ToString("N2");
                        else
                            control.lbleak.Content = (control.DATA as DataRow)["LEAK_QTY"].ToString(); 
                }

                else if(control.DATA == null)
                {
                    control.lbRate.Content = "";
                    control.img.Visibility = Visibility.Collapsed;
                    control.lbbefo.Content = "";
                    control.lbsupp.Content = "";
                    control.lbuse.Content = "";
                    control.lbleak.Content = "";
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        #region OWNER
        public Hashtable OWNER
        {
            get { return (Hashtable)GetValue(OWNERProperty); }
            set { SetValue(OWNERProperty, value); }
        }

        public static DependencyProperty OWNERProperty =
            DependencyProperty.Register(
                "OWNER", typeof(Hashtable), typeof(RevenueView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(OWNERPropertyChanged)));

        public static void OWNERPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (RevenueView)obj;

                if (control.OWNER is Hashtable)
                {
                    control.lbname.Content = (control.OWNER as Hashtable)["BLK_NM"].ToString();

                    _objOWNER = (control.OWNER as Hashtable)["OWNER"] as object;
                    _STRBLK_CD = (control.OWNER as Hashtable)["BLK_CD"].ToString();
                    _STRBLK_NM = (control.OWNER as Hashtable)["BLK_NM"].ToString();

                    control.InfoSave(_objOWNER, _STRBLK_CD, _STRBLK_NM);
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

        #region BorderColor
        public string BorderColor
        {
            get { return (string)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static DependencyProperty BorderColorProperty =
            DependencyProperty.Register(
                "BorderColor", typeof(string), typeof(RevenueView),
                new FrameworkPropertyMetadata(new PropertyChangedCallback(BorderColorPropertyChanged)));

        public static void BorderColorPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var control = (RevenueView)obj;

                control.BorderTH.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString(control.BorderColor.ToString()));
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion
        public RevenueView()
        {
            InitializeComponent();
            btnChart.Click += BtnChart_Click;
        }

        private void BtnChart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                object[] objParam = { STRBLK_CD, STRBLK_NM };
                objOWNER.GetType().GetMethod("PopupChart").Invoke(objOWNER, objParam);
            }
            catch (Exception ex)
            {
            }
        }

        private void InfoSave(object OWNER, string BLK_CD, string BLK_NM)
        {
            objOWNER = OWNER;
            STRBLK_CD = BLK_CD;
            STRBLK_NM = BLK_NM;
        }
    }
}