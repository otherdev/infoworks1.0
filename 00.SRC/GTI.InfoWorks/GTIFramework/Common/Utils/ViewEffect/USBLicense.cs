﻿//using GTIFramework.Common.MessageBox;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace GTIFramework.Common.Utils.ViewEffect
{
    public class USBLicense
    {
        protected Thread thread;  //쓰레드 선언
        protected System.Timers.Timer timer; //자동쓰레드 타이머
        Application App;

        DataTable dtRockeyInfo = new DataTable();

        //USB Rockey 관련
        const ushort RY_FIND = 1;
        const ushort RY_FIND_NEXT = 2;
        const ushort RY_OPEN = 3;
        const ushort RY_CLOSE = 4;
        const ushort RY_READ = 5;
        const ushort RY_WRITE = 6;
        const ushort RY_RANDOM = 7;
        const ushort RY_SEED = 8;
        const ushort RY_WRITE_USERID = 9;
        const ushort RY_READ_USERID = 10;
        const ushort RY_SET_MOUDLE = 11;
        const ushort RY_CHECK_MOUDLE = 12;
        const ushort RY_WRITE_ARITHMETIC = 13;
        const ushort RY_CALCULATE1 = 14;
        const ushort RY_CALCULATE2 = 15;
        const ushort RY_CALCULATE3 = 16;
        const ushort RY_DECREASE = 17;

        Rockey4NDClass.Rockey4ND r4nd = new Rockey4NDClass.Rockey4ND();
        ushort[] m_Handle = new ushort[32];
        int m_HandleNum = 0;            //ROCKEY 설치 개수(멀티시 코드 수정 필요)


        // 키로 사용하기 위한 암호 정의(최소 16자리이상)
        private static readonly string PASSWORD = "GreenTechINcSolutionTeam";

        // 인증키 정의
        private static readonly string KEY = PASSWORD.Substring(0, 16);

        //테스트용 사이트코드 및 블록개수
        string strSiteCD = string.Empty;
        int iBlockCnt = 0;

        
        public USBLicense(object _app, DataTable _dtRockeyInfo)
        {
            if (_app is Application)
            {
                App = _app as Application;
                dtRockeyInfo = _dtRockeyInfo;

                Timer_Elapsed(null, null);

                thread = new Thread(new ThreadStart(ThreadFX)) { IsBackground = true };
                thread.Start();
            }
        }


        private void ThreadFX()
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000 * 10;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                ushort rtnFind = ROCKEY_FIND();

                if (rtnFind != 0)
                {
                    App.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action(delegate ()
                        {
                            Messages.ShowInfoMsgBox("ROCKEY를 인식하지 못했습니다.");
                            Environment.Exit(0);
                            System.Diagnostics.Process.GetCurrentProcess().Kill();
                        }));
                }
                else
                {
                    string strResult = ROCKEY_READ();
                    string[] strVal = strResult.Split('|');

                    //dtRockeyInfo = work.Select_Rockey_Certification(null);
                    if (!strVal[0].Equals(dtRockeyInfo.Rows[0]["SITE_CD"]))
                    {
                        App.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                            new Action(delegate ()
                            {
                                Messages.ShowInfoMsgBox("잘못된 사이트 입니다.");
                                Environment.Exit(0);
                                System.Diagnostics.Process.GetCurrentProcess().Kill();
                            }));
                        
                    }
                    else if (Convert.ToInt32(strVal[1]) > Convert.ToInt32(dtRockeyInfo.Rows[0]["BLK_CNT"].ToString()))
                    {
                        App.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                            new Action(delegate ()
                            {
                                Messages.ShowInfoMsgBox("블록 갯수를 초과했습니다.");
                                Environment.Exit(0);
                                System.Diagnostics.Process.GetCurrentProcess().Kill();
                            }));
                    }
                }
            }
            catch (Exception ex)
            {
            }
            
        }

        public ushort ROCKEY_FIND()
        {
            ushort ret;
            ushort p1 = 0, p2 = 0, p3 = 0, p4 = 0;
            uint lp1 = 0, lp2 = 0;
            byte[] buffer = new byte[1000];

            p1 = 0x16F8;
            p2 = 0x15EA;
            p3 = 0xD9EF;
            p4 = 0xE616;

            ret = r4nd.Rockey(RY_FIND, ref m_Handle[0], ref lp1, ref lp2, ref p1, ref p2, ref p3, ref p4, buffer);

            if (ret != 0)
            {
                return 9999;
            }
            else
            {
                ret = r4nd.Rockey(RY_OPEN, ref m_Handle[0], ref lp1, ref lp2, ref p1, ref p2, ref p3, ref p4, buffer);
                if (ret != 0)
                {
                    return 9999;
                }
                m_HandleNum = 1;
            }
            return ret;

        }

        public string ROCKEY_READ()
        {
            int i, k;
            ushort ret;
            ushort p1 = 0, p2 = 0, p3 = 0, p4 = 0;
            uint lp1 = 0, lp2 = 0;
            byte[] buffer = new byte[1000];

            String[] strReadVal = new String[m_HandleNum];

            string strReturnVal = string.Empty;


            for (i = 0; i < m_HandleNum; i++)
            {
                p1 = 0; //offset  - USER DATA ZONE1(1부터 시작)
                //p1 = 500; //offset  - USER DATA ZONE2(500부터 시작)
                p2 = 100; //length
                ret = r4nd.Rockey(RY_READ, ref m_Handle[0], ref lp1, ref lp2, ref p1, ref p2, ref p3, ref p4, buffer);

                if (ret != 0)
                {
                    App.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action(delegate ()
                        {
                            //Messages.NotificationBox("USB ROCKEY 경보", "ROCKEY를 읽을수 없습니다. 업체에 문의하세요.", "ROCKEY를 읽을수 없습니다. 업체에 문의하세요.");
                        }));
                    //Messages.ShowInfoMsgBox("ROCKEY를 읽을수 없습니다. 업체에 문의하세요.");
                    //this.Close();
                }
                else
                {
                    for (k = 0; k < p2; k++)
                    {
                        //if (buffer[k] == 0)
                        //{
                        //    break;
                        //}
                        strReadVal[i] += Convert.ToChar(buffer[k]);
                    }
                    strReturnVal = AESDecrypt128(strReadVal[i].Replace("\0", ""));
                }
                //lblReadVal_Copy.Content = strReadVal[i];
            }

            //lblReadVal.Content = strReturnVal;

            //txtValue.Text = strReturnVal;

            return strReturnVal;
        }

        public static string AESDecrypt128(string encrypt)
        {
            byte[] encryptBytes = Convert.FromBase64String(encrypt);

            RijndaelManaged myRijndael = new RijndaelManaged();
            myRijndael.Mode = CipherMode.CBC;
            myRijndael.Padding = PaddingMode.PKCS7;
            myRijndael.KeySize = 128;

            MemoryStream memoryStream = new MemoryStream(encryptBytes);

            ICryptoTransform decryptor = myRijndael.CreateDecryptor(Encoding.UTF8.GetBytes(KEY), Encoding.UTF8.GetBytes(KEY));

            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

            byte[] plainBytes = new byte[encryptBytes.Length];

            int plainCount = cryptoStream.Read(plainBytes, 0, plainBytes.Length);

            string plainString = Encoding.UTF8.GetString(plainBytes, 0, plainCount);

            cryptoStream.Close();
            memoryStream.Close();

            return plainString;
        }
    }
}
