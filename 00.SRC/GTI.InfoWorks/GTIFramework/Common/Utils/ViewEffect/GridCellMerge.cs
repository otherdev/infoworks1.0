﻿using DevExpress.Xpf.Grid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace GTIFramework.Common.Utils.ViewEffect
{
    public class GridCellMerge : DataTemplateSelector
    {
        //고정된 행 번호 List
        List<int> listFixRowNum = new List<int>();

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            try
            {
                CellEditor cellEditor = container as CellEditor;
                EditGridCellData cellData = item as EditGridCellData;
                TableView view = cellData.View as TableView;

                DataTable dtData = new DataTable();

                //Cell Merge 하고자 하는 컬럼 Tag에 합칠 대상이 되는 cell 값과 왼쪽으로 merge 하려는 셀의 개수를 parameter로 받아옴
                //ex) Column Tag 값 => 최대값_1,최소값_1,평균값_1
                //    cell 값이 '최대값'인 cell의 왼쪽 1개 cell과 merge함 (최소값,평균값도 마찬가지)
                if (cellData.Column.Tag != null)
                {
                    string[] strArray = cellData.Column.Tag.ToString().Split(',');

                    foreach (string str in strArray)
                    {
                        string[] strAry = str.Split('_');

                        if (strAry.Length > 0)
                        {
                            if (cellData.Value.ToString().Equals(strAry[0]))
                            {
                                //밴드 없을때
                                if (((GridColumn)cellEditor.Column).Parent is GridControl)
                                {
                                    dtData = ((GridControl)((GridColumn)cellEditor.Column).Parent).ItemsSource as DataTable;

                                }//밴드 존재할때
                                else if (((GridColumn)cellEditor.Column).Parent is GridControlBand)
                                {
                                    if (((GridControlBand)((GridColumn)cellEditor.Column).Parent).Parent != null)
                                    {
                                        dtData = ((GridControl)(((GridControlBand)((GridColumn)cellEditor.Column).Parent).Parent)).ItemsSource as DataTable;
                                    }
                                }

                                //Merge
                                //margin과 width로 merge처럼 보이게
                                //2021-05-11 GridControrl에 FormatCondition이 추가되어 있어야 아래 포맷이 적용되는듯
                                bool bCali = false;
                                double dMargin_Cali = 0d;
                                for (int i = 1; i <= Convert.ToInt32(strAry[1]); i++)
                                {
                                    if (view.VisibleColumns.Count > 0)
                                    //if (view.GroupedColumns.Count > 0)
                                    {
                                        GridColumn gc = view.VisibleColumns[cellData.Column.VisibleIndex - i];
                                        cellEditor.Width += gc.ActualDataWidth;
                                        dMargin_Cali += gc.ActualDataWidth;
                                        bCali = true;
                                    }
                                }
                                if (bCali)
                                {
                                    cellEditor.Margin = new Thickness(-dMargin_Cali, 0, 0, 0);
                                }
                            }
                        }
                    }
                }

                #region 고정 셀 색상 변경
                if (view.Grid != null)
                {
                    if (!((DataRowView)cellEditor.RowData.Row).Row["RN"].ToString().Equals(""))
                    {
                        (cellEditor as LightweightCellEditor).Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Application.Current.FindResource("GridControlTotal1").ToString()));
                    }
                }
                #endregion

                return base.SelectTemplate(item, container);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
