﻿using DevExpress.Xpf.Grid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace GTIFramework.Common.Utils.ViewEffect
{
    public class GridCellColor : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            try
            {
                CellEditor cellEditor = container as CellEditor;
                EditGridCellData cellData = item as EditGridCellData;
                //TableView view = cellData.View as TableView;

                DataTable dtData = new DataTable();

                dtData = ((GridControl)((GridColumn)cellEditor.Column).Parent).ItemsSource as DataTable;

                //구분 셀 색상 변경
                if (((GridColumn)cellData.Column).Header.ToString().Equals("구분"))
                {
                    if (((DataRowView)cellData.Row).Row["LIMIT_CHK"].ToString().Equals("Y"))
                    {
                        (cellEditor as LightweightCellEditor).Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#88e66c8c"));
                    }
                    else
                    {
                        if (Application.Current.TryFindResource("GridControlDefault") != null)
                        {
                            (cellEditor as LightweightCellEditor).Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Application.Current.FindResource("GridControlDefault").ToString()));
                        }
                    }
                }

                //블록 셀 색상 변경
                if (((GridColumn)cellData.Column).Header.ToString().Equals("블록"))
                {
                    string strLIMIT_COLOR = ((DataRowView)cellData.Row).Row["LIMIT_COLOR"].ToString();
                    if (strLIMIT_COLOR.Equals(""))
                    {
                        if (Application.Current.TryFindResource("GridControlDefault") != null)
                        {
                            (cellEditor as LightweightCellEditor).Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString(Application.Current.FindResource("GridControlDefault").ToString()));
                        }
                    }
                    else
                    {
                        (cellEditor as LightweightCellEditor).Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#" + strLIMIT_COLOR));
                    }
                }

                return base.SelectTemplate(item, container);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
