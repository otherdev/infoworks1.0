﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace GTIFramework.Common.Utils.Handle
{
    public class iniHandle
    {
        private string iniPath;

        [DllImport("kernel32.dll")]
        private static extern int GetPrivateProfileString(
            String section,
            String key,
            String def,
            StringBuilder retVal,
            int size,
            String filePath);
        [DllImport("kernel32.dll")]
        private static extern long WritePrivateProfileString(
            String section,
            String key,
            String val,
            String filePath);

        public iniHandle(string path)
        {
            iniPath = path;
        }

        public String GetIniValue(String Section, String Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, temp.Capacity, iniPath);
            return temp.ToString();
        }

        public void SetIniValue(String Section, String Key, String Value)
        {
            WritePrivateProfileString(Section, Key, Value, iniPath);
        }
    }
}
