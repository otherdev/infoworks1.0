﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTIFramework.Common.Utils.Handle
{
    public static class BuildHandle
    {
        /// <summary>
        /// 현재버전 user.config 체크 한후 없을경우 과거 user.config 복사
        /// </summary>
        public static void userconfigCHK()
        {
            try
            {
                //현재 config
                Configuration curconfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);


                string curconfigpath = curconfig.FilePath;

                if (!Directory.GetParent(curconfigpath).Parent.Exists)
                    return;

                if (!curconfig.HasFile)
                {
                    //과거 config 유무 확인
                    if (Directory.GetParent(curconfigpath).Parent.GetDirectories().LastOrDefault() != null)
                    {
                        string oldconfigpath = Path.Combine(Directory.GetParent(curconfigpath).Parent.GetDirectories().LastOrDefault().FullName, "user.config");

                        //현재 버전의 경로가 없을경우 Create
                        if (!Directory.GetParent(curconfigpath).Exists)
                            Directory.GetParent(curconfigpath).Create();

                        if (File.Exists(oldconfigpath))
                            //user.config 복사
                            File.Copy(oldconfigpath, curconfigpath);
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
    }
}
