﻿using GTIFramework.Analysis.Epanet.Items;
using GTIFramework.Analysis.Epanet.Sections;
using GTIFramework.Common.Log;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GTIFramework.Analysis.Epanet
{
    [System.Serializable]
    public class ModelAnalysis
    {
        private string m_run_dir = AppDomain.CurrentDomain.BaseDirectory + "Resources\\temp";
        private string m_inp_file;
        private string m_rpt_file;
        private string m_net_file;

        private ModelNetwork Network;

        public int Err
        {
            get; private set;
        }

      
        public ModelAnalysis(ModelNetwork network)
        {
            Network = network;
            m_simulation = false;
        }

        #region 해석 프로퍼티
        //analysis time
        private long m_reportstep = 0;
        private long m_reportstart = 0;

        private int m_nodes = 0;
        private int m_links = 0;
        private int m_tanks = 0;
        private int m_pumps = 0;
        private long m_periods = 0;

        private IList<long> m_report_times = new List<long>();
        public IList<long> ReportTimes
        {
            get
            {
                return m_report_times;
            }
        }

        private DateTime m_clockstart;
        public DateTime ClockStart
        {
            get
            {
                return m_clockstart;
            }
        }

        private bool m_simulation = false;
        public bool IsSimulation 
        {
            get
            {
                return m_simulation; 
            }
        }
        #endregion 

        public void Run(bool isMSX = false)
        {
            if (Network == null) return;

            //파일생성
            string f = RandomString(40);
            m_inp_file = System.IO.Path.Combine(m_run_dir, string.Format("{0}.inp", f));
            m_rpt_file = System.IO.Path.Combine(m_run_dir, string.Format("{0}.rpt", f));
            m_net_file = System.IO.Path.Combine(m_run_dir, string.Format("{0}.out", f));

            DirectoryInfo di = new DirectoryInfo(m_run_dir);

            if (di.Exists == false) di.Create();

            var flag = Network.WriteFIle(m_inp_file);
            if (!flag) 
            {
                throw new Exception("Error : Write INP");
            }

            if (isMSX)  //MSX
            {

            }
            else        //Epanet
            {
                try
                {
                    Err = Epanet22md.ENopen(m_inp_file, m_rpt_file, m_net_file);
                    switch (Err)
                    {
                        case 200:
                            throw new Exception("One or more errors in input file !");
                        case 301:
                            throw new Exception("Cannot open input file !");
                        
                    }

                    RunAnalysis();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                finally
                {
                    Epanet22md.ENclose();
                }
                m_simulation = true;
            }

        }

        private void RunAnalysis()
        {
            if (Err == 0)
            {
                InitializeAnalysis();
            }

            if (Err == 0)
            {
                Accumulate();
            }

            SetAnalysisResult();
        }

        private void InitializeAnalysis()
        {
            Epanet22md.ENgettimeparam(Epanet22md.EN_REPORTSTEP, ref m_reportstep);
            Epanet22md.ENgettimeparam(Epanet22md.EN_REPORTSTART, ref m_reportstart);
            Epanet22md.ENgetcount(Epanet22md.EN_NODECOUNT, ref m_nodes);
            Epanet22md.ENgetcount(Epanet22md.EN_LINKCOUNT, ref m_links);
            Epanet22md.ENgetcount(Epanet22md.EN_TANKCOUNT, ref m_tanks);

            m_clockstart = Convert.ToDateTime(((TimeSection)Network.GetSection("TIMES")).StartClockTime);
            m_report_times.Clear();
        }

        private void Accumulate()
        {
            long time = 0;
            long timestep = 0;

            try
            {
                Err = Epanet22md.ENsolveH();
                if (Err > 100)
                {
                    new Exception("Hydraulic Error");
                }
                Err = Epanet22md.ENopenQ();
                if (Err > 100)
                {
                    new Exception("Quality Error");
                }
                Err = Epanet22md.ENinitQ(Epanet22md.EN_SAVE);
                if (Err > 100)
                {
                    new Exception("Quality Error");
                }
                do
                {
                    Err = Epanet22md.ENrunQ(ref time);
                    if (Err > 100)
                    {
                        new Exception("Quality Error");
                    }

                    if (time % m_reportstep == 0)
                    {
                        //리포트 시간 배열 저장
                        m_report_times.Add(time);
                    }

                    if (Err <= 100)
                    {
                        Err = Epanet22md.ENnextQ(ref timestep);
                        if (Err > 100)
                        {
                            new Exception("Quality Error");
                        }
                    }
                }
                while (timestep > 0);

                Err = Epanet22md.ENcloseQ();
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                if (Err <= 100)
                    Err = 0;
            }
        }

        

        #region Dynamic result byte size
        //dynamic result byte size
        private readonly int RECORDSIZE = 4;
        private readonly int IDSIZE = 32;
        private readonly int NODEBARS = 4;
        private readonly int LINKBARS = 8;

        private int m_offset1 = -1;
        private int m_offset2 = -1;

        #endregion Dynamic result byte size


        private void SetOffset()
        {
            m_offset1 = 15 * RECORDSIZE
                        + 3 * 80
                        + 2 * 260
                        + 2 * IDSIZE
                        + m_nodes * IDSIZE
                        + m_links * IDSIZE
                        + 3 * m_links * RECORDSIZE
                        + 2 * m_tanks * RECORDSIZE
                        + m_nodes * RECORDSIZE
                        + 2 * m_links * RECORDSIZE;

            m_offset2 = (7 * m_pumps + 1) * RECORDSIZE;
        }

        private List<TNodeValue> nodeValues = new List<TNodeValue>();
        private List<TLinkValue> linkValues = new List<TLinkValue>();

        void SetAnalysisResult()
        {
            if (!File.Exists(m_net_file)) return;

            nodeValues.Clear();
            linkValues.Clear();

            var alldata = System.IO.File.ReadAllBytes(m_net_file);
            using (var stream = new MemoryStream(alldata))
            {
                using (var reader = new BinaryReader(stream))
                {
                    reader.BaseStream.Seek(reader.BaseStream.Length - (3 * RECORDSIZE), SeekOrigin.Begin);
                    this.m_periods = reader.ReadInt32();
                    reader.BaseStream.Seek(5 * RECORDSIZE, SeekOrigin.Begin);
                    this.m_pumps = reader.ReadInt32();

                    SetOffset();

                    SetResultNode(reader);
                    SetResultLink(reader);
                }
            }  
        }

        #region 해석결과 파서
        private void SetResultNode(BinaryReader reader)
        {
            StringBuilder id = new StringBuilder();
            int iType = 0;

            for (int period = 0; period < this.m_periods; period++)
            {
                var v1 = GetNodeValues(reader, 1, period);
                var v2 = GetNodeValues(reader, 2, period);
                var v3 = GetNodeValues(reader, 3, period);
                var v4 = GetNodeValues(reader, 4, period);


                for (int n = 1; n <= m_nodes; n++)
                {
                    Epanet22md.ENgetnodeid2(n, id);
                    Epanet22md.ENgetnodetype(n, ref iType);

                    var value = new TNodeValue()
                    {
                        ID = id.ToString(),
                        itype = iType,
                        period = period,
                        demand = v1[n-1],
                        head = v2[n - 1],
                        pressure = v3[n-1],
                        quality = v4[n-1],
                    };

                    nodeValues.Add(value);
                }
            }

        }

        private void SetResultLink(BinaryReader reader)
        {
            StringBuilder id = new StringBuilder();
            int iType = 0;

            for (int period = 0; period < this.m_periods; period++)
            {
                var v1 = GetLinkValues(reader, 1, period);
                var v2 = GetLinkValues(reader, 2, period);
                var v3 = GetLinkValues(reader, 3, period);
                var v4 = GetLinkValues(reader, 4, period);
                var v5 = GetLinkValues(reader, 5, period);
                var v6 = GetLinkValues(reader, 6, period);
                var v7 = GetLinkValues(reader, 7, period);
                var v8 = GetLinkValues(reader, 8, period);


                for (int n = 1; n <= m_links; n++)
                {
                    Epanet22md.ENgetlinkid2(n, id);
                    Epanet22md.ENgetlinktype(n, ref iType);

                    var value = new TLinkValue()
                    {
                        ID = id.ToString(),
                        itype = iType,
                        period = period,
                        flow = v1[n-1],
                        velocity = v2[n - 1],
                        headloss = v3[n - 1],
                        quality = v4[n - 1],
                        status = v5[n - 1],
                        setting = v6[n - 1],
                        reactionRate = v7[n - 1],
                        frictionFactor = v8[n - 1],
                    };

                    linkValues.Add(value);
                }
            }
        }

        private List<float> GetNodeValues(BinaryReader reader, int varindex, int timePeriod)
        {
            int p1 = RECORDSIZE * (m_nodes * this.NODEBARS + m_links * this.LINKBARS);
            int p2 = RECORDSIZE * m_nodes * (varindex - 1);
            int p3 = m_offset1 + m_offset2 + (timePeriod * p1) + p2;

            reader.BaseStream.Seek(p3, SeekOrigin.Begin);
            byte[] b = reader.ReadBytes(m_nodes * sizeof(float));
            float[] d = new float[m_nodes];
            Buffer.BlockCopy(b, 0, d, 0, d.Length * sizeof(float));
            return d.ToList();
        }

        private List<float> GetLinkValues(BinaryReader reader, int varindex, int timePeriod)
        {
            int p1 = RECORDSIZE * (m_nodes * this.NODEBARS + m_links * this.LINKBARS);
            int p2 = RECORDSIZE * (m_nodes * this.NODEBARS + m_links * (varindex - 1));
            int p3 = m_offset1 + m_offset2 + (timePeriod * p1) + p2;

            reader.BaseStream.Seek(p3, SeekOrigin.Begin);
            byte[] b = reader.ReadBytes(m_links * sizeof(float));
            float[] d = new float[m_links];
            Buffer.BlockCopy(b, 0, d, 0, d.Length * sizeof(float));

            return d.ToList();
        }

        #endregion 해석결과

        public TNodeValue GetNodeValue(int period, string id)
        {
            var max = nodeValues.Max(x => x.period);
            if (max < period) period = max;
            return nodeValues.Where(x => x.period == period).Where(x => x.ID == id).Select(x => x).FirstOrDefault();
        }
        public TLinkValue GetLinkValue(int period, string id)
        {
            var max = nodeValues.Max(x => x.period);
            if (max < period) period = max;
            return linkValues.Where(x => x.period == period).Where(x => x.ID == id).Select(x => x).FirstOrDefault();
        }
        public List<TNodeValue> GetNodeValues(int period)
        {
            var max = nodeValues.Max(x => x.period);
            if (max < period) period = max;
            return nodeValues.Where(x => x.period == period).Select(x => x).ToList();
        }
        public List<TLinkValue> GetLinkValues(int period)
        {
            var max = nodeValues.Max(x => x.period);
            if (max < period) period = max;
            return linkValues.Where(x => x.period == period).Select(x => x).ToList();
        }
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString().ToLower();
        }

        /// <summary>
        /// 리포트 설정에 따른 시간 계산
        /// </summary>
        /// <returns></returns>
        public DataTable GetTimeList()
        {
            EpanetVar.tINPConvert.dtTimeList = null;
            EpanetVar.tINPConvert.dtTimeList = new DataTable();
            EpanetVar.tINPConvert.dtTimeList = EpanetVar.tINPConvert.dtDataComboList.Clone();

            string strtime = string.Empty;
            foreach (long longsec in ReportTimes)
            {
                strtime = string.Empty;
                strtime = String.Format("{0}:{1}", TimeSpan.FromSeconds(longsec).TotalHours, TimeSpan.FromSeconds(longsec).Minutes.ToString().PadLeft(2, '0'));
                EpanetVar.tINPConvert.dtTimeList.Rows.Add(ReportTimes.IndexOf(longsec).ToString(), strtime);
            }

            return EpanetVar.tINPConvert.dtTimeList;
        }
    }

    [System.Serializable]
    public class TNodeValue
    {
        public string ID { get; set; }
        public int itype { get; set; }
        public int period { get; set; }
        public double demand { get; set; }
        public double head { get; set; }
        public double pressure { get; set; }
        public double quality { get; set; }
    }

    [System.Serializable]
    public class TLinkValue
    {
        public string ID { get; set; }
        public int itype { get; set; }
        public int period { get; set; }
        public double flow { get; set; }
        public double velocity { get; set; }
        public double headloss { get; set; }
        public double quality { get; set; }
        public double status { get; set; }
        public double setting { get; set; }
        public double reactionRate { get; set; }
        public double frictionFactor { get; set; }
    }
}
