﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTIFramework.Analysis.Epanet.Structures
{
    public static class NetworkExtension
    {
        public static IEnumerable<Link> Adgen(this ModelNetwork network, Node node, string[] visited)
        {
            return EpanetVar.tINPConvert.network.Links
               .Where(x => (new string[] { x.StartNode, x.EndNode }).Contains(node.ID))
               .Where(x => !visited.Contains(x.ID))
               .Select(x => x);
        }
    }
}
