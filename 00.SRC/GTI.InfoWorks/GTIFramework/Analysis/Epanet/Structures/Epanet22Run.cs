﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Windows;

namespace GTIFramework.Analysis.Epanet
{
    public class Epanet22Run
    {
        static string DirInp = string.Empty;
        static string DirMSX = string.Empty;
        static string DirRpt = string.Empty;
        static string DirOut = string.Empty;

        static Hashtable htErrls = new Hashtable();

        public Epanet22Run()
        {
            #region Err리스트
            htErrls.Add("err101", "An analysis was terminated due to insufficient memory available.");
            htErrls.Add("err110", "An analysis was terminated because the network hydraulic equations could not be solved. Check for portions of the network not having any physical links back to a tank or reservoir or for unreasonable values for network input data.");
            htErrls.Add("err200", "O ne or more errors were detected in the input data (see 200-series error messages below).");
            htErrls.Add("err201", "A syntax error was found in a line of the input file created from your network data.");
            htErrls.Add("err202", "An illegal numeric value was assigned to a property.");
            htErrls.Add("err203", "An object referred to undefined node.");
            htErrls.Add("err204", "An object referred to an undefined link.");
            htErrls.Add("err205", "An object referred to an undefined time pattern.");
            htErrls.Add("err206", "An object referred to an undefined curve.");
            htErrls.Add("err207", "An attempt was made to control a check valve. Once a pipe is assigned a Check Valve, its status cannot be changed by either simple or rule-based controls.");
            htErrls.Add("err208", "Illegal pressure limits were assigned for a Pressure Driven demand analysis. The required limit to deliver full demand must be at least 0.1 psi (or m) higher than the minimum limit below which no demand is delivered.");
            htErrls.Add("err209", "An illegal value was assigned to a node property.");
            htErrls.Add("err211", "An illegal value was assigned to a link property.");
            htErrls.Add("err212", "A source tracing analysis referred to an undefined trace node.");
            htErrls.Add("err213", "An analysis option had an illegal value (for example, a negative time step value).");
            htErrls.Add("err214", "Too many characters were in a line read from an input file. Lines in an input file are limited to 1024 characters.");
            htErrls.Add("err215", "Two or more objects of the same type share the same ID label.");
            htErrls.Add("err216", "Energy data were supplied for an undefined pump.");
            htErrls.Add("err217", "Invalid energy data were supplied for a pump.");
            htErrls.Add("err219", "A valve was illegally connected to a tank. A PRV, PSV, or FCV cannot be directly connected to a reservoir or tank. Use a length of pipe to separate the two.");
            htErrls.Add("err220", "A valve was illegally connected to another valve. PRVs cannot share the same downstream node or be linked in series. PSVs cannot share the same upstream node or be linked in series, and a PSV cannot be directly connected to the downstream node of a PRV.");
            htErrls.Add("err221", "A rule-based control contains a misplaced clause.");
            htErrls.Add("err222", "The same start and end node was assigned to a link.");
            htErrls.Add("err223", "There were not enough nodes in the network to analyze. A valid network must contain at least one tank/reservoir and one junction node.");
            htErrls.Add("err224", "There was not at least one tank or reservoir in the network.");
            htErrls.Add("err225", "Invalid lower/upper levels were specified for a tank (e.g., the lower level was higher than the upper level).");
            htErrls.Add("err226", "No pump curve or power rating was supplied for a pump. A pump must either be assigned a curve ID in its Pump Curve property or a power rating in its Power property. If both properties are assigned then the Pump Curve is used.");
            htErrls.Add("err227", "A pump had an invalid pump curve. A valid pump curve must have decreasing head with increasing flow.");
            htErrls.Add("err230", "A curve had non-increasing X-values.");
            htErrls.Add("err233", "A node was not connected to any links.");
            htErrls.Add("err303", "The status report file could not be opened.");
            htErrls.Add("err304", "The binary output file could not be opened.");
            htErrls.Add("err308", "Intermediate results could not be saved to a binary file. This can occur if the disk becomes full.");
            htErrls.Add("err309", "Results could not be written to the report file. This can occur if the disk becomes full."); 
            #endregion
        }

        /// <summary>
        /// EpanetRealTimeRun
        /// </summary>
        /// <returns>분석 완료시 0, 실패시 ErrCode</returns>
        public static int EpanetRealTimeRun(string strINPpath)
        {
            int Err = 0, FlowUnit, nLinks = 0, nNodes = 0;

            long num = (long)0;
            long num1 = (long)0;

            try
            {
                //inp파일 같은 경로에 rpt, out 파일 생성 경로
                DirInp = strINPpath;
                DirRpt = DirInp.Substring(0, DirInp.Length - 4) + "_realtime_22.rpt";
                DirOut = DirInp.Substring(0, DirInp.Length - 4) + "_realtime_22.out";

                //Use of epanet functions

                //Open the toolkit
                Err = Epanet22md.ENopen(DirInp, DirRpt, DirOut);
                if (!ErrMSG(Err))
                    return Err;

                #region // 수리해석 실행(수질해석을 돌리기 위하여 먼저 수리해석을 돌려야 함) 실시간, 시간 구분
                Err = Epanet22md.ENopenH();
                if (!ErrMSG(Err))
                    return Err;

                Err = Epanet22md.ENinitH(1);
                if (!ErrMSG(Err))
                    return Err;

                Err = Epanet22md.ENrunH(ref num);
                if (!ErrMSG(Err))
                    return Err;

                Err = Epanet22md.ENnextH(ref num1);
                if (!ErrMSG(Err))
                    return Err;
                #endregion

                #region // 수질해석 실행
                Err = Epanet22md.ENopenQ();
                if (!ErrMSG(Err))
                    return Err;

                Err = Epanet22md.ENinitQ(1);
                if (!ErrMSG(Err))
                    return Err;

                Err = Epanet22md.ENrunQ(ref num);
                if (!ErrMSG(Err))
                    return Err;

                Err = Epanet22md.ENnextQ(ref num1);
                if (!ErrMSG(Err))
                    return Err;
                #endregion

                return Err;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return Err;
            }
            finally
            {
                //Close the toolkit]    
                Err = Epanet22md.ENcloseH();
                Err = Epanet22md.ENcloseQ();
                Err = Epanet22md.ENclose();
            }
        }

        /// <summary>
        /// Epanet Version
        /// </summary>
        public int EpanetVersion()
        {
            int iversion = 0;
            Epanet22md.ENgetversion(ref iversion);
            return iversion;
        }

        private static bool ErrMSG(int err)
        {
            if (err > 100)
            {
                MessageBox.Show(htErrls["err" + err.ToString()].ToString(), "확인", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }       
            else
                return true;
        }
    }
}
