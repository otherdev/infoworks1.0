﻿using System;
using System.ComponentModel;

namespace GTIFramework.Analysis.Epanet
{
    [Serializable]
    public class SectionItemBase 
    {
        private SectionBase m_section;
        private string m_id;
        private string m_tag = "";

        [Browsable(false)]
        public SectionBase Section
        {
            get
            {
                return m_section;
            }
            set
            {
                m_section = value;
            }
        }

        public SectionItemBase(SectionBase section)
        {
            m_section = section;
        }

        public virtual string ID
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public virtual string Tag
        {
            get { return m_tag; }
            set { m_tag = value; }
        }

    }
}
