﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet
{
    [Serializable]
    public class SectionBase
    {
        /// <summary>
        /// 섹션명
        /// </summary>
        protected string m_name = null;

        /// <summary>
        /// 섹션내 아이템 목록
        /// 예) 파이프섹션일경우 파이프 객체 목록
        /// </summary>
        protected List<object> m_list = null;

        private ModelNetwork m_network;

        public SectionBase(ModelNetwork network)
        {
            m_name = "NONE";
            m_list = new List<object>();
            m_network = network;
        }

        public string Name
        {
            get
            {
                return m_name;
            }
        }

        public List<object> SectionItems
        {
            get
            {
                return m_list;
            }
        }

        public ModelNetwork Network
        {
            get
            {
                return m_network;
            }
        }

        public void SetSection(string[] sectionLines)
        {
            List<string> sectionFilterLines = new List<string>();

            int comment = 0;

            foreach (string sectionLine in sectionLines)
            {
                if (0 < sectionLine.Length)
                {
                    if (sectionLine[0] != '[' && sectionLine[0] != ';')
                        sectionFilterLines.Add(sectionLine);
                    else if (sectionLine[0] == ';')
                    {
                        comment++;

                        if (comment >= 2)
                            sectionFilterLines.Add(sectionLine);
                    }
                }
            }

            Add(sectionFilterLines);

            #region 기존주석
            //if (sectionLines[0].Contains("JUNCTIONS"))
            //{
            //    Add(sectionLines.Where(x => !x[0].Equals('[') && !x[0].Equals(';')));
            //}
            //else
            //{
            //    int comment = 0;

            //    foreach (string sectionLine in sectionLines)
            //    {
            //        if (0 < sectionLine.Length)
            //        {
            //            if (sectionLine[0] == ';')
            //            {
            //                comment++;
            //            }

            //            if (sectionLine[0] != '[' && sectionLine[0] != ';')
            //            {
            //                Add(sectionLine);
            //            }
            //            else if (sectionLine[0] == ';')
            //            {
            //                if (comment >= 2)
            //                {
            //                    Add(sectionLine);
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion
        }

        public virtual void Add(string sectionLine)
        {
            //m_list.Add(sectionLine);
        }

        public virtual void Add(IEnumerable<string> strList)
        {

        }

        public virtual void Add(object sectionItem)
        {
            //m_list.Add(sectionItem);
        }

        public virtual StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            sectionBuilder.Remove(0, sectionBuilder.Length);
            sectionBuilder.Append("[").Append(m_name).Append("]");
            sectionBuilder.AppendLine();

            return sectionBuilder;
        }

        public virtual void Clear()
        {
            m_list.Clear();
        }

        protected IEnumerable<string>[] GetTokens(IEnumerable<string> stringToSplit)
        {
            return stringToSplit.Select(x => x.Split(' ', '\t').Where(y => !string.IsNullOrWhiteSpace(y) & y != ";" & y.Length != 0).Select(z => z.Trim())).ToArray();
        }

        protected List<string> GetTokens(string stringToSplit)
        {
            return stringToSplit.Split(' ', '\t').Where(x => !string.IsNullOrWhiteSpace(x) & x != ";").Select(x => x.Trim()).ToList();
        }

        /// <summary>
        /// 사용안함
        /// </summary>
        /// <param name="stringToSplit"></param>
        /// <returns></returns>
        protected List<string> GetTokens2(string stringToSplit)
        {
            List<string> results = new List<string>();

            bool inQuote = false;
            var tok = new StringBuilder();

            foreach (char c in stringToSplit)
            {
                if (c == ';')
                    break;

                if (c == '"')
                {
                    inQuote = !inQuote;
                }
                else if (!inQuote && char.IsWhiteSpace(c))
                {
                    if (tok.Length > 0)
                    {
                        string result = tok.ToString().Trim();
                        if (!string.IsNullOrEmpty(result))
                            results.Add(result);
                    }

                    tok.Length = 0;
                }
                else
                {
                    tok.Append(c);
                }
            }

            if (tok.Length > 0)
            {
                string lastResult = tok.ToString().Trim();
                if (!string.IsNullOrEmpty(lastResult))
                    results.Add(lastResult);
            }

            return results;
        }

        protected double convToDouble(object val)
        {
            try
            {
                double temp;
                if (double.TryParse(val.ToString(), out temp))
                {
                    return temp;
                }
                return double.NaN;
            }
            catch (Exception)
            {
                return double.NaN;
            }
        }

    }
}
