﻿using GTIFramework.Analysis.Epanet.Items;
using GTIFramework.Analysis.Epanet.Sections;
using GTIFramework.Common.MessageBox;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace GTIFramework.Analysis.Epanet
{
    [Serializable]
    public class ModelNetwork
    {
        protected IList<SectionBase> m_sections = null;

        public virtual IList<SectionBase> Sections
        {
            get
            {
                return m_sections;
            }
        }

        public ModelNetwork()
        {
            m_sections = new List<SectionBase>();
            m_sections.Add(new TitleSection(this));
            m_sections.Add(new JunctionSection(this));
            m_sections.Add(new ReservoirSection(this));
            m_sections.Add(new TankSection(this));
            m_sections.Add(new PipeSection(this));
            m_sections.Add(new PumpSection(this));
            m_sections.Add(new ValveSection(this));
            m_sections.Add(new TagSection(this));
            m_sections.Add(new DemandSection(this));
            m_sections.Add(new StatusSection(this));
            m_sections.Add(new PatternSection(this));
            m_sections.Add(new CurveSection(this));
            m_sections.Add(new ControlSection(this));
            m_sections.Add(new RuleSection(this));
            m_sections.Add(new EnergySection(this));
            m_sections.Add(new EmitterSection(this));
            m_sections.Add(new QualitySection(this));
            m_sections.Add(new SourceSection(this));
            m_sections.Add(new ReactionSection(this));
            m_sections.Add(new MixingSection(this));
            m_sections.Add(new TimeSection(this));
            m_sections.Add(new ReportSection(this));
            m_sections.Add(new OptionSection(this));
            m_sections.Add(new CoordinateSection(this));
            m_sections.Add(new VertexSection(this));
            m_sections.Add(new LabelSection(this));
            m_sections.Add(new BackdropSection(this));
            m_sections.Add(new EndSection(this));

            Analysis = new ModelAnalysis(this);
        }

        public bool ReadFile(string filePath)
        {
            bool result = true;
            Dictionary<string, List<string>> sections = new Dictionary<string, List<string>>();
            using (FileStream fs = File.OpenRead(filePath))
            {
                var reader = new StreamReader(fs, Encoding.Default);
                string line = null;
                List<string> lines = null;

                while ((line = reader.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (string.IsNullOrEmpty(line))
                        continue;

                    if (line[0] == '[')
                    {
                        var name = line.ToUpper().Replace("[", "").Replace("]", "");
                        if (!sections.ContainsKey(name))
                            sections.Add(name, new List<string>());

                        lines = sections[name];
                    }

                    lines.Add(line);
                }
            }

            try
            {
                string[] vs = { "PATTERNS", "CURVES" };
                foreach (var v in vs)
                {
                    if(sections.ContainsKey(v))
                        SetSection(sections[v]?.ToArray());
                }

                foreach (var section in sections)
                {
                    if (vs.Contains(section.Key)) continue;

                    SetSection(section.Value?.ToArray());
                }

                if (!IsValidation())
                {
                    throw new Exception("Input Error : no tanks or reservoirs in network");
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
                return false;
            }

            return result;
        }

        public bool IsValidation()
        {
            return this.Nodes.OfType<Tank>().Any() || this.Nodes.OfType<Reservoir>().Any();
        }

        public bool WriteFIle(string filePath)
        {
            bool result = true;
            StringBuilder sectionBuilder = new StringBuilder();

            try
            {
                using (StreamWriter streamWriter = new StreamWriter(filePath, false, Encoding.Default))
                {
                    foreach (SectionBase section in m_sections)
                    {
                        section.GetSectionBuilder(sectionBuilder);
                        streamWriter.Write(sectionBuilder.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                result = false;
            }
            return result;
        }

        public SectionBase GetSection(string sectionType)
        {
            return m_sections.Where(x => x.Name == sectionType).FirstOrDefault();
        }

        public void SetSection(string[] sectionLines)
        {
            var name = sectionLines[0].ToUpper().Replace("[", "").Replace("]", "");
            DateTime sdt = DateTime.Now;

            var section = GetSection(name);
            section.SetSection(sectionLines);

            Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,, " + name + " 완료");
        }

        public ModelAnalysis Analysis
        {
            get; private set;
        }

        public void Clear()
        {
            foreach (SectionBase section in m_sections)
                section.Clear();
            Analysis = new ModelAnalysis(this);

        }


        public List<Node> Nodes
        {
            get
            {
                var n = m_sections.Where(x => (new string[] { "JUNCTIONS", "TANKS", "RESERVOIRS" }).Contains(x.Name)).Select(x => x);
                return n.SelectMany(x => x.SectionItems).OfType<Node>().ToList();
            }
        }

        public List<Label> Labels
        {
            get
            {
                var n = m_sections.Where(x => (new string[] { "LABELS" }).Contains(x.Name)).Select(x => x);
                return n.SelectMany(x => x.SectionItems).OfType<Label>().ToList();
            }
        }

        public List<Link> Links
        {
            get
            {
                var n = m_sections.Where(x => (new string[] { "PIPES", "VALVES", "PUMPS" }).Contains(x.Name)).Select(x => x);
                return n.SelectMany(x => x.SectionItems).OfType<Link>().ToList();
            }
        }

        public INode FIndNode(string id)
        {
            return Nodes.SingleOrDefault(x => x.ID == id);
        }

        public ILink FIndLink(string id)
        {
            return Links.SingleOrDefault(x => x.ID == id);
        }

        public ModelNetwork Copy()
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, this);
                ms.Position = 0;

                return (ModelNetwork)formatter.Deserialize(ms);
            }
        }

    }

}
