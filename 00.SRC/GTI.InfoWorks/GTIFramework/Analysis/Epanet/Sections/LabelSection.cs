﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GTIFramework.Analysis.Epanet.Items;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class LabelSection : SectionBase
    {
        public LabelSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "LABELS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                var sectionItem = new Label(this);

                sectionItem.X = Convert.ToDouble(propertis[0].Trim());
                sectionItem.Y = Convert.ToDouble(propertis[1].Trim());
                sectionItem.Text = propertis[2].Trim();

                base.SectionItems.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            var query = from c in strtemp
                        select new Label(this)
                        {
                            X = Convert.ToDouble(c.ToArray()[0].Trim()),
                            Y = Convert.ToDouble(c.ToArray()[1].Trim()),
                            Text = c.ToArray()[2],
                        };

            base.SectionItems.AddRange(query.ToArray() as Label[]);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("X-Coord".PadRight(16, ' ')).Append("\t");
            builder.Append("Y-Coord".PadRight(16, ' ')).Append("\t");
            builder.Append("Label & Anchor Node");
            builder.AppendLine();

            foreach (Label sectionItem in base.SectionItems)
            {
                builder.Append(" ");
                builder.Append(sectionItem.X.ToString().PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.Y.ToString().PadRight(16, ' ')).Append("\t");
                builder.Append("\"").Append(sectionItem.Text).Append("\"");
                builder.AppendLine();
            }

            builder.AppendLine();
            return builder;
        }

        //public override System.Drawing.Bitmap GetSymbol()
        //{
        //    return new Bitmap(GetType(), GetType().Name + ".png");
        //}
    }
}
