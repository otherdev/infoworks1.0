﻿using System.Collections.Generic;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class ControlSection : SectionBase
    {
        public ControlSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "CONTROLS";
        }

        public override void Add(string sectionLine)
        {
            base.SectionItems.Add(sectionLine);
        }

        //일단 string add
        public override void Add(IEnumerable<string> strList)
        {
            foreach (var item in strList)
            {
                base.SectionItems.Add(item);
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            foreach (string sectionItem in base.SectionItems)
            {
                builder.AppendLine(sectionItem);
            }

            builder.AppendLine();

            return builder;
        }
    }
}
