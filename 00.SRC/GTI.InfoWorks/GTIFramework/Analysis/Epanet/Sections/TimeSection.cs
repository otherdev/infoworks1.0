﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [Serializable]
    public class TimeSection : SectionBase
    {
        private string m_duration = "0:00";
        private string m_hydraulicTimestep = "0:01";
        private string m_qualityTimestep = "0:05";
        private string m_patternTimestep = "0:01";
        private string m_patternStart = "0:00";
        private string m_reportTimestep = "0:01";
        private string m_reportStart = "0:00";
        private string m_clockStartTime = "12:00 AM";
        private string m_statistic = "None";

        public string Duration
        {
            get { return m_duration; }
            set { m_duration = value; }
        }

        public string HydraulicTimestep
        {
            get { return m_hydraulicTimestep; }
            set { m_hydraulicTimestep = value; }
        }

        public string QualityTimestep
        {
            get { return m_qualityTimestep; }
            set { m_qualityTimestep = value; }
        }

        public string PatternTimestep
        {
            get { return m_patternTimestep; }
            set { m_patternTimestep = value; }
        }

        public string PatternStart
        {
            get { return m_patternStart; }
            set { m_patternStart = value; }
        }

        public string ReportTimestep
        {
            get { return m_reportTimestep; }
            set { m_reportTimestep = value; }
        }

        public string ReportStart
        {
            get { return m_reportStart; }
            set { m_reportStart = value; }
        }

        public string StartClockTime
        {
            get { return m_clockStartTime; }
            set { m_clockStartTime = value; }
        }

        public string Statistic
        {
            get { return m_statistic; }
            set { m_statistic = value; }
        }

        public TimeSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "TIMES";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (propertis.Count() == 2)
                {
                    if (propertis[0] == "Duration")
                    {
                        m_duration = propertis[1];
                    }
                    else if (propertis[0] == "Statistic")
                    {
                        m_statistic = propertis[1];
                    }
                }
                else if (propertis.Count() == 3)
                {
                    if (propertis[0] == "Hydraulic")
                    {
                        m_hydraulicTimestep = propertis[2];
                    }
                    else if (propertis[0] == "Quality")
                    {
                        m_qualityTimestep = propertis[2];
                    }
                    else if (propertis[0] == "Pattern")
                    {
                        if (propertis[1] == "Timestep")
                        {
                            m_patternTimestep = propertis[2];
                        }
                        else if (propertis[1] == "Start")
                        {
                            m_patternStart = propertis[2];
                        }
                    }
                    else if (propertis[0] == "Report")
                    {
                        if (propertis[1] == "Timestep")
                        {
                            m_reportTimestep = propertis[2];
                        }
                        else if (propertis[1] == "Start")
                        {
                            m_reportStart = propertis[2];
                        }
                    }
                    else if (propertis[0] == "Start")
                    {
                        m_clockStartTime = propertis[2];
                    }
                }
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (item.ToArray().Length == 2)
                {
                    if (item.ToArray()[0] == "Duration")
                        m_duration = item.ToArray()[1];
                    else if (item.ToArray()[0] == "Statistic")
                        m_statistic = item.ToArray()[1];
                }
                else if (item.ToArray().Length == 3)
                {
                    if (item.ToArray()[0] == "Hydraulic")
                        m_hydraulicTimestep = item.ToArray()[2];
                    else if (item.ToArray()[0] == "Quality")
                        m_qualityTimestep = item.ToArray()[2];
                    else if (item.ToArray()[0] == "Pattern")
                    {
                        if (item.ToArray()[1] == "Timestep")
                            m_patternTimestep = item.ToArray()[2];
                        else if (item.ToArray()[1] == "Start")
                            m_patternStart = item.ToArray()[2];
                    }
                    else if (item.ToArray()[0] == "Report")
                    {
                        if (item.ToArray()[1] == "Timestep")
                            m_reportTimestep = item.ToArray()[2];
                        else if (item.ToArray()[1] == "Start")
                            m_reportStart = item.ToArray()[2];
                    }
                    else if (item.ToArray()[0] == "Start")
                        m_clockStartTime = item.ToArray()[2];
                }
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(" Duration".PadRight(20, ' ')).Append("\t").Append(m_duration);
            builder.AppendLine();
            builder.Append(" Hydraulic Timestep".PadRight(20, ' ')).Append("\t").Append(m_hydraulicTimestep);
            builder.AppendLine();
            builder.Append(" Quality Timestep".PadRight(20, ' ')).Append("\t").Append(m_qualityTimestep);
            builder.AppendLine();
            builder.Append(" Pattern Timestep".PadRight(20, ' ')).Append("\t").Append(m_patternTimestep);
            builder.AppendLine();
            builder.Append(" Pattern Start".PadRight(20, ' ')).Append("\t").Append(m_patternStart);
            builder.AppendLine();
            builder.Append(" Report Timestep".PadRight(20, ' ')).Append("\t").Append(m_reportTimestep);
            builder.AppendLine();
            builder.Append(" Report Start".PadRight(20, ' ')).Append("\t").Append(m_reportStart);
            builder.AppendLine();
            builder.Append(" Start ClockTime".PadRight(20, ' ')).Append("\t").Append(m_clockStartTime);
            builder.AppendLine();
            builder.Append(" Statistic".PadRight(20, ' ')).Append("\t").Append(m_statistic);
            builder.AppendLine();
            builder.AppendLine();

            return builder;
        }
    }
}
