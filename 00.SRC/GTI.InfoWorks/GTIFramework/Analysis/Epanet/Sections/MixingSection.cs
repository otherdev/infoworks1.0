﻿using GTIFramework.Analysis.Epanet.Items;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class MixingSection : SectionBase
    {
        public MixingSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "MIXING";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                string id = propertis[0].Trim();
                string model = propertis[1].Trim();
                string fraction = propertis[2].Trim();

                var tank = Network.FIndNode(id) as Tank;
                if (tank == null) return;
                tank.MixingModel = model;
                tank.MixingFraction = fraction;
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                string id = item.ToArray()[0].Trim();
                string model = item.ToArray()[1].Trim();
                string fraction = item.ToArray()[2].Trim();

                var tank = Network.FIndNode(id) as Tank;
                if (tank == null) return;
                tank.MixingModel = model;
                tank.MixingFraction = fraction;
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("Tank".PadRight(16, ' ')).Append("\t");
            builder.Append("Model".PadRight(16, ' ')).Append("\t");
            builder.Append("Fraction");
            builder.AppendLine();

            foreach (Tank tank in Network.Nodes.OfType<Tank>())
            {
                if (tank.MixingModel != null && tank.MixingModel != "Mixed")
                {
                    builder.Append(" ");
                    builder.Append(tank.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(tank.MixingModel.ToString().PadRight(12, ' ')).Append("\t");
                    builder.Append(tank.MixingFraction);
                    builder.AppendLine();
                }
            }

            builder.AppendLine();
            return builder;
        }
    }
}
