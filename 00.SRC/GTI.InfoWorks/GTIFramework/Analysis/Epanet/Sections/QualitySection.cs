﻿using GTIFramework.Analysis.Epanet.Items;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class QualitySection : SectionBase
    {
        public QualitySection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "QUALITY";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                string id = propertis[0];
                var initQual = convToDouble(propertis[1]);

                var node = Network.FIndNode(id);
                if (node == null) return;
                ((Node)node).InitQuality = initQual;

            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                string id = item.ToArray()[0];
                var initQual = convToDouble(item.ToArray()[1]);

                var node = Network.FIndNode(id);
                if (node == null) return;
                ((Node)node).InitQuality = initQual;
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("Node".PadRight(16, ' ')).Append("\t");
            builder.Append("InitQual");
            builder.AppendLine();


            foreach (Node node in Network.Nodes)
            {
                if (!double.IsNaN(node.InitQuality))
                {
                    builder.Append(" ");
                    builder.Append(node.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(node.InitQuality.ToString().PadRight(12, ' ')).Append("\t");
                    builder.AppendLine();
                }
            }


            builder.AppendLine();
            return builder;
        }
    }
}
