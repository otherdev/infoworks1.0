﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [Serializable]
    public class ValveSection : SectionBase
    {
        public ValveSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "VALVES";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (Network.FIndLink(propertis[0]) != null)
                {
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", propertis[0]));
                }
                Valve sectionItem = new Valve(this);
                sectionItem.ID = propertis[0].Trim();
                sectionItem.StartNode = propertis[1].Trim();
                sectionItem.EndNode = propertis[2].Trim();
                sectionItem.Diameter = convToDouble(propertis[3]);
                sectionItem.Type = propertis[4].Trim();
                sectionItem.Setting = propertis[5].Trim();
                sectionItem.MinorLoss = convToDouble(propertis[6]);
                sectionItem.Description = propertis.Count > 7 ? propertis[7] : "";



                base.SectionItems.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (Network.FIndLink(item.ToArray()[0]) != null)
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", item.ToArray()[0]));
            }

            var query = from c in strtemp
                        select new Valve(this)
                        {
                            ID = c.ToArray()[0].Trim(),
                            StartNode = c.ToArray()[1].Trim(),
                            EndNode = c.ToArray()[2].Trim(),
                            Diameter = convToDouble(c.ToArray()[3]),
                            Type = c.ToArray()[4].Trim(),
                            Setting = c.ToArray()[5].Trim(),
                            MinorLoss = convToDouble(c.ToArray()[6]),
                            Description = c.ToArray().Length > 7 ? c.ToArray()[7] : "",
                        };

            base.SectionItems.AddRange(query.ToArray() as Valve[]);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("Node1".PadRight(16, ' ')).Append("\t");
            builder.Append("Node2".PadRight(16, ' ')).Append("\t");
            builder.Append("Diameter".PadRight(12, ' ')).Append("\t");
            builder.Append("Type".PadRight(4, ' ')).Append("\t");
            builder.Append("Setting".PadRight(12, ' ')).Append("\t");
            builder.Append("MinorLoss".PadRight(12, ' '));
            builder.AppendLine();

            foreach (Valve sectionItem in Network.Links.OfType<Valve>())
            {
                builder.Append(" ");
                builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.StartNode.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.EndNode.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.Diameter.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.Type.PadRight(4, ' ')).Append("\t");
                builder.Append(sectionItem.Setting.PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.MinorLoss.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(";").Append(sectionItem.Description);
                builder.AppendLine();
            }

            builder.AppendLine();

            return builder;
        }

    }
}
