﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [Serializable]
    public class TankSection : SectionBase
    {
        public TankSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "TANKS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (Network.FIndNode(propertis[0]) != null)
                {
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", propertis[0]));
                }
                Tank sectionItem = new Tank(this);

                sectionItem.ID = propertis[0];
                sectionItem.Elevation = convToDouble(propertis[1]);
                sectionItem.InitLevel = convToDouble(propertis[2]);
                sectionItem.MinLevel = convToDouble(propertis[3]);
                sectionItem.MaxLevel = convToDouble(propertis[4]);
                sectionItem.Diameter = convToDouble(propertis[5]);
                sectionItem.MinVol = propertis[6];
                sectionItem.VolCurve = propertis.Count > 7 ? propertis[7] : "";
                sectionItem.Description = propertis.Count > 8 ? propertis[8] : "";

                base.SectionItems.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (Network.FIndNode(item.ToArray()[0]) != null)
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", item.ToArray()[0]));
            }

            var query = from c in strtemp
                        select new Tank(this)
                        {
                            ID = c.ToArray()[0],
                            Elevation = convToDouble(c.ToArray()[1]),
                            InitLevel = convToDouble(c.ToArray()[2]),
                            MinLevel = convToDouble(c.ToArray()[3]),
                            MaxLevel = convToDouble(c.ToArray()[4]),
                            Diameter = convToDouble(c.ToArray()[5]),
                            MinVol = c.ToArray()[6],
                            VolCurve = c.ToArray().Length > 7 ? c.ToArray()[7] : "",
                            Description = c.ToArray().Length > 8 ? c.ToArray()[8] : "",
                        };

            base.SectionItems.AddRange(query.ToArray() as Tank[]);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("Elevation".PadRight(12, ' ')).Append("\t");
            builder.Append("InitLevel".PadRight(12, ' ')).Append("\t");
            builder.Append("MinLevel".PadRight(12, ' ')).Append("\t");
            builder.Append("MaxLevel".PadRight(12, ' ')).Append("\t");
            builder.Append("Diameter".PadRight(12, ' ')).Append("\t");
            builder.Append("MinVol".PadRight(12, ' ')).Append("\t");
            builder.Append("VolCurve");
            builder.AppendLine();

            foreach (Tank sectionItem in Network.Nodes.OfType<Tank>())
            {
                builder.Append(" ");
                builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.Elevation.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.InitLevel.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.MinLevel.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.MaxLevel.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.Diameter.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.MinVol.PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.VolCurve).Append("\t");
                builder.Append(";").Append(sectionItem.Description);
                builder.AppendLine();
            }

            builder.AppendLine();

            return builder;
        }

    }
}
