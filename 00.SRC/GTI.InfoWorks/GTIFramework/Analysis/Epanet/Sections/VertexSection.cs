﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [Serializable]
    public class VertexSection : SectionBase
    {
        public VertexSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "VERTICES";
            //base.m_visible = true;
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                Vertex sectionItem = new Vertex(this);
                sectionItem.ID = propertis[0].Trim();
                sectionItem.X = Convert.ToDouble(propertis[1].Trim());
                sectionItem.Y = Convert.ToDouble(propertis[2].Trim());

                var link = Network.FIndLink(sectionItem.ID);
                if (link == null) return;

                ((Link)link).Vertices.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            var query = from c in strtemp
                        select new Vertex(this)
                        {
                            ID = c.ToArray()[0].ToString().Trim(),
                            X = Convert.ToDouble(c.ToArray()[1].Trim()),
                            Y = Convert.ToDouble(c.ToArray()[2].Trim()),
                        };

            var querydemand = from c in Network.Links
                              join d in query.ToArray() on c.ID.ToString() equals d.ID.ToString()
                              select new { c, d };

            foreach (var item in querydemand.ToArray())
                item.c.Vertices.Add(item.d);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("Link".PadRight(16, ' ')).Append("\t");
            builder.Append("X-Coord".PadRight(16, ' ')).Append("\t");
            builder.Append("Y-Coord".PadRight(16, ' '));
            builder.AppendLine();

            foreach (var link in Network.Links)
            {
                foreach (Vertex sectionItem in link.Vertices)
                {
                    builder.Append(" ");
                    builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(sectionItem.X.ToString().PadRight(16, ' ')).Append("\t");
                    builder.Append(sectionItem.Y.ToString().PadRight(16, ' '));
                    builder.AppendLine();
                }
            }


            builder.AppendLine();

            return builder;
        }

    }
}
