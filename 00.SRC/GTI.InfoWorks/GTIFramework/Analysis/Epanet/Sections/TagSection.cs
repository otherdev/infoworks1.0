﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class TagSection : SectionBase
    {
        public TagSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "TAGS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);
            if (propertis.Count() != 3) return;


            string type = propertis[0].Trim();
            string id = propertis[1].Trim();
            string description = propertis[2].Trim();

            SectionItemBase itemBase = null;

            if (type == "NODE")
            {
                itemBase = (Node)Network.FIndNode(id);
            }
            else if (type == "LINK")
            {
                itemBase = (Link)Network.FIndLink(id);
            }

            if (itemBase != null)
            {
                itemBase.Tag = description;
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList).Where(x=>x.ToArray().Length == 3).ToArray();

            var querynode = (from c in Network.Nodes
                             join d in strtemp.Where(x=>x.ToArray()[0].Equals("NODE")) on c.ID equals d.ToArray()[0]
                             select c.Tag = d.ToArray()[2].Trim()).ToArray();

            var querylink = (from c in Network.Links
                             join d in strtemp.Where(x => x.ToArray()[0].Equals("LINK")) on c.ID equals d.ToArray()[0]
                             select c.Tag = d.ToArray()[2].Trim()).ToArray();
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);
            foreach (var node in Network.Nodes)
            {
                if (string.IsNullOrWhiteSpace(node.Tag)) continue;
                builder.Append(" ");
                builder.Append("NODE".PadRight(5, ' ')).Append("\t");
                builder.Append(node.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(node.Tag);
                builder.AppendLine();
            }
            foreach (var link in Network.Links)
            {
                if (string.IsNullOrWhiteSpace(link.Tag)) continue;
                builder.Append(" ");
                builder.Append("LINK".PadRight(5, ' ')).Append("\t");
                builder.Append(link.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(link.Tag);
                builder.AppendLine();

            }

            builder.AppendLine();

            return builder;
        }
    }
}
