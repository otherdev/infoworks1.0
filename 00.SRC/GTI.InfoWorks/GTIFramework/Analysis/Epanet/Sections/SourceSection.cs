﻿using GTIFramework.Analysis.Epanet.Items;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class SourceSection : SectionBase
    {
        public SourceSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "SOURCES";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                string id = propertis[0].Trim();
                string type = propertis[1].Trim();
                string quality = propertis[2].Trim();
                string pattern = propertis[3].Trim();

                var node = Network.FIndNode(id);
                if (node == null) return;

                ((Node)node).SourceType = type;
                ((Node)node).SourceQuality = quality;
                ((Node)node).SourcePattern = pattern;

            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                string id = item.ToArray()[0].Trim();
                string type = item.ToArray()[1].Trim();
                string quality = item.ToArray()[2].Trim();
                string pattern = item.ToArray()[3].Trim();

                var node = Network.FIndNode(id);
                if (node == null) continue;

                ((Node)node).SourceType = type;
                ((Node)node).SourceQuality = quality;
                ((Node)node).SourcePattern = pattern;
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("Node".PadRight(16, ' ')).Append("\t");
            builder.Append("Type".PadRight(12, ' ')).Append("\t");
            builder.Append("Quality".PadRight(12, ' ')).Append("\t");
            builder.Append("Pattern");
            builder.AppendLine();

            foreach (Node node in Network.Nodes)
            {
                if (node.SourceQuality != null)
                {
                    builder.Append(" ");
                    builder.Append(node.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(node.SourceType?.PadRight(12, ' ')).Append("\t");
                    builder.Append(node.SourceQuality?.PadRight(12, ' ')).Append("\t");
                    builder.Append(node.SourcePattern);
                    builder.AppendLine();
                }
            }


            builder.AppendLine();
            return builder;
        }
    }
}
