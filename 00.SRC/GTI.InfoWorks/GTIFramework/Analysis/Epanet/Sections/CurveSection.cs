﻿using GTIFramework.Analysis.Epanet.Items;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class CurveSection : SectionBase
    {
        public CurveSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "CURVES";
        }

        private string m_comment = "";

        public override void Add(string sectionLine)
        {
            if (sectionLine[0] == ';')
            {
                m_comment = sectionLine.Replace(";", "");
                return;
            }

            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                Curve sectionItem = new Curve(this);
                sectionItem.ID = propertis[0];
                sectionItem.XValue = convToDouble(propertis[1]);
                sectionItem.YValue = convToDouble(propertis[2]);
                sectionItem.Comment = m_comment;

                base.SectionItems.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            foreach (var item in strList)
            {
                if (item[0] == ';')
                {
                    m_comment = item.Replace(";", "");
                    continue;
                }

                var propertis = GetTokens(item);

                if (propertis.Count() != 0)
                {
                    Curve sectionItem = new Curve(this);
                    sectionItem.ID = propertis[0];
                    sectionItem.XValue = convToDouble(propertis[1]);
                    sectionItem.YValue = convToDouble(propertis[2]);
                    sectionItem.Comment = m_comment;

                    base.SectionItems.Add(sectionItem);
                }
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("X-Value".PadRight(12, ' ')).Append("\t");
            builder.Append("Y-Value");
            builder.AppendLine();

            for (int i = 0; i < base.SectionItems.Count; i++)
            {
                Curve sectionItem = base.SectionItems[i] as Curve;

                if (i == 0)
                {
                    builder.AppendLine(";" + sectionItem.Comment);
                }
                else
                {
                    if (((Curve)SectionItems[i - 1]).ID != sectionItem.ID)
                    {
                        builder.AppendLine(";" + sectionItem.Comment);
                    }
                }

                builder.Append(" ");
                builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.XValue.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.YValue.ToString().PadRight(12, ' ')).Append("\t");
                builder.AppendLine();
            }

            builder.AppendLine();

            return builder;
        }
    }
}
