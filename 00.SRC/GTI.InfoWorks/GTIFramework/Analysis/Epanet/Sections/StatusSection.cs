﻿using GTIFramework.Analysis.Epanet.Items;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class StatusSection : SectionBase
    {
        public StatusSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "STATUS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                string id = propertis[0].Trim();
                string value = propertis[1].Trim();

                var sectionitem = new Status(this);
                sectionitem.ID = id;
                sectionitem.Value = value;

                var link = Network.FIndLink(id);

                if (link is Pump)
                {
                    var pump = (Pump)link;
                    if (value.ToLower() != "closed")
                    {
                        pump.Speed = value;
                        pump.InitialStatus = "Open";
                    }
                    else
                    {
                        pump.InitialStatus = value;
                    }
                }
                else if (link is Valve)
                {
                    var valve = (Valve)link;
                    if (value != "None")
                    {
                        valve.FixedStatus = value;
                    }
                }

                //foreach (Pump pump in m_network.Sections[5].SectionItems)  //pump
                //{
                //    if (pump.ID == id)
                //    {
                //        check = true;
                //        if (value.ToLower() != "closed")
                //        {
                //            pump.Speed = value;
                //            pump.InitialStatus = "Open";
                //        }
                //        else
                //        {
                //            pump.InitialStatus = value;
                //        }
                //    }
                //}

                //if (check)
                //{
                //    return;
                //}

                //foreach (Valve valve in m_network.Sections[6].SectionItems) //valve
                //{
                //    if (valve.ID == id)
                //    {
                //        if (value != "None")
                //        {
                //            valve.FixedStatus = value;
                //        }
                //    }
                //}
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            var query = from c in strtemp
                        select new Status(this)
                        {
                            ID = c.ToArray()[0].Trim(),
                            Value = c.ToArray()[1].Trim(),
                        };

            foreach (Status item in query.ToArray())
            {
                var link = Network.FIndLink(item.ID);

                if (link is Pump)
                {
                    var pump = (Pump)link;
                    if (item.Value.ToLower() != "closed")
                    {
                        pump.Speed = item.Value;
                        pump.InitialStatus = "Open";
                    }
                    else
                        pump.InitialStatus = item.Value;
                }
                else if (link is Valve)
                {
                    var valve = (Valve)link;
                    if (item.Value != "None")
                        valve.FixedStatus = item.Value;
                }
            }

            base.SectionItems.AddRange(query.ToArray() as Status[]);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("Status/Setting");
            builder.AppendLine();

            foreach (var link in Network.Links)
            {
                if (link is Pipe) continue;
                if (link is Pump)
                {
                    builder.Append(" ");
                    builder.Append(link.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(((Pump)link).InitialStatus);
                    builder.AppendLine();
                }
                else
                {
                    if (((Valve)link).FixedStatus.ToLower() != "none")
                    {
                        builder.Append(" ");
                        builder.Append(link.ID.PadRight(16, ' ')).Append("\t");
                        builder.Append(((Valve)link).FixedStatus);
                        builder.AppendLine();
                    }
                }

            }

            builder.AppendLine();

            return builder;
        }
    }
}
