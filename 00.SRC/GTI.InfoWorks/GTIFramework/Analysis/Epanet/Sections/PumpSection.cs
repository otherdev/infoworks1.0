﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class PumpSection : SectionBase
    {
        public PumpSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "PUMPS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (Network.FIndLink(propertis[0]) != null)
                {
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", propertis[0]));
                }
                Pump sectionItem = new Pump(this);

                sectionItem.ID = propertis[0].Trim();
                sectionItem.StartNode = propertis[1].Trim();
                sectionItem.EndNode = propertis[2].Trim();


                if (propertis.Count > 3)
                {
                    for (int i = 3; i < propertis.Count; i++)
                    {
                        if (propertis[i].IndexOf("HEAD") != -1)
                        {
                            sectionItem.Head = propertis[i + 1].Trim();
                        }

                        if (propertis[i].IndexOf("POWER") != -1)
                        {
                            sectionItem.Power = propertis[i + 1].Trim();
                        }

                        if (propertis[i].IndexOf("SPEED") != -1)
                        {
                            sectionItem.Speed = propertis[i + 1].Trim();
                        }

                        if (propertis[i].IndexOf("PATTERN") != -1)
                        {
                            sectionItem.Pattern = propertis[i + 1].Trim();
                        }

                        if (propertis[i].Trim()[0] == ';')
                        {
                            sectionItem.Description = propertis[i];
                        }
                    }
                }

                base.SectionItems.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (Network.FIndLink(item.ToArray()[0]) != null)
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", item.ToArray()[0]));

                Pump sectionItem = new Pump(this);
                sectionItem.ID = item.ToArray()[0].Trim();
                sectionItem.StartNode = item.ToArray()[1].Trim();
                sectionItem.EndNode = item.ToArray()[2].Trim();
                if (item.ToArray().Length > 3)
                {
                    for (int i = 3; i < item.ToArray().Length; i++)
                    {
                        if (item.ToArray()[i].IndexOf("HEAD") != -1)
                        {
                            sectionItem.Head = item.ToArray()[i + 1].Trim();
                        }

                        if (item.ToArray()[i].IndexOf("POWER") != -1)
                        {
                            sectionItem.Power = item.ToArray()[i + 1].Trim();
                        }

                        if (item.ToArray()[i].IndexOf("SPEED") != -1)
                        {
                            sectionItem.Speed = item.ToArray()[i + 1].Trim();
                        }

                        if (item.ToArray()[i].IndexOf("PATTERN") != -1)
                        {
                            sectionItem.Pattern = item.ToArray()[i + 1].Trim();
                        }

                        if (item.ToArray()[i].Trim()[0] == ';')
                        {
                            sectionItem.Description = item.ToArray()[i];
                        }
                    }
                }

                base.SectionItems.Add(sectionItem);
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("Node1".PadRight(16, ' ')).Append("\t");
            builder.Append("Node2".PadRight(16, ' ')).Append("\t");
            builder.Append("Parameters");
            builder.AppendLine();

            foreach (Pump sectionItem in Network.Links.OfType<Pump>())
            {
                builder.Append(" ");
                builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.StartNode.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.EndNode.PadRight(16, ' ')).Append("\t");

                if (sectionItem.Head != null && sectionItem.Head != "")
                {
                    builder.Append("HEAD ").Append(sectionItem.Head).Append("\t");
                }
                else if (sectionItem.Power != null && sectionItem.Power != "")
                {
                    builder.Append("POWER ").Append(sectionItem.Power).Append("\t");
                }

                if (sectionItem.Speed != null && sectionItem.Speed != "")
                {
                    builder.Append("SPEED ").Append(sectionItem.Speed).Append("\t");
                }

                if (sectionItem.Pattern != null && sectionItem.Pattern != "")
                {
                    builder.Append("PATTERN ").Append(sectionItem.Pattern).Append("\t");
                }

                builder.Append(";").Append(sectionItem.Description);
                builder.AppendLine();
            }

            builder.AppendLine();

            return builder;
        }




        //public override System.Drawing.Bitmap GetSymbol()
        //{
        //    return new Bitmap(GetType(), GetType().Name + ".png");
        //}
    }
}
