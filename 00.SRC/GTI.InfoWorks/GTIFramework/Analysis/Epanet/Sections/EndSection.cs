﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class EndSection : SectionBase
    {
        public EndSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "END";
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            foreach (string sectionItem in base.SectionItems)
            {
                builder.AppendLine(sectionItem);
            }

            builder.AppendLine();

            return builder;
        }
    }
}
