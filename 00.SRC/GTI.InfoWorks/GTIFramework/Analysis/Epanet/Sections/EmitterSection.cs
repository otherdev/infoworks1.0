﻿using GTIFramework.Analysis.Epanet.Items;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class EmitterSection : SectionBase
    {
        public EmitterSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "EMITTERS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                string id = propertis[0];
                string coefficient = propertis[1];

                var junc = Network.FIndNode(id) as Junction;
                if (junc == null) return;
                junc.EmitterCoeff = coefficient;
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                string id = item.ToArray()[0];
                string coefficient = item.ToArray()[1];

                var junc = Network.FIndNode(id) as Junction;
                if (junc == null) return;
                junc.EmitterCoeff = coefficient;
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("Junction".PadRight(16, ' ')).Append("\t");
            builder.Append("Coefficient");
            builder.AppendLine();

            foreach (Junction junction in Network.Nodes.OfType<Junction>())
            {
                if (junction.EmitterCoeff != null)
                {
                    builder.Append(" ");
                    builder.Append(junction.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(junction.EmitterCoeff);
                    builder.AppendLine();
                }
            }

            builder.AppendLine();
            return builder;
        }
    }
}
