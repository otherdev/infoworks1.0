﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class ReactionSection : SectionBase
    {
        private double m_bulkOrder = 1;
        private double m_tankOrder = 1;
        private double m_wallOrder = 1;
        private double m_bulkCoeff = 0;
        private double m_wallCoeff = 0;
        private double m_concentration = 0;
        private double m_correlation = 0;

        public double BulkOrder
        {
            get { return m_bulkOrder; }
            set { m_bulkOrder = value; }
        }

        public double TankOrder
        {
            get { return m_tankOrder; }
            set { m_tankOrder = value; }
        }

        public double WallOrder
        {
            get { return m_wallOrder; }
            set { m_wallOrder = value; }
        }

        public double BulkCoeff
        {
            get { return m_bulkCoeff; }
            set { m_bulkCoeff = value; }
        }

        public double WallCoeff
        {
            get { return m_wallCoeff; }
            set { m_wallCoeff = value; }
        }

        public double Concentration
        {
            get { return m_concentration; }
            set { m_concentration = value; }
        }

        public double Correlation
        {
            get { return m_correlation; }
            set { m_correlation = value; }
        }

        public ReactionSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "REACTIONS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis[0].Trim() == "Bulk" || propertis[0].Trim() == "Wall" || propertis[0].Trim() == "Tank")
            {
                string type = propertis[0].Trim();
                string id = propertis[1].Trim();
                string coeff = propertis[2].Trim();

                var pipe = Network.FIndLink(id) as Pipe;
                var tank = Network.FIndNode(id) as Tank;
                if (pipe == null && tank == null) return;

                if (type == "Bulk")
                {
                    if (pipe != null)
                    {
                        pipe.BulkCoeff = coeff;
                    }
                }

                if (type == "Wall")
                {
                    if (pipe != null)
                    {
                        pipe.WallCoeff = coeff;
                    }
                }

                if (type == "Tank")
                {
                    if (tank != null)
                        tank.ReactionCoeff = coeff;
                }
            }

            if (propertis[0] == "Order" || propertis[0] == "Global")
            {
                if (propertis[0] == "Order" && propertis[1] == "Bulk")
                {
                    m_bulkOrder = Convert.ToDouble(propertis[2]);
                }
                else if (propertis[0] == "Order" && propertis[1] == "Tank")
                {
                    m_tankOrder = Convert.ToDouble(propertis[2]);
                }
                else if (propertis[0] == "Order" && propertis[1] == "Wall")
                {
                    m_wallOrder = Convert.ToDouble(propertis[2]);
                }
                else if (propertis[0] == "Global" && propertis[1] == "Bulk")
                {
                    m_bulkCoeff = Convert.ToDouble(propertis[2]);
                }
                else if (propertis[0] == "Global" && propertis[1] == "Wall")
                {
                    m_wallCoeff = Convert.ToDouble(propertis[2]);
                }
            }

            else if (propertis[0] == "Limiting")
            {
                m_concentration = Convert.ToDouble(propertis[2]);
            }

            else if (propertis[0] == "Roughness")
            {
                m_correlation = Convert.ToDouble(propertis[2]);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (item.ToArray()[0].Trim() == "Bulk" || item.ToArray()[0].Trim() == "Wall" || item.ToArray()[0].Trim() == "Tank")
                {
                    string type = item.ToArray()[0].Trim();
                    string id = item.ToArray()[1].Trim();
                    string coeff = item.ToArray()[2].Trim();

                    var pipe = Network.FIndLink(id) as Pipe;
                    var tank = Network.FIndNode(id) as Tank;
                    if (pipe == null && tank == null) return;

                    if (type == "Bulk")
                    {
                        if (pipe != null)
                            pipe.BulkCoeff = coeff;
                    }

                    if (type == "Wall")
                    {
                        if (pipe != null)
                            pipe.WallCoeff = coeff;
                    }

                    if (type == "Tank")
                    {
                        if (tank != null)
                            tank.ReactionCoeff = coeff;
                    }
                }

                if (item.ToArray()[0] == "Order" || item.ToArray()[0] == "Global")
                {
                    if (item.ToArray()[0] == "Order" && item.ToArray()[1] == "Bulk")
                        m_bulkOrder = Convert.ToDouble(item.ToArray()[2]);
                    else if (item.ToArray()[0] == "Order" && item.ToArray()[1] == "Tank")
                        m_tankOrder = Convert.ToDouble(item.ToArray()[2]);
                    else if (item.ToArray()[0] == "Order" && item.ToArray()[1] == "Wall")
                        m_wallOrder = Convert.ToDouble(item.ToArray()[2]);
                    else if (item.ToArray()[0] == "Global" && item.ToArray()[1] == "Bulk")
                        m_bulkCoeff = Convert.ToDouble(item.ToArray()[2]);
                    else if (item.ToArray()[0] == "Global" && item.ToArray()[1] == "Wall")
                        m_wallCoeff = Convert.ToDouble(item.ToArray()[2]);
                }
                else if (item.ToArray()[0] == "Limiting")
                    m_concentration = Convert.ToDouble(item.ToArray()[2]);
                else if (item.ToArray()[0] == "Roughness")
                    m_correlation = Convert.ToDouble(item.ToArray()[2]);
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("Type".PadRight(12, ' ')).Append("\t");
            builder.Append("Pipe/Tank".PadRight(16, ' ')).Append("\t");
            builder.Append("Coefficient");
            builder.AppendLine();

            foreach (var pipe in Network.Links.OfType<Pipe>())
            {
                if (pipe.BulkCoeff != null && pipe.BulkCoeff != "")
                {
                    builder.Append(" ");
                    builder.Append("Bulk".PadRight(12, ' ')).Append("\t");
                    builder.Append(pipe.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(pipe.BulkCoeff);
                    builder.AppendLine();
                }

                if (pipe.WallCoeff != null && pipe.WallCoeff != "")
                {
                    builder.Append(" ");
                    builder.Append("Wall".PadRight(12, ' ')).Append("\t");
                    builder.Append(pipe.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(pipe.WallCoeff);
                    builder.AppendLine();
                }
            }

            foreach (Tank tank in Network.Nodes.OfType<Tank>())
            {
                if (tank.ReactionCoeff != null && tank.ReactionCoeff != "")
                {
                    builder.Append(" ");
                    builder.Append("Tank".PadRight(12, ' ')).Append("\t");
                    builder.Append(tank.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append(tank.ReactionCoeff);
                    builder.AppendLine();
                }
            }


            foreach (string sectionItem in base.SectionItems)
            {
                builder.AppendLine(sectionItem);
            }

            if (base.SectionItems.Count == 0)
            {
                builder.AppendLine();
                builder.AppendLine("[REACTIONS]");

                builder.AppendLine(" Order Bulk            	" + m_bulkOrder);
                builder.AppendLine(" Order Tank            	" + m_tankOrder);
                builder.AppendLine(" Order Wall            	" + m_wallOrder);
                builder.AppendLine(" Global Bulk           	" + m_bulkCoeff);
                builder.AppendLine(" Global Wall           	" + m_wallCoeff);
                builder.AppendLine(" Limiting Potential    	" + m_concentration);
                builder.AppendLine(" Roughness Correlation 	" + m_correlation);
            }

            builder.AppendLine();
            return builder;
        }
    }
}
