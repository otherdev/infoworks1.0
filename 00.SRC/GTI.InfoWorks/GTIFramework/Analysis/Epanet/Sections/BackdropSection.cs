﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class BackdropSection : SectionBase
    {
        private Backdrop sectionItem = null;

        public BackdropSection(ModelNetwork network) : base(network)
        {
            sectionItem = new Backdrop(this);
            base.m_name = "BACKDROP";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (propertis[0] == "DIMENSIONS" && 5 < propertis.Count())
                {
                    PointF lowerLeft = sectionItem.LowerLeft;
                    lowerLeft.X = Convert.ToSingle(propertis[1]);
                    lowerLeft.Y = Convert.ToSingle(propertis[2]);

                    PointF upperRight = sectionItem.UpperRight;
                    upperRight.X = Convert.ToSingle(propertis[3]);
                    upperRight.Y = Convert.ToSingle(propertis[4]);

                    sectionItem.LowerLeft = lowerLeft;
                    sectionItem.UpperRight = upperRight;
                }
                else if (propertis[0] == "UNITS" && 1 < propertis.Count())
                {
                    //index 없는경우 무시
                    if(propertis.Count == 2)
                        sectionItem.Units = propertis[1];
                }
                else if (propertis[0] == "FILE" && 1 < propertis.Count())
                {
                        sectionItem.Units = propertis[1];
                }
                else if (propertis[0] == "OFFSET" && 2 < propertis.Count())
                {
                    PointF offset = sectionItem.Offset;
                    offset.X = Convert.ToSingle(propertis[1]);
                    offset.Y = Convert.ToSingle(propertis[2]);

                    sectionItem.Offset = offset;
                }

                if (base.SectionItems.Count > 0)
                {
                    base.SectionItems[0] = sectionItem;
                }
                else
                {
                    base.SectionItems.Add(sectionItem);
                }
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (item.ToArray()[0] == "DIMENSIONS" && 5 < item.ToArray().Length)
                {
                    PointF lowerLeft = sectionItem.LowerLeft;
                    lowerLeft.X = Convert.ToSingle(item.ToArray()[1]);
                    lowerLeft.Y = Convert.ToSingle(item.ToArray()[2]);

                    PointF upperRight = sectionItem.UpperRight;
                    upperRight.X = Convert.ToSingle(item.ToArray()[3]);
                    upperRight.Y = Convert.ToSingle(item.ToArray()[4]);

                    sectionItem.LowerLeft = lowerLeft;
                    sectionItem.UpperRight = upperRight;
                }
                else if (item.ToArray()[0] == "UNITS" && 1 < item.ToArray().Length)
                {
                    //index 없는경우 무시
                    if (item.ToArray().Length == 2)
                        sectionItem.Units = item.ToArray()[1];
                }
                else if (item.ToArray()[0] == "FILE" && 1 < item.ToArray().Length)
                {
                    sectionItem.Units = item.ToArray()[1];
                }
                else if (item.ToArray()[0] == "OFFSET" && 2 < item.ToArray().Length)
                {
                    PointF offset = sectionItem.Offset;
                    offset.X = Convert.ToSingle(item.ToArray()[1]);
                    offset.Y = Convert.ToSingle(item.ToArray()[2]);

                    sectionItem.Offset = offset;
                }

                if (base.SectionItems.Count > 0)
                {
                    base.SectionItems[0] = sectionItem;
                }
                else
                {
                    base.SectionItems.Add(sectionItem);
                }
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            if (base.SectionItems.Count == 0)
            {
                base.SectionItems.Add(new Backdrop(this));
            }

            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);
            Backdrop backdrop = base.SectionItems[0] as Backdrop;

            builder.Append(" ");
            builder.Append("DIMENSIONS".PadRight(16, ' ')).Append("\t");
            builder.Append(backdrop.LowerLeft.X.ToString().PadRight(16, ' ')).Append("\t");
            builder.Append(backdrop.LowerLeft.Y.ToString().PadRight(16, ' ')).Append("\t");
            builder.Append(backdrop.UpperRight.X.ToString().PadRight(16, ' ')).Append("\t");
            builder.Append(backdrop.UpperRight.Y.ToString().PadRight(16, ' ')).Append("\t");
            builder.AppendLine();

            builder.Append(" ");
            builder.Append("UNITS".PadRight(16, ' ')).Append("\t");
            builder.Append(backdrop.Units).Append("\t");
            builder.AppendLine();

            builder.Append(" ");
            builder.Append("FILE".PadRight(16, ' ')).Append("\t");
            builder.Append(backdrop.Filepath).Append("\t");
            builder.AppendLine();

            builder.Append(" ");
            builder.Append("OFFSET".PadRight(16, ' ')).Append("\t");
            builder.Append(backdrop.Offset.X.ToString().PadRight(16, ' ')).Append("\t");
            builder.Append(backdrop.Offset.Y.ToString().PadRight(16, ' ')).Append("\t");
            builder.AppendLine();

            builder.AppendLine();
            return builder;
        }
    }
}
