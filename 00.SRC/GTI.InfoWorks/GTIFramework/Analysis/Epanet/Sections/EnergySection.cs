﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class EnergySection : SectionBase
    {
        private double m_efficiency;
        private double m_price;
        private string m_pattern = null;
        private double m_charge;

        public double Efficiency
        {
            get { return m_efficiency; }
            set { m_efficiency = value; }
        }

        public double Price
        {
            get { return m_price; }
            set { m_price = value; }
        }

        public string Pattern
        {
            get { return m_pattern; }
            set { m_pattern = value; }
        }

        public double Charge
        {
            get { return m_charge; }
            set { m_charge = value; }
        }

        public EnergySection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "ENERGY";

            m_efficiency = 75;
            m_price = 0;
            m_pattern = "";
            m_charge = 0;
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (propertis[0].ToUpper().IndexOf("GLOBAL") != -1 || propertis[0].ToUpper().IndexOf("DEMAND") != -1)
                {
                    if (propertis[1].ToUpper().IndexOf("EFFICIENCY") != -1)
                    {
                        m_efficiency = Convert.ToDouble(propertis[2]);
                    }
                    if (propertis[1].ToUpper().IndexOf("PRICE") != -1)
                    {
                        m_price = Convert.ToDouble(propertis[2]);
                    }
                    if (propertis[1].ToUpper().IndexOf("PATTERN") != -1)
                    {
                        m_pattern = propertis[2];
                    }
                    if (propertis[1].ToUpper().IndexOf("CHARGE") != -1)
                    {
                        m_charge = Convert.ToDouble(propertis[2]);
                    }
                }

                if (propertis[0].ToUpper().IndexOf("PUMP") != -1)
                {
                    string id = propertis[1];

                    var pump = Network.FIndLink(id) as Pump;
                    if (pump == null) return;

                    if (propertis[2].ToUpper().IndexOf("EFFICIENCY") != -1)
                    {
                        pump.EfficCurve = propertis[3];
                    }
                    if (propertis[2].ToUpper().IndexOf("PRICE") != -1)
                    {
                        pump.EnergyPrice = propertis[3];
                    }
                    if (propertis[2].ToUpper().IndexOf("PATTERN") != -1)
                    {
                        pump.PricePattern = propertis[3];
                    }

                }
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (item.ToArray()[0].ToUpper().IndexOf("GLOBAL") != -1 || item.ToArray()[0].ToUpper().IndexOf("DEMAND") != -1)
                {
                    if (item.ToArray()[1].ToUpper().IndexOf("EFFICIENCY") != -1)
                    {
                        m_efficiency = Convert.ToDouble(item.ToArray()[2]);
                    }
                    if (item.ToArray()[1].ToUpper().IndexOf("PRICE") != -1)
                    {
                        m_price = Convert.ToDouble(item.ToArray()[2]);
                    }
                    if (item.ToArray()[1].ToUpper().IndexOf("PATTERN") != -1)
                    {
                        m_pattern = item.ToArray()[2];
                    }
                    if (item.ToArray()[1].ToUpper().IndexOf("CHARGE") != -1)
                    {
                        m_charge = Convert.ToDouble(item.ToArray()[2]);
                    }
                }

                if (item.ToArray()[0].ToUpper().IndexOf("PUMP") != -1)
                {
                    string id = item.ToArray()[1];

                    var pump = Network.FIndLink(id) as Pump;
                    if (pump == null) continue;

                    if (item.ToArray()[2].ToUpper().IndexOf("EFFICIENCY") != -1)
                    {
                        pump.EfficCurve = item.ToArray()[3];
                    }
                    if (item.ToArray()[2].ToUpper().IndexOf("PRICE") != -1)
                    {
                        pump.EnergyPrice = item.ToArray()[3];
                    }
                    if (item.ToArray()[2].ToUpper().IndexOf("PATTERN") != -1)
                    {
                        pump.PricePattern = item.ToArray()[3];
                    }
                }
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(" Global Efficiency".PadRight(20, ' ')).Append("\t");
            builder.Append(m_efficiency);
            builder.AppendLine();

            builder.Append(" Global Price".PadRight(20, ' ')).Append("\t");
            builder.Append(m_price);
            builder.AppendLine();

            if (m_pattern != "")
            {
                builder.Append(" Global Pattern".PadRight(20, ' ')).Append("\t");
                builder.Append(m_pattern);
                builder.AppendLine();
            }

            builder.Append(" Demand Charge".PadRight(20, ' ')).Append("\t");
            builder.Append(m_charge);
            builder.AppendLine();

            foreach (Pump pump in Network.Links.OfType<Pump>())
            {
                if (pump.EfficCurve != null && pump.EfficCurve != "")
                {
                    builder.Append(" Pump".PadRight(6, ' ')).Append("\t");
                    builder.Append(pump.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append("Efficiency".PadRight(10, ' ')).Append("\t");
                    builder.Append(pump.EfficCurve);
                    builder.AppendLine();
                }
                if (pump.EnergyPrice != null && pump.EnergyPrice != "")
                {
                    builder.Append(" Pump".PadRight(6, ' ')).Append("\t");
                    builder.Append(pump.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append("Price".PadRight(10, ' ')).Append("\t");
                    builder.Append(pump.EnergyPrice);
                    builder.AppendLine();
                }
                if (pump.PricePattern != null && pump.PricePattern != "")
                {
                    builder.Append(" Pump".PadRight(6, ' ')).Append("\t");
                    builder.Append(pump.ID.PadRight(16, ' ')).Append("\t");
                    builder.Append("Pattern".PadRight(10, ' ')).Append("\t");
                    builder.Append(pump.PricePattern);
                    builder.AppendLine();
                }
            }

            builder.AppendLine();
            return builder;
        }
    }
}
