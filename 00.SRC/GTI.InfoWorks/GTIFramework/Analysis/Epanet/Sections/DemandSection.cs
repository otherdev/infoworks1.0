﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class DemandSection : SectionBase
    {
        public DemandSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "DEMANDS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                Demand sectionItem = new Demand(this);
                sectionItem.ID = propertis[0];
                sectionItem.Value = Convert.ToDouble(propertis[1]);
                sectionItem.Pattern = propertis.Count > 2 ? propertis[2] : "";
                sectionItem.Category = propertis.Count > 3 ? propertis[3] : "";

                var junc = Network.FIndNode(sectionItem.ID) as Junction;
                if (junc == null) return;
                junc.GetDemand().Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            var query = from c in strtemp
                        select new Demand(this)
                        {
                            ID = c.ToArray()[0].ToString().Trim(),
                            Value = Convert.ToDouble(c.ToArray()[1]),
                            Pattern = c.ToArray().Length > 2 ? c.ToArray()[2] : "",
                            Category = c.ToArray().Length > 3 ? c.ToArray()[3] : ""
                        };

            var querydemand = from c in Network.Nodes
                              join d in query.ToArray() on c.ID.ToString() equals d.ID.ToString()
                              select new { c, d };

            foreach (var item in querydemand.ToArray())
                (item.c as Junction).GetDemand().Add(item.d);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("Junction".PadRight(16, ' ')).Append("\t");
            builder.Append("Demand".PadRight(12, ' ')).Append("\t");
            builder.Append("Pattern".PadRight(16, ' ')).Append("\t");
            builder.Append("Category");
            builder.AppendLine();

            foreach (Junction junction in Network.Nodes.OfType<Junction>())
            {
                if (junction.GetDemand().Count != 0)
                {
                    foreach (Demand demand in junction.GetDemand())
                    {
                        builder.Append(" ");
                        builder.Append(demand.ID.PadRight(16, ' ')).Append("\t");
                        builder.Append(demand.Value.ToString().PadRight(12, ' ')).Append("\t");
                        builder.Append(demand.Pattern.PadRight(16, ' ')).Append("\t");
                        builder.Append(demand.Category);
                        builder.AppendLine();
                    }
                }
            }

            builder.AppendLine();
            return builder;
        }
    }
}
