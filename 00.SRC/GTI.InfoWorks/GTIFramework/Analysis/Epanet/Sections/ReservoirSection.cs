﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class ReservoirSection : SectionBase
    {
        public ReservoirSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "RESERVOIRS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (Network.FIndNode(propertis[0]) != null)
                {
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", propertis[0]));
                }
                Reservoir sectionItem = new Reservoir(this);

                sectionItem.ID = propertis[0];
                sectionItem.Head = convToDouble(propertis[1]);
                sectionItem.Pattern = propertis.Count > 2 ? propertis[2] : "";
                sectionItem.Description = propertis.Count > 3 ? propertis[3] : "";

                base.SectionItems.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (Network.FIndNode(item.ToArray()[0]) != null)
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", item.ToArray()[0]));
            }

            var query = from c in strtemp
                        select new Reservoir(this)
                        {
                            ID = c.ToArray()[0],
                            Head = convToDouble(c.ToArray()[1]),
                            Pattern = c.ToArray().Length > 2 ? c.ToArray()[2] : "",
                            Description = c.ToArray().Length > 3 ? c.ToArray()[3] : "",
                        };

            base.SectionItems.AddRange(query.ToArray() as Reservoir[]);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("Head".PadRight(12, ' ')).Append("\t");
            builder.Append("Pattern".PadRight(12, ' ')).Append("\t");
            builder.AppendLine();

            foreach (Reservoir sectionItem in Network.Nodes.OfType<Reservoir>())
            {
                builder.Append(" ");
                builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.Head.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.Pattern.PadRight(12, ' ')).Append("\t");
                builder.Append(";").Append(sectionItem.Description);
                builder.AppendLine();
            }

            builder.AppendLine();

            return builder;
        }
    }
}
