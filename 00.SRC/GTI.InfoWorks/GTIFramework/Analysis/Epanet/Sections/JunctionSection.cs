﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class JunctionSection : SectionBase
    {
        public JunctionSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "JUNCTIONS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (Network.FIndNode(propertis[0]) != null)
                {
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", propertis[0]));
                }

                Junction sectionItem = new Junction(this);
                sectionItem.ID = propertis[0];
                sectionItem.Elevation = convToDouble(propertis[1]);
                sectionItem.BaseDemand = convToDouble(propertis[2]);
                sectionItem.DemandPattern = propertis.Count > 3 ? propertis[3] : "";
                sectionItem.Description = propertis.Count > 4 ? propertis[4] : "";

                base.SectionItems.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (Network.FIndNode(item.ToArray()[0]) != null)
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", item.ToArray()[0]));
            }

            var query = from c in strtemp
                        select new Junction(this)
                        {
                            ID = c.ToArray()[0],
                            Elevation = convToDouble(c.ToArray()[1]),
                            BaseDemand = convToDouble(c.ToArray()[2]),
                            DemandPattern = c.ToArray().Length > 3 ? c.ToArray()[3] : "",
                            Description = c.ToArray().Length > 4 ? c.ToArray()[4] : "",
                        };
            
            base.SectionItems.AddRange(query.ToArray() as Junction[]);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("Elev".PadRight(12, ' ')).Append("\t");
            builder.Append("Demand".PadRight(12, ' ')).Append("\t");
            builder.Append("Pattern".PadRight(16, ' ')).Append("\t");
            builder.AppendLine();

            foreach (Junction sectionItem in Network.Nodes.OfType<Junction>())
            {
                builder.Append(" ");
                builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.Elevation.ToString().PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.BaseDemand.ToString().PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.DemandPattern.PadRight(16, ' ')).Append("\t");
                builder.Append(";").Append(sectionItem.Description);
                builder.AppendLine();
            }

            builder.AppendLine();

            return builder;
        }

    }
}
