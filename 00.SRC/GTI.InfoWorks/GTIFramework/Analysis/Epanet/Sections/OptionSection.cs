﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class OptionSection : SectionBase
    {
        private string m_units = "GPM";
        private string m_formula = "H-W";
        private double m_gravity = 1;
        private double m_viscosity = 1;
        private double m_trials = 40;
        private double m_accuracy = 0.001;
        private double m_CHECKFREQ = 2;
        private double m_MAXCHECK = 10;
        private double m_DAMPLIMIT = 0;
        private string m_unbalanced = "Continue 10";
        private string m_pattern = "1";
        private double m_multiplier = 1;
        private double m_exponent = 0.5;
        private string m_report = "No";
        private string m_quality = "";
        private string m_qualityParameter = "None";
        private string m_qualityUnit = "mg/L";
        private string m_qualityTraceNode = "";
        private double m_diffusivity = 1;
        private double m_tolerance = 0.01;

        public string Units
        {
            get { return m_units; }
            set { m_units = value; }
        }

        public string Formula
        {
            get { return m_formula; }
            set { m_formula = value; }
        }

        public double Gravity
        {
            get { return m_gravity; }
            set { m_gravity = value; }
        }

        public double Viscosity
        {
            get { return m_viscosity; }
            set { m_viscosity = value; }
        }

        public double Trials
        {
            get { return m_trials; }
            set { m_trials = value; }
        }

        public double Accuracy
        {
            get { return m_accuracy; }
            set { m_accuracy = value; }
        }

        public double CHECKFREQ
        {
            get { return m_CHECKFREQ; }
            set { m_CHECKFREQ = value; }
        }

        public double MAXCHECK
        {
            get { return m_MAXCHECK; }
            set { m_MAXCHECK = value; }
        }

        public double DAMPLIMIT
        {
            get { return m_DAMPLIMIT; }
            set { m_DAMPLIMIT = value; }
        }

        public string Unbalanced
        {
            get { return m_unbalanced; }
            set { m_unbalanced = value; }
        }

        public string Pattern
        {
            get { return m_pattern; }
            set { m_pattern = value; }
        }

        public double Multiplier
        {
            get { return m_multiplier; }
            set { m_multiplier = value; }
        }

        public double Exponent
        {
            get { return m_exponent; }
            set { m_exponent = value; }
        }

        public string Report
        {
            get { return m_report; }
            set { m_report = value; }
        }

        public string QualityParameter
        {
            get { return m_qualityParameter; }
            set { m_qualityParameter = value; }
        }

        public string QualityUnit
        {
            get { return m_qualityUnit; }
            set { m_qualityUnit = value; }
        }

        public string QualityTraceNode
        {
            get { return m_qualityTraceNode; }
            set { m_qualityTraceNode = value; }
        }

        public string Quality
        {
            get
            {
                if (m_qualityParameter == "Trace")
                {
                    m_quality = m_qualityParameter + " " + m_qualityTraceNode;
                }
                else
                {
                    m_quality = m_qualityParameter + " " + m_qualityUnit;
                }

                return m_quality;
            }
            set { m_quality = value; }
        }

        public double Diffusivity
        {
            get { return m_diffusivity; }
            set { m_diffusivity = value; }
        }

        public double Tolerance
        {
            get { return m_tolerance; }
            set { m_tolerance = value; }
        }

        public OptionSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "OPTIONS";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis[0] == "Units")
            {
                m_units = propertis[1];
            }
            else if (propertis[0] == "Headloss")
            {
                m_formula = propertis[1];
            }
            else if (propertis[0] == "Specific")
            {
                m_gravity = Convert.ToDouble(propertis[2]);
            }
            else if (propertis[0] == "Viscosity")
            {
                m_viscosity = Convert.ToDouble(propertis[1]);
            }
            else if (propertis[0] == "Trials")
            {
                m_trials = Convert.ToDouble(propertis[1]);
            }
            else if (propertis[0] == "Accuracy")
            {
                m_accuracy = Convert.ToDouble(propertis[1]);
            }
            else if (propertis[0] == "CHECKFREQ")
            {
                m_CHECKFREQ = Convert.ToDouble(propertis[1]);
            }
            else if (propertis[0] == "MAXCHECK")
            {
                m_MAXCHECK = Convert.ToDouble(propertis[1]);
            }
            else if (propertis[0] == "DAMPLIMIT")
            {
                m_DAMPLIMIT = Convert.ToDouble(propertis[1]);
            }
            else if (propertis[0] == "Unbalanced")
            {
                m_unbalanced = propertis[1];
            }
            else if (propertis[0] == "Pattern")
            {
                m_pattern = propertis[1];
            }
            else if (propertis[0] == "Demand")
            {
                m_multiplier = Convert.ToDouble(propertis[2]);
            }
            else if (propertis[0] == "Emitter")
            {
                m_exponent = Convert.ToDouble(propertis[2]);
            }
            else if (propertis[0] == "Quality")
            {
                m_qualityParameter = propertis[1];
                if (m_qualityParameter == "Trace")
                {
                    m_qualityTraceNode = propertis[2];
                }
                else
                {
                    m_qualityUnit = propertis[2];
                }
            }
            else if (propertis[0] == "Diffusivity")
            {
                m_diffusivity = Convert.ToDouble(propertis[1]);
            }
            else if (propertis[0] == "Tolerance")
            {
                m_tolerance = Convert.ToDouble(propertis[1]);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (item.ToArray()[0] == "Units")
                {
                    m_units = item.ToArray()[1];
                }
                else if (item.ToArray()[0] == "Headloss")
                {
                    m_formula = item.ToArray()[1];
                }
                else if (item.ToArray()[0] == "Specific")
                {
                    m_gravity = Convert.ToDouble(item.ToArray()[2]);
                }
                else if (item.ToArray()[0] == "Viscosity")
                {
                    m_viscosity = Convert.ToDouble(item.ToArray()[1]);
                }
                else if (item.ToArray()[0] == "Trials")
                {
                    m_trials = Convert.ToDouble(item.ToArray()[1]);
                }
                else if (item.ToArray()[0] == "Accuracy")
                {
                    m_accuracy = Convert.ToDouble(item.ToArray()[1]);
                }
                else if (item.ToArray()[0] == "CHECKFREQ")
                {
                    m_CHECKFREQ = Convert.ToDouble(item.ToArray()[1]);
                }
                else if (item.ToArray()[0] == "MAXCHECK")
                {
                    m_MAXCHECK = Convert.ToDouble(item.ToArray()[1]);
                }
                else if (item.ToArray()[0] == "DAMPLIMIT")
                {
                    m_DAMPLIMIT = Convert.ToDouble(item.ToArray()[1]);
                }
                else if (item.ToArray()[0] == "Unbalanced")
                {
                    m_unbalanced = item.ToArray()[1];
                }
                else if (item.ToArray()[0] == "Pattern")
                {
                    m_pattern = item.ToArray()[1];
                }
                else if (item.ToArray()[0] == "Demand")
                {
                    m_multiplier = Convert.ToDouble(item.ToArray()[2]);
                }
                else if (item.ToArray()[0] == "Emitter")
                {
                    m_exponent = Convert.ToDouble(item.ToArray()[2]);
                }
                else if (item.ToArray()[0] == "Quality")
                {
                    m_qualityParameter = item.ToArray()[1];
                    if (m_qualityParameter == "Trace")
                    {
                        m_qualityTraceNode = item.ToArray()[2];
                    }
                    else
                    {
                        m_qualityUnit = item.ToArray()[2];
                    }
                }
                else if (item.ToArray()[0] == "Diffusivity")
                {
                    m_diffusivity = Convert.ToDouble(item.ToArray()[1]);
                }
                else if (item.ToArray()[0] == "Tolerance")
                {
                    m_tolerance = Convert.ToDouble(item.ToArray()[1]);
                }
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            foreach (string sectionItem in base.SectionItems)
            {
                builder.AppendLine(sectionItem);
            }

            if (base.SectionItems.Count == 0)
            {
                var unbalanced = Unbalanced == "Continue" ? string.Format("Continue 10") : Unbalanced;
                builder.AppendLine(" Units              	" + Units);
                builder.AppendLine(" Headloss           	" + Formula);
                builder.AppendLine(" Specific Gravity   	" + Gravity);
                builder.AppendLine(" Viscosity          	" + Viscosity);
                builder.AppendLine(" Trials             	" + Trials);
                builder.AppendLine(" Accuracy           	" + Accuracy);
                builder.AppendLine(" CHECKFREQ          	" + CHECKFREQ);
                builder.AppendLine(" MAXCHECK           	" + MAXCHECK);
                builder.AppendLine(" DAMPLIMIT              " + DAMPLIMIT);
                builder.AppendLine(" Unbalanced         	" + unbalanced);
                builder.AppendLine(" Pattern            	" + Pattern);
                builder.AppendLine(" Demand Multiplier  	" + Multiplier);
                builder.AppendLine(" Emitter Exponent   	" + Exponent);
                builder.AppendLine(" Quality            	" + Quality);
                builder.AppendLine(" Diffusivity        	" + Diffusivity);
                builder.AppendLine(" Tolerance          	" + Tolerance);
            }

            builder.AppendLine();

            return builder;
        }
    }
}
