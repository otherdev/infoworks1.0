﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class PipeSection : SectionBase
    {
        public PipeSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "PIPES";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                if (Network.FIndLink(propertis[0]) != null)
                {
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", propertis[0]));
                }
                Pipe sectionItem = new Pipe(this);

                sectionItem.ID = propertis[0].Trim();
                sectionItem.StartNode = propertis[1].Trim();
                sectionItem.EndNode = propertis[2].Trim();
                sectionItem.Length = convToDouble(propertis[3]);
                sectionItem.Diameter = convToDouble(propertis[4]);
                sectionItem.Roughness = convToDouble(propertis[5]);
                sectionItem.MinorLoss = convToDouble(propertis[6]);
                sectionItem.InitialStatus = propertis.Count > 7 ? propertis[7] : "";
                sectionItem.Description = propertis.Count > 8 ? propertis[8] : "";

                base.SectionItems.Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            foreach (var item in strtemp)
            {
                if (Network.FIndLink(item.ToArray()[0]) != null)
                    throw new Exception(string.Format("{0}와 같은 ID가 이미 있습니다.", item.ToArray()[0]));
            }

            var query = from c in strtemp
                        select new Pipe(this)
                        {
                            ID = c.ToArray()[0].Trim(),
                            StartNode = c.ToArray()[1].Trim(),
                            EndNode = c.ToArray()[2].Trim(),
                            Length = convToDouble(c.ToArray()[3]),
                            Diameter = convToDouble(c.ToArray()[4]),
                            Roughness = convToDouble(c.ToArray()[5]),
                            MinorLoss = convToDouble(c.ToArray()[6]),
                            InitialStatus = c.ToArray().Length > 7 ? c.ToArray()[7] : "",
                            Description = c.ToArray().Length > 8 ? c.ToArray()[8] : "",
                        };

            base.SectionItems.AddRange(query.ToArray() as Pipe[]);
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("Node1".PadRight(16, ' ')).Append("\t");
            builder.Append("Node2".PadRight(16, ' ')).Append("\t");
            builder.Append("Length".PadRight(12, ' ')).Append("\t");
            builder.Append("Diameter".PadRight(12, ' ')).Append("\t");
            builder.Append("Roughness".PadRight(12, ' ')).Append("\t");
            builder.Append("MinorLoss".PadRight(12, ' ')).Append("\t");
            builder.Append("Status".PadRight(6, ' ')).Append("\t");
            builder.AppendLine();

            foreach (Pipe sectionItem in Network.Links.OfType<Pipe>())
            {
                builder.Append(" ");
                builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.StartNode.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.EndNode.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.Length.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.Diameter.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.Roughness.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.MinorLoss.ToString().PadRight(12, ' ')).Append("\t");
                builder.Append(sectionItem.InitialStatus.PadRight(6, ' ')).Append("\t");
                builder.Append(";").Append(sectionItem.Description);
                builder.AppendLine();
            }

            builder.AppendLine();

            return builder;
        }

    }
}
