﻿using GTIFramework.Analysis.Epanet.Items;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class PatternSection : SectionBase
    {
        public PatternSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "PATTERNS";
        }

        private string m_comment = "";

        public override void Add(string sectionLine)
        {
            if (sectionLine[0] == ';')
            {
                m_comment = sectionLine.Replace(";", "");
                return;
            }

            var propertis = GetTokens(sectionLine);

            if (propertis.Count != 0)
            {
                var sectionItem = this.SectionItems.Cast<Pattern>().Where(x => x.ID == propertis[0].Trim()).FirstOrDefault();
                if (sectionItem == null)
                {
                    sectionItem = new Pattern(this);
                    sectionItem.ID = propertis[0].Trim();
                    sectionItem.Comment = m_comment.Trim();
                    base.SectionItems.Add(sectionItem);
                }
                for (int i = 1; i < propertis.Count(); i++)
                {
                    var value = base.convToDouble(propertis[i].Trim());
                    sectionItem.Multipliers.Add(value);

                }
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            foreach (var item in strList)
            {
                if (item[0] == ';')
                {
                    m_comment = item.Replace(";", "");
                    continue;
                }

                var propertis = GetTokens(item);

                if (propertis.Count != 0)
                {
                    var sectionItem = this.SectionItems.Cast<Pattern>().Where(x => x.ID == propertis[0].Trim()).FirstOrDefault();
                    if (sectionItem == null)
                    {
                        sectionItem = new Pattern(this);
                        sectionItem.ID = propertis[0].Trim();
                        sectionItem.Comment = m_comment.Trim();
                        base.SectionItems.Add(sectionItem);
                    }
                    for (int i = 1; i < propertis.Count(); i++)
                    {
                        var value = base.convToDouble(propertis[i].Trim());
                        sectionItem.Multipliers.Add(value);
                    }
                }
            }
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("ID".PadRight(16, ' ')).Append("\t");
            builder.Append("Multipliers");
            builder.AppendLine();

            foreach (var sectionItem in this.SectionItems.Cast<Pattern>())
            {
                builder.Append(";" + sectionItem.Comment).Append("\t").AppendLine();
                string value = "";

                for (int i = 0; i < sectionItem.Multipliers.Count; i++)
                {
                    value += string.Format("{0,-16}\t", sectionItem.Multipliers[i]);
                    if ((((i + 1) % 10) == 0) || i == sectionItem.Multipliers.Count - 1)
                    {
                        builder.Append(" ");
                        builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                        builder.Append(value);
                        builder.AppendLine();
                        value = "";
                    }
                }

            }


            builder.AppendLine();

            return builder;
        }
    }
}
