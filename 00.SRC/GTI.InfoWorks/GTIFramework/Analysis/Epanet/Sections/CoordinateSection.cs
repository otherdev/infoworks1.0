﻿using GTIFramework.Analysis.Epanet.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GTIFramework.Analysis.Epanet.Sections
{
    [System.Serializable]
    public class CoordinateSection : SectionBase
    {

        public CoordinateSection(ModelNetwork network)
            : base(network)
        {
            base.m_name = "COORDINATES";
        }

        public override void Add(string sectionLine)
        {
            var propertis = GetTokens(sectionLine);

            if (propertis.Count() != 0)
            {
                var ID = propertis[0];
                var X = Convert.ToDouble(propertis[1]);
                var Y = Convert.ToDouble(propertis[2]);

                var node = Network.FIndNode(ID);
                if (node == null) return;

                node.X = X;
                node.Y = Y;
                //if (!base.NetworkNodes.ContainsKey(sectionItem.ID))
                //{
                //    base.NetworkNodes.Add(sectionItem.ID, new List<INode>());
                //}
                //base.NetworkNodes[sectionItem.ID].Add(sectionItem);
            }
        }

        public override void Add(IEnumerable<string> strList)
        {
            IEnumerable<string>[] strtemp = GetTokens(strList);

            var queryx = (from c in Network.Nodes
                          join d in strtemp on c.ID equals d.ToArray()[0]
                          select c.X = Convert.ToDouble(d.ToArray()[1].Trim())).ToArray();

            var queryy = (from c in Network.Nodes
                          join d in strtemp on c.ID equals d.ToArray()[0]
                          select c.Y = Convert.ToDouble(d.ToArray()[2].Trim())).ToArray();
        }

        public override StringBuilder GetSectionBuilder(StringBuilder sectionBuilder)
        {
            StringBuilder builder = base.GetSectionBuilder(sectionBuilder);

            builder.Append(";");
            builder.Append("Node".PadRight(16, ' ')).Append("\t");
            builder.Append("X-Coord".PadRight(16, ' ')).Append("\t");
            builder.Append("Y-Coord".PadRight(16, ' '));
            builder.AppendLine();

            foreach (var sectionItem in Network.Nodes)
            {
                builder.Append(" ");
                builder.Append(sectionItem.ID.PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.X.ToString().PadRight(16, ' ')).Append("\t");
                builder.Append(sectionItem.Y.ToString().PadRight(16, ' '));
                builder.AppendLine();
            }

            builder.AppendLine();

            return builder;
        }
    }
}
