﻿using System.Collections.Generic;

namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Junction : Node
    {
        private double m_elevation;
        private double m_base_demand;
        private string m_demand_pattern = null;
        private string m_description = null;

        private IList<Demand> m_demands = null;
        private string m_emitter = null;

        public IList<Demand> GetDemand()
        {
            return m_demands;
        }

        public void SetDemand(IList<Demand> demands)
        {
            m_demands = demands;
        }

        public Junction(SectionBase section) : base(section)
        {
            this.m_elevation = 0.0;
            this.m_base_demand = 0;
            this.m_demand_pattern = "";
            this.m_description = ";";

            this.m_demands = new List<Demand>();
        }

        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }


        public override double X
        {
            get
            {
                return base.X;
            }
            set
            {
                base.X = value;
            }
        }


        public override double Y
        {
            get
            {
                return base.Y;
            }
            set
            {
                base.Y = value;
            }
        }


        public string Description
        {
            set
            {
                this.m_description = value;
            }
            get
            {
                return this.m_description;
            }
        }


        public override string Tag
        {
            set
            {
                base.Tag = value;
            }
            get
            {
                return base.Tag;
            }
        }


        public double Elevation
        {
            set
            {
                this.m_elevation = value;
            }
            get
            {
                return this.m_elevation;
            }
        }


        public double BaseDemand
        {
            set
            {
                if (this.m_demands.Count == 0)
                {
                    this.m_base_demand = value;
                }
                else
                {
                    this.m_demands[0].Value = value;
                }
            }
            get
            {
                if (this.m_demands.Count != 0)
                {
                    this.m_base_demand = this.m_demands[0].Value;
                }

                return this.m_base_demand;
            }
        }


        public string DemandPattern
        {
            set
            {
                if (this.m_demands.Count == 0)
                {
                    this.m_demand_pattern = value;
                }
                else
                {
                    this.m_demands[0].Pattern = value;
                }

                this.m_demand_pattern = value;
            }
            get
            {
                if (this.m_demands.Count != 0)
                {
                    this.m_demand_pattern = this.m_demands[0].Pattern;
                }

                return this.m_demand_pattern;
            }
        }


        public int Categories
        {
            set
            {
            }
            get
            {
                if (m_demands.Count == 0)
                {
                    return 1;
                }
                else
                {
                    return m_demands.Count;
                }
            }
        }

        public string EmitterCoeff
        {
            set
            {
                if (value == null || value == "")
                {
                    m_emitter = null;
                }
                else
                {
                    m_emitter = double.Parse(value).ToString();
                }
            }
            get
            {
                return m_emitter;
            }
        }

        public new double InitQuality
        {
            set
            {
                if (double.IsNaN(value) || double.IsInfinity(value))
                {
                    m_initQuality = double.NaN;
                }
                else
                {
                    m_initQuality = value;
                }
            }
            get
            {
                return m_initQuality;
            }
        }


        public new string SourceQuality
        {
            set
            {
            }
            get
            {
                return m_sourceQuality;
            }
        }

        public object ActualDemand
        {
            get
            {
                return base.m_analysisDemand;
            }
        }

        public object TotalHead
        {
            get
            {
                return base.m_analysisHead;
            }
        }

        public object Pressure
        {
            get
            {
                return base.m_analysisPressure;
            }
        }

        public object Quality
        {
            get
            {
                return base.m_analysisQuality;
            }
        }


    }
}
