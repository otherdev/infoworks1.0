﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// vertics section item
    /// 링크 내 노드 집합
    /// NODE {X, Y}
    /// </summary>
    public class Vertex : SectionItemBase
    {
        public Vertex(SectionBase section) : base(section)
        {

        }

        public double X { get; set; }
        public double Y { get; set; }

    }
}
