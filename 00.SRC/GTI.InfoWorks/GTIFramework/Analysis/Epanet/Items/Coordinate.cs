﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// coordinate section item
    /// 노드 집합 (junction, reservoir, tank)
    /// </summary>
    public class Coordinate : SectionItemBase
    {
        /// <summary>
        /// NODE
        /// ID, X, Y
        /// </summary>
        /// <param name="section"></param>
        public Coordinate(SectionBase section) : base(section)
        {
        }

        public double X { get; set; }
        public double Y { get; set; }
    }
}
