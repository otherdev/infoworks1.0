﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Valve : Link
    {
        private double m_diameter;
        private string m_type = null;
        private string m_setting = null;
        private double m_minorloss;
        private string m_description = null;

        private string m_fixedStatus = null;

        public Valve(SectionBase section) : base(section)
        {
            this.m_diameter = 12;
            this.m_type = "PRV";
            this.m_setting = "0";
            this.m_minorloss = 0;
            this.m_description = ";";

            this.m_fixedStatus = "None";
        }

        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }

        public override string StartNode
        {
            get
            {
                return base.StartNode;
            }
            set
            {
                base.StartNode = value;
            }
        }

        public override double startX
        {
            get
            {
                return base.m_start_X;
            }
            set
            {
                base.m_start_X = value;
            }
        }

        public override double startY
        {
            get
            {
                return m_start_Y;
            }
            set
            {
                m_start_Y = value;
            }
        }

        public override string EndNode
        {
            get
            {
                return base.EndNode;
            }
            set
            {
                base.EndNode = value;
            }
        }

        public override double endX
        {
            get
            {
                return base.m_end_X;
            }
            set
            {
                base.m_end_X = value;
            }
        }

        public override double endY
        {
            get
            {
                return m_end_Y;
            }
            set
            {
                m_end_Y = value;
            }
        }

        public string Description
        {
            set
            {
                this.m_description = value;
            }
            get
            {
                return this.m_description;
            }
        }

        public override string Tag
        {
            set
            {
                base.Tag = value;
            }
            get
            {
                return base.Tag;
            }
        }

        public double Diameter
        {
            set
            {
                this.m_diameter = value;
            }
            get
            {
                return this.m_diameter;
            }
        }

        public string Type
        {
            set
            {
                this.m_type = value;
            }
            get
            {
                return this.m_type;
            }
        }

        public string Setting
        {
            set
            {
                this.m_setting = value;
            }
            get
            {
                return this.m_setting;
            }
        }

        public double MinorLoss
        {
            set
            {
                this.m_minorloss = value;
            }
            get
            {
                return this.m_minorloss;
            }
        }

        public string FixedStatus
        {
            set
            {
                this.m_fixedStatus = value;
            }
            get
            {
                return this.m_fixedStatus;
            }
        }

        public object Flow
        {
            get
            {
                return base.m_analysisFlow;
            }
        }

        public object Velocity
        {
            get
            {
                return base.m_analysisVelocity;
            }
        }

        public object Headloss
        {
            get
            {
                return base.m_analysisHeadloss;
            }
        }

        public object Quality
        {
            get
            {
                return base.m_analysisQuality;
            }
        }

        public object Status
        {
            get
            {
                return base.m_analysisStatus;
            }
        }

        #region 단수사고 모듈관련
        /// <summary>
        /// 차단밸브(제수밸브)
        /// </summary>
        public string GISValveType
        {
            get; set;
        }
        #endregion 단수사고 모듈관련
    }
}
