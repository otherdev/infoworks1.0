﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// status section item
    /// 모델 링크상태 설정 (펌프, 밸브)
    /// </summary>
    public class Status : SectionItemBase
    {
        private string m_value = null;

        public Status(SectionBase section) : base(section)
        {
            this.m_value = "";
        }

        public override string ID { get => base.ID; set => base.ID = value; }

        public string Value
        {
            set
            {
                this.m_value = value;
            }
            get
            {
                return this.m_value;
            }
        }
    }
}
