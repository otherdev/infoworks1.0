﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// source section item
    /// 모델 시간별 수질패턴 설정
    /// </summary>
    public class Source : SectionItemBase
    {
        private string m_type = null;
        private string m_quality = null;
        private string m_pattern = null;

        public Source(SectionBase section) : base(section)
        {
            this.m_type = "";
            this.m_quality = "";
            this.m_pattern = "";
        }

        public override string ID { get => base.ID; set => base.ID = value; }

        public string Type
        {
            set
            {
                this.m_type = value;
            }
            get
            {
                return this.m_type;
            }
        }

        public string Quality
        {
            set
            {
                this.m_quality = value;
            }
            get
            {
                return this.m_quality;
            }
        }

        public string Pattern
        {
            set
            {
                this.m_pattern = value;
            }
            get
            {
                return this.m_pattern;
            }
        }
    }
}
