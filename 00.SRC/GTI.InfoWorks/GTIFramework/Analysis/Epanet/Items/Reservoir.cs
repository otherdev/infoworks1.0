﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Reservoir : Node
    {
        private double m_head;
        private string m_pattern = null;
        private string m_description = null;

        public Reservoir(SectionBase section) : base(section)
        {
            this.m_head = 0;
            this.m_pattern = "";
            this.m_description = ";";
        }

        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }

        public override double X
        {
            get
            {
                return base.X;
            }
            set
            {
                base.X = value;
            }
        }

        public override double Y
        {
            get
            {
                return base.Y;
            }
            set
            {
                base.Y = value;
            }
        }

        public string Description
        {
            set
            {
                this.m_description = value;
            }
            get
            {
                return this.m_description;
            }
        }

        public override string Tag
        {
            set
            {
                base.Tag = value;
            }
            get
            {
                return base.Tag;
            }
        }

        public double Head
        {
            set
            {
                this.m_head = value;
            }
            get
            {
                return this.m_head;
            }
        }

        public string Pattern
        {
            set
            {
                this.m_pattern = value;
            }
            get
            {
                return this.m_pattern;
            }
        }

        public new double InitQuality
        {
            set
            {
                if (double.IsNaN(value) || double.IsInfinity(value))
                {
                    m_initQuality = double.NaN;
                }
                else
                {
                    m_initQuality = value;
                }
            }
            get
            {
                return m_initQuality;
            }
        }

        public new string SourceQuality
        {
            set
            {
            }
            get
            {
                return m_sourceQuality;
            }
        }

        public object NetInflow
        {
            get
            {
                return base.m_analysisDemand;
            }
        }

        public object Elevation
        {
            get
            {
                return base.m_analysisHead;
            }
        }

        public object Pressure
        {
            get
            {
                return base.m_analysisPressure;
            }
        }

        public object Quality
        {
            get
            {
                return base.m_analysisQuality;
            }
        }


    }
}
