﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Label : SectionItemBase
    {
        private double m_x;
        private double m_y;
        private string m_text = "";


        public Label(SectionBase section)
            : base(section)
        {
        }



        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value.Replace("\"", "");
            }
        }

        public double X
        {
            get
            {
                return m_x;
            }
            set
            {
                m_x = value;
            }
        }

        public double Y
        {
            get
            {
                return m_y;
            }
            set
            {
                m_y = value;
            }
        }

    }
}
