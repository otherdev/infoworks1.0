﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// demand section item
    /// 모델 내 유량 설정
    /// </summary>
    public class Demand : SectionItemBase
    {
        private double m_value;
        private string m_pattern = null;
        private string m_category = null;

        public Demand(SectionBase section) : base(section)
        {
            this.m_value = 0;
            this.m_pattern = "";
            this.m_category = "";
        }

        public override string ID { get => base.ID; set => base.ID = value; }

        public double Value
        {
            set
            {
                this.m_value = value;
            }
            get
            {
                return this.m_value;
            }
        }

        public string Pattern
        {
            set
            {
                this.m_pattern = value;
            }
            get
            {
                return this.m_pattern;
            }
        }

        public string Category
        {
            set
            {
                this.m_category = value;
            }
            get
            {
                return this.m_category;
            }
        }
    }
}
