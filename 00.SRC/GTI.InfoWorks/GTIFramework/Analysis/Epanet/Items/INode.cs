﻿namespace GTIFramework.Analysis.Epanet.Items
{
    public interface INode
    {
        double X { get; set; }
        double Y { get; set; }
    }
}
