﻿namespace GTIFramework.Analysis.Epanet.Items
{
    public interface ILink
    {
        string StartNode { get; set; }
        double startX { get; set; }
        double startY { get; set; }
        string EndNode { get; set; }
        double endX { get; set; }
        double endY { get; set; }
    }
}
