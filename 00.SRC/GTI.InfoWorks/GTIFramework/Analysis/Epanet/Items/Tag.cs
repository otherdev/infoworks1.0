﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// tag section item
    /// 모델 내 TAG 설정 (노드및링크의 설명을 설정)
    /// </summary>
    public class Tag : SectionItemBase
    {
        private string m_type = null;
        private string m_description = null;

        public Tag(SectionBase section) : base(section)
        {
            this.m_type = "";
            this.m_description = "";
        }

        public string Type
        {
            set
            {
                this.m_type = value;
            }
            get
            {
                return this.m_type;
            }
        }

        public override string ID { get => base.ID; set => base.ID = value; }

        public string Description
        {
            set
            {
                this.m_description = value;
            }
            get
            {
                return this.m_description;
            }
        }
    }
}
