﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Tank : Node
    {
        private double m_elevation;
        private double m_initlevel;
        private double m_minlevel;
        private double m_maxlevel;
        private double m_diameter;
        private string m_minvol = null;
        private string m_volcurve = null;
        private string m_description = null;

        private string m_mixingModel = null;
        private string m_mixingFraction = null;

        private string m_reactionCoeff = null;

        public Tank(SectionBase section) : base(section)
        {
            this.m_elevation = 0;
            this.m_initlevel = 10;
            this.m_minlevel = 0;
            this.m_maxlevel = 20;
            this.m_diameter = 50;
            this.m_minvol = "";
            this.m_volcurve = "";
            this.m_description = ";";

            this.m_mixingModel = "Mixed";
        }

        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }

        public override double X
        {
            get
            {
                return base.X;
            }
            set
            {
                base.X = value;
            }
        }

        public override double Y
        {
            get
            {
                return base.Y;
            }
            set
            {
                base.Y = value;
            }
        }

        public string Description
        {
            set
            {
                this.m_description = value;
            }
            get
            {
                return this.m_description;
            }
        }

        public override string Tag
        {
            set
            {
                base.Tag = value;
            }
            get
            {
                return base.Tag;
            }
        }

        public double Elevation
        {
            set
            {
                this.m_elevation = value;
            }
            get
            {
                return this.m_elevation;
            }
        }

        public double InitLevel
        {
            set
            {
                this.m_initlevel = value;
            }
            get
            {
                return this.m_initlevel;
            }
        }

        public double MinLevel
        {
            set
            {
                this.m_minlevel = value;
            }
            get
            {
                return this.m_minlevel;
            }
        }

        public double MaxLevel
        {
            set
            {
                this.m_maxlevel = value;
            }
            get
            {
                return this.m_maxlevel;
            }
        }

        public double Diameter
        {
            set
            {
                this.m_diameter = value;
            }
            get
            {
                return this.m_diameter;
            }
        }

        public string MinVol
        {
            set
            {
                this.m_minvol = value;
            }
            get
            {
                return this.m_minvol;
            }
        }

        public string VolCurve
        {
            set
            {
                this.m_volcurve = value;
            }
            get
            {
                return this.m_volcurve;
            }
        }

        public string MixingModel
        {
            set
            {
                m_mixingModel = value;
            }
            get
            {
                return m_mixingModel;
            }
        }

        public string MixingFraction
        {
            set
            {
                if (value == null || value == "")
                {
                    m_mixingFraction = null;
                }
                else
                {
                    m_mixingFraction = double.Parse(value).ToString();
                }
            }
            get
            {
                return m_mixingFraction;
            }
        }

        public string ReactionCoeff
        {
            set
            {
                if (value == null || value == "")
                {
                    m_reactionCoeff = null;
                }
                else
                {
                    m_reactionCoeff = double.Parse(value).ToString();
                }
            }
            get
            {
                return m_reactionCoeff;
            }
        }

        public new double InitQuality
        {
            set
            {
                if (double.IsNaN(value) || double.IsInfinity(value))
                {
                    m_initQuality = double.NaN;
                }
                else
                {
                    m_initQuality = value;
                }
            }
            get
            {
                return m_initQuality;
            }
        }

        public new string SourceQuality
        {
            set
            {
            }
            get
            {
                return m_sourceQuality;
            }
        }

        public object NetInflow
        {
            get
            {
                return base.m_analysisDemand;
            }
        }

        public object ElevationResult
        {
            get
            {
                return base.m_analysisHead;
            }
        }

        public object Pressure
        {
            get
            {
                return base.m_analysisPressure;
            }
        }

        public object Quality
        {
            get
            {
                return base.m_analysisQuality;
            }
        }

    }
}
