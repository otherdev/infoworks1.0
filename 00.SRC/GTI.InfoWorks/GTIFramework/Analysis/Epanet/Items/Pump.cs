﻿using System.ComponentModel;

namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Pump : Link
    {
        private string m_head = null;
        private string m_power = null;
        private string m_speed = null;
        private string m_pattern = null;
        private string m_initialStatus = null;


        private string m_efficCurve = null;
        private string m_energyPrice = null;
        private string m_pricePattern = null;


        private string m_parameters = null;
        private string m_description = null;



        public Pump(SectionBase section) : base(section)
        {
            this.m_parameters = "";
            this.m_description = ";";

            this.m_initialStatus = "Open";
        }

        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }

        public override string StartNode
        {
            get
            {
                return base.StartNode;
            }
            set
            {
                base.StartNode = value;
            }
        }

        public override double startX
        {
            get
            {
                return base.m_start_X;
            }
            set
            {
                base.m_start_X = value;
            }
        }

        public override double startY
        {
            get
            {
                return m_start_Y;
            }
            set
            {
                m_start_Y = value;
            }
        }

        public override string EndNode
        {
            get
            {
                return base.EndNode;
            }
            set
            {
                base.EndNode = value;
            }
        }

        public override double endX
        {
            get
            {
                return base.m_end_X;
            }
            set
            {
                base.m_end_X = value;
            }
        }

        public override double endY
        {
            get
            {
                return m_end_Y;
            }
            set
            {
                m_end_Y = value;
            }
        }

        public string Description
        {
            set
            {
                this.m_description = value;
            }
            get
            {
                return this.m_description;
            }
        }

        public override string Tag
        {
            set
            {
                base.Tag = value;
            }
            get
            {
                return base.Tag;
            }
        }

        public string Head
        {
            set
            {
                this.m_head = value;
            }
            get
            {
                return this.m_head;
            }
        }

        public string Power
        {
            set
            {
                if (value == null || value == "")
                {
                    m_power = null;
                }
                else
                {
                    m_power = double.Parse(value).ToString();
                }
            }
            get
            {
                return m_power;
            }
        }

        public string Speed
        {
            set
            {
                if (value == null || value == "")
                {
                    m_speed = null;
                }
                else
                {
                    m_speed = double.Parse(value).ToString();
                }
            }
            get
            {
                return m_speed;
            }
        }

        public string Pattern
        {
            set
            {
                this.m_pattern = value;
            }
            get
            {
                return this.m_pattern;
            }
        }

        public string InitialStatus
        {
            set
            {
                this.m_initialStatus = value;
            }
            get
            {
                return this.m_initialStatus;
            }
        }

        public string EfficCurve
        {
            set
            {
                m_efficCurve = value;
            }
            get
            {
                return m_efficCurve;
            }
        }

        public string EnergyPrice
        {
            set
            {
                if (value == null || value == "")
                {
                    m_energyPrice = null;
                }
                else
                {
                    m_energyPrice = double.Parse(value).ToString();
                }
            }
            get
            {
                return m_energyPrice;
            }
        }

        public string PricePattern
        {
            set
            {
                m_pricePattern = value;
            }
            get
            {
                return m_pricePattern;
            }
        }

        [Browsable(false)]
        public string Parameters
        {
            set
            {
                this.m_parameters = value;
            }
            get
            {
                return this.m_parameters;
            }
        }

        public object Flow
        {
            get
            {
                return base.m_analysisFlow;
            }
        }

        public object Headloss
        {
            get
            {
                return base.m_analysisHeadloss;
            }
        }

        public object Quality
        {
            get
            {
                return base.m_analysisQuality;
            }
        }

        public object Status
        {
            get
            {
                return base.m_analysisStatus;
            }
        }

    }
}
