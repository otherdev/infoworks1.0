﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// emitter section item
    /// 모델 내 이미터 설정
    /// </summary>
    public class Emitter : SectionItemBase
    {
        private double m_coefficient;

        public Emitter(SectionBase section) : base(section)
        {
        }

        public override string ID { get => base.ID; set => base.ID = value; }

        public double Coefficient
        {
            set
            {
                this.m_coefficient = value;
            }
            get
            {
                return this.m_coefficient;
            }
        }
    }
}
