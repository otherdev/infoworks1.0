﻿using System.Collections.Generic;

namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// pattern section item
    /// 모델 내 물패턴 설정
    /// </summary>
    public class Pattern : SectionItemBase
    {
        private List<double> m_multipliers = null;
        private string m_comment = "";

        public Pattern(SectionBase section) : base(section)
        {
            m_multipliers = new List<double>();
        }

        public override string ID { get => base.ID; set => base.ID = value; }

        public List<double> Multipliers
        {
            set
            {
                m_multipliers = value;
            }
            get
            {
                return m_multipliers;
            }
        }

        public string Comment
        {
            set
            {
                m_comment = value;
            }
            get
            {
                return m_comment;
            }
        }
    }
}
