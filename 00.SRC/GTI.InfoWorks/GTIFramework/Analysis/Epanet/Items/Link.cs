﻿using System.Collections.Generic;

namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Link : SectionItemBase, ILink
    {
        public Link(SectionBase section) : base(section) { }

        private string m_start_node = null;
        protected double m_start_X;
        protected double m_start_Y;
        private string m_end_node = null;
        protected double m_end_X;
        protected double m_end_Y;

        protected object m_analysisFlow = "#N/A";
        protected object m_analysisVelocity = "#N/A";
        protected object m_analysisHeadloss = "#N/A";
        protected object m_analysisFrictionFactor = "#N/A";
        protected object m_analysisReactionRate = "#N/A";
        protected object m_analysisQuality = "#N/A";
        protected object m_analysisStatus = "#N/A";
        protected object m_analysisSetting = "#N/A";

        private List<Vertex> m_vertices = new List<Vertex>();

        public List<Vertex> Vertices
        {
            get { return m_vertices; }
        }
        public virtual string StartNode
        {
            set
            {
                m_start_node = value;
            }
            get
            {
                return m_start_node;
            }
        }

        public virtual string EndNode
        {
            set
            {
                m_end_node = value;
            }
            get
            {
                return m_end_node;
            }
        }

        public void SetAnalysisFlow(float value)
        {
            m_analysisFlow = value;
        }

        public void SetAnalysisVelocity(float value)
        {
            m_analysisVelocity = value;
        }

        public void SetAnalysisHeadLoss(float value)
        {
            m_analysisHeadloss = value;
        }

        public void SetAnalysisFrictionFactor(float value)
        {
            m_analysisFrictionFactor = value;
        }

        public void SetAnalysisReactionRate(float value)
        {
            m_analysisReactionRate = value;
        }

        public void SetAnalysisQuality(float value)
        {
            m_analysisQuality = value;
        }

        public void SetAnalysisStatus(float value)
        {
            m_analysisStatus = value;
        }

        public void SetAnalysisSetting(float value)
        {
            m_analysisSetting = value;
        }

        #region INode 멤버
        public virtual INode NodeStart
        {
            get
            {
                return Section.Network.FIndNode(StartNode);
            }
        }

        public virtual double startX
        {
            get
            {
                return m_start_X;
            }
            set
            {
                m_start_X = value;
            }
        }

        public virtual double startY
        {
            get
            {
                return m_start_Y;
            }
            set
            {
                m_start_Y = value;
            }
        }

        public virtual INode NodeEnd
        {
            get
            {
                return Section.Network.FIndNode(EndNode);
            }
        }

        public virtual double endX
        {
            get
            {
                return m_end_X;
            }
            set
            {
                m_end_X = value;
            }
        }

        public virtual double endY
        {
            get
            {
                return m_end_Y;
            }
            set
            {
                m_end_Y = value;
            }
        }
        #endregion
    }
}
