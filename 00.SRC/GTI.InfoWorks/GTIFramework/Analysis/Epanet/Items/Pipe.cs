﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Pipe : Link
    {
        private double m_length;
        private double m_diameter;
        private double m_roughness;
        private double m_minorloss;
        private string m_status = null;
        private string m_description = null;

        public Coordinate Coordinate1 = null;
        public Coordinate Coordinate2 = null;

        private string m_bulkCoeff = null;
        private string m_wallCoeff = null;

        public Pipe(SectionBase section) : base(section)
        {
            this.m_length = 1000;
            this.m_diameter = 12;
            this.m_roughness = 100;
            this.m_minorloss = 0;
            this.m_status = "Open";
            this.m_description = ";";
        }

        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }

        public override string StartNode
        {
            get
            {
                return base.StartNode;
            }
            set
            {
                base.StartNode = value;
            }
        }

        public override double startX
        {
            get
            {
                return base.m_start_X;
            }
            set
            {
                base.m_start_X = value;
            }
        }

        public override double startY
        {
            get
            {
                return m_start_Y;
            }
            set
            {
                m_start_Y = value;
            }
        }

        public override string EndNode
        {
            get
            {
                return base.EndNode;
            }
            set
            {
                base.EndNode = value;
            }
        }

        public override double endX
        {
            get
            {
                return base.m_end_X;
            }
            set
            {
                base.m_end_X = value;
            }
        }

        public override double endY
        {
            get
            {
                return m_end_Y;
            }
            set
            {
                m_end_Y = value;
            }
        }

        public string Description
        {
            set
            {
                this.m_description = value;
            }
            get
            {
                return this.m_description;
            }
        }

        public override string Tag
        {
            set
            {
                base.Tag = value;
            }
            get
            {
                return base.Tag;
            }
        }

        public double Length
        {
            set
            {
                this.m_length = value;
            }
            get
            {
                return this.m_length;
            }
        }

        public double Diameter
        {
            set
            {
                this.m_diameter = value;
            }
            get
            {
                return this.m_diameter;
            }
        }

        public double Roughness
        {
            set
            {
                this.m_roughness = value;
            }
            get
            {
                return this.m_roughness;
            }
        }

        public double MinorLoss
        {
            set
            {
                this.m_minorloss = value;
            }
            get
            {
                return this.m_minorloss;
            }
        }

        public string InitialStatus
        {
            set
            {
                this.m_status = value;
            }
            get
            {
                return this.m_status;
            }
        }

        public string BulkCoeff
        {
            set
            {
                if (value == null || value == "")
                {
                    m_bulkCoeff = null;
                }
                else
                {
                    m_bulkCoeff = double.Parse(value).ToString();
                }
            }
            get
            {
                return m_bulkCoeff;
            }
        }

        public string WallCoeff
        {
            set
            {
                if (value == null || value == "")
                {
                    m_wallCoeff = null;
                }
                else
                {
                    m_wallCoeff = double.Parse(value).ToString();
                }
            }
            get
            {
                return m_wallCoeff;
            }
        }

        public object Flow
        {
            get
            {
                return base.m_analysisFlow;
            }
        }

        public object Velocity
        {
            get
            {
                return base.m_analysisVelocity;
            }
        }

        public object UnitHeadloss
        {
            get
            {
                return base.m_analysisHeadloss;
            }
        }

        public object FrictionFactor
        {
            get
            {
                return base.m_analysisFrictionFactor;
            }
        }

        public object ReactionRate
        {
            get
            {
                return base.m_analysisReactionRate;
            }
        }

        public object Quality
        {
            get
            {
                return base.m_analysisQuality;
            }
        }

        public object Status
        {
            get
            {
                return base.m_analysisStatus;
            }
        }

    }
}
