﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// curve section item
    /// 모델 성능 곡선 (x : 유량, y : 양정)
    /// </summary>
    public class Curve : SectionItemBase
    {
        private double m_xvalue = double.NaN;
        private double m_yvalue = double.NaN;
        private string m_comment = "";

        public Curve(SectionBase section) : base(section)
        {
        }

        public override string ID
        {
            get => base.ID;
            set => base.ID = value;
        }

        public double XValue
        {
            set
            {
                this.m_xvalue = value;
            }
            get
            {
                return this.m_xvalue;
            }
        }

        public double YValue
        {
            set
            {
                this.m_yvalue = value;
            }
            get
            {
                return this.m_yvalue;
            }
        }

        public string Comment
        {
            set
            {
                m_comment = value;
            }
            get
            {
                return m_comment;
            }
        }
    }
}
