﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// mixing section item
    /// 모델 내 물혼합 설정
    /// </summary>
    public class Mixing : SectionItemBase
    {
        private string m_model = null;

        public Mixing(SectionBase section) : base(section)
        {
            this.m_model = "";
        }

        public override string ID { get => base.ID; set => base.ID = value; }

        public string Model
        {
            set
            {
                this.m_model = value;
            }
            get
            {
                return this.m_model;
            }
        }
    }
}
