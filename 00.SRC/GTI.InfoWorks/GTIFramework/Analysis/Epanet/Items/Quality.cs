﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// quality section item
    /// 모델 내 수질 설정
    /// </summary>
    public class Quality : SectionItemBase
    {
        private double m_initQual;

        public Quality(SectionBase section)
            : base(section)
        {
            m_initQual = double.NaN;
        }

        public override string ID { get => base.ID; set => base.ID = value; }

        public double InitQual
        {
            set
            {
                m_initQual = value;
            }
            get
            {
                return m_initQual;
            }
        }
    }
}
