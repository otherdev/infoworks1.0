﻿namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    public class Node : SectionItemBase, INode
    {
        public Node(SectionBase section) : base(section) { }

        protected double m_initQuality = double.NaN;
        protected string m_sourceType = null;
        protected string m_sourceQuality = null;
        protected string m_sourcePattern = null;

        protected double m_x;
        protected double m_y;

        protected object m_analysisDemand = "#N/A";
        protected object m_analysisHead = "#N/A";
        protected object m_analysisPressure = "#N/A";
        protected object m_analysisQuality = "#N/A";

        public double InitQuality
        {
            get { return m_initQuality; }
            set { m_initQuality = value; }
        }

        public string SourceType
        {
            get { return m_sourceType; }
            set { m_sourceType = value; }
        }

        public string SourceQuality
        {
            get { return m_sourceQuality; }
            set { m_sourceQuality = value; }
        }

        public string SourcePattern
        {
            get { return m_sourcePattern; }
            set { m_sourcePattern = value; }
        }

        public void SetAnalysisDemand(float value)
        {
            m_analysisDemand = value;
        }

        public void SetAnalysisHead(float value)
        {
            m_analysisHead = value;
        }

        public void SetAnalysisPressure(float value)
        {
            m_analysisPressure = value;
        }

        public void SetAnalysisQuality(float value)
        {
            m_analysisQuality = value;
        }

        #region INode 멤버
        public virtual double X
        {
            get
            {
                return m_x;
            }
            set
            {
                m_x = value;
            }
        }

        public virtual double Y
        {
            get
            {
                return m_y;
            }
            set
            {
                m_y = value;
            }
        }
        #endregion
    }
}
