﻿using System.Drawing;

namespace GTIFramework.Analysis.Epanet.Items
{
    [System.Serializable]
    /// <summary>
    /// backdrop section item
    /// 맵 시작 영역, 배경파일정보, 맵시작 위치 관리
    /// </summary>
    public class Backdrop : SectionItemBase
    {
        private PointF m_lowerLeft;
        private PointF m_upperRight;
        private string m_units;
        private string m_filepath;
        private PointF m_offset;

        public Backdrop(SectionBase section) : base(section)
        {
            m_lowerLeft.X = 0.00F;
            m_lowerLeft.Y = 0.00F;
            m_upperRight.X = 10000.00F;
            m_upperRight.Y = 10000.00F;
            m_filepath = string.Empty;
            m_offset.X = 0.00F;
            m_offset.Y = 0.00F;
        }

        /// <summary>
        /// 맵 영역의 낮은 포인트 위치
        /// </summary>
        public PointF LowerLeft
        {
            set
            {
                this.m_lowerLeft = value;
            }
            get
            {
                return this.m_lowerLeft;
            }
        }

        /// <summary>
        /// 맵 영역의 높은 포인트 위치
        /// </summary>
        public PointF UpperRight
        {
            set
            {
                this.m_upperRight = value;
            }
            get
            {
                return this.m_upperRight;
            }
        }

        /// <summary>
        /// 단위
        /// </summary>
        public string Units
        {
            set
            {
                this.m_units = value;
            }
            get
            {
                return this.m_units;
            }
        }

        /// <summary>
        /// 배경 파일 경로
        /// </summary>
        public string Filepath
        {
            set
            {
                this.m_filepath = value;
            }
            get
            {
                return this.m_filepath;
            }
        }

        /// <summary>
        /// 맵 영역의 시작 위치
        /// </summary>
        public PointF Offset
        {
            set
            {
                this.m_offset = value;
            }
            get
            {
                return this.m_offset;
            }
        }
    }
}
