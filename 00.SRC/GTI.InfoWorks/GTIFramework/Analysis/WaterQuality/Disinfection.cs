﻿using GTIFramework.Common.MessageBox;
using System;
using System.Collections;
using System.Data;

// ====================================
// 제      목 : 소독능 분석 Class
// 작  성  자 : 이기연
// 작  성  일 : 2020-07-22
// 모 듈 설 명: 수질관리 솔루션에 들어가는 소독능 분석 Class
#region // 개발이력
/*
 *  구분 : 신규개발
 *  일자 : 2020-07-22
 *  작성자 : 이기연
 *  내용 : 최초작성
 *  이슈 : 
 */
#endregion
// ====================================

namespace GTIFramework.Analysis.WaterQuality
{
    public class Disinfection
    {
        //000001	취수장
        //000002	도수
        //000003	전오존
        //000004	착수정
        //000005	약품혼화지
        //000006	응집지
        //000007	침전지
        //000008	여과지
        //000009	후오존
        //000010	입상활성탄지
        //000011	정수지
        //000012	송수관로
        //000013	배수지

        public Disinfection()
        {

        }

        public DataTable DSNFRun(DataTable dtdata, Hashtable htparam)
        {
            try
            {
                double dlen;//길이
                double dbt;//폭 | 반지름
                double dhg;//높이
                double dlev;//수심

                double ddnsty;//소독제 농도
                double dflow;//유량
                double dph;//ph
                double dwt;//수온
                double dtub;//탁도

                string INTV_GBN; //시설("F"), 관("P") 구분

                double? dk = null;//환산계수
                double? dusevol = null;//사용용량
                double? dstaytime = null;//체류시간
                double? dCT = null;//CT값 산출

                double? dVIRUS = null;//VIRUS 산출
                double? dGIARDIA = null;//GIARDIA 산출
                double? dCRYPORI = null;//CRYPORI 산출

                double? dVIRUS_INATV = null;//VIRUS_INATV 산출
                double? dGIARDIA_INATV = null;//GIARDIA_INATV 산출
                double? dCRYPORI_INATV = null;//CRYPORI_INATV 산출

                DataTable dtresult = new DataTable();
                Hashtable htresult = new Hashtable();
                dtresult = dtdata.Copy();
                
                //시설, 관 구분
                switch (htparam["INTV_CD"].ToString())
                {
                    case "000002": //도수
                        INTV_GBN = "P";
                        break;
                    case "000012": //송수관로
                        INTV_GBN = "P";
                        break;
                    default:
                        INTV_GBN = "F";
                        break;
                }

                //입력 자료 분석
                if (double.TryParse(htparam["LEN"].ToString(), out dlen)
                    && double.TryParse(htparam["BT"].ToString(), out dbt)
                    && double.TryParse(htparam["HG"].ToString(), out dhg))
                {
                    foreach (DataRow dr in dtresult.Rows)
                    {
                        dk = null;//환산계수
                        dusevol = null;//사용용량
                        dstaytime = null;//체류시간
                        dCT = null;//CT값 산출

                        dVIRUS = null;//VIRUS 산출
                        dGIARDIA = null;//GIARDIA 산출
                        dCRYPORI = null;//CRYPORI 산출

                        dVIRUS_INATV = null;//VIRUS_INATV 산출
                        dGIARDIA_INATV = null;//GIARDIA_INATV 산출
                        dCRYPORI_INATV = null;//CRYPORI_INATV 산출

                        #region 장비환산계수
                        if (INTV_GBN.Equals("F"))
                        {
                            //길이/폭 환산계수(시설) 산출
                            dk = KAnal((dlen / dbt));

                            if (double.TryParse(dr["WAL"].ToString(), out dlev))
                                dusevol = dlen * dbt * dlev;
                            //else
                            //    continue;
                        }
                        else if (INTV_GBN.Equals("P"))
                        {
                            //환산계수(관) 산출 (1.0)
                            dk = 1.0d;
                            dusevol = dlen * dbt * Math.Pow(Math.PI, 2);
                        }

                        #region //테스트
                        //dtresult.Rows[0]["USE_VOL"] = 5600;
                        //dtresult.Rows[1]["USE_VOL"] = 6880;
                        //dtresult.Rows[2]["USE_VOL"] = 7680;
                        //dtresult.Rows[3]["USE_VOL"] = 6720;
                        //dtresult.Rows[4]["USE_VOL"] = 8000;
                        //dtresult.Rows[5]["USE_VOL"] = 5440;

                        //switch (dtresult.Rows.IndexOf(dr))
                        //{
                        //    case 0:
                        //        dusevol = 5600;
                        //        break;
                        //    case 1:
                        //        dusevol = 6880;
                        //        break;
                        //    case 2:
                        //        dusevol = 7680;
                        //        break;
                        //    case 3:
                        //        dusevol = 6720;
                        //        break;
                        //    case 4:
                        //        dusevol = 8000;
                        //        break;
                        //    case 5:
                        //        dusevol = 5440;
                        //        break;
                        //    default:
                        //        dusevol = 0;
                        //        break;
                        //}
                        #endregion

                        if (dk == null || dusevol == null)
                            continue;
                        dr["BT_PER"] = Math.Round(Convert.ToDouble(dk), 2);
                        dr["USE_VOL"] = Math.Round(Convert.ToDouble(dusevol), 2);
                        #endregion

                        if (double.TryParse(dr["DSNF_DNSTY"].ToString(), out ddnsty)
                            && double.TryParse(dr["FLOW"].ToString(), out dflow)
                            && double.TryParse(dr["PH"].ToString(), out dph)
                            && double.TryParse(dr["WT"].ToString(), out dwt)
                            && double.TryParse(dr["TUB"].ToString(), out dtub))
                        {
                            #region 체류시간 계산
                            if (dk > 0)
                                dstaytime = dusevol / dflow * 60;

                            if (dstaytime == null)
                                continue;
                            dr["STAY_TM"] = Math.Round(Convert.ToDouble(dstaytime), 2);
                            #endregion

                            #region 실시간 CT값 계산
                            dCT = dstaytime * ddnsty * dk;

                            if (dCT == null)
                                continue;
                            dr["CT_VAL"] = Math.Round(Convert.ToDouble(dCT), 2);
                            #endregion

                            #region 제거율
                            //000001  염소(차염)
                            //000002  이산화염소
                            //000003  오존
                            //000004  자외선
                            switch (htparam["INTV_CD"].ToString())
                            {
                                case "000001":
                                    //바이러스 제거율
                                    if (dph > 9.0d)
                                        dVIRUS = ((dCT * Math.Exp(0.071 * dwt)) - 0.21) / 22.37;
                                    else
                                        dVIRUS = ((dCT * Math.Exp(0.071 * dwt)) - 0.42) / 2.94;

                                    //지아디아 제거율
                                    if (ddnsty <= 0.4d)
                                        dGIARDIA = dCT / (0.2828 * Math.Pow(dph, 2.69) * Math.Pow(0.4, 0.15) * Math.Pow(0.933, (dwt - 5)));
                                    else if (ddnsty > 0.4d)
                                        dGIARDIA = dCT / (0.2828 * Math.Pow(dph, 2.69) * Math.Pow(ddnsty, 0.15) * Math.Pow(0.933, (dwt - 5)));

                                    //크립토스포리디움 제거율

                                    break;
                                case "000002":
                                    //바이러스 제거율
                                    dVIRUS = ((dCT * Math.Exp(0.072 * dwt)) + 35.15) / 21.25;

                                    //지아디아 제거율
                                    if (ddnsty > 1d)
                                        dGIARDIA = ((dCT * Math.Pow(dwt, 0.49)) + 0.18) / 23.85;
                                    else if (ddnsty <= 1d)
                                        dGIARDIA = (dCT + 0.18) / 23.85;

                                    //크립토스포리디움 제거율
                                    dCRYPORI = 0.001506 * Math.Pow(1.09116, dwt) * dCT;

                                    break;
                                case "000003":
                                    //바이러스 제거율
                                    dVIRUS = ((dCT * Math.Exp(0.068 * dwt)) - 0.01) / 0.47;

                                    //지아디아 제거율
                                    dGIARDIA = ((dCT * Math.Exp(0.072 * dwt)) + 0.01) / 0.98;

                                    //크립토스포리디움 제거율
                                    dCRYPORI = 0.0397 * Math.Pow(1.0957, dwt) * dCT;

                                    break;
                                case "000004":
                                    //바이러스 제거율

                                    //지아디아 제거율

                                    //크립토스포리디움 제거율

                                    break;
                                default:
                                    return null;
                            }

                            if (Math.Sign(Convert.ToDouble(dVIRUS)) == -1)
                                dVIRUS = 0;

                            if (Math.Sign(Convert.ToDouble(dGIARDIA)) == -1)
                                dGIARDIA = 0;

                            if (Math.Sign(Convert.ToDouble(dCRYPORI)) == -1)
                                dCRYPORI = 0;

                            dr["VIRUS_CLRRT"] = Math.Round(Convert.ToDouble(dVIRUS), 2);
                            dr["GIARDIA_CLRRT"] = Math.Round(Convert.ToDouble(dGIARDIA), 2);
                            dr["CRYPORI_CLRRT"] = Math.Round(Convert.ToDouble(dCRYPORI), 2);
                            #endregion

                            #region 불활성화비
                            //000001 급속여과
                            //000002 직접여과
                            //000003 완속여과
                            //000004 소독
                            //000005 기타여과
                            //000006 정밀여과(MF)
                            //000007 한외여과(UF)
                            //000008 나노여과(NF)
                            //000009 역삼투(RO)
                            switch (htparam["FILTER_CD"].ToString())
                            {
                                case "000001":
                                    if (ddnsty > 0)
                                    {
                                        if (dVIRUS != null)
                                            dVIRUS_INATV = dVIRUS / 2;
                                        if (dGIARDIA != null)
                                            dGIARDIA_INATV = dGIARDIA / 0.5;
                                        if (dCRYPORI != null)
                                            dCRYPORI_INATV = dCRYPORI / 2;
                                    }
                                    break;
                                case "000002":
                                    if (ddnsty > 0)
                                    {
                                        if (dVIRUS != null)
                                            dVIRUS_INATV = dVIRUS / 2;
                                        if (dGIARDIA != null)
                                            dGIARDIA_INATV = dGIARDIA / 2;
                                        if (dCRYPORI != null)
                                            dCRYPORI_INATV = dCRYPORI / 2;
                                    }
                                    break;
                                case "000003":
                                    if (ddnsty > 0)
                                    {
                                        if (dVIRUS != null)
                                            dVIRUS_INATV = dVIRUS / 2;
                                        if (dGIARDIA != null)
                                            dGIARDIA_INATV = dGIARDIA / 2;
                                        if (dCRYPORI != null)
                                            dCRYPORI_INATV = dCRYPORI / 2;
                                    }
                                    break;
                                case "000004":
                                    if (ddnsty > 0)
                                    {

                                        if (dVIRUS != null)
                                            dVIRUS_INATV = dVIRUS / 3;
                                        if (dGIARDIA != null)
                                            dGIARDIA_INATV = dGIARDIA / 3;
                                        if (dCRYPORI != null)
                                        {
                                            if (ddnsty > 5.8)
                                                dCRYPORI_INATV = dCRYPORI / 2;
                                            else
                                                dCRYPORI_INATV = 0;
                                        }
                                    }
                                    break;
                                case "000005":
                                    if (ddnsty > 0)
                                    {
                                        if (dVIRUS != null)
                                            dVIRUS_INATV = dVIRUS / 2;
                                        if (dGIARDIA != null)
                                            dGIARDIA_INATV = dGIARDIA / 2;
                                        if (dCRYPORI != null)
                                            dCRYPORI_INATV = dCRYPORI / 2;
                                    }
                                    break;
                                case "000006":
                                    if (ddnsty > 0)
                                    {
                                        if (dVIRUS != null)
                                            dVIRUS_INATV = dVIRUS / 0.5;
                                        if (dGIARDIA != null)
                                            dGIARDIA_INATV = dGIARDIA / 0.5;
                                        if (dCRYPORI != null)
                                            dCRYPORI_INATV = dCRYPORI / 2;
                                    }
                                    break;
                                case "000007":
                                    if (ddnsty > 0)
                                    {
                                        if (dVIRUS != null)
                                            dVIRUS_INATV = dVIRUS / 0.5;
                                        if (dGIARDIA != null)
                                            dGIARDIA_INATV = dGIARDIA / 0.5;
                                        if (dCRYPORI != null)
                                            dCRYPORI_INATV = dCRYPORI / 2;
                                    }
                                    break;
                                case "000008":
                                    if (ddnsty > 0)
                                    {
                                        dVIRUS_INATV = 1;
                                        dGIARDIA_INATV = 1;
                                        dCRYPORI_INATV = 1;
                                    }
                                    break;
                                case "000009":
                                    if (ddnsty > 0)
                                    {
                                        dVIRUS_INATV = 1;
                                        dGIARDIA_INATV = 1;
                                        dCRYPORI_INATV = 1;
                                    }
                                    break;
                            }
                            dr["VIRUS_INATV"] = Math.Round(Convert.ToDouble(dVIRUS_INATV), 2);
                            dr["GIARDIA_INATV"] = Math.Round(Convert.ToDouble(dGIARDIA_INATV), 2);
                            dr["CRYPORI_INATV"] = Math.Round(Convert.ToDouble(dCRYPORI_INATV), 2);
                            #endregion

                            //만족, 불만족
                            if (Math.Round(Convert.ToDouble(dVIRUS_INATV), 2) >= 1 && Math.Round(Convert.ToDouble(dGIARDIA_INATV), 2) >= 1)
                                dr["RSLT"] = "만족";
                            else
                                dr["RSLT"] = "불만족";
                        }
                        else
                            continue;
                    }
                }
                else
                    return null;

                return dtresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }

        public Hashtable DSNFStatic(DataTable dtdata, Hashtable htparam)
        {
            try
            {
                Hashtable htresult = new Hashtable();

                htresult.Add("DSNF_DNSTY_RSLT", "");    //소독제 농도 만족, 불만족
                htresult.Add("DSNF_DNSTY_VAL", "");     //소독제 농도 MIN (2)
                htresult.Add("FLOW_VAL", "");           //유량 MAX (3)
                htresult.Add("PH_VAL", "");             //PH MAX (4)
                htresult.Add("WT_VAL", "");             //수온 MIN (5)
                htresult.Add("TUB_RSLT", "");           //탁도 만족, 불만족
                htresult.Add("TUB_VAL", "");            //탁도 MAX (6)
                htresult.Add("BT_PER", "");             //장폭비 (1)
                htresult.Add("USE_VOL", "");            //사용용량 MIN (7)
                htresult.Add("STAY_TM", "");            //htresult["BT_PER"]가 0이상 이면 (htresult["USE_VOL"] / htresult["FLOW_VAL"] * 60)
                htresult.Add("CT_VAL", "");             //htresult["DSNF_DNSTY_VAL"]가 0이상 이면 (htresult["STAY_TM"] * htresult["DSNF_DNSTY_VAL"] * htresult["BT_PER"])
                htresult.Add("VIRUS_CLRRT", "");        //계산 로직 적용     
                htresult.Add("GIARDIA_CLRRT", "");      //계산 로직 적용       
                htresult.Add("CRYPORI_CLRRT", "");      //계산 로직 적용       
                htresult.Add("VIRUS_INATV", "");        //계산 로직 적용     
                htresult.Add("GIARDIA_INATV", "");      //계산 로직 적용
                htresult.Add("CRYPORI_INATV", "");      //계산 로직 적용
                htresult.Add("RSLT", "");               //(만족,불만족)

                double? dBT_PER = null;
                double? dMINDSNF_DNSTY_VAL = null;
                double? dMAXDSNF_DNSTY_VAL = null;
                double? dFLOW_VAL = null;
                double? dPH_VAL = null;
                double? dWT_VAL = null;
                double? dTUB_VAL = null;
                double? dUSE_VOL = null;

                double? dSTAY_TM = null;
                double? dCT_VAL = null;
                double? dVIRUS_CLRRT = null;
                double? dGIARDIA_CLRRT = null;
                double? dCRYPORI_CLRRT = null;
                double? dVIRUS_INATV = null;
                double? dGIARDIA_INATV = null;
                double? dCRYPORI_INATV = null;

                double dtemp;

                foreach (DataRow dr in dtdata.Rows)
                {
                    #region MIN, MAX
                    //장폭비 (1)
                    if (dtdata.Rows.IndexOf(dr) == 0)
                    {
                        if (double.TryParse(dr["BT_PER"].ToString(), out dtemp))
                            dBT_PER = dtemp;
                    }

                    //소독제 농도 MIN (2)
                    if (double.TryParse(dr["DSNF_DNSTY"].ToString(), out dtemp))
                    {
                        if (dMINDSNF_DNSTY_VAL == null || dMINDSNF_DNSTY_VAL > dtemp)
                            dMINDSNF_DNSTY_VAL = dtemp;

                        if (dMAXDSNF_DNSTY_VAL == null || dMAXDSNF_DNSTY_VAL < dtemp)
                            dMAXDSNF_DNSTY_VAL = dtemp;
                    }

                    //유량 MAX (3)
                    if (double.TryParse(dr["FLOW"].ToString(), out dtemp))
                    {
                        if (dFLOW_VAL == null || dFLOW_VAL < dtemp)
                            dFLOW_VAL = dtemp;
                    }

                    //PH MAX (4)
                    if (double.TryParse(dr["PH"].ToString(), out dtemp))
                    {
                        if (dPH_VAL == null || dPH_VAL < dtemp)
                            dPH_VAL = dtemp;
                    }

                    //수온 MIN (5)
                    if (double.TryParse(dr["WT"].ToString(), out dtemp))
                    {
                        if (dWT_VAL == null || dWT_VAL > dtemp)
                            dWT_VAL = dtemp;
                    }

                    //탁도 MAX (6)
                    if (double.TryParse(dr["TUB"].ToString(), out dtemp))
                    {
                        if (dTUB_VAL == null || dTUB_VAL < dtemp)
                            dTUB_VAL = dtemp;
                    }

                    //사용용량 MIN (7)
                    if (double.TryParse(dr["USE_VOL"].ToString(), out dtemp))
                    {
                        if (dUSE_VOL == null || (dUSE_VOL > dtemp & dtemp != 0))
                            dUSE_VOL = dtemp;
                    }
                    #endregion
                }

                //STAY_TM
                //htresult["BT_PER"]가 0이상 이면 (htresult["USE_VOL"] / htresult["FLOW_VAL"] * 60)
                if (dBT_PER != null & dUSE_VOL != null & dFLOW_VAL != null)
                {
                    if (dBT_PER != 0)
                        dSTAY_TM = dUSE_VOL / dFLOW_VAL * 60;
                }

                //CT_VAL
                //htresult["DSNF_DNSTY_VAL"]가 0이상 이면 (htresult["STAY_TM"] * htresult["DSNF_DNSTY_VAL"] * htresult["BT_PER"])
                if (dMINDSNF_DNSTY_VAL != null & dSTAY_TM != null & dBT_PER != null)
                {
                    if (dMINDSNF_DNSTY_VAL > 0)
                        dCT_VAL = dSTAY_TM * dMINDSNF_DNSTY_VAL * dBT_PER;
                }

                #region 제거율
                //000001  염소(차염)
                //000002  이산화염소
                //000003  오존
                //000004  자외선
                switch (htparam["INTV_CD"].ToString())
                {
                    case "000001":

                        if (dWT_VAL != null & dCT_VAL != null)
                        {
                            //바이러스 제거율
                            if (dPH_VAL != null)
                            {
                                if (dPH_VAL > 9.0d)
                                    dVIRUS_CLRRT = ((dCT_VAL * Math.Exp(0.071 * Convert.ToDouble(dWT_VAL))) - 0.21) / 22.37;
                                else
                                    dVIRUS_CLRRT = ((dCT_VAL * Math.Exp(0.071 * Convert.ToDouble(dWT_VAL))) - 0.42) / 2.94;
                            }

                            //지아디아 제거율
                            if (dMINDSNF_DNSTY_VAL != null)
                            {
                                if (dMINDSNF_DNSTY_VAL <= 0.4d)
                                    dGIARDIA_CLRRT = dCT_VAL / (0.2828 * Math.Pow(Convert.ToDouble(dPH_VAL), 2.69) * Math.Pow(0.4, 0.15) * Math.Pow(0.933, (Convert.ToDouble(dWT_VAL) - 5)));
                                else if (dMINDSNF_DNSTY_VAL > 0.4d)
                                    dGIARDIA_CLRRT = dCT_VAL / (0.2828 * Math.Pow(Convert.ToDouble(dPH_VAL), 2.69) * Math.Pow(Convert.ToDouble(dMINDSNF_DNSTY_VAL), 0.15) * Math.Pow(0.933, (Convert.ToDouble(dWT_VAL) - 5)));
                            }

                            //크립토스포리디움 제거율
                        }
                        break;
                    case "000002":
                        if (dCT_VAL != null & dWT_VAL != null)
                        {
                            //바이러스 제거율
                            dVIRUS_CLRRT = ((dCT_VAL * Math.Exp(0.072 * Convert.ToDouble(dWT_VAL))) + 35.15) / 21.25;

                            //지아디아 제거율
                            if (dMINDSNF_DNSTY_VAL != null)
                            {
                                if (dMINDSNF_DNSTY_VAL > 1d)
                                    dGIARDIA_CLRRT = ((dCT_VAL * Math.Pow(Convert.ToDouble(dWT_VAL), 0.49)) + 0.18) / 23.85;
                                else if (dMINDSNF_DNSTY_VAL <= 1d)
                                    dGIARDIA_CLRRT = (dCT_VAL + 0.18) / 23.85;
                            }

                            //크립토스포리디움 제거율
                            dCRYPORI_CLRRT = 0.001506 * Math.Pow(1.09116, Convert.ToDouble(dWT_VAL)) * dCT_VAL;
                        }
                        break;
                    case "000003":
                        if (dWT_VAL != null & dCT_VAL != null)
                        {
                            //바이러스 제거율
                            dVIRUS_CLRRT = ((dCT_VAL * Math.Exp(0.068 * Convert.ToDouble(dWT_VAL))) - 0.01) / 0.47;

                            //지아디아 제거율
                            dGIARDIA_CLRRT = ((dCT_VAL * Math.Exp(0.072 * Convert.ToDouble(dWT_VAL))) + 0.01) / 0.98;

                            //크립토스포리디움 제거율
                            dCRYPORI_CLRRT = 0.0397 * Math.Pow(1.0957, Convert.ToDouble(dWT_VAL)) * dCT_VAL;
                        }
                        break;
                    case "000004":
                        //바이러스 제거율

                        //지아디아 제거율

                        //크립토스포리디움 제거율

                        break;
                    default:
                        return null;
                }

                if (Math.Sign(Convert.ToDouble(dVIRUS_CLRRT)) == -1)
                    dVIRUS_CLRRT = 0;

                if (Math.Sign(Convert.ToDouble(dGIARDIA_CLRRT)) == -1)
                    dGIARDIA_CLRRT = 0;

                if (Math.Sign(Convert.ToDouble(dCRYPORI_CLRRT)) == -1)
                    dCRYPORI_CLRRT = 0;
                #endregion

                #region 불활성화비
                //000001 급속여과
                //000002 직접여과
                //000003 완속여과
                //000004 소독
                //000005 기타여과
                //000006 정밀여과(MF)
                //000007 한외여과(UF)
                //000008 나노여과(NF)
                //000009 역삼투(RO)
                switch (htparam["FILTER_CD"].ToString())
                {
                    case "000001":
                        if (dVIRUS_CLRRT != null)
                            dVIRUS_INATV = dVIRUS_CLRRT / 2;
                        if (dGIARDIA_CLRRT != null)
                            dGIARDIA_INATV = dGIARDIA_CLRRT / 2;
                        if (dCRYPORI_CLRRT != null)
                            dCRYPORI_INATV = dCRYPORI_CLRRT / 2;
                        break;
                    case "000002":
                        if (dVIRUS_CLRRT != null)
                            dVIRUS_INATV = dVIRUS_CLRRT / 2;
                        if (dGIARDIA_CLRRT != null)
                            dGIARDIA_INATV = dGIARDIA_CLRRT / 0.5;
                        if (dCRYPORI_CLRRT != null)
                            dCRYPORI_INATV = dCRYPORI_CLRRT / 2;
                        break;
                    case "000003":
                        if (dVIRUS_CLRRT != null)
                            dVIRUS_INATV = dVIRUS_CLRRT / 2;
                        if (dGIARDIA_CLRRT != null)
                            dGIARDIA_INATV = dGIARDIA_CLRRT / 2;
                        if (dCRYPORI_CLRRT != null)
                            dCRYPORI_INATV = dCRYPORI_CLRRT / 2;
                        break;
                    case "000004":
                        if (dVIRUS_CLRRT != null)
                            dVIRUS_INATV = dVIRUS_CLRRT / 3;
                        if (dGIARDIA_CLRRT != null)
                            dGIARDIA_INATV = dGIARDIA_CLRRT / 3;
                        if (dCRYPORI_CLRRT != null)
                        {
                            if (dMINDSNF_DNSTY_VAL > 5.8)
                                dCRYPORI_INATV = dCRYPORI_CLRRT / 2;
                            else
                                dCRYPORI_INATV = 0;
                        }
                        break;
                    case "000005":
                        if (dVIRUS_CLRRT != null)
                            dVIRUS_INATV = dVIRUS_CLRRT / 2;
                        if (dGIARDIA_CLRRT != null)
                            dGIARDIA_INATV = dGIARDIA_CLRRT / 2;
                        if (dCRYPORI_CLRRT != null)
                            dCRYPORI_INATV = dCRYPORI_CLRRT / 2;
                        break;
                    case "000006":
                        if (dVIRUS_CLRRT != null)
                            dVIRUS_INATV = dVIRUS_CLRRT / 0.5;
                        if (dGIARDIA_CLRRT != null)
                            dGIARDIA_INATV = dGIARDIA_CLRRT / 0.5;
                        if (dCRYPORI_CLRRT != null)
                            dCRYPORI_INATV = dCRYPORI_CLRRT / 2;
                        break;
                    case "000007":
                        if (dVIRUS_CLRRT != null)
                            dVIRUS_INATV = dVIRUS_CLRRT / 0.5;
                        if (dGIARDIA_CLRRT != null)
                            dGIARDIA_INATV = dGIARDIA_CLRRT / 0.5;
                        if (dCRYPORI_CLRRT != null)
                            dCRYPORI_INATV = dCRYPORI_CLRRT / 2;
                        break;
                    case "000008":
                        dVIRUS_INATV = 1;
                        dGIARDIA_INATV = 1;
                        dCRYPORI_INATV = 1;
                        break;
                    case "000009":
                        dVIRUS_INATV = 1;
                        dGIARDIA_INATV = 1;
                        dCRYPORI_INATV = 1;
                        break;
                }
                #endregion

                htresult["BT_PER"] = Math.Round(Convert.ToDouble(dBT_PER), 2);                       //장폭비 (1)
                htresult["DSNF_DNSTY_VAL"] = dMINDSNF_DNSTY_VAL;                                     //소독제 농도 MIN (2)
                htresult["FLOW_VAL"] = dFLOW_VAL;                                                    //유량 MAX (3)
                htresult["PH_VAL"] = dPH_VAL;                                                        //PH MAX (4)
                htresult["WT_VAL"] = dWT_VAL;                                                        //수온 MIN (5)
                htresult["TUB_VAL"] = dTUB_VAL;                                                      //탁도 MAX (6)
                htresult["USE_VOL"] = Math.Round(Convert.ToDouble(dUSE_VOL), 2);                     //사용용량 MIN (7)

                htresult["STAY_TM"] = Math.Round(Convert.ToDouble(dSTAY_TM), 2);                     //htresult["BT_PER"]가 0이상 이면 (htresult["USE_VOL"] / htresult["FLOW_VAL"] * 60)
                htresult["CT_VAL"] = Math.Round(Convert.ToDouble(dCT_VAL), 2);                       //htresult["DSNF_DNSTY_VAL"]가 0이상 이면 (htresult["STAY_TM"] * htresult["DSNF_DNSTY_VAL"] * htresult["BT_PER"])

                htresult["VIRUS_CLRRT"] = Math.Round(Convert.ToDouble(dVIRUS_CLRRT), 2);
                htresult["GIARDIA_CLRRT"] = Math.Round(Convert.ToDouble(dGIARDIA_CLRRT), 2);
                htresult["CRYPORI_CLRRT"] = Math.Round(Convert.ToDouble(dCRYPORI_CLRRT), 2);
                htresult["VIRUS_INATV"] = Math.Round(Convert.ToDouble(dVIRUS_INATV), 2);
                htresult["GIARDIA_INATV"] = Math.Round(Convert.ToDouble(dGIARDIA_INATV), 2);
                htresult["CRYPORI_INATV"] = Math.Round(Convert.ToDouble(dCRYPORI_INATV), 2);

                //일만족, 불만족
                if (Math.Round(Convert.ToDouble(dVIRUS_INATV), 2) >= 1 && Math.Round(Convert.ToDouble(dGIARDIA_INATV), 2) >= 1)
                    htresult["RSLT"] = "만족";
                else
                    htresult["RSLT"] = "불만족";

                return htresult;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return null;
            }
        }

        //환산계수(K) 계산
        private double KAnal(double dval)
        {
            double bkresult = 0.1;

            try
            {
                if (dval < 2)
                    bkresult = 0.10;
                else if (2 <= dval && dval < 5)
                    bkresult = 0.20;
                else if (5 <= dval && dval < 10)
                    bkresult = 0.30;
                else if (10 <= dval && dval < 15)
                    bkresult = 0.40;
                else if (15 <= dval && dval < 20)
                    bkresult = 0.50;
                else if (20 <= dval && dval < 30)
                    bkresult = 0.60;
                else if (30 <= dval && dval < 40)
                    bkresult = 0.65;
                else if (40 <= dval && dval < 50)
                    bkresult = 0.70;
                else if (50 <= dval && dval < 60)
                    bkresult = 0.75;
                else if (60 <= dval && dval < 70)
                    bkresult = 0.80;
                else if (70 <= dval && dval < 80)
                    bkresult = 0.83;
                else if (80 <= dval && dval < 90)
                    bkresult = 0.86;
                else if (90 <= dval && dval < 100)
                    bkresult = 0.88;
                else if (100 <= dval && dval < 50)
                    bkresult = 0.91;
                else if (110 <= dval)
                    bkresult = 1.00;

                return bkresult;
            }
            catch (Exception)
            {
                return bkresult;
            }
        }
    }
}
