﻿using DevExpress.Xpf.Accordion;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using GTI.InfoWorks.Main.View.Popup;
using GTI.InfoWorks.Models.DataMng.Work;
using GTI.InfoWorks.Models.Main.Work;
using GTIFramework.Analysis.Epanet;
using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Converters.TatukGeo.Epanet;
using GTIFramework.Common.Utils.ViewEffect;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace GTI.InfoWorks.Main
{
    public class MainWinViewModel : BindableBase
    {
        [DllImport("user32.dll")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        MainWork work = new MainWork();
        INPWork inpwork = new INPWork();
        Hashtable htconditions = new Hashtable();
        MainWin mainwin;

        private readonly IRegionManager regionManager;
        private bool bMenuShowHiden = true;
        private bool bQuickShowHiden = true;

        string strSelectMenu = string.Empty;

        public static DataTable dtMenuList = new DataTable();
        DataTable dtQuickMenuList = new DataTable();
        DataTable dtcbSystem = new DataTable();

        StackPanel stQuickMenu;

        string strLastSEQ = string.Empty;

        protected Thread thread;  //쓰레드 선언
        protected System.Timers.Timer timer; //자동쓰레드 타이머
        DateTime datetimenow;
        Hashtable htAlertconditions = new Hashtable();
        DataTable dtAlert = new DataTable();
        MainWork Alertwork = new MainWork();
        Button btnAlert;
        PopupAlertContent popupcontent;
        Button btnWindowSize;

        ComboBoxEdit cbModel;
        DataTable dtModelList;

        ComboBoxEdit cbGBN;
        DataTable dtGBNList;

        WaitIndicator waitindicator;

        #region ==========  Properties 정의 ========== 
        /// <summary>
        /// Loaded Event
        /// </summary>
        public DelegateCommand<object> LoadedCommand { get; set; }

        /// <summary>
        /// UnLoadedCommand
        /// </summary>
        public DelegateCommand<object> UnLoadedCommand { get; set; }

        /// <summary>
        /// Close Event
        /// </summary>
        public DelegateCommand<object> CloseCommand { get; set; }

        /// <summary>
        /// Maximize Event
        /// </summary>
        public DelegateCommand<object> MaximizeCommand { get; set; }

        /// <summary>
        /// minimize Event
        /// </summary>
        public DelegateCommand<object> MinimizeCommand { get; set; }

        /// <summary>
        /// WindowMove Event
        /// </summary>
        public DelegateCommand<object> WindowMoveCommand { get; set; }

        /// <summary>
        /// 사용자정보관리
        /// </summary>
        public DelegateCommand<object> UserInfoMngCommand { get; set; }

        /// <summary>
        /// 테마 변경 
        /// </summary>
        public DelegateCommand<object> ChangeThemeCommand { get; set; }

        /// <summary>
        /// 메뉴 show/hiden
        /// </summary>
        public DelegateCommand<object> MenuShowHidenCommand { get; set; }

        /// <summary>
        /// Quick show/hiden
        /// </summary>
        public DelegateCommand<object> QuickShowHidenCommand { get; set; }

        /// <summary>
        /// QuestionCommand
        /// </summary>
        public DelegateCommand<object> QuestionCommand { get; set; }

        /// <summary>
        /// MenuControlCommand
        /// </summary>
        public DelegateCommand<object> MenuControlCommand { get; set; }

        /// <summary>
        /// QuickMngCommand
        /// </summary>
        public DelegateCommand<object> QuickMngCommand { get; set; }

        /// <summary>
        /// InterestBlkCommand
        /// </summary>
        public DelegateCommand<object> InterestBlkCommand { get; set; }

        /// <summary>
        /// btnAlertCommand
        /// </summary>
        public DelegateCommand<object> AlertCommand { get; set; }

        /// <summary>
        /// InfomationCommand
        /// </summary>
        public DelegateCommand<object> InfomationCommand { get; set; }

        /// <summary>
        /// SearchCommand
        /// </summary>
        public DelegateCommand<object> SearchCommand { get; set; }

        /// <summary>
        /// SearchCommand
        /// </summary>
        public DelegateCommand<object> RealTimeRunCommand { get; set; }
        
        #endregion

        /// <summary>
        /// 생성자
        /// </summary>
        public MainWinViewModel(IRegionManager regionManager)
        {
            this.regionManager = regionManager;
            LoadedCommand = new DelegateCommand<object>(OnLoaded);
            UnLoadedCommand = new DelegateCommand<object>(UnLoaded);
            CloseCommand = new DelegateCommand<object>(CloseAction);
            MaximizeCommand = new DelegateCommand<object>(MaximizeAction);
            MinimizeCommand = new DelegateCommand<object>(MinimiizeAction);
            WindowMoveCommand = new DelegateCommand<object>(WindowMoveAction);
            MenuShowHidenCommand = new DelegateCommand<object>(MenuShowHidenAction);
            QuickShowHidenCommand = new DelegateCommand<object>(QuickShowHidenAction);
            QuestionCommand = new DelegateCommand<object>(QuestionAction);
            MenuControlCommand = new DelegateCommand<object>(MenuControlAction);
            UserInfoMngCommand = new DelegateCommand<object>(UserInfoMngAction);
            QuickMngCommand = new DelegateCommand<object>(QuickMngAction);
            InterestBlkCommand = new DelegateCommand<object>(InterestBlkAction);
            AlertCommand = new DelegateCommand<object>(AlertAction);
            InfomationCommand = new DelegateCommand<object>(InfomationAction);

            SearchCommand = new DelegateCommand<object>(SearchAction);
            RealTimeRunCommand = new DelegateCommand<object>(RealTimeRunAction);

        }

        #region ========== Events 정의 ==========
        bool bRDCChk = true;
        /// <summary>
        /// formLoad
        /// </summary>
        /// <param name="obj"></param>
        private void OnLoaded(object obj)
        {
            if (!bRDCChk) return;

            if (obj == null) return;

            try
            {
                bRDCChk = false;
                mainwin = obj as MainWin;

                mainwin.KeyDown += Mainwin_KeyDown;
                mainwin.ContentRendered += Mainwin_ContentRendered;

                Messages.AppNotificationService = mainwin.FindName("AppNotificationService") as DevExpress.Mvvm.UI.NotificationService;

                Logs.progressunlimit = mainwin.FindName("progressunlimit") as ProgressBar;
                stQuickMenu = mainwin.FindName("stQuickMenu") as StackPanel;
                btnAlert = mainwin.FindName("btnAlert") as Button;

                btnWindowSize = mainwin.FindName("btnWindowSize") as Button;

                cbModel = mainwin.FindName("cbModel") as ComboBoxEdit;
                cbGBN = mainwin.FindName("cbGBN") as ComboBoxEdit;

                waitindicator = mainwin.FindName("waitindicator") as WaitIndicator;

                #region 모델 선택 리스트
                #region //모델 리스트 조회
                dtModelList = inpwork.Select_INP_File_List(null);
                cbModel.ItemsSource = dtModelList;
                cbModel.SelectedIndex = 0;
                #endregion

                #region 예측구분(조건) 리스트 (ComboBox)
                //구분
                dtGBNList = new DataTable();
                dtGBNList.Columns.Add("CD");
                dtGBNList.Columns.Add("NM");

                DataRow dradd = dtGBNList.NewRow();
                dradd["CD"] = "REAL";
                dradd["NM"] = "단일시간";
                dtGBNList.Rows.Add(dradd.ItemArray);

                cbGBN.ItemsSource = dtGBNList;
                cbGBN.SelectedIndex = 0;
                #endregion 
                #endregion

                //Image imgtitle = mainwin.FindName("imgtitle") as Image;
                //imgtitle.PreviewMouseLeftButtonDown += LbTitle_PreviewMouseLeftButtonDown;

                #region 시스템 바로가기
                ComboBoxEdit cbSystem = mainwin.FindName("cbSystem") as ComboBoxEdit;
                htconditions.Clear();
                htconditions.Add("MST_CD", "000002");
                htconditions.Add("SYS_CD", "000004");
                htconditions.Add("USER_ID", Logs.strLogin_ID);

                dtcbSystem = work.Select_Installed_System(htconditions);

                DataRow dr = dtcbSystem.NewRow();
                dr["SYS_NM"] = "시스템을 선택하세요.";
                dr["SYS_CD"] = "000000";
                dtcbSystem.Rows.InsertAt(dr, 0);

                cbSystem.ItemsSource = dtcbSystem;
                cbSystem.SelectedIndex = 0;
                cbSystem.EditValueChanged += CbSystem_EditValueChanged;
                #endregion

                SearchAction(null);

                MenuDataInit(obj);

                QuickMnuBinding();

                ThemeApply.Themeapply(mainwin);

                Messages.NotificationBox("InfoWorks", "InfoWorks에 접속하셨습니다.", "InfoWorks에 접속하셨습니다.");

                ALERT_reload(DateTime.Now);

                thread = new Thread(new ThreadStart(thread_FX)) { IsBackground = true };
                thread.Name = "Alertthread";
                thread.Start();

                LbTitle_PreviewMouseLeftButtonDown(null, null);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }


        FileStream fs, fsmsx;
        Thread inpthread;
        private void inpthreadfx(object obj)
        {
            try
            {
                DataRowView selectitem = null;

                mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                    new Action(delegate ()
                    {
                        #region //조건 선택 확인
                        if (cbModel.SelectedIndex == -1)
                        {
                            Messages.ShowInfoMsgBox("모델을 선택해야 합니다");
                            return;
                        }
                        else
                            selectitem = cbModel.SelectedItem as DataRowView;

                        if (cbGBN.SelectedIndex == -1)
                        {
                            Messages.ShowInfoMsgBox("예측구분을 선택해야 합니다");
                            return;
                        }
                        #endregion

                        waitindicator.Content = "모델 불러오는중...";
                        waitindicator.DeferedVisibility = true;

                        Mouse.OverrideCursor = Cursors.Wait;
                    }));

                DateTime sdt = DateTime.Now;

                //INP경로 초기화 & 설정 (File 생성)
                string strPathINP = string.Empty;
                //strPathINP = AppDomain.CurrentDomain.BaseDirectory + "Resources\\TEMPINP\\Model" + (cbModel.SelectedItem as DataRowView).Row["INP_SEQ"] + "\\" + (cbModel.SelectedItem as DataRowView).Row["INP_SEQ"] + ".inp";
                strPathINP = AppDomain.CurrentDomain.BaseDirectory + "Resources\\TEMPINP\\Model" + selectitem.Row["INP_SEQ"] + "\\";

                #region INP 경로 및 INP 파일 생성
                DirectoryInfo di = new DirectoryInfo(strPathINP);

                if (di.Exists == false) di.Create();

                try
                {
                    strPathINP = strPathINP + selectitem.Row["INP_SEQ"] + ".inp";
                    Byte[] byteINP_FILE = selectitem.Row["INP_FLE"] as Byte[];
                    fs = new FileStream(strPathINP, FileMode.OpenOrCreate, FileAccess.Write);
                    fs.Write(byteINP_FILE, 0, byteINP_FILE.GetUpperBound(0));

                    if (selectitem.Row["MSX_FLE"] is Byte[])
                    {
                        if ((selectitem.Row["MSX_FLE"] as Byte[]).Length > 0)
                        {
                            string strPathMSX = string.Empty;

                            strPathMSX = AppDomain.CurrentDomain.BaseDirectory + "Resources\\TEMPINP\\Model" + selectitem.Row["INP_SEQ"] + "\\" + selectitem.Row["INP_SEQ"] + ".msx";
                            Byte[] byteMSX_FILE = selectitem.Row["MSX_FLE"] as Byte[];
                            fsmsx = new FileStream(strPathMSX, FileMode.OpenOrCreate, FileAccess.Write);
                            fsmsx.Write(byteMSX_FILE, 0, byteMSX_FILE.GetUpperBound(0));
                        }
                    }
                }
                catch (Exception ex)
                {
                    mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action(delegate ()
                        {
                            Messages.ShowErrMsgBoxLog(ex);
                        }));
                    
                }
                finally
                {
                    fs.Close();

                    if (selectitem.Row["MSX_FLE"] is Byte[])
                    {
                        if ((selectitem.Row["MSX_FLE"] as Byte[]).Length > 0)
                            fsmsx.Close();
                    }
                }
                #endregion

                #region //INP 파일 확인
                if (!(selectitem.Row["INP_FLE"] is Byte[]) | ((selectitem.Row["INP_FLE"] as Byte[]).Length <= 0))
                {
                    Messages.ShowInfoMsgBox("모델을 확인해야 합니다.");
                    return;
                }
                #endregion

                //INP 조회조건 바인딩 & 로드
                EpanetVar.tINPConvert = new TINPConvert(strPathINP, selectitem.Row["INP_TTL"].ToString());

                Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,,, ModelNetwork 완료");

                RealTimeRunAction(null);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
            finally
            {
                mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                    new Action(delegate ()
                    {
                        waitindicator.DeferedVisibility = false;
                        waitindicator.Content = "처리중...";
                        Mouse.OverrideCursor = null;
                    })); 
            }
        }

        /// <summary>
        /// Search Event
        /// </summary>
        /// <param name="obj"></param>
        private void SearchAction(object obj)
        {
            try
            {
                if (inpthread != null)
                {
                    inpthread.Abort();
                    inpthread = null;
                }

                inpthread = new Thread(inpthreadfx) { IsBackground = true };
                inpthread.Start();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }


        private void RealTimeRunAction(object obj)
        {
            try
            {
                mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                    new Action(delegate ()
                    {
                        waitindicator.Content = "모델 분석중...";
                    }));
                
                DateTime sdt = DateTime.Now;

                EpanetVar.tINPConvert.network.Analysis.Run();

                Console.WriteLine((DateTime.Now - sdt).TotalSeconds.ToString() + "초 ,,,,, ModelAnalysis 완료");
            }
            catch (Exception ex)
            {
                mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                    new Action(delegate ()
                    {
                        Messages.ShowErrMsgBoxLog(ex);
                    }));
            }
            finally
            {
                mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                    new Action(delegate ()
                    {
                        waitindicator.DeferedVisibility = false;
                        waitindicator.Content = "처리중...";
                    }));
            }
        }


        private void UnLoaded(object obj)
        {
            if (obj == null) return;

            try
            {
                timer.Stop();
                timer.Dispose();
                thread.Abort();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void thread_FX()
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            datetimenow = DateTime.Now;

            if (datetimenow.Second == 0)
            {
                ALERT_reload(datetimenow);
            }
        }

        private void ALERT_reload(DateTime datetime)
        {
            try
            {
                //경보리스트
                htAlertconditions.Clear();
                htAlertconditions.Add("SDT", datetime.AddDays(-1).ToString("yyyyMMddHHmmss"));
                htAlertconditions.Add("EDT", datetime.ToString("yyyyMMddHHmmss"));
                dtAlert = Alertwork.Select_ALERT_RECORD_MNG_Main(htAlertconditions);

                if (dtAlert.Rows.Count > 0)
                {
                    //그시간에 발생한 경보인지 확인
                    if (dtAlert.Rows[0]["OCCUR_DT"].ToString().Equals(datetime.ToString("yyyy-MM-dd HH:mm:ss")))
                    {
                        mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                            new Action(delegate ()
                            {
                                Messages.NotificationBox("경보", dtAlert.Rows[0]["OCCUR_DT"].ToString(), dtAlert.Rows[0]["BLK_NM"].ToString() + " " + dtAlert.Rows[0]["ALERT_CONTENTS"].ToString() + " 외 " + dtAlert.Rows[0]["CNT"].ToString() + "건 발생");
                            }));
                    }

                    mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action(delegate ()
                        {
                            btnAlert.Content = dtAlert.Rows[0]["TTCNT"].ToString();
                            btnAlert.ToolTip = "경보 " + dtAlert.Rows[0]["TTCNT"].ToString() + "건";
                            btnAlert.Style = Application.Current.FindResource("MainAlertONButton") as Style;
                        }));
                }
                else
                {
                    mainwin.Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action(delegate ()
                        {
                            btnAlert.Content = "";
                            btnAlert.ToolTip = "경보 0건";
                            btnAlert.Style = Application.Current.FindResource("MainAlertButton") as Style;
                        }));
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void AlertAction(object obj)
        {
            if (obj == null) return;

            try
            {
                popupcontent = null;

                popupcontent = new PopupAlertContent(dtAlert);
                popupcontent.Closed += Popupcontent_Closed;
                popupcontent.ShowDialog();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void Popupcontent_Closed(object sender, EventArgs e)
        {
            if (popupcontent.DialogResult == true)
            {
                ALERT_reload(DateTime.Now);
            }
        }

        private void CbSystem_EditValueChanged(object sender, EditValueChangedEventArgs e)
        {
            try
            {
                if ((sender as ComboBoxEdit).SelectedIndex == 0) return;

                string strcd = (sender as ComboBoxEdit).EditValue.ToString();
                (sender as ComboBoxEdit).SelectedIndex = 0;

                string strpgnm = string.Empty;
                string strpath = string.Empty;

                switch (strcd)
                {
                    //InfoView
                    case "000002":
                        //Messages.ShowInfoMsgBox("InfoView");
                        strpgnm = "InfoView";
                        strpath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\")) + @"InfoView\InfoView";
                        break;
                    //InfoManager
                    case "000003":
                        //Messages.ShowInfoMsgBox("InfoManager");
                        strpgnm = "InfoManager";
                        strpath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\")) + @"InfoManager\InfoManager";
                        break;
                    //InfoWorks
                    case "000004":
                        //Messages.ShowInfoMsgBox("InfoWorks");
                        strpgnm = "";
                        strpath = "";
                        break;
                    //InfoManufacture
                    case "000005":
                        //Messages.ShowInfoMsgBox("InfoManufacture");
                        strpgnm = "";
                        strpath = "";
                        break;
                    //InfoWQuality
                    case "000006":
                        //Messages.ShowInfoMsgBox("InfoWQuality");
                        strpgnm = "InfoWQuality";
                        strpath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\")) + @"InfoWQuality\InfoWQuality";
                        break;
                    //InfoFMS
                    case "000007":
                        //Messages.ShowInfoMsgBox("InfoFMS");
                        strpgnm = "InfoFMS";
                        strpath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\")) + @"InfoFMS\InfoFMS";
                        break;
                }

                Process[] process = Process.GetProcessesByName(strpgnm);

                //있을경우 창위로
                if (process.Length > 0)
                {
                    IntPtr hWnd = FindWindow(null, strpgnm);

                    if (!hWnd.Equals(IntPtr.Zero))
                    {
                        // 윈도우가 최소화 되어 있다면 활성화 시킨다
                        ShowWindowAsync(hWnd, 9);
                        // 윈도우에 포커스를 줘서 최상위로 만든다
                        SetForegroundWindow(hWnd);
                    }
                }
                //없을경우 실행
                else
                {
                    if (strpath.Equals(""))
                    {
                        Messages.ShowInfoMsgBox("프로그램 경로를 확인해주세요.");
                        return;
                    }

                    if (!System.IO.File.Exists(strpath + ".exe"))
                    {
                        Messages.ShowInfoMsgBox("프로그램을 설치 해야 합니다.");
                        return;
                    }

                    Process.Start(strpath, Logs.strLogin_ID + " " + Logs.strLogin_PW + " " + Logs.strLogin_SITE);
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Mainwin_ContentRendered(object sender, EventArgs e)
        {
            try
            {
                if ((mainwin.FindName("spMenuArea") as StackPanel).Children.Count > 0)
                    btnMenu_Click(((mainwin.FindName("spMenuArea") as StackPanel).Children[0] as Button), null);
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// InterestBlkAction 즐겨찾기 편집
        /// </summary>
        /// <param name="obj"></param>
        private void InterestBlkAction(object obj)
        {
            
        }

        /// <summary>
        /// 회사 정보 팝업
        /// </summary>
        /// <param name="obj"></param>
        public void InfomationAction(object obj)
        {
            PopupInfomation popupInterestBlk = new PopupInfomation();
            popupInterestBlk.ShowDialog();
        }

        private void PopupInterestBlk_Closed(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 단축키 입력 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mainwin_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                AccordionControl accrMenu = mainwin.FindName("accrMenu") as AccordionControl;
                DataRow[] dr = null;

                if (Application.Current.Windows.Count > 2) return;


                if (Keyboard.IsKeyDown(Key.F1))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F1'");
                else if (Keyboard.IsKeyDown(Key.F2))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F2'");
                else if (Keyboard.IsKeyDown(Key.F3))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F3'");
                else if (Keyboard.IsKeyDown(Key.F4))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F4'");
                else if (Keyboard.IsKeyDown(Key.F5))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F5'");
                else if (Keyboard.IsKeyDown(Key.F6))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F6'");
                else if (Keyboard.IsKeyDown(Key.F7))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F7'");
                else if (Keyboard.IsKeyDown(Key.F8))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F8'");
                else if (Keyboard.IsKeyDown(Key.F9))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F9'");
                else if (Keyboard.IsKeyDown(Key.F10))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F10'");
                else if (Keyboard.IsKeyDown(Key.F11))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F11'");
                else if (Keyboard.IsKeyDown(Key.F12))
                    dr = dtQuickMenuList.Select("SHRTEN_KEY = 'F12'");

                if (dr != null && dr.Length == 1)
                {
                    dr = dtMenuList.Select("MNU_CD = '" + dr[0]["MNU_CD"].ToString() + "' AND MNU_STEP = '3'");

                    if (dr.Length == 1)
                    {
                        if (!Logs.htPermission[dr[0]["MNU_CD"]].ToString().Equals("N"))
                        {
                            if (!dr[0]["MNU_PATH"].ToString().Equals(""))
                            {
                                btnMenu_Click(mainwin.FindName("MN_" + dr[0]["MNU_CD"].ToString().Substring(0, 4)), null);
                                accrMenu.SelectedItem = mainwin.FindName("MN_" + dr[0]["MNU_CD"].ToString());
                                MenuControlAction(null);

                                if (!bQuickShowHiden)
                                    QuickShowHidenAction(null);
                            }
                        }
                    }
                }

                //Alt + F4 입력 시 처리
                //미처리 시 프로그램 닫히고 시스템 종료 확인 얼럿창 뜸. 선택에 상관없이 폼은 종료됨.
                if (Keyboard.IsKeyDown(Key.LeftAlt) && Keyboard.IsKeyDown(Key.F4))
                {
                    e.Handled = true;

                    CloseAction(null);
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 메시지 왼쪽 하단 알림창
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbTitle_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Label lbTitle = mainwin.FindName("lbTitle") as Label;
            lbTitle.Content = "메인";

            //Grid gridtitle = mainwin.FindName("gridtitle") as Grid;
            //gridtitle.RowDefinitions[0].Height = new GridLength(0, GridUnitType.Pixel);

            AccordionControl accrMenu = mainwin.FindName("accrMenu") as AccordionControl;
            accrMenu.SelectedItem = null;

            regionManager.Regions["ContentRegion"].RemoveAll();
            regionManager.RequestNavigate("ContentRegion", new Uri("UcMainPageView", UriKind.Relative));
        }

        /// <summary>
        /// 창을 닫는다.
        /// </summary>
        /// <param name="strArg">object</param>
        private void CloseAction(object obj)
        {
            try
            {
                if (Messages.ShowExitMsgBox("InfoWorks") == MessageBoxResult.Yes)
                {
                    //관망분석
                    Hashtable logconditions = new Hashtable();
                    logconditions.Add("USER_ID", Logs.strLogin_ID);
                    logconditions.Add("MNU_CD", "LOGOUT");
                    logconditions.Add("CONN_IP", Logs.strLogin_IP);
                    logconditions.Add("SYS_CD", "000004");
                    work.Insert_SYS_LOG_BY_SEQ(logconditions);
                    Environment.Exit(0);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }

            }
            catch (Exception)
            {
                Hashtable logconditions = new Hashtable();
                logconditions.Add("USER_ID", Logs.strLogin_ID);
                logconditions.Add("MNU_CD", "LOGOUT");
                logconditions.Add("CONN_IP", Logs.strLogin_IP);
                logconditions.Add("SYS_CD", "000004");
                work.Insert_SYS_LOG_BY_SEQ(logconditions);
                Environment.Exit(0);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        /// <summary>
        /// 창을 최대화 한다.
        /// </summary>
        /// <param name="obj">object</param>
        private void MaximizeAction(object obj)
        {
            if (mainwin.WindowState == WindowState.Maximized)
            {
                mainwin.WindowState = WindowState.Normal;

                btnWindowSize.Style = (Style)Application.Current.FindResource("MaxWindowButton");
            }
            else if (mainwin.WindowState == WindowState.Normal)
            {
                mainwin.WindowState = WindowState.Maximized;

                btnWindowSize.Style = (Style)Application.Current.FindResource("BeforeWindowButton");
            }
        }

        /// <summary>
        /// 창을 최소화 한다
        /// </summary>
        /// <param name="arg">object</param>
        private void MinimiizeAction(object arg)
        {
            mainwin.WindowState = System.Windows.WindowState.Minimized;
        }

        /// <summary>
        /// 창화면이동 
        /// (튀는 현상 수정 완료)
        /// </summary>
        /// <param name="obj"></param>
        private void WindowMoveAction(object obj)
        {
            try
            {
                if (Mouse.LeftButton == MouseButtonState.Pressed)
                {
                    if (mainwin.WindowState == WindowState.Maximized)
                    {
                        mainwin.Top = Mouse.GetPosition(mainwin).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                        mainwin.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(mainwin).X + 20;

                        mainwin.WindowState = WindowState.Normal;

                        btnWindowSize.Style = (Style)Application.Current.FindResource("MaxWindowButton");
                    }
                    mainwin.DragMove();
                }

                if (Mouse.LeftButton == MouseButtonState.Released)
                {
                    if (mainwin.WindowState == WindowState.Maximized)
                    {
                        btnWindowSize.Style = (Style)Application.Current.FindResource("BeforeWindowButton");
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 사용자 정보 변경
        /// </summary>
        /// <param name="obj"></param>
        private void UserInfoMngAction(object obj)
        {
            PopupUserInfoMng userInfoMng = new PopupUserInfoMng(mainwin);
            userInfoMng.ShowDialog();

            Button btnUser = mainwin.FindName("btnUser") as Button;
            btnUser.Content = Logs.strLogin_NM;
        }

        /// <summary>
        /// 메뉴 Show/Hiden
        /// </summary>
        /// <param name="obj"></param>
        private void MenuShowHidenAction(object obj)
        {
            Storyboard sb;

            AccordionControl accr = (AccordionControl)mainwin.FindName("accrMenu");
            Button btn = (Button)mainwin.FindName("btnMenuSlide");

            if (bMenuShowHiden)
            {
                sb = mainwin.FindResource("Menuin") as Storyboard;

                btn.Margin = new Thickness(0, 0, 18, 0);
                accr.RootItemExpandButtonPosition = ExpandButtonPosition.None;

                foreach (AccordionItem item in accr.Items)
                {
                    item.ToolTip = item.Header;

                    foreach (AccordionItem iitem in item.Items)
                    {
                        iitem.ToolTip = iitem.Header.ToString().Replace(" ⁃ ", "");
                    }
                }
                //accr.CollapseAll();
                //accr.ExpandItemOnHeaderClick = true;
            }
            else
            {
                sb = mainwin.FindResource("Menuout") as Storyboard;

                btn.Margin = new Thickness(0, 0, 6, 0);
                accr.RootItemExpandButtonPosition = ExpandButtonPosition.Right;

                foreach (AccordionItem item in accr.Items)
                {
                    item.ToolTip = null;

                    foreach (AccordionItem iitem in item.Items)
                    {
                        iitem.ToolTip = null;
                    }
                }

                //accr.ExpandAll();
                //accr.ExpandItemOnHeaderClick = true;
            }

            sb.Begin(mainwin);
            bMenuShowHiden = !bMenuShowHiden;
        }

        /// <summary>
        /// 퀵메뉴 Show/Hiden 이벤트
        /// </summary>
        /// <param name="obj"></param>
        private void QuickShowHidenAction(object obj)
        {
            Storyboard sb;

            Button btnQuick = (Button)mainwin.FindName("btnQuick");
            Border Quickborder = (Border)mainwin.FindName("Quickborder");

            if (bQuickShowHiden)
            {
                sb = mainwin.FindResource("QuickShow") as Storyboard;
                btnQuick.SetResourceReference(Control.StyleProperty, "btn_Quick_Slide_CLOSE");
                Quickborder.Margin = new Thickness(0, 0, 0, 0);
            }
            else
            {
                sb = mainwin.FindResource("QuickHiden") as Storyboard;
                btnQuick.SetResourceReference(Control.StyleProperty, "btn_Quick_Slide_OPEN");
                Quickborder.Margin = new Thickness(7, 0, 0, 0);
            }

            sb.Begin(mainwin);
            bQuickShowHiden = !bQuickShowHiden;
        }
        #endregion

        #region 기능
        /// <summary>
        /// 메뉴, 기본 초기화
        /// </summary>
        private void MenuDataInit(object obj)
        {
            try
            {
                mainwin = obj as MainWin;

                //사용자 정보 변경 버튼
                Button btnUser = mainwin.FindName("btnUser") as Button;
                btnUser.Content = Logs.strLogin_NM;

                //대메뉴 StackPanel
                StackPanel spMenuArea = mainwin.FindName("spMenuArea") as StackPanel;

                DataTable dtONEmnutemp = new DataTable();

                htconditions.Clear();
                htconditions.Add("SYS_CD", "000004");

                dtMenuList = work.Select_MNU_LIST(htconditions);

                foreach (DataRow r in dtMenuList.Select("MNU_STEP = '1'", "ORD"))
                {
                    try
                    {
                        if (!Logs.htPermission[r["MNU_CD"].ToString()].ToString().Equals("N"))
                        {
                            Button btnMenu = new Button
                            {
                                Name = "MN_" + r["MNU_CD"].ToString(),
                                Content = r["MNU_NM"].ToString(),
                                //Style = Application.Current.Resources["MainMNUButton"] as Style,
                                Tag = "/Resources/Blue/Images/MNUImage/" + r["MNU_IMG"].ToString()
                            };

                            if (ThemeApply.strThemeName.Equals("GTINavyTheme"))
                                btnMenu.Tag = "/Resources/Navy/Images/MNUImage/" + r["MNU_IMG"].ToString();
                            else if (ThemeApply.strThemeName.Equals("GTIBlueTheme"))
                                btnMenu.Tag = "/Resources/Blue/Images/MNUImage/" + r["MNU_IMG"].ToString();

                            btnMenu.SetResourceReference(Control.StyleProperty, "MainMNUButton");
                            btnMenu.Click += btnMenu_Click;
                            mainwin.RegisterName(btnMenu.Name, btnMenu);
                            spMenuArea.Children.Add(btnMenu);
                        }
                    }
                    catch (Exception ex)
                    {
                        Messages.ErrLog(ex);
                    }
                }

                //Logs.configChange(Logs.WNMSConfig);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 대메뉴 클릭시 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            AccordionControl accrMenu = mainwin.FindName("accrMenu") as AccordionControl;

            foreach (AccordionItem item in accrMenu.Items)
            {
                mainwin.UnregisterName(item.Name);

                foreach (AccordionItem citem in item.Items)
                {
                    item.UnregisterName(citem.Name);
                }
            }

            accrMenu.Items.Clear();

            DataRow[] drmidMENU;
            DataRow[] drsmMENU;
            drmidMENU = dtMenuList.Select("MNU_STEP = '2' AND UPPER_CD ='" + ((Button)sender).Name.Replace("MN_", "").ToString() + "'", "ORD");

            //중메뉴
            foreach (DataRow r in drmidMENU)
            {
                try
                {
                    //중메뉴 권한 필터링 (N)인경우 NO
                    if (!Logs.htPermission[r["MNU_CD"].ToString()].ToString().Equals("N"))
                    {
                        AccordionItem acctwoitem = new AccordionItem
                        {
                            Name = "MN_" + r["MNU_CD"].ToString(),
                            Header = r["MNU_NM"].ToString(),
                            Foreground = new SolidColorBrush(Colors.White),
                            FontSize = 14
                        };

                        if (!bMenuShowHiden)
                            acctwoitem.ToolTip = r["MNU_NM"].ToString();

                        if (ThemeApply.strThemeName.Equals("GTINavyTheme"))
                            acctwoitem.Glyph = new BitmapImage(new Uri("/Resources/Navy/Images/MNUImage/" + r["MNU_IMG"].ToString(), UriKind.Relative));
                        else if (ThemeApply.strThemeName.Equals("GTIBlueTheme"))
                            acctwoitem.Glyph = new BitmapImage(new Uri("/Resources/Blue/Images/MNUImage/" + r["MNU_IMG"].ToString(), UriKind.Relative));

                        mainwin.RegisterName(acctwoitem.Name, acctwoitem);
                        accrMenu.Items.Add(acctwoitem);

                        drsmMENU = null;
                        drsmMENU = dtMenuList.Select("MNU_STEP = '3' AND UPPER_CD ='" + acctwoitem.Name.Replace("MN_", "").ToString() + "'", "ORD");

                        //소메뉴
                        foreach (DataRow drthree in drsmMENU)
                        {
                            try
                            {
                                //소메뉴 권한 필터링 (N)인경우 NO
                                if (!Logs.htPermission[drthree["MNU_CD"].ToString()].ToString().Equals("N"))
                                {
                                    AccordionItem accthreeitem = new AccordionItem
                                    {
                                        Name = "MN_" + drthree["MNU_CD"].ToString(),
                                        Header = " ⁃ " + drthree["MNU_NM"].ToString(),
                                        Foreground = new SolidColorBrush(Colors.White),
                                        FontSize = 13
                                    };

                                    if (!bMenuShowHiden)
                                        accthreeitem.ToolTip = drthree["MNU_NM"].ToString();

                                    mainwin.RegisterName(accthreeitem.Name, accthreeitem);
                                    acctwoitem.Items.Add(accthreeitem);
                                }
                            }
                            catch (Exception ex)
                            {
                                Messages.ErrLog(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Messages.ErrLog(ex);
                }

                //편의를 위해 확장 추후 결정
                accrMenu.ExpandAll();
            }
        }

        /// <summary>
        /// 메뉴선택 이벤트
        /// </summary>
        /// <param name="obj"></param>
        private void MenuControlAction(object obj)
        {
            AccordionControl accrMenu = mainwin.FindName("accrMenu") as AccordionControl;

            try
            {
                AccordionItem accitemSelect = accrMenu.SelectedItem as AccordionItem;

                if (accitemSelect != null)
                {
                    strSelectMenu = accitemSelect.Name.Replace("MN_", "");
                    DataRow[] dr = dtMenuList.Select("MNU_CD = '" + strSelectMenu + "' AND MNU_STEP = '3'");

                    if (dr.Length == 1)
                    {
                        if (!Logs.htPermission[strSelectMenu].ToString().Equals("N"))
                        {
                            if (!dr[0]["MNU_PATH"].ToString().Equals(""))
                            {
                                Label lbTitle = mainwin.FindName("lbTitle") as Label;
                                lbTitle.Content = dr[0]["MNU_NM"].ToString();

                                Grid gridtitle = mainwin.FindName("gridtitle") as Grid;
                                gridtitle.RowDefinitions[0].Height = new GridLength(40, GridUnitType.Pixel);

                                regionManager.Regions["ContentRegion"].RemoveAll();
                                regionManager.RequestNavigate("ContentRegion", new Uri(dr[0]["MNU_PATH"].ToString(), UriKind.Relative));

                                //기존 포커스 메뉴코드와 새로 선택된 메뉴코드가 다를시 로그 기록 절차
                                if (!strSelectMenu.Equals(Logs.strFocusMNU_CD))
                                {

                                    Hashtable htConditions = new Hashtable();
                                    htConditions.Add("USER_ID", Logs.strLogin_ID);
                                    htConditions.Add("MNU_CD", strSelectMenu);
                                    htConditions.Add("CONN_IP", Logs.strLogin_IP);
                                    htConditions.Add("SYS_CD", "000004");

                                    if (strLastSEQ != string.Empty)
                                    {
                                        htConditions.Add("LOG_SEQ", strLastSEQ);
                                        work.Update_SYS_LOG_END_DT(htConditions);
                                    }

                                    strLastSEQ = work.Insert_SYS_LOG_BY_SEQ(htConditions).ToString();
                                }

                                Logs.strFocusMNU_CD = strSelectMenu;
                            }
                            else
                            {
                                Messages.ShowInfoMsgBox("메뉴 경로가 부적합 합니다.");
                                return;
                            }
                        }
                        else
                        {
                            Messages.ShowInfoMsgBox("해당메뉴에 권한이 없습니다.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 물음표 선택
        /// </summary>
        /// <param name="obj"></param>
        private void QuestionAction(object obj)
        {
            if (!strSelectMenu.Equals(""))
            {
                PopupHelp popupHelp = new PopupHelp(strSelectMenu);
                popupHelp.ShowDialog();
            }
            else
            {
                Messages.ShowInfoMsgBox("선택된 메뉴가 없습니다.");
            }
        }

        /// <summary>
        /// 즐겨찾기관리 
        /// </summary>
        /// <param name="obj"></param>
        private void QuickMngAction(object obj)
        {
            //PopupQuickMenuMng popupQuickMng = new PopupQuickMenuMng();
            //popupQuickMng.Closed += popupQuickMng_Closed;
            //popupQuickMng.ShowDialog();
        }

        /// <summary>
        /// 즐겨찾기 관리 닫기 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void popupQuickMng_Closed(object sender, EventArgs e)
        {
            QuickMnuBinding();
        }

        /// <summary>
        /// 즐겨찾기 바인딩
        /// </summary>
        public void QuickMnuBinding()
        {
            try
            {
                htconditions.Clear();
                htconditions.Add("SYS_CD", "000004");
                htconditions.Add("USER_ID", Logs.strLogin_ID);

                dtQuickMenuList = work.Select_MNU_BKMK_MNG_R(htconditions);

                stQuickMenu.Children.Clear();

                foreach (DataRow dr in dtQuickMenuList.Rows)
                {
                    Button btnQuickMenu = new Button
                    {
                        Name = "MN_" + dr["MNU_CD"].ToString(),
                        Content = dr["MNU_NM"].ToString(),
                        Tag = dr["SHRTEN_KEY"].ToString(),
                        ToolTip = dr["MNU_NM"].ToString()
                    };
                    btnQuickMenu.Click += BtnQuickMenu_Click;
                    btnQuickMenu.SetResourceReference(Control.StyleProperty, "Quick_Menu_Button");

                    if (btnQuickMenu.Tag.ToString().Equals(""))
                    {
                        btnQuickMenu.Tag = "Collapsed";
                    }

                    stQuickMenu.Children.Add(btnQuickMenu);
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 즐겨찾기 액션
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnQuickMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                strSelectMenu = (sender as Button).Name.Replace("MN_", "");
                AccordionControl accrMenu = mainwin.FindName("accrMenu") as AccordionControl;

                DataRow[] dr = dtMenuList.Select("MNU_CD = '" + strSelectMenu + "' AND MNU_STEP = '3'");

                if (dr.Length == 1)
                {
                    if (!Logs.htPermission[strSelectMenu].ToString().Equals("N"))
                    {
                        if (!dr[0]["MNU_PATH"].ToString().Equals(""))
                        {
                            btnMenu_Click(mainwin.FindName("MN_" + dr[0]["MNU_CD"].ToString().Substring(0, 4)), null);
                            accrMenu.SelectedItem = mainwin.FindName("MN_" + dr[0]["MNU_CD"].ToString());
                            MenuControlAction(null);

                            if (!bQuickShowHiden)
                                QuickShowHidenAction(null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }
        #endregion
    }
}
