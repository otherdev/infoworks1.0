﻿using GTI.InfoWorks.Main.View;
using GTI.InfoWorks.Models.Main.Work;
using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Handle;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace GTI.InfoWorks.Main
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : Application
    {
        MainWork work = new MainWork();
        DataTable dtRockeyInfo = new DataTable();

        Mutex _mutex = null;
        USBLicense USBLicense;

        /// <summary>
        /// 컴파일한 날짜를 구한다.
        ///   단, AssemblyInfo.cs 파일에서 AssemblyVersion는 다음 형식으로 되어있어야만 한다.
        ///   [assembly: AssemblyVersion("4.0.*")]
        /// </summary>
        /// <returns>컴파일한 날짜</returns>
        public static string BuildDate
        {
            get
            {
                //버전 정보 가져온다.
                Version assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version;

                //프로그램 빌드 한 날짜 구하기
                DateTime buildDate = new DateTime(2000, 1, 1).AddDays(assemblyVersion.Build).AddSeconds(assemblyVersion.Revision * 2);
                string fileVer = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
                string tmp = string.Format("{0} [{1}]", fileVer, buildDate.ToString("yyyy-MM-dd HH:mm:ss"));
                return tmp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            BuildHandle.userconfigCHK();

            string strtitle = "InfoWorks";
            Logs.setLogFile("InfoWorks");
            bool bcreateNew = false;

            try
            {
                //dtRockeyInfo = work.Select_Rockey_Certification(null);

                _mutex = new Mutex(true, strtitle, out bcreateNew);

                if (bcreateNew)
                {
                    Logs.DBdefault();

                    //Rock Key 체크
                    try
                    {
                        dtRockeyInfo = work.Select_Rockey_Certification(null);
                    }
                    catch (Exception ex)
                    {
                    }

                    //USBLicense = new USBLicense(Application.Current, dtRockeyInfo);

                    Login login = new Login();
                    TatukGIS.NDK.GisLicense.Initialize();
                    bool? res = null;

                    if (e.Args.Length == 3)
                    {
                        login = new Login(e.Args[0].ToString(), e.Args[1].ToString(), e.Args[2].ToString());
                        Application.Current.ShutdownMode = ShutdownMode.OnLastWindowClose;
                    }
                    else
                    {
                        login = new Login();
                        Application.Current.ShutdownMode = ShutdownMode.OnLastWindowClose;
                        res = login.ShowDialog();
                    }

                    if (res != null)
                    {
                        if (!res ?? true)
                        {
                            Shutdown(1);
                        }
                        else
                        {
                            base.OnStartup(e);
                            Bootstrapper bs = new Bootstrapper();
                            bs.Run();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("InfoManager 실행중입니다.", "알림", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    Environment.Exit(0);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                Console.WriteLine(e.Exception.ToString());
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            try
            {
                for (int i = 0; i < Logs.htthread.Count - 1; i++)
                {
                    if (Logs.htthread[i] is Thread)
                    {
                        try
                        {
                            (Logs.htthread[i] as Thread).Abort();
                            Logs.htthread.Remove((Logs.htthread[i] as Thread).Name);
                        }
                        catch (Exception ex) { }
                    }
                }

                Hashtable htconditions = new Hashtable();
                htconditions.Add("USER_ID", Logs.strLogin_ID);
                htconditions.Add("MNU_CD", "LOGOUT");
                htconditions.Add("CONN_IP", Logs.strLogin_IP);
                htconditions.Add("SYS_CD", "000003");
                work.Insert_SYS_LOG_BY_SEQ(htconditions);

                //dbmanager.publicCloseAll();
            }
            catch (Exception ex)
            {
            }
        }
    }
}
