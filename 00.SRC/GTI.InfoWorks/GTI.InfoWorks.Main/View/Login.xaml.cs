﻿using GTI.InfoWorks.Main.View.Popup;
using GTI.InfoWorks.Models.Main.Work;
using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Converters;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GTI.InfoWorks.Main.View
{
    /// <summary>
    /// Login.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Login : Window
    {
        MainWork work = new MainWork();
        Hashtable htconditions = new Hashtable();

        protected Thread thread;  //쓰레드 선언
        protected System.Timers.Timer timer; //자동쓰레드 타이머

        DataTable dtBaseSiteInfo = new DataTable();

        DataTable dtDBInfo;

        bool bcbSiteping = false;

        public Login()
        {
            InitializeComponent();

            if (Properties.Settings.Default.strThemeName.Equals(""))
                ThemeApply.strThemeName = "GTINavyTheme";
            else
                ThemeApply.strThemeName = Properties.Settings.Default.strThemeName;
            ThemeApply.ThemeChange(this);
            ThemeApply.Themeapply(this);

            Loaded += Login_Loaded;
            Unloaded += Login_Unloaded;
        }

        public Login(string strid, string strpw, string strsitecd)
        {
            InitializeComponent();

            if (Properties.Settings.Default.strThemeName.Equals(""))
                ThemeApply.strThemeName = "GTINavyTheme";
            else
                ThemeApply.strThemeName = Properties.Settings.Default.strThemeName;
            ThemeApply.ThemeChange(this);
            ThemeApply.Themeapply(this);

            InitializeEvent();

            txtID.Text = strid;
            pwdPW.EditValue = strpw;

            PGStart(strid, strpw, strsitecd);
        }

        private void PGStart(string strid, string strpw, string strsitecd)
        {
            try
            {
                if (Logs.DefaultConfig.strIP.Equals("")
                    || Logs.DefaultConfig.strPort.Equals("")
                    || Logs.DefaultConfig.strSID.Equals("")
                    || Logs.DefaultConfig.strID.Equals("")
                    || Logs.DefaultConfig.strPWD.Equals(""))
                {
                    Messages.ShowInfoMsgBox("InfoWorks 데이터베이스 설정 먼저 해야 합니다.");
                    Environment.Exit(0);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }

                htconditions.Clear();
                htconditions.Add("SITE_CD", strsitecd);
                htconditions.Add("USER_ID", strid);
                htconditions.Add("USER_PWD", EncryptionConvert.SHA256Hash(strid + strpw));
                htconditions.Add("SYS_CD", "000004");

                dtBaseSiteInfo = work.SelectBaseSiteInfo(null);

                DataRow[] dtsite = dtBaseSiteInfo.Select("[SITE_CD] = '" + strsitecd + "'");

                if (dtsite.Length == 1)
                {
                    dtDBInfo = work.SelectDBInfo(htconditions);

                    DataTable dtLoginCheck = new DataTable();
                    dtLoginCheck = work.LoginCheck(htconditions);

                    //사용자가 입력한 로그인 정보로 일치하는 데이터가 없을 경우
                    if (dtDBInfo.Rows.Count == 0)
                    {
                        Messages.ShowInfoMsgBox("시스템 접속정보가 없습니다.");
                        return;
                    }

                    //사용자가 입력한 로그인 정보로 일치하는 데이터가 없을 경우
                    if (dtLoginCheck.Rows.Count == 0)
                    {
                        Messages.ShowInfoMsgBox("로그인 정보가 틀립니다.");
                        return;
                    }

                    //비밀번호 체크
                    if (!dtLoginCheck.Rows[0]["USER_PWD"].ToString().Equals(htconditions["USER_PWD"].ToString()))
                    {
                        Messages.ShowInfoMsgBox("로그인 정보가 틀립니다.");
                        return;
                    }

                    //사용여부체크
                    if (dtLoginCheck.Rows[0]["USE_YN"].ToString().Equals("N"))
                    {
                        Messages.ShowInfoMsgBox("미사용 계정입니다.");
                        return;
                    }

                    //시스템 권한 체크
                    if (!dtLoginCheck.Rows[0]["SYS_CD"].ToString().Equals("1"))
                    {
                        Messages.ShowInfoMsgBox("해당 시스템에 권한이 없습니다.");
                        return;
                    }

                    //시스템 그룹 권한 체크
                    if (!dtLoginCheck.Rows[0]["GRP_CD"].ToString().Equals("1"))
                    {
                        Messages.ShowInfoMsgBox("해당 시스템에 권한이 없습니다.");
                        return;
                    }

                    //로그인 성공이후
                    //..로그인 성공한 아이디, 사이트, IP  저장
                    Logs.strLogin_ID = strid;
                    Logs.strLogin_SITE = strsitecd;
                    foreach (var item in ((IPAddress[])Dns.GetHostAddresses("")))
                    {
                        if (item != null)
                        {
                            //ipv4 확인
                            if (!item.IsIPv6LinkLocal)
                            {
                                //A, B클래스
                                if (item.ToString().Split('.')[0].Equals(Logs.DefaultConfig.strIP.Split('.')[0]) && item.ToString().Split('.')[1].Equals(Logs.DefaultConfig.strIP.Split('.')[1]))
                                {
                                    //빈값일 경우에만
                                    if (Logs.strLogin_IP.Equals(""))
                                    {
                                        Logs.strLogin_IP = item.ToString();
                                    }
                                }
                            }
                        }
                    }
                    Logs.strLogin_NM = dtLoginCheck.Rows[0]["USER_NM"].ToString();
                    Logs.strLogin_PW = strpw;

                    //..로그인 성공한 메뉴별 권한 저장
                    DataTable dtPermission = new DataTable();
                    dtPermission = work.Select_LoginUser_Permission(htconditions);

                    foreach (DataRow dr in dtPermission.Rows)
                    {
                        Logs.htPermission.Add(dr["MNU_CD"].ToString(), dr["MNU_AUTH"].ToString());
                    }

                    Logs.WNMSConfig.strIP = GTIFramework.Properties.Settings.Default.strIP;
                    Logs.WNMSConfig.strPort = GTIFramework.Properties.Settings.Default.strPort;
                    Logs.WNMSConfig.strSID = GTIFramework.Properties.Settings.Default.strSID;
                    Logs.WNMSConfig.strID = GTIFramework.Properties.Settings.Default.strID;
                    Logs.WNMSConfig.strPWD = GTIFramework.Properties.Settings.Default.strPWD;

                    Bootstrapper bs = new Bootstrapper();
                    bs.Run();

                    this.Close();
                }
                else
                {
                    Messages.ShowInfoMsgBox("사업소 정보가 없습니다.");
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Login_Unloaded(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            timer.Dispose();
            thread.Abort();
        }

        private void Login_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeEvent();
            InitializeData();

            try
            {
                thread = new Thread(new ThreadStart(thread_FX)) { IsBackground = true };
                thread.Name = "thread";
                thread.Start();
            }
            catch (Exception ex)
            {

            }
        }

        #region ========== 함수 정의 ==========

        private void thread_FX()
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                CheckNetStat();
            }
            catch (Exception ex) { Messages.ErrLog(ex); }
        }
        /// <summary>
        /// 이벤트 초기화
        /// </summary>
        private void InitializeEvent()
        {
            btnClose.Click += BtnClose_Click;
            btnLogin.Click += BtnLogin_Click;
            btnDataBaseInfoEdit.Click += BtnDataBaseInfoEdit_Click;
            pwdPW.KeyDown += PwdPW_KeyDown;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
        }

        /// <summary>
        /// 초기화
        /// (접속상태 4번클릭)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 4)
            {
                if (Messages.ShowYesNoMsgBox("데이터베이스 설정은 초기화 하시겠습니까?") == MessageBoxResult.Yes)
                {
                    GTIFramework.Properties.Settings.Default.strIP = "";
                    GTIFramework.Properties.Settings.Default.strPort = "";
                    GTIFramework.Properties.Settings.Default.strSID = "";
                    GTIFramework.Properties.Settings.Default.strID = "";
                    GTIFramework.Properties.Settings.Default.strPWD = "";
                    GTIFramework.Properties.Settings.Default.RES_DB_INS_DEFAULT = "";
                    GTIFramework.Properties.Settings.Default.Save();
                    Messages.ShowInfoMsgBox("초기화 완료");
                    Environment.Exit(0);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
        }

        private void PwdPW_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnLogin_Click(null, null);
            }
        }

        /// <summary>
        /// 로그인 폼 데이터 초기화
        /// </summary>
        public void InitializeData()
        {
            try
            {
                //사업소 정보가 셋팅되어 있는지 체크
                if (ConfigValueCheck())
                {
                    try
                    {
                        // 사업소정보 콤보박스 바인딩
                        dtBaseSiteInfo = work.SelectBaseSiteInfo(null);

                        if (dtBaseSiteInfo.Rows.Count > 0)
                        {
                            cbSite.ItemsSource = dtBaseSiteInfo;
                            cbSite.DisplayMember = "SITE_NM";
                            cbSite.ValueMember = "SITE_CD";
                            cbSite.SelectedIndex = 0;

                            txtID.Focus();

                            //최근 저장되어 있는 아이디 바인딩
                            if (Properties.Settings.Default.bSaveID)
                            {
                                chkSaveID.EditValue = Properties.Settings.Default.bSaveID;

                                if (!Properties.Settings.Default.strRecentID.Equals(""))
                                {
                                    txtID.Text = Properties.Settings.Default.strRecentID;
                                    pwdPW.Focus();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Messages.ShowErrMsgBoxLog(ex);
                    }
                }

                CheckNetStat();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 접속상태 체크
        /// 사업소정보설정폼에서 셋팅한 IP주소로 ping 체크
        /// 핑 응답이 있을시 '정상' 표시
        /// 사업소정보설정 저장값이 없으면 '비정상' 표시
        /// </summary>
        public void CheckNetStat()
        {
            try
            {
                if (ConfigValueCheck())
                {
                    //2020-02-05
                    //프로퍼티 세팅 IP로 핑 체크로 변경
                    //네트워크 정상 비정상 체크
                    if (PingChecker(GTIFramework.Properties.Settings.Default.strIP))
                    {
                        Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                            new Action(delegate ()
                            {
                                imgConnAbnormal.Visibility = Visibility.Collapsed;
                            }));
                    }
                    else
                    {
                        Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                            new Action(delegate ()
                            {
                                imgConnAbnormal.Visibility = Visibility.Visible;
                            }));
                    }
                }
                else
                {
                    Dispatcher.Invoke(DispatcherPriority.ApplicationIdle,
                        new Action(delegate ()
                        {
                            imgConnAbnormal.Visibility = Visibility.Visible;
                        }));
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 핑 체크 (유효한 ip 범위로 넘겨줘야함)
        /// 핑 관련 클래스를 통해 핑 체크를 시도한다.
        /// 파라미터로 넘겨주는 ip는 반드시 유효한 ip 주소 범위로 전달되야 하며 아닐시 익셉션 처리됨.
        /// </summary>
        /// <param name="strip"></param>
        /// <returns></returns>
        public bool PingChecker(string strip)
        {
            try
            {
                Ping pingSender = new Ping();
                PingReply reply = pingSender.Send(strip);

                if (reply.Status == IPStatus.Success)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
                return false;
            }
        }

        /// <summary>
        /// 사업소 정보 셋팅 체크
        /// Property 값 6개 모두 설정되어 있을 때 true 반환.
        /// RES_DB_INS_DEFAULT = Tibero / Oracle
        /// </summary>
        /// <returns></returns>
        public bool ConfigValueCheck()
        {
            bool bConfChk = false;

            if (!GTIFramework.Properties.Settings.Default.strIP.Equals("")
                && !GTIFramework.Properties.Settings.Default.strPort.Equals("")
                && !GTIFramework.Properties.Settings.Default.strSID.Equals("")
                && !GTIFramework.Properties.Settings.Default.strID.Equals("")
                && !GTIFramework.Properties.Settings.Default.strPWD.Equals("")
                && !GTIFramework.Properties.Settings.Default.RES_DB_INS_DEFAULT.Equals(""))
            {
                bConfChk = true;
            }
            return bConfChk;
        }
        #endregion

        #region ========== 이벤트 정의 ==========
        /// <summary>
        /// 닫기버튼 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            if (Messages.ShowExitMsgBox("InfoWorks") == MessageBoxResult.Yes)
            {
                Environment.Exit(0);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        /// <summary>
        /// 사업소 정보 설정 Popup 이벤트 ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDataBaseInfoEdit_Click(object sender, RoutedEventArgs e)
        {
            //2020-02-05
            //아이디 입력과 상관없이, 사이트 선택 유무 상관없이 프로퍼티 세팅값으로 사업소 정보 바인딩
            DBManagement dbManagement = new DBManagement(this);
            dbManagement.ShowDialog();

            //if (!cbSite.Text.Equals(""))
            //{
            //    if (txtID.Text.Equals(""))
            //    {
            //        Messages.ShowInfoMsgBox("아이디를 입력해 주세요.");
            //        return;
            //    }

            //    DBManagement dbManagement = new DBManagement(dtDBInfo, this);
            //    dbManagement.ShowDialog();
            //}
            //else
            //{
            //    DBManagement dbManagement = new DBManagement(this);
            //    dbManagement.ShowDialog();
            //}
        }

        int intpolicycnt = 0;
        /// <summary>
        /// 로그인 버튼 이벤트 (메뉴 권한 바인딩 해야됨)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //로그인 정보가 없는 경우 체크
                if (cbSite.Text == "" || txtID.Text == "" || pwdPW.Text == "")
                {
                    Messages.ShowInfoMsgBox("로그인 정보가 틀립니다.");
                    return;
                }

                htconditions.Clear();
                htconditions.Add("SITE_CD", cbSite.EditValue.ToString());
                htconditions.Add("USER_ID", txtID.EditValue.ToString());
                htconditions.Add("USER_PWD", EncryptionConvert.SHA256Hash(txtID.EditValue.ToString() + pwdPW.EditValue.ToString()));
                htconditions.Add("SYS_CD", "000004");
                htconditions.Add("POLICY_CNT", "");

                DataTable dtLoginCheck = new DataTable();
                dtLoginCheck = work.LoginCheck(htconditions);

                //사용자가 입력한 로그인 정보로 일치하는 데이터가 없을 경우
                if (dtLoginCheck.Rows.Count == 0)
                {
                    Messages.ShowInfoMsgBox("로그인 정보가 틀립니다.");
                    return;
                }

                //사용여부체크
                if (dtLoginCheck.Rows[0]["USE_YN"].ToString().Equals("N"))
                {
                    Messages.ShowInfoMsgBox("미사용 계정입니다.");
                    return;
                }

                //시스템 권한 체크
                if (!dtLoginCheck.Rows[0]["SYS_CD"].ToString().Equals("1"))
                {
                    Messages.ShowInfoMsgBox("해당 시스템에 권한이 없습니다.");
                    return;
                }

                //시스템 그룹 권한 체크
                if (!dtLoginCheck.Rows[0]["GRP_CD"].ToString().Equals("1"))
                {
                    Messages.ShowInfoMsgBox("해당 시스템에 권한이 없습니다.");
                    return;
                }

                //틀린횟수 가져오기
                if (int.TryParse(dtLoginCheck.Rows[0]["POLICY_CNT"].ToString(), out intpolicycnt))
                {
                    if (intpolicycnt > 4)
                    {
                        Messages.ShowInfoMsgBox("계정이 잠긴 상태입니다.\n관리자에게 문의해 주세요.");
                        return;
                    }
                    else if (intpolicycnt == 0)
                    {
                        Popup.PopupTempPwdChange popuppwdchange = new Popup.PopupTempPwdChange(htconditions);
                        bool? result = popuppwdchange.ShowDialog();

                        if (!result ?? true)
                            return;
                    }
                }
                else
                    intpolicycnt = 0;

                //비밀번호 체크
                //비밀번호 체크 해서 틀리면 CNT++ 5번 이상 틀렸을 경우 Lock
                //정상 로그인 성공시 카운트값은 null
                //임시비밀번호 일경우에는 카운트 0
                //카운트 0일경우에는 임시비밀번호 창 띄우기
                if (!dtLoginCheck.Rows[0]["USER_PWD"].ToString().Equals(htconditions["USER_PWD"].ToString()))
                {
                    intpolicycnt++;
                    htconditions.Remove("USER_PWD");
                    htconditions["POLICY_CNT"] = intpolicycnt.ToString();
                    work.Update_SYS_USER_INFO_Policy(htconditions);
                    Messages.ShowInfoMsgBox("로그인 정보가 " + intpolicycnt + "회 틀렸습니다.\n5회 실패시 계정이 잠깁니다.");
                    return;
                }

                //아이디 저장 체크박스 확인 후 최근 접속아이디 및 체크 값 setting.
                Properties.Settings.Default.bSaveID = (bool)chkSaveID.EditValue;
                if ((bool)chkSaveID.EditValue)
                {
                    Properties.Settings.Default.strRecentID = txtID.Text;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    Properties.Settings.Default.strRecentID = "";
                    Properties.Settings.Default.Save();
                }

                DialogResult = true;

                //로그인 성공이후
                //..로그인 성공한 아이디, 사이트, IP  저장
                Logs.strLogin_ID = txtID.Text;
                Logs.strLogin_SITE = cbSite.EditValue.ToString();
                foreach (var item in ((IPAddress[])Dns.GetHostAddresses("")))
                {
                    if (item != null)
                    {
                        //ipv4 확인
                        if (!item.IsIPv6LinkLocal)
                        {
                            //A, B클래스
                            if (item.ToString().Split('.')[0].Equals(Logs.DefaultConfig.strIP.Split('.')[0]) && item.ToString().Split('.')[1].Equals(Logs.DefaultConfig.strIP.Split('.')[1]))
                            {
                                //빈값일 경우에만
                                if (Logs.strLogin_IP.Equals(""))
                                {
                                    Logs.strLogin_IP = item.ToString();
                                }
                            }
                        }
                    }
                }
                Logs.strLogin_NM = dtLoginCheck.Rows[0]["USER_NM"].ToString();
                Logs.strLogin_PW = pwdPW.EditValue.ToString();

                //..로그인 성공한 메뉴별 권한 저장
                DataTable dtPermission = new DataTable();
                dtPermission = work.Select_LoginUser_Permission(htconditions);

                foreach (DataRow dr in dtPermission.Rows)
                {
                    Logs.htPermission.Add(dr["MNU_CD"].ToString(), dr["MNU_AUTH"].ToString());
                }

                Logs.WNMSConfig.strIP = GTIFramework.Properties.Settings.Default.strIP;
                Logs.WNMSConfig.strPort = GTIFramework.Properties.Settings.Default.strPort;
                Logs.WNMSConfig.strSID = GTIFramework.Properties.Settings.Default.strSID;
                Logs.WNMSConfig.strID = GTIFramework.Properties.Settings.Default.strID;
                Logs.WNMSConfig.strPWD = GTIFramework.Properties.Settings.Default.strPWD;

                htconditions.Clear();
                htconditions.Add("USER_ID", Logs.strLogin_ID);
                htconditions.Add("MNU_CD", "LOGIN");
                htconditions.Add("CONN_IP", Logs.strLogin_IP);
                htconditions.Add("SYS_CD", "000004");
                work.Insert_SYS_LOG_BY_SEQ(htconditions);

                htconditions.Clear();
                htconditions.Add("USER_ID", Logs.strLogin_ID);
                htconditions.Add("POLICY_CNT", "");
                work.Update_SYS_USER_INFO_Policy(htconditions);

                Close();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// 윈도우 창 이동 이벤트 ok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == WindowState.Maximized)
                {
                    Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    WindowState = WindowState.Normal;
                }
                DragMove();
            }
        }
        #endregion
    }
}
