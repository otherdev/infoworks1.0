﻿using DevExpress.Xpf.Accordion;
using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GTI.InfoWorks.Main
{
    /// <summary>
    /// MainWin.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWin : Window
    {
        public MainWin()
        {
            InitializeComponent();
        }

        /// <summary>
        /// o1 메뉴코드
        /// o2 파라미터
        /// </summary>
        /// <param name="o1"></param>
        /// <param name="o2"></param>
        public void MenuGo(object o1, object o2)
        {
            try
            {
                string strBLKTrendMenu = o1 as string;
                AccordionControl accrMenu = FindName("accrMenu") as AccordionControl;

                if (!Logs.htPermission[strBLKTrendMenu].ToString().Equals("N"))
                {
                    (FindName("MN_" + strBLKTrendMenu.Substring(0, 4)) as Button).RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                    accrMenu.SelectedItem = FindName("MN_" + strBLKTrendMenu);
                }
                else
                {
                    Messages.ShowInfoMsgBox("권한이 없습니다.");
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Cmnavy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Properties.Settings.Default.strThemeName = "GTINavyTheme";
                Properties.Settings.Default.Save();

                ThemeApply.strThemeName = "GTINavyTheme";
                ThemeApply.ThemeChange(this);
                ThemeApply.Themeapply(this);

                //메뉴 Image 변경
                foreach (var item in spMenuArea.Children)
                {
                    if (item is Button)
                        (item as Button).Tag = (item as Button).Tag.ToString().Replace("Blue", "Navy");
                }

                //((sender as MenuItem).Parent as ContextMenu).IsOpen = false;
            }
            catch (Exception ex)
            {
            }
        }

        public void Cmblue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Properties.Settings.Default.strThemeName = "GTIBlueTheme";
                Properties.Settings.Default.Save();

                ThemeApply.strThemeName = "GTIBlueTheme";
                ThemeApply.ThemeChange(this);
                ThemeApply.Themeapply(this);

                //메뉴 Image 변경
                foreach (var item in spMenuArea.Children)
                {
                    if (item is Button)
                        (item as Button).Tag = (item as Button).Tag.ToString().Replace("Navy", "Blue");
                }

                //((sender as MenuItem).Parent as ContextMenu).IsOpen = false;
            }
            catch (Exception ex)
            {
            }
        }
    }
}
