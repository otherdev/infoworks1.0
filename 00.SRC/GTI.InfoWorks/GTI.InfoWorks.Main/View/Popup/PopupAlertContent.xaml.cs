﻿using GTI.InfoWorks.Models.Main.Work;
using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GTI.InfoWorks.Main.View.Popup
{
    /// <summary>
    /// PopupAlertContent.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupAlertContent : Window
    {

        MainWork work = new MainWork();
        Hashtable htcondition = new Hashtable();
        DataTable dtData = new DataTable();

        public PopupAlertContent(DataTable _dtData)
        {
            dtData = _dtData;
            ThemeApply.Themeapply(this);
            InitializeComponent();
            Loaded += PopupAlertContent_Loaded;
        }

        private void PopupAlertContent_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                btnClose.Click += BtnClose_Click;
                btnSave.Click += BtnSave_Click;
                btnXSignClose.Click += BtnClose_Click;
                bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;

                wrappanelAlert.Children.Clear();

                foreach (DataRow dr in dtData.Rows)
                {
                    TextBlock lb = new TextBlock
                    {
                        Text = "[" + dr["OCCUR_DT"].ToString() + "] " + dr["BLK_NM"].ToString() + " " + dr["ALERT_CONTENTS"].ToString(),
                        FontSize = 15,
                        Margin = new Thickness(0, 0, 0, 5),
                        Foreground = (Brush)new BrushConverter().ConvertFrom(Application.Current.FindResource("Default_Chart_Color").ToString())
                    };

                    wrappanelAlert.Children.Add(lb);
                }
            }
            catch (Exception ex)
            {
            }
        }



        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (dtData.Rows.Count == 0)
            {
                Messages.ShowInfoMsgBox("확인할 데이터가 없습니다. ");
                return;
            }

            try
            {
                if (Messages.ShowYesNoMsgBox("경보확인을 하시겠습니까?") == MessageBoxResult.Yes)
                {
                    htcondition.Clear();
                    htcondition.Add("CHK_ID", Logs.strLogin_ID);
                    htcondition.Add("SDT", dtData.Compute("MIN(OCCUR_DT)", "1=1").ToString());
                    htcondition.Add("EDT", dtData.Compute("MAX(OCCUR_DT)", "1=1").ToString());
                    work.Update_ALERT_RECORD_MNG_Main(htcondition);

                    this.DialogResult = true;

                    Close();
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    this.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    this.WindowState = WindowState.Normal;
                }
                this.DragMove();
            }
        }
    }
}
