﻿using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GTI.InfoWorks.Main.View.Popup
{
    /// <summary>
    /// PopupInfomation.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupInfomation : Window
    {
        System.Windows.Forms.SaveFileDialog saveFileDialog;
        string strManualPath = AppDomain.CurrentDomain.BaseDirectory + "/Resources/IM4.0_Manual.pdf";

        public PopupInfomation()
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            Loaded += PopupInfomation_Loaded;

        }

        private void PopupInfomation_Loaded(object sender, RoutedEventArgs e)
        {
            btnDownload.Click += BtnDownload_Click;
            btnClose.Click += BtnClose_Click;
            btnXSignClose.Click += BtnXSignClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
        }

        private void BtnDownload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                saveFileDialog = null;
                saveFileDialog = new System.Windows.Forms.SaveFileDialog();
                saveFileDialog.Title = "저장경로를 지정하세요.";
                saveFileDialog.FileName = "실시간계측 데이터 기반의 상수도 블록 유수,누수량 관리시스템 사용자 운영 매뉴얼.pdf";

                saveFileDialog.OverwritePrompt = true;
                saveFileDialog.Filter = "PDF Files|*.pdf";

                if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    FileInfo file = new FileInfo(strManualPath);

                    file.CopyTo(saveFileDialog.FileName, true);
                    System.Diagnostics.Process.Start(saveFileDialog.FileName);

                }

            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        private void BtnXSignClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    this.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    this.WindowState = WindowState.Normal;
                }
                this.DragMove();
            }
        }
    }
}
