﻿using GTI.InfoWorks.Models.Main.Work;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Converters;
using GTIFramework.Common.Utils.ViewEffect;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GTI.InfoWorks.Main.View.Popup
{
    /// <summary>
    /// PopupTempPwdChange.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupTempPwdChange : Window
    {
        MainWork work = new MainWork();
        Hashtable htInfo;
        Hashtable htconditions;


        public PopupTempPwdChange(Hashtable _htInfo)
        {
            InitializeComponent();
            htInfo = _htInfo;
            ThemeApply.Themeapply(this);
            Load();
        }

        private void Load()
        {
            InitializeEvent();
            InitializeData();
        }

        /// <summary>
        /// 이벤트 초기화
        /// </summary>
        private void InitializeEvent()
        {
            btnSave.Click += BtnSave_Click;
            btnClose.Click += BtnClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
            btnXSignClose.Click += BtnClose_Click;
        }

        /// <summary>
        /// 윈도우 창 이동 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    this.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    this.WindowState = WindowState.Normal;
                }
                this.DragMove();
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            if (System.Windows.MessageBox.Show("비밀번호를 변경해야 로그인이 가능합니다.\n프로그램을 종료하시겠습니까?", "확인", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Environment.Exit(0);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            else
            {
                DialogResult = false;
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //임시비밀번호 빈값
                if (temppwd.Text.ToString().Equals(""))
                {
                    Messages.ShowInfoMsgBox("임시비밀번호를 입력해주세요.");
                    temppwd.Focus();
                    return;
                }
                //변경비밀번호 빈값
                if (pwdChange.Text.ToString().Equals(""))
                {
                    Messages.ShowInfoMsgBox("변경비밀번호를 입력해주세요.");
                    pwdChange.Focus();
                    return;
                }
                //변경비밀번호확인 빈값
                if (pwdChangeChk.Text.ToString().Equals(""))
                {
                    Messages.ShowInfoMsgBox("변경비밀번호확인을 입력해주세요.");
                    pwdChangeChk.Focus();
                    return;
                }
                //대,소문자, 숫자, 특수문자, 8자리 이상 64자리 이하 정규식
                Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\W]).{8,64}$");
                if (!regex.IsMatch(pwdChange.EditValue.ToString()))
                {
                    Messages.ShowInfoMsgBox("비밀번호는 알파벳 대문자와 소문자, 숫자, 특수문자를 사용하여 8자리 이상으로 설정해주세요.");
                    return;
                }
                //변경비밀번호, 변경비밀번호확인 비교
                if (!pwdChange.Text.ToString().Equals(pwdChangeChk.Text.ToString()))
                {
                    Messages.ShowInfoMsgBox("변경비밀번호를 확인해주세요.");
                    pwdChange.Focus();
                    return;
                }

                DataTable dtLogUserInfo = work.Select_Log_User_Info(htInfo);

                //입력한 현재 비밀번호가 일치하는지 체크
                if (!EncryptionConvert.SHA256Hash(htInfo["USER_ID"].ToString() + temppwd.Text.ToString()).Equals(dtLogUserInfo.Rows[0]["USER_PWD"].ToString()))
                {
                    Messages.ShowInfoMsgBox("임시비밀번호가 일치하지 않습니다.");
                    temppwd.Focus();
                    return;
                }
                htconditions = null;
                htconditions = new Hashtable();
                htconditions.Add("USER_PWD", EncryptionConvert.SHA256Hash(htInfo["USER_ID"].ToString() + pwdChange.EditValue.ToString()));
                htconditions.Add("USER_ID", htInfo["USER_ID"].ToString());
                htconditions.Add("POLICY_CNT", "");

                work.Update_SYS_USER_INFO_Policy(htconditions);

                Messages.ShowOkMsgBox();

                this.Close();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void InitializeData()
        {

        }
    }
}
