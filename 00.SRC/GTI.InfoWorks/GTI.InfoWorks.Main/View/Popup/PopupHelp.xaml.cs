﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Collections;
using System.Data;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.ViewEffect;
using GTI.InfoWorks.Models.Main.Work;

namespace GTI.InfoWorks.Main.View.Popup
{
    /// <summary>
    /// PopupHelp.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PopupHelp : Window
    {
        MainWork mainWork = new MainWork();
        Hashtable htConditions = new Hashtable();

        /// <summary>
        /// 생성자
        /// </summary>
        public PopupHelp()
        {
            InitializeComponent();
        }

        public PopupHelp(string strMNU_CD)
        {
            InitializeComponent();
            ThemeApply.Themeapply(this);
            Loaded += PopupHelp_Loaded;

            try
            {
                htConditions.Clear();
                htConditions.Add("MNU_CD", strMNU_CD);
                htConditions.Add("SYS_CD", "000003");

                DataTable dtHelp = new DataTable();
                dtHelp = mainWork.Select_MNU_HELP_POP(htConditions);

                if (dtHelp.Rows.Count == 1)
                {
                    lbMNU_NM_PATH.Content = dtHelp.Rows[0]["MNU_NM_PATH"].ToString();
                    //txtMNU_DESC.Text = dtHelp.Rows[0]["MNU_DESC"].ToString();
                    txtMNU_DESC.AppendText(dtHelp.Rows[0]["MNU_DESC"].ToString());
                }

                DataTable dtAlm = new DataTable();
                dtAlm = mainWork.Select_MNU_HELP_POP_ALM(htConditions);

                gcAlm.ItemsSource = dtAlm.Copy();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void PopupHelp_Loaded(object sender, RoutedEventArgs e)
        {
            btnClose.Click += BtnClose_Click;
            btnXSignClose.Click += BtnClose_Click;
            bdTitle.PreviewMouseDown += BdTitle_PreviewMouseDown;
        }


        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 윈도우 창 이동 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BdTitle_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.Top = Mouse.GetPosition(this).Y - System.Windows.Forms.Cursor.Position.Y - 6;
                    this.Left = System.Windows.Forms.Cursor.Position.X - Mouse.GetPosition(this).X + 20;

                    this.WindowState = WindowState.Normal;
                }
                this.DragMove();
            }
        }

    }
}
