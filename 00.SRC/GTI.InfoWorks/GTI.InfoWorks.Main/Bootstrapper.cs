﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using System.Data;

using GTI.InfoWorks.Main.View;
using Prism.Unity;
using Unity;
using GTI.InfoWorks.Modules.DataMng;
using GTI.InfoWorks.Modules.Hydraulic;
using GTI.InfoWorks.Modules.Hydraulic.Views;

namespace GTI.InfoWorks.Main
{
    public class Bootstrapper : UnityBootstrapper
    {
        /// <summary>
        /// 순서 (1)
        /// </summary>
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            //MainPage
            Container.RegisterTypeForNavigation<UcMainPageView>("UcMainPageView");                  //MainPage

            //관망해석
            Container.RegisterTypeForNavigation<UcRealTimeAnalView>("UcRealTimeAnalView");          //UcRealTimeAnalView
            Container.RegisterTypeForNavigation<UcTimeAnalView>("UcTimeAnalView");                  //UcTimeAnalView

            //사고분석
            Container.RegisterTypeForNavigation<UcAccidentView>("UcAccidentView");                  //UcAccidentView

            //데이터관리
            Container.RegisterTypeForNavigation<UcINPFileMngView>("UcINPFileMngView");              //UcINPFileMngView
        }

        /// <summary>
        /// 순서 (2)
        /// </summary>
        /// <returns></returns>
        protected override DependencyObject CreateShell()
        {
            return this.Container.Resolve<MainWin>();
        }

        /// <summary>
        /// 순서 (3)
        /// </summary>
        protected override void InitializeShell()
        {
            base.InitializeShell();

            Application.Current.MainWindow = (Window)this.Shell;
            Application.Current.MainWindow.Show();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class UnityExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="container"></param>
        /// <param name="name"></param>
        public static void RegisterTypeForNavigation<T>(this IUnityContainer container, string name)
        {
            Type type = typeof(T);
            string strviewName = String.IsNullOrWhiteSpace(name) ? type.Name : name;
            container.RegisterType(typeof(object), typeof(T), strviewName);
        }
    }
}
