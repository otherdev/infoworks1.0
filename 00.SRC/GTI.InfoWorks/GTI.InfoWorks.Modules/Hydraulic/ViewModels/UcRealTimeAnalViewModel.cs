﻿using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using GTI.InfoWorks.Models.DataMng.Work;
using GTIFramework.Analysis.Epanet;
using GTIFramework.Common.Log;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Converters.TatukGeo.Epanet;
using GTIFramework.Common.Utils.Handle;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TatukGIS.NDK;
using TatukGIS.NDK.WPF;

namespace GTI.InfoWorks.Modules.Hydraulic
{
    public class UcRealTimeAnalViewModel : BindableBase
    {
        UcRealTimeAnalView ucRealTimeAnalView;
        LoadingDecorator waitindicator;

        INPWork work = new INPWork();
        Hashtable htconditions = new Hashtable();

        Label lbModelNM;
        Label lbGBN;
        TGIS_ViewerWnd mapcontrol;
        TGIS_LayerWebTiles localBASE_Layer;

        #region ==========  Properties 정의 ==========
        /// <summary>
        /// LoadedCommand
        /// </summary>
        public DelegateCommand<object> LoadedCommand { get; set; }

        /// <summary>
        /// UnLoadedCommand
        /// </summary>
        public DelegateCommand<object> UnLoadedCommand { get; set; }
        
        #endregion

        public UcRealTimeAnalViewModel()
        {
            LoadedCommand = new DelegateCommand<object>(OnLoaded);
            UnLoadedCommand = new DelegateCommand<object>(UnLoadedAction);
        }

        bool bRDCChk = true;

        /// <summary>
        /// Loaded Event
        /// </summary>
        /// <param name="obj"></param>
        private void OnLoaded(object obj)
        {
            if (!bRDCChk) return;

            if (obj == null) return;

            var values = (object[])obj;

            try
            {
                bRDCChk = false;
                ucRealTimeAnalView = values[0] as UcRealTimeAnalView;
                lbModelNM = ucRealTimeAnalView.FindName("lbModelNM") as Label;
                lbGBN = ucRealTimeAnalView.FindName("lbGBN") as Label;
                mapcontrol = (ucRealTimeAnalView.FindName("mapcontrol") as EpanetTatukMapControl).mapcontrol;
                waitindicator = ucRealTimeAnalView.FindName("waitindicator") as LoadingDecorator;

                mapcontrol.Loaded += Mapcontrol_Loaded;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        private void Mapcontrol_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //1.GIS 타일 지도 초기화
                GISInit();

                //기본 모델리스트, 예측구분 리스트 조회
                DataInit();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
            
        }

        /// <summary>
        /// Unload Event
        /// </summary>
        /// <param name="obj"></param>
        private void UnLoadedAction(object obj)
        {
            try
            {
                //timer.Stop();
                //timer.Dispose();
                //threadplay.Abort();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// GIS 타일 지도 초기화
        /// </summary>
        ContextMenu menu = new ContextMenu();
        private void GISInit()
        {
            try
            {
                //파일 서버 사용했을경우 \\추가
                iniHandle ini = new iniHandle(Environment.CurrentDirectory + "\\GTIConfig.ini");
                string url = ini.GetIniValue("GIS", "strGISpath").ToString();
                if (url.IndexOf(@"\") == 0) url = @"\\" + url;

                //START LEVEL (사실상 MIN LEVEL) 설정
                int intSLev = 5; if (int.TryParse(ini.GetIniValue("GIS", "intStartLevel").ToString(), out intSLev)) { }

                //END LEVEL (사실상 MAX LEVEL) 설정
                int intELev = 14; if (int.TryParse(ini.GetIniValue("GIS", "intEndLevel").ToString(), out intELev)) { }

                double extXmin = -20037508.342789244d; if (double.TryParse(ini.GetIniValue("GIS", "XMin").ToString(), out extXmin)) { }
                double extYmin = -20037508.342789244d; if (double.TryParse(ini.GetIniValue("GIS", "YMin").ToString(), out extYmin)) { }
                double extXMax = 20037508.342789244d; if (double.TryParse(ini.GetIniValue("GIS", "XMax").ToString(), out extXMax)) { }
                double extYMax = 20037508.342789244d; if (double.TryParse(ini.GetIniValue("GIS", "YMax").ToString(), out extYMax)) { }

                string strpath = @"[TatukGIS Layer]" + "\n" +
                    @"Storage=WEBTILES" + "\n" +
                    @"name=BASEMAP" + "\n" +
                    @"Type=CUSTOM" + "\n" +
                    @"Caption=BASEMAP" + "\n" +
                    @"Url1=" + url + @"\{level}\{col}\{row}.png" + "\n" +
                    @"EPSG=3857" + "\n" +
                    @"Format=png" + "\n" +

                    @"StartLevel=" + intSLev.ToString() + "\n" +
                    @"EndLevel=" + intELev.ToString() + "\n" +

                    @"Extent.XMin=" + extXmin.ToString() + "\n" +
                    @"Extent.YMin=" + extYmin.ToString() + "\n" +
                    @"Extent.XMax=" + extXMax.ToString() + "\n" +
                    @"Extent.YMax=" + extYMax.ToString() + "\n" +

                    //@"Offline=True\n" + 
                    @"TimeOut=1000\n" +

                    @"Copyright=GREENTECHINC" + "\n";

                localBASE_Layer = null;
                localBASE_Layer = new TGIS_LayerWebTiles()
                {
                    Path = strpath,
                    Basemap = true,
                    CachedPaint = false,
                    UnSupportedOperations = TGIS_OperationType.Select,
                };
                //localBASE_Layer.DrawSelectedEx(new TGIS_Extent(extXmin, extYmin, extXMax, extYMax));
                

                mapcontrol.Add(localBASE_Layer);
                mapcontrol.FullExtent();
                //mapcontrol.Center = mapcontrol.CS.FromGeocs(new TGIS_Point(126.96, 37.39));
                //mapcontrol.VisibleExtent = new TGIS_Extent(13913337.2165, 3892333.4485, 14698329.0271, 4722803.3741);

                #region context메뉴
                mapcontrol.PreviewMouseRightButtonDown += Mapcontrol_PreviewMouseRightButtonDown;

                menu = null;
                menu = new ContextMenu();
                MenuItem cmcoord = new MenuItem();
                cmcoord.Click += Cmcoord_Click;
                cmcoord.Header = "좌표계 Korea 1985 Modified central belt(5174)";
                menu.Items.Add(cmcoord);

                MenuItem cmcoordinp = new MenuItem();
                cmcoordinp.Click += Cmcoordinp_Click;
                cmcoordinp.Header = "현재 지도 WGS 84 Pseudo-Mercator(3857) 좌표계의 좌표로 변환하여 INP생성";
                menu.Items.Add(cmcoordinp); 
                #endregion
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 기본 모델리스트 및 해석결과 Binding
        /// </summary>
        private void DataInit()
        {
            try
            {
                #region 예외처리
                if (EpanetVar.tINPConvert.network == null)
                {
                    Messages.ShowInfoMsgBox("모델을 먼저 선택해야 합니다.");
                    return;
                }

                if (!EpanetVar.tINPConvert.network.Analysis.IsSimulation)
                {
                    Messages.ShowInfoMsgBox("모델을 먼저 분석해야 합니다.");
                    return;
                }
                #endregion

                EpanetVar.tINPConvert.INPtoGIS(mapcontrol);
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        #region Context메뉴 관련 이벤트
        /// <summary>
        /// 변환한 좌표계로 내보내기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cmcoordinp_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                string strFileNamePath = string.Empty;
                string strFileName = "WGS84inpEXP";

                System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog()
                {
                    Title = "저장경로를 지정하세요.",
                    FileName = strFileName,
                    OverwritePrompt = true,
                    Filter = "INPFile | *.inp"
                };

                if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    strFileNamePath = saveFileDialog.FileName;
                    EpanetVar.tINPConvert.INPWrite(strFileNamePath);
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 좌표계변환
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cmcoord_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                (mapcontrol.Items[1] as TGIS_LayerVector).SetCSByEPSG(5174);
                (mapcontrol.Items[2] as TGIS_LayerVector).SetCSByEPSG(5174);

                mapcontrol.FullExtent();
                mapcontrol.InvalidateWholeMap();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// context메뉴 Open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_PreviewMouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                menu.IsOpen = true;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        } 
        #endregion
    }
}
