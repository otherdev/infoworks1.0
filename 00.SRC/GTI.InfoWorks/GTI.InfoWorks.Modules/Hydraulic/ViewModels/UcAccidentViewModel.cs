﻿using DevExpress.Xpf.Core;
using GTI.InfoWorks.Modules.Hydraulic.Views;
using GTIFramework.Analysis.Epanet;
using GTIFramework.Analysis.Epanet.Structures;
using GTIFramework.Common.MessageBox;
using GTIFramework.Common.Utils.Converters.TatukGeo.Epanet;
using GTIFramework.Common.Utils.Handle;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TatukGIS.NDK;
using TatukGIS.NDK.WPF;

namespace GTI.InfoWorks.Modules.Hydraulic.ViewModels
{
    public class UcAccidentViewModel : BindableBase
    {
        UcAccidentView ucAccidentView;
        LoadingDecorator waitindicator;

        TGIS_ViewerWnd mapcontrol;
        TGIS_LayerWebTiles localBASE_Layer;


        #region ==========  Properties 정의 ==========
        /// <summary>
        /// LoadedCommand
        /// </summary>
        public DelegateCommand<object> LoadedCommand { get; set; }

        public DelegateCommand<object> ImergencyCommand { get; set; }

        #endregion

        public UcAccidentViewModel()
        {
            LoadedCommand = new DelegateCommand<object>(OnLoaded);

            ImergencyCommand = new DelegateCommand<object>(ImergencyAction);
        }

        private void OnLoaded(object obj)
        {
            var values = (object[])obj;

            ucAccidentView = values[0] as UcAccidentView;
            mapcontrol = (ucAccidentView.FindName("mapcontrol") as EpanetTatukMapControl).mapcontrol;
            waitindicator = ucAccidentView.FindName("waitindicator") as LoadingDecorator;

            mapcontrol.Loaded += Mapcontrol_Loaded;
        }

        private void Mapcontrol_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                //1.GIS 타일 지도 초기화
                GISInit();

                //기본 모델리스트, 예측구분 리스트 조회
                DataInit();

                mapcontrol.MouseDown += Mapcontrol_MouseDown;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }

        }

        private TGIS_Shape selectshp;

        private void Mapcontrol_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            try
            {
                if (mapcontrol.IsEmpty) return;

                selectshp = null;

                foreach (var item in mapcontrol.Items)
                {
                    if (item is TGIS_LayerVector)
                        (item as TGIS_LayerVector).DeselectAll();
                }

                System.Drawing.Point pt = new System.Drawing.Point((int)Math.Truncate(e.GetPosition(mapcontrol).X), (int)Math.Truncate(e.GetPosition(mapcontrol).Y));
                selectshp = (TGIS_Shape)mapcontrol.Locate(mapcontrol.ScreenToMap(pt), 5 / mapcontrol.Zoom);

                if (selectshp == null) return;
                selectshp.IsSelected = !selectshp.IsSelected;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// GIS 타일 지도 초기화
        /// </summary>
        ContextMenu menu = new ContextMenu();
        private void GISInit()
        {
            try
            {
                //파일 서버 사용했을경우 \\추가
                iniHandle ini = new iniHandle(Environment.CurrentDirectory + "\\GTIConfig.ini");
                string url = ini.GetIniValue("GIS", "strGISpath").ToString();
                if (url.IndexOf(@"\") == 0) url = @"\\" + url;

                //START LEVEL (사실상 MIN LEVEL) 설정
                int intSLev = 5; if (int.TryParse(ini.GetIniValue("GIS", "intStartLevel").ToString(), out intSLev)) { }

                //END LEVEL (사실상 MAX LEVEL) 설정
                int intELev = 14; if (int.TryParse(ini.GetIniValue("GIS", "intEndLevel").ToString(), out intELev)) { }

                double extXmin = -20037508.342789244d; if (double.TryParse(ini.GetIniValue("GIS", "XMin").ToString(), out extXmin)) { }
                double extYmin = -20037508.342789244d; if (double.TryParse(ini.GetIniValue("GIS", "YMin").ToString(), out extYmin)) { }
                double extXMax = 20037508.342789244d; if (double.TryParse(ini.GetIniValue("GIS", "XMax").ToString(), out extXMax)) { }
                double extYMax = 20037508.342789244d; if (double.TryParse(ini.GetIniValue("GIS", "YMax").ToString(), out extYMax)) { }

                string strpath = @"[TatukGIS Layer]" + "\n" +
                    @"Storage=WEBTILES" + "\n" +
                    @"name=BASEMAP" + "\n" +
                    @"Type=CUSTOM" + "\n" +
                    @"Caption=BASEMAP" + "\n" +
                    @"Url1=" + url + @"\{level}\{col}\{row}.png" + "\n" +
                    @"EPSG=3857" + "\n" +
                    @"Format=png" + "\n" +

                    @"StartLevel=" + intSLev.ToString() + "\n" +
                    @"EndLevel=" + intELev.ToString() + "\n" +

                    @"Extent.XMin=" + extXmin.ToString() + "\n" +
                    @"Extent.YMin=" + extYmin.ToString() + "\n" +
                    @"Extent.XMax=" + extXMax.ToString() + "\n" +
                    @"Extent.YMax=" + extYMax.ToString() + "\n" +

                    @"Copyright=GREENTECHINC" + "\n";

                localBASE_Layer = null;
                localBASE_Layer = new TGIS_LayerWebTiles()
                {
                    Path = strpath,
                    Basemap = true,
                    CachedPaint = true,
                    UnSupportedOperations = TGIS_OperationType.Select,

                };

                mapcontrol.Add(localBASE_Layer);
                mapcontrol.FullExtent();
                //mapcontrol.Center = mapcontrol.CS.FromGeocs(new TGIS_Point(126.96, 37.39));
                //mapcontrol.VisibleExtent = new TGIS_Extent(13913337.2165, 3892333.4485, 14698329.0271, 4722803.3741);

                #region context메뉴
                mapcontrol.PreviewMouseRightButtonDown += Mapcontrol_PreviewMouseRightButtonDown;

                menu = null;
                menu = new ContextMenu();
                MenuItem cmcoord = new MenuItem();
                cmcoord.Click += Cmcoord_Click;
                cmcoord.Header = "좌표계 Korea 1985 Modified central belt(5174)";
                menu.Items.Add(cmcoord);

                MenuItem cmcoordinp = new MenuItem();
                cmcoordinp.Click += Cmcoordinp_Click;
                cmcoordinp.Header = "현재 지도 WGS 84 Pseudo-Mercator(3857) 좌표계의 좌표로 변환하여 INP생성";
                menu.Items.Add(cmcoordinp);
                #endregion
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 기본 모델리스트 및 해석결과 Binding
        /// </summary>
        private void DataInit()
        {
            try
            {
                #region 예외처리
                if (EpanetVar.tINPConvert.network == null)
                {
                    Messages.ShowInfoMsgBox("모델을 먼저 선택해야 합니다.");
                    return;
                }

                if (!EpanetVar.tINPConvert.network.Analysis.IsSimulation)
                {
                    EpanetVar.tINPConvert.network.Analysis.Run();
                    //Messages.ShowInfoMsgBox("모델을 먼저 분석해야 합니다.");
                    //return;
                }
                #endregion

                EpanetVar.tINPConvert.INPtoGIS(mapcontrol);

                var valves = EpanetVar.tINPConvert.network.Links.OfType<GTIFramework.Analysis.Epanet.Items.Valve>().Select(x => x);
                for (int i = 0; i < valves.Count(); i = i + 2)
                {
                    valves.ElementAt(i).GISValveType = "SA200";
                }

                //EpanetVar.tINPConvert.TSEPointConvert();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }


        public void ImergencyAction(object obj)
        {
            if (selectshp == null) return;
            var id = selectshp.GetField("ID");

            var crack = EpanetVar.tINPConvert.network.FIndLink(id.ToString());
            if (!(crack is GTIFramework.Analysis.Epanet.Items.Pipe)) return;

            VisitedLink.Clear();

            VisitedLink.Add(((GTIFramework.Analysis.Epanet.Items.Pipe)crack).ID);

            var v = EpanetVar.tINPConvert.network.FIndNode(crack.StartNode);
            SearchExclusionValve((GTIFramework.Analysis.Epanet.Items.Node)v);
        }

        private TGIS_Shape GetNodeShape(string id)
        {
            var layer = mapcontrol.Get("nodelayer") as TNode_Layer;
            var v = layer.Items.Where(x => x.GetField("ID").ToString() == id).FirstOrDefault();
            return v;
        }
        private TGIS_Shape GetLinkShape(string id)
        {
            var layer = mapcontrol.Get("linklayer") as TLink_Layer;
            var v = layer.Items.Where(x => x.GetField("ID").ToString() == id).FirstOrDefault();
            return v;
        }
        //crack and 인근 pipe
        private List<string> VisitedLink = new List<string>();
        private void SearchExclusionValve(GTIFramework.Analysis.Epanet.Items.INode node)
        {
            {
                var shp = GetNodeShape((node as GTIFramework.Analysis.Epanet.Items.Node).ID);

                if (shp != null)
                {
                    shp.Params.Marker.Color = TGIS_Color.Yellow;
                    mapcontrol.InvalidateWholeMap();
                }
            }

            var vs = EpanetVar.tINPConvert.network.Adgen((node as GTIFramework.Analysis.Epanet.Items.Node), VisitedLink.ToArray());

            foreach (var v in vs)
            {
                Console.WriteLine(v.ID);
                VisitedLink.Add(v.ID);

                {
                    var shp = GetLinkShape(v.ID);
                    TGIS_Color colorbk = shp.Params.Line.Color;
                    try
                    {
                        if (shp != null)
                        {
                            shp.Params.Line.Color = TGIS_Color.Lime;
                            mapcontrol.InvalidateWholeMap();
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    finally
                    {
                        //shp.Params.Line.Color = colorbk;
                        mapcontrol.InvalidateWholeMap();
                    }
                }
                

                if (v is GTIFramework.Analysis.Epanet.Items.Valve)
                {
                    if ((v as GTIFramework.Analysis.Epanet.Items.Valve).GISValveType == "SA200")
                    {
                        {
                            var shp = GetLinkShape(v.ID);

                            if (shp != null)
                            {
                                shp.Params.Line.Color = TGIS_Color.Yellow;
                                mapcontrol.InvalidateWholeMap();
                            }
                        }

                        if (Messages.ShowYesNoMsgBox("차단밸브로 설정하시겠습니까?") == System.Windows.MessageBoxResult.Yes) break;
                    }   
                }

                SearchExclusionValve(EpanetVar.tINPConvert.network.FIndNode(v.StartNode));
            }

            
        }

        #region Context메뉴 관련 이벤트
        /// <summary>
        /// 변환한 좌표계로 내보내기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cmcoordinp_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                string strFileNamePath = string.Empty;
                string strFileName = "WGS84inpEXP";

                System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog()
                {
                    Title = "저장경로를 지정하세요.",
                    FileName = strFileName,
                    OverwritePrompt = true,
                    Filter = "INPFile | *.inp"
                };

                if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    strFileNamePath = saveFileDialog.FileName;
                    EpanetVar.tINPConvert.INPWrite(strFileNamePath);
                }
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// 좌표계변환
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cmcoord_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                (mapcontrol.Items[1] as TGIS_LayerVector).SetCSByEPSG(5174);
                (mapcontrol.Items[2] as TGIS_LayerVector).SetCSByEPSG(5174);

                mapcontrol.FullExtent();
                mapcontrol.InvalidateWholeMap();
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }

        /// <summary>
        /// context메뉴 Open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mapcontrol_PreviewMouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                menu.IsOpen = true;
            }
            catch (Exception ex)
            {
                Messages.ErrLog(ex);
            }
        }
        #endregion

    }
}
