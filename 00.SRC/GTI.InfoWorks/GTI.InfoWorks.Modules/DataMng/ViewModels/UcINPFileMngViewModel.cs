﻿using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.RichEdit;
using GTI.InfoWorks.Models.DataMng.Work;
using GTIFramework.Common.MessageBox;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;

namespace GTI.InfoWorks.Modules.DataMng
{
    public class UcINPFileMngViewModel : BindableBase
    {
        UcINPFileMngView ucINPFileMngView;

        INPWork work = new INPWork();

        Hashtable htconditions = new Hashtable();
        DataTable dtresult;
        Hashtable htresult = new Hashtable();

        TextEdit txtModelNM;
        TextEdit txtINPpath;
        TextEdit txtMSXpath;
        CheckBox chkMSX_YN;
        TextEdit txtETC;
        GridControl gcList;
        FileStream fs;
        FileStream fsmsx;
        RichTextBox richboxINP;
        RichTextBox richboxMSX;
        ToggleSwitch tgUse;

        bool badd = false;

        RadioButton radioR;
        RadioButton radioT;
        RadioButton radioAge;
        RadioButton radioMSX;
        RadioButton radioTrace;
        SpinEdit spinqual;

        #region ==========  Properties 정의 ==========
        /// <summary>
        /// Loaded Event
        /// </summary>
        public DelegateCommand<object> LoadedCommand { get; set; }

        /// <summary>
        /// INPImportCommand
        /// </summary>
        public DelegateCommand<object> FileIMPCommand { get; set; }

        /// <summary>
        /// ModelSaveCommand
        /// </summary>
        public DelegateCommand<object> SaveCommand { get; set; }

        /// <summary>
        /// ModelAddCommand
        /// </summary>
        public DelegateCommand<object> AddCommand { get; set; }

        /// <summary>
        /// ModelDelCommnad
        /// </summary>
        public DelegateCommand<object> DelCommand { get; set; }

        /// <summary>
        /// INPSelectCommand
        /// </summary>
        public DelegateCommand<object> INPSelectCommand { get; set; }

        /// <summary>
        /// SelectedItemChangedCommand
        /// </summary>
        public DelegateCommand<object> SelectedItemChangedCommand { get; set; }

        /// <summary>
        /// INPIRunCommand
        /// </summary>
        public DelegateCommand<object> INPIRunCommand { get; set; }

        /// <summary>
        /// EpanetRunCommand
        /// </summary>
        public DelegateCommand<object> EpanetRunCommand { get; set; }

        /// <summary>
        /// ModelLoadCommand
        /// </summary>
        public DelegateCommand<object> ModelLoadCommand { get; set; }
        #endregion

        public UcINPFileMngViewModel()
        {
            LoadedCommand = new DelegateCommand<object>(OnLoaded);
            FileIMPCommand = new DelegateCommand<object>(FileIMPAction);
            SaveCommand = new DelegateCommand<object>(SaveAction);
            AddCommand = new DelegateCommand<object>(AddAction);
            DelCommand = new DelegateCommand<object>(DelAction);
            INPSelectCommand = new DelegateCommand<object>(INPSelectAction);
            SelectedItemChangedCommand = new DelegateCommand<object>(SelectedItemChangedAction);
            //INPIRunCommand = new DelegateCommand<object>(INPIRunAction);
            //EpanetRunCommand = new DelegateCommand<object>(EpanetRunAction);
            //ModelLoadCommand = new DelegateCommand<object>(ModelLoadAction);
        }

        #region ========== Event 정의 ==========
        bool bRDCChk = true;

        /// <summary>
        /// Loaded Event
        /// </summary>
        /// <param name="obj"></param>
        private void OnLoaded(object obj)
        {
            if (!bRDCChk) return;

            if (obj == null) return;

            var values = (object[])obj;

            try
            {
                bRDCChk = false;
                ucINPFileMngView = values[0] as UcINPFileMngView;

                txtModelNM = ucINPFileMngView.FindName("txtModelNM") as TextEdit;
                txtINPpath = ucINPFileMngView.FindName("txtINPpath") as TextEdit;
                txtMSXpath = ucINPFileMngView.FindName("txtMSXpath") as TextEdit;
                chkMSX_YN = ucINPFileMngView.FindName("chkMSX_YN") as CheckBox;
                tgUse = ucINPFileMngView.FindName("tgUse") as ToggleSwitch;
                txtETC = ucINPFileMngView.FindName("txtETC") as TextEdit;

                gcList = ucINPFileMngView.FindName("gcList") as GridControl;
                richboxINP = ucINPFileMngView.FindName("richboxINP") as RichTextBox;
                richboxMSX = ucINPFileMngView.FindName("richboxMSX") as RichTextBox;
                radioR = ucINPFileMngView.FindName("radioR") as RadioButton;
                radioT = ucINPFileMngView.FindName("radioT") as RadioButton;
                radioAge = ucINPFileMngView.FindName("radioAge") as RadioButton;
                radioMSX = ucINPFileMngView.FindName("radioMSX") as RadioButton;
                radioTrace = ucINPFileMngView.FindName("radioTrace") as RadioButton;
                spinqual = ucINPFileMngView.FindName("spinqual") as SpinEdit;

                INPSelectAction(null);

                SelectedItemChangedAction(null);
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// INP 불러오기
        /// </summary>
        /// <param name="obj"></param>
        private void FileIMPAction(object obj)
        {
            if (obj == null) return;

            var values = (object[])obj;

            TextEdit txt = values[0] as TextEdit;

            try
            {
                System.Windows.Forms.OpenFileDialog openFile = new System.Windows.Forms.OpenFileDialog();
                openFile.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

                if (txt.Name.Equals("txtINPpath"))
                {
                    openFile.Title = "INP모델 가져오기";
                    openFile.Filter = "All inp Files | *.inp";
                }
                else if (txt.Name.Equals("txtMSXpath"))
                {
                    openFile.Title = "msx 가져오기";
                    openFile.Filter = "All msx Files | *.msx";
                }

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    FileStream stream = new FileStream(openFile.FileName, FileMode.Open, FileAccess.Read);
                    BinaryReader reader = new BinaryReader(stream, Encoding.Default, true);

                    txt.Text = openFile.FileName;
                    //TextEdit Tag 값에 BinaryReader 저장
                    txt.Tag = reader.ReadBytes((int)stream.Length);
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// ModelSaveCommand
        /// </summary>
        /// <param name="obj"></param>
        private void SaveAction(object obj)
        {
            try
            {
                if (txtINPpath.Tag == null)
                {
                    Messages.ShowInfoMsgBox("INP는 필수 선택입니다.");
                    return;
                }

                if (txtModelNM.Text == string.Empty)
                {
                    Messages.ShowInfoMsgBox("모델명은 필수 선택입니다.");
                    return;
                }

                if (chkMSX_YN.IsChecked == true)
                {
                    if (txtMSXpath.Tag == null || txtMSXpath.Tag.ToString().Equals(""))
                    {
                        Messages.ShowInfoMsgBox("MSX은 필수 선택입니다.");
                        return;
                    }

                }

                htconditions.Clear();
                htconditions.Add("INP_TTL", txtModelNM.Text);
                htconditions.Add("INP_FLE", txtINPpath.Tag);
                htconditions.Add("MSX_FLE", txtMSXpath.Tag);
                htconditions.Add("ETC", txtETC.Text);

                if (tgUse.IsChecked == true)
                    htconditions.Add("USE_YN", "Y");
                else
                    htconditions.Add("USE_YN", "N");

                if (chkMSX_YN.IsChecked == true)
                    htconditions.Add("MSX_YN", "Y");
                else
                    htconditions.Add("MSX_YN", "N");

                if (badd)
                {
                    htconditions.Add("INP_SEQ", DateTime.Now.ToString("yyyyMMddHHmmss"));
                    work.Insert_INP_File(htconditions);

                    badd = false;
                }
                else
                {
                    if ((gcList.SelectedItem as DataRowView) == null)
                    {
                        Messages.ShowInfoMsgBox("선택한 모델이 없습니다.");
                        return;
                    }

                    htconditions.Add("INP_SEQ", (gcList.SelectedItem as DataRowView).Row["INP_SEQ"].ToString());
                    work.Update_INP_File(htconditions);
                }

                INPSelectAction(null);
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// ModelAddAction
        /// </summary>
        /// <param name="obj"></param>
        private void AddAction(object obj)
        {
            try
            {
                txtModelNM.Text = string.Empty;

                txtINPpath.Text = string.Empty;
                txtINPpath.Tag = null;

                txtMSXpath.Text = string.Empty;
                txtMSXpath.Tag = null;

                chkMSX_YN.IsChecked = false;

                txtETC.Text = string.Empty;
                tgUse.IsChecked = true;

                badd = true;
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// ModelDelAction
        /// </summary>
        /// <param name="obj"></param>
        private void DelAction(object obj)
        {
            try
            {
                if ((gcList.SelectedItem as DataRowView) == null)
                {
                    Messages.ShowInfoMsgBox("선택한 모델이 없습니다.");
                    return;
                }

                if (Messages.ShowYesNoMsgBox("선택한 모델을 삭제 하시겠습니까?") == System.Windows.MessageBoxResult.Yes)
                {
                    htconditions.Clear();
                    htconditions.Add("INP_SEQ", (gcList.SelectedItem as DataRowView).Row["INP_SEQ"].ToString());
                    work.Delete_INP_File(htconditions);

                    INPSelectAction(null);
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }



        /// <summary>
        /// INP 선택 & 내보내기
        /// </summary>
        /// <param name="obj"></param>
        private void INPSelectAction(object obj)
        {
            try
            {
                dtresult = work.Select_INP_File_List(null);

                gcList.ItemsSource = dtresult.Copy();
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        /// <summary>
        /// INP 선택 & 보여주기
        /// </summary>
        /// <param name="obj"></param>
        private void SelectedItemChangedAction(object obj)
        {
            //선택로우 확인
            if (gcList.SelectedItem == null)
            {
                badd = true;
                tgUse.IsChecked = true;
                return;
            }

            DataRow drSelect = (gcList.SelectedItem as DataRowView).Row;

            if (drSelect != null)
            {
                txtModelNM.Text = drSelect["INP_TTL"].ToString();

                if (drSelect["USE_YN"].ToString().Equals("Y"))
                    tgUse.IsChecked = true;
                else
                    tgUse.IsChecked = false;

                if (drSelect["MSX_YN"].ToString().Equals("Y"))
                    chkMSX_YN.IsChecked = true;
                else
                    chkMSX_YN.IsChecked = false;

                txtETC.Text = drSelect["ETC"].ToString();

                txtINPpath.Tag = drSelect["INP_FLE"];
                txtINPpath.Text = "모델파일 미등록";
                txtMSXpath.Tag = drSelect["MSX_FLE"];
                txtMSXpath.Text = "모델파일 미등록";
            }

            //선택 데이터 확인
            if (!(drSelect["INP_FLE"] is Byte[])) return;

            txtINPpath.Text = "모델파일 등록";
            string strtempPathinp = AppDomain.CurrentDomain.BaseDirectory + "Resources\\TEMPINP\\Model" + drSelect["INP_SEQ"] + "\\";
            string strtempPathmsx = AppDomain.CurrentDomain.BaseDirectory + "Resources\\TEMPINP\\Model" + drSelect["INP_SEQ"] + "\\";

            DirectoryInfo di = new DirectoryInfo(strtempPathinp);

            if (di.Exists == false) di.Create();

            //BLOB파일 INP파일로 변환
            try
            {
                strtempPathinp = strtempPathinp + drSelect["INP_SEQ"] + ".inp";

                Byte[] byteINP_FILE = drSelect["INP_FLE"] as Byte[];
                fs = new FileStream(strtempPathinp, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Write(byteINP_FILE, 0, byteINP_FILE.GetUpperBound(0));

                if (drSelect["MSX_FLE"] is Byte[])
                {
                    if ((drSelect["MSX_FLE"] as Byte[]).Length > 0)
                    {
                        txtMSXpath.Text = "모델파일 등록";

                        strtempPathmsx = strtempPathmsx + drSelect["INP_SEQ"] + ".msx";
                        Byte[] byteMSX_FILE = drSelect["MSX_FLE"] as Byte[];
                        fsmsx = new FileStream(strtempPathmsx, FileMode.OpenOrCreate, FileAccess.Write);
                        fsmsx.Write(byteMSX_FILE, 0, byteMSX_FILE.GetUpperBound(0));
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
            finally
            {
                fs.Close();

                if (drSelect["MSX_FLE"] is Byte[])
                {
                    if ((drSelect["MSX_FLE"] as Byte[]).Length > 0)
                        fsmsx.Close();
                }
            }

            //INP파일 SHP파일로 변환?
            try
            {

            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }

            //richbox에 보여주기
            try
            {
                richboxINP.Document.Blocks.Clear();
                richboxMSX.Document.Blocks.Clear();

                string strINPIndex = System.IO.File.ReadAllText(strtempPathinp, Encoding.Default);
                string[] INPlines;

                #region 100줄만 표시
                if (Regex.Match(strINPIndex, @"\r\n").Length > 0)
                    INPlines = Regex.Split(strINPIndex, "\r\n");
                else if (Regex.Match(strINPIndex, @"\n").Length > 0)
                    INPlines = Regex.Split(strINPIndex, "\n");
                else
                    INPlines = null;

                int intINPCNT = 0;
                foreach (string strINP in INPlines)
                {
                    if (intINPCNT >= 1000)
                        break;

                    Paragraph paragraphinp = new Paragraph();
                    paragraphinp.Inlines.Add(INPlines[intINPCNT]);
                    richboxINP.Document.Blocks.Add(paragraphinp);

                    intINPCNT++;
                }
                #endregion

                if (drSelect["MSX_FLE"] is Byte[])
                {
                    if ((drSelect["MSX_FLE"] as Byte[]).Length > 0)
                    {
                        string strMSXIndex = System.IO.File.ReadAllText(strtempPathmsx, Encoding.Default);
                        string[] MSXlines;
                        

                        #region 100줄만 표시
                        if (Regex.Match(strMSXIndex, @"\r\n").Length > 0)
                            MSXlines = Regex.Split(strMSXIndex, "\r\n");
                        else if (Regex.Match(strMSXIndex, @"\n").Length > 0)
                            MSXlines = Regex.Split(strMSXIndex, "\n");
                        else
                            MSXlines = null;

                        int intMSXCNT = 0;
                        foreach (string strINP in INPlines)
                        {
                            if (intMSXCNT >= 1000)
                                break;

                            Paragraph paragraphmsx = new Paragraph();
                            paragraphmsx.Inlines.Add(MSXlines[intMSXCNT]);
                            richboxMSX.Document.Blocks.Add(paragraphmsx);
                            intMSXCNT++;
                        }
                        
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.ShowErrMsgBoxLog(ex);
            }
        }

        #endregion
    }
}
